//general
var wrapper, shimer;
//collapsed
var clCode, containerCollapsed, clArrow, clArrowMask, clClickDrag, clInsulin, dragRail, dragWidth, knobWidth, clRevealMask, clRevealBg, clInsulinShadow, clCta, clReplay, clClickHere, clFT;
//expanded
var expandedCollapsed, exClose, exVideo, exReplay, exClickHere;
//buttons
var clCtaExit, clExit, exExit, exCtaExit;

//ISI
var objDiv, autoScroll, autoScroll2;
var clScrollText, clTextWrapper, clScrollBar, clKnob, clISI, clScrollBarTop, clKnobTop, clKnobHeight, clScrollBarHeight, clScrollTextHeight, clWholeTextHeight;
var exScrollText, exTextWrapper, exScrollBar, exKnob, exISI, exScrollBarTop, exKnobTop, exKnobHeight, exScrollBarHeight, exScrollTextHeight, exWholeTextHeight;
var playbutton;
var selected = null;
var y_pos = 0;
var y_elem = 0;
var selected2 = null;
var y_pos2 = 0;
var y_elem2 = 0;
//
var revealCalled = false;
var firstExpand = true;
var istouched = false;
var firstLoop = true;
var userAtention = 0;
var expanded=false;

var w=125;
var l=-40;

// DC Enabler
function initEnabler(){
	if (Enabler.isInitialized) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
	}
}

function enablerInitHandler(){
	if (Enabler.isPageLoaded()) {
		pageLoadedHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler);
	}
}

function pageLoadedHandler() {
	if (Enabler.isVisible()) {
		adVisibilityHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler);
	}
}

function adVisibilityHandler() {
	Enabler.setExpandingPixelOffsets(0, 0, 500, 250, false, true);
	Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START, onCollapseStarted);
	Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START, onExpandStarted);
	init();
}

//*************************************************************************************************
//start 
function init(){
	//SCROLL
	clScrollText = document.getElementById("cl-scrollContainer");
	clTextWrapper = document.getElementById("cl-textWrapper");
	clScrollBar = document.getElementById("cl-scrollBar");
	clKnob = document.getElementById("cl-knob");
	clISI = document.getElementById("cl-ISI");
	clFT = document.getElementById("cl-footer");
	clCode = document.getElementById('cl-code')
	//
	
		
	clScrollBarTop = function(){ return parseInt(getComputedStyle(clScrollBar).getPropertyValue("top")) };
	clKnobTop = function(){ return parseInt(getComputedStyle(clKnob).getPropertyValue("top")) };
	clKnobHeight = function(){ return parseInt(getComputedStyle(clKnob).getPropertyValue("height")) };
	clScrollBarHeight = function(){ return parseInt(getComputedStyle(clScrollBar).getPropertyValue("height")) };
	clScrollTextHeight = function(){ return parseInt(getComputedStyle(clScrollText).getPropertyValue("height")) };
	clWholeTextHeight = function(){ return parseInt(getComputedStyle(clTextWrapper).getPropertyValue("height")) };
	//
	exScrollText = document.getElementById("ex-scrollContainer");
	exTextWrapper = document.getElementById("ex-textWrapper");
	exScrollBar = document.getElementById("ex-scrollBar");
	exKnob = document.getElementById("ex-knob");
	exISI = document.getElementById("ex-ISI");
	//
	
		
	exScrollBarTop = function(){ return parseInt(getComputedStyle(exScrollBar).getPropertyValue("top")) };
	exKnobTop = function(){ return parseInt(getComputedStyle(exKnob).getPropertyValue("top")) };
	exKnobHeight = function(){ return parseInt(getComputedStyle(exKnob).getPropertyValue("height")) };
	exScrollBarHeight = function(){ return parseInt(getComputedStyle(exScrollBar).getPropertyValue("height")) };
	exScrollTextHeight = function(){ return parseInt(getComputedStyle(exScrollText).getPropertyValue("height")) };
	exWholeTextHeight = function(){ return parseInt(getComputedStyle(exTextWrapper).getPropertyValue("height")) };
	//
	
	//
	wrapper = document.getElementById('wrapper');
	expandedCollapsed = document.getElementById('expanded_container_dc');
	exExit = document.getElementById('ex-exit');
	exCtaExit = document.getElementById('ex-cta-exit')
	exClose = document.getElementById('ex-close');
	shimer = document.getElementsByClassName('shimer');
	exVideo = document.getElementById('ex-video');
	playbutton = document.getElementById('playbutton');
	exReplay = document.getElementById('ex-replay');
	exClickHere = document.getElementById('ex-clickHere');
	exClickHere.addEventListener('click', FPIexit);
	//
	clCtaExit = document.getElementById('cl-cta-exit');
	clExit = document.getElementById('cl-exit');
	//clExit.addEventListener('click', clexitAction);
	clClickHere = document.getElementById('cl-clickHere');
	clClickHere.addEventListener('click', FPIexit);
	clReplay = document.getElementById('cl-replay');
	//
	containerCollapsed = document.getElementById('container_dc');
	clArrow = document.getElementById('cl-arrow');
	clArrowMask = document.getElementById('cl-arrow-mask');
	
	clClickDrag = document.getElementById('cl-clickDrag');
	clInsulin = document.getElementById('cl-insulin');
	clInsulinShadow = document.getElementById('cl-insulin-shadow');
	//
	dragRail = document.getElementById('dragRail');
	dragWidth = parseInt(getComputedStyle(dragRail).getPropertyValue("width"));
	knobWidth = parseInt(getComputedStyle(clInsulin).getPropertyValue("width"));
	//
	clRevealMask = document.getElementById('cl-revealMask');
	clRevealBg = document.getElementById('cl-revealBg');
	clCta = document.getElementById('cl-cta');
	//
	containerCollapsed.style.display = "block";
	startCollapsedAnimation();
	loadScroll();
	clInsulin.addEventListener("click", function(){ istouched = true;});
	}
//*************************************************************************************************
//Colapsed	
function startCollapsedAnimation(){
	TweenLite.to(clArrow, .5, {alpha:1, top:0, delay:.6, ease:Power1.easeOut});
	TweenLite.to(clArrow, .5, {top:5, delay:1.1, ease:Power1.easeOut});
	TweenLite.to(clClickDrag, .5, {alpha:1, delay:.4, ease:Power1.easeOut});
	TweenLite.to(clInsulin, 0, {scaleX:.2, scaleY:.2});
	TweenLite.to(clInsulin, .2, {alpha:1, scaleX:1.2, scaleY:1.2, delay:.2, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .3, {scaleX:.9, scaleY:.9, delay:.4, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .2, {scaleX:1, scaleY:1, delay:.7, ease:Power1.easeOut});
	TweenLite.to(shimer, .5, {left:150, delay:.1, ease:Power1.easeOut});
	setTimeout(startAutoScroll, 500);
	setTimeout(activateInteraction, 1000);
	
	var myInterval = setInterval(function(){
		
		console.log("istouched", istouched)
		userAtention++;
		console.log("userAtention", userAtention)
			if(istouched ==  false){
				if(userAtention == 1){
					pulse();	
				}else if(userAtention == 2){
					repeatNoInteraction();	
				}else{
					clearInterval(myInterval);
				}
			}else{
				clearInterval(myInterval);
			}
		}, 3000);
		
		setTimeout(function(){
			if(istouched ==  false){
				//TweenLite.to(clInsulin, .5, {top:-4, ease:Power1.easeOut});
				//TweenLite.to(clArrowMask, .5, { height:"0px", ease:Linear.easeNone});
				setTimeout(reveal, 500);
				
			}
		}, 9000);
}
	
function pulse(){
		console.log("pulsing")
		TweenLite.to(clInsulin, .2, {alpha:1, scaleX:1.2, scaleY:1.2, delay:.2, ease:Power1.easeOut});
		TweenLite.to(clInsulin, .3, {scaleX:.9, scaleY:.9, delay:.4, ease:Power1.easeOut});
		TweenLite.to(clInsulin, .2, {scaleX:1, scaleY:1, delay:.7, ease:Power1.easeOut});

	    TweenLite.to(clArrow, .5, {alpha:1, top:100});
		TweenLite.to(clArrow, .5, {alpha:1, top:0, delay:.5});
		TweenLite.to(clArrow, .5, {alpha:1, top:5, delay:.8, ease:Power1.easeOut});
		//TweenLite.to(clArrow, .5, {top:10, delay:1.1, ease:Power1.easeOut});
}
function reveal(){
	var speed= 1.5;
	console.log("reveal")
	//istouched = true;
	//userAtention = 3;
	revealCalled = true;
	Draggable.get(clInsulin).kill();
	clRevealMask.style.opacity = 1;
	TweenLite.to(clRevealMask, speed, {left:-100, top:-100, width:"400px", height:"460px", delay:.1, ease:Power1.easeOut});
	TweenLite.to(clRevealBg, speed, {left:94, top:94, delay:.1, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .5, {left:-62, top:166, delay:.1, ease:Power1.easeOut});
	TweenLite.to(clCode, .5, {opacity:1, delay:.5});

	//TweenLite.to(clInsulinShadow, .5, {alpha:1, delay:.4, ease:Power1.easeOut});
	TweenLite.to(clCta, .5, {alpha:1, delay:.4, ease:Power1.easeOut});
	clReplay.style.display = "block";
	TweenLite.to(clReplay, .5, {opacity:1, delay:.4, ease:Power1.easeOut});
	TweenLite.to(shimer, .5, {left:150, delay:1, ease:Power1.easeOut});
	clCtaExit.style.display = "block";
	clReplay.addEventListener('click', repeat);
	setTimeout(function(){ 
		clCtaExit.addEventListener('click', onExpandHandler);
		clCtaExit.addEventListener('mouseover', clctaOver);
		clCtaExit.addEventListener('mouseout', clctaOut);
	}, 1500);
}

//*************************************************************************************************
//Drag
function activateInteraction(){
	console.log("draggg")
	Draggable.create(clInsulin, {bounds:dragRail, type:"top", cursor:"pointer", onDrag:updateValue});
	//TweenLite.set(clInsulin,{left:"0px"});
}
function updateValue(){
	userAtention = 3;
	knobLeftPosition = parseInt(getComputedStyle(clInsulin).getPropertyValue("top"));
	w=knobLeftPosition - 70;
	l++;
	console.log(knobLeftPosition);
	//console.log("l" , l);
	TweenLite.to(clArrowMask, 0, { height:w + "px", ease:Linear.easeNone});
	//TweenLite.to(clInsulin, 0, { left:l, ease:Linear.easeNone});
	
	if(knobLeftPosition <= 75){

		console.log("knobLeftPosition entro")
		console.log("revealCalled", revealCalled)
		reveal();
		clCtaExit.style.zIndex = "1000";
		clInsulin.style.zIndex = "979";
		clArrow.style.opacity= 0;

		if(revealCalled == false){		
		clReplay.style.zIndex = "999";
		clExit.style.zIndex = "998";
		
		Draggable.get(clInsulin).kill();
		
		}
	}
}


//autoscroll
function startAutoScroll(){
	clearInterval(autoScroll);
	autoScroll = setInterval(function(){clScrollText.scrollTop += 1}, 140);
	//
	clScrollText.addEventListener('mousewheel', destroyAutoScroll);
	clScrollText.addEventListener("DOMMouseScroll", destroyAutoScroll);
	clScrollText.addEventListener("touchstart", destroyAutoScroll);
	clScrollText.addEventListener("mousedown", destroyAutoScroll);
	//
	}
		
function destroyAutoScroll(){
	clearInterval(autoScroll);
	}

function loadScroll(){
	//adjust auto knob
	clScrollText.onscroll = function(){
	if(selected == null) clKnob.style.top = (clScrollBarHeight()-clKnobHeight()) * (clScrollText.scrollTop / (clTextWrapper.clientHeight - clScrollText.clientHeight))  + "px";
	}
	//start scroll functionality
	
	//
	clKnob.addEventListener('mousedown', knobDragging);
	clKnob.addEventListener('mousemove', _move_elem);
	clKnob.addEventListener('mouseup', _destroy);
	clKnob.addEventListener('mouseout', _destroy);
}
//scroll
function _drag_init(elem) {
	selected = elem;
	y_elem = y_pos - selected.offsetTop;
}
function _move_elem(e) {
	if(clKnobTop() > -1 && clKnobTop() < (clScrollBarHeight() - (clKnobHeight()-1))){
		y_pos = document.all ? window.event.clientY : e.pageY;
				
		if (selected !== null) {
			if((y_pos - y_elem) < 0){
				clKnob.style.top = "0px";
			}else if((y_pos - y_elem) > (clScrollBarHeight() - clKnobHeight()-1)){
				clKnob.style.top = clScrollBarHeight() - 23 + "px";
			}else{
				clKnob.style.top = (y_pos - y_elem)+"px";
			}
			var percent = clKnobTop() / (clScrollBarHeight() - clKnobHeight());
			clScrollText.scrollTop =  (clWholeTextHeight() - clScrollTextHeight()) * percent;
		}
	}
}
function _destroy() {
	selected = null;
}
function knobDragging() {
	_drag_init(this);
	destroyAutoScroll();
	return false;
};



//autoscroll
function startAutoScroll2(){
	clearInterval(autoScroll2);
	autoScroll2 = setInterval(function(){exScrollText.scrollTop += 1}, 70);
	//
	exScrollText.addEventListener('mousewheel', destroyAutoScroll2);
	exScrollText.addEventListener("DOMMouseScroll", destroyAutoScroll2);
	exScrollText.addEventListener("touchstart", destroyAutoScroll2);
	exScrollText.addEventListener("mousedown", destroyAutoScroll2);
	//
	}
		
function destroyAutoScroll2(){
	clearInterval(autoScroll2);
	}

function loadScroll2(){
	//adjust auto knob
	exScrollText.onscroll = function(){
	if(selected2 == null) exKnob.style.top = (exScrollBarHeight()-exKnobHeight()) * (exScrollText.scrollTop / (exTextWrapper.clientHeight - exScrollText.clientHeight))  + "px";
	}
	//start scroll functionality
	
	//
	exKnob.addEventListener('mousedown', knobDragging2);
	exKnob.addEventListener('mousemove', _move_elem2);
	exKnob.addEventListener('mouseup', _destroy2);
	exKnob.addEventListener('mouseout', _destroy2);
}
//scroll
function _drag_init2(elem) {
	selected2 = elem;
	y_elem2 = y_pos2 - selected2.offsetTop;
}
function _move_elem2(e) {
	if(exKnobTop() > -1 && exKnobTop() < (exScrollBarHeight() - (exKnobHeight()-1))){
		y_pos2 = document.all ? window.event.clientY : e.pageY;
				
		if (selected2 !== null) {
			if((y_pos2 - y_elem2) < 0){
				exKnob.style.top = "0px";
			}else if((y_pos2 - y_elem2) > (exScrollBarHeight() - exKnobHeight()-1)){
				exKnob.style.top = exScrollBarHeight() - 23 + "px";
			}else{
				exKnob.style.top = (y_pos2 - y_elem2)+"px";
			}
			var percent = exKnobTop() / (exScrollBarHeight() - exKnobHeight());
			exScrollText.scrollTop =  (exWholeTextHeight() - exScrollTextHeight()) * percent;
		}
	}
}
function _destroy2() {
	selected2 = null;
}
function knobDragging2() {
	_drag_init2(this);
	destroyAutoScroll2();
	return false;
};

//*************************************************************************************************
//repeat
function repeat(){
	clReplay.removeEventListener('click', repeat);
	
	//clCtaExit.style.zIndex = "997";
	clReplay.style.zIndex = "998";
	clExit.style.zIndex = "998";
	clInsulin.style.zIndex = "979";
	TweenLite.to(clCode, 0, {opacity:0});
	TweenMax.set(clRevealMask, {clearProps:"all"});
	TweenMax.set(clArrowMask, {clearProps:"all"});
	TweenMax.set(clArrow, {clearProps:"all"});

	TweenMax.set(clInsulin, {clearProps:"all"});
	TweenMax.set(clRevealBg, {clearProps:"all"});
	//TweenLite.to(clArrowMask, 0, { height:125});
	//TweenLite.to(clArrow, 0, {alpha:0});
	
	clClickDrag.style.opacity = 0;
	//TweenLite.to(clInsulin, 0, {alpha:0, left:-62, top:166});
	//
	revealCalled = false;
	istouched = false;
	userAtention = 0;
	//TweenLite.to(clRevealMask, 0, {alpha:0, height:1});
	
	//clInsulinShadow.style.opacity = 0;
	clCta.style.opacity = 0;
	clCtaExit.style.display = "none";
	clReplay.style.opacity = 0;
	clReplay.style.display = "none";
	TweenLite.to(shimer, 0, {left:-150});
	startCollapsedAnimation();
	}
	
function repeatNoInteraction(){
	TweenLite.to(clArrow, 0, {alpha:0, top:200});
	clClickDrag.style.opacity = 0;
	TweenLite.to(clInsulin, 0, {alpha:0});
	clCta.style.opacity = 0;
	clCtaExit.style.display = "none";
	TweenLite.to(shimer, 0, {left:-150});
	//
	TweenLite.to(clArrow, .5, {alpha:1, left:0, delay:.6, ease:Power1.easeOut});
	TweenLite.to(clArrow, .5, {top:10, delay:1.1, ease:Power1.easeOut});
	TweenLite.to(clClickDrag, .5, {alpha:1, delay:.4, ease:Power1.easeOut});
	TweenLite.to(clInsulin, 0, {scaleX:.2, scaleY:.2});
	TweenLite.to(clInsulin, .2, {alpha:1, scaleX:1.2, scaleY:1.2, delay:.2, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .3, {scaleX:.9, scaleY:.9, delay:.4, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .2, {scaleX:1, scaleY:1, delay:.7, ease:Power1.easeOut});
	TweenLite.to(clInsulin, .2, {scaleX:1, scaleY:1, delay:.7, ease:Power1.easeOut});
	TweenLite.to(shimer, .5, {left:150, delay:.1, ease:Power1.easeOut});

	TweenLite.to(clInsulin, .5, {top:80, left:-27, delay:1.5, ease:Power1.easeOut});
	TweenLite.to(clArrow, .6, {height:0, delay:1.5, onComplete:reveal });

	}
	
//*************************************************************************************************
//Expanded	
function userPlayIpad(){
	exVideo.play();
	playbutton.style.display = 'none';
}


function startExpandedAnimation(){
	TweenLite.to(wrapper, .5, {width:"498px", ease:Power1.easeOut});
	exClose.style.display = "block";
	exExit.style.display = "block";
	exCtaExit.style.display = "block";
	console.log(exVideo.src);
	exVideo.style.display = "block";

	expanded = true;

	if( /iPad/i.test(navigator.userAgent) ) {
		playbutton.style.display = 'block';
		playbutton.addEventListener("click", userPlayIpad);
	}else{
		playbutton.style.display = 'none';
		exVideo.play();
	}

	
	
	exClose.addEventListener('click', onCollapseHandler);
	exExit.addEventListener('click', clexitAction);
	exCtaExit.addEventListener('click', exExitAction);
	exCtaExit.addEventListener('mouseover', clctaOver);
	exCtaExit.addEventListener('mouseout', clctaOut);
	
	/*exVideo.onended = function(e) {
      exReplay.style.display = "block";
	  exReplay.addEventListener('click', videoReplay);
	  console.log("VIDEO ENDED: REPLAY BUTTON MUST BE VISIBLE");
    };*/
	
	//IOS IPAD COMPATIBLE
	exVideo.addEventListener('ended', function(){
      exReplay.style.display = "block";
	  exReplay.addEventListener('click', videoReplay);
	  console.log("VIDEO ENDED: REPLAY BUTTON MUST BE VISIBLE");
	});
	
	if(firstExpand){ 
		firstExpand = false;
		setTimeout(startAutoScroll2, 500);
	}
	loadScroll2();
}
	
//*************************************************************************************************
//Exits
function clexitAction(e) {
	Enabler.exit("main_Exit")
	e.stopImmediatePropagation();
	onCollapseHandler(e);
	}
function exExitAction(e) {
	Enabler.exit("cta_Exit")
	e.stopImmediatePropagation();
	onCollapseHandler(e)
	}
function FPIexit(e) {
	Enabler.exit("FPI_Exit");
	e.stopImmediatePropagation();
	if(expanded) onCollapseHandler(e);
}
	
function onExpandHandler(){ Enabler.requestExpand();


 }
function onCollapseHandler(e){
		playbutton.removeEventListener("click", userPlayIpad);
		expanded = false;
		e.stopPropagation();
		clISI.style.display = "block";
		clFT.style.display = "block";
        Enabler.counter('Rich Media Manual Closes');    
        Enabler.requestCollapse();
        Enabler.stopTimer('panel_1 Expansion');
		//Hide Expand
		expandedCollapsed.style.display = "none";
		exClose.style.display = "none";
		exExit.style.display = "none";
		exCtaExit.style.display = "none";
		exVideo.style.display = "none";
		exVideo.pause();
		exVideo.currentTime = 0.1;
	//	exVideo.load();
		
}

function clctaOver(e) {
	clCtaExit.removeEventListener('mouseover', clctaOver);
	TweenLite.to(shimer, 0, {left:-150});
	TweenLite.to(shimer, .5, {left:180, delay:.1, ease:Power1.easeOut});
	}
function clctaOut(e) { clCtaExit.addEventListener('mouseover', clctaOver); }

function videoReplay(e) { 
	exVideo.currentTime = 0.1; 
	exVideo.play();
	exReplay.style.display = "none";
	playbutton.style.display = 'none';
	}

//*************************************************************************************************
//DC
function onCollapseStarted(){
	Enabler.finishCollapse();
	wrapper.style.width = "298px";
}

function onExpandStarted(){
	exReplay.style.display = "none";
	expandedCollapsed.className = 'direction-' + Enabler.getExpandDirection();
	showExpandedAssets();
	Enabler.startTimer('panel_1 Expansion');
	Enabler.finishExpand();
}
function showExpandedAssets(){
	clISI.style.display = "none";
	clFT.style.display = "none";
	expandedCollapsed.style.display = "block";
	exExit.style.display = "block";
	exExit.addEventListener('click', clexitAction);
	startExpandedAnimation();
}
	
