var tab1 = document.getElementById("tab1");
var tab2 = document.getElementById("tab2");
var checks = tab1.getElementsByClassName("checks");
var tab1CTA = document.getElementById("tab1CTA");
var tab2CTA = document.getElementById("tab2CTA");
var tab1Button = document.getElementById("tab1Button");
var tab2Button = document.getElementById("tab2Button");
var tab2Button = document.getElementById("tab2Button");
var exExit1 = document.getElementById("bgExitTab1");
var exExit2 = document.getElementById("bgExit2Tab1");

var exExit3 = document.getElementById("bgExitTab2");
var exExit4 = document.getElementById("bgExit2Tab2");
var firstExpand = true;


var tabVisible = "tab1";

//exExit1.onclick = function(){ Enabler.exit('mainTab1_Exit') ;  };
//exExit2.onclick = function(){ Enabler.exit('mainTab1_Exit') ;  };
//exExit3.onclick = function(){ Enabler.exit('mainTab2_Exit') ;  };
//exExit4.onclick = function(){ Enabler.exit('mainTab2_Exit') ;  };


tab1Button.onclick = showTab1;
tab2Button.onclick = showTab2;


for(var i = 0; i < checks.length; i++){
	checks[i].onclick = function(){
		
		if(this != document.getElementById("check1")) document.getElementById("check1").style.opacity = 0;
		if(this != document.getElementById("check2")) document.getElementById("check2").style.opacity = 0;
		if(this != document.getElementById("check3")) document.getElementById("check3").style.opacity = 0;
		
		if(getComputedStyle(this).getPropertyValue("opacity") == 1){
			console.log("this ", this)
			this.style.opacity = 0;
			TweenLite.to(tab1CTA, 0.2, {opacity:"0.3", cursor:"auto", ease:Expo.easeOut});
			tab1CTA.onclick = function(){ return false };
		}else{
			this.style.opacity = 1;
			TweenLite.to(tab1CTA, 0.2, {opacity:"1", cursor:"pointer", ease:Expo.easeOut});
			tab1CTA.onclick = function(){ Enabler.exit('CTA1_EXIT'); Enabler.requestCollapse(); };
		}	
	}
}

function showTab1(){
	if(tabVisible == "tab2"){
		tabVisible = "tab1";
		TweenLite.to(tab2, 0.2, {opacity:0, onComplete:function(){
			TweenLite.set(tab2, {display:"none"});
		}});
	}
}

function showTab2(){
	if(tabVisible == "tab1"){
		tabVisible = "tab2";
		TweenLite.set(tab2, {display:"block", opacity:0});
		TweenLite.to(tab2, 0.2, {opacity:1});
		
		TweenLite.set(["#check4", "#check5", "#check6", "#check7"], {opacity:0, transformOrigin:"50% 50%", scale:2});
		TweenLite.to("#check4", 0.8, {opacity:1, scale:1, delay:0.5});
		TweenLite.to("#check5", 0.8, {opacity:1, scale:1, delay:1.2});
		TweenLite.to("#check6", 0.8, {opacity:1, scale:1, delay:1.9});
		TweenLite.to("#check7", 0.8, {opacity:1, scale:1, delay:2.6});
		
		TweenLite.set(tab2CTA,  {opacity:0, cursor:"auto"});
		tab2CTA.onclick = function(){return false};
		
		TweenLite.to(tab2CTA, 0.3, {opacity:1,cursor:"pointer", delay:3.3, onComplete:function(){
			tab2CTA.onclick = function(){
				Enabler.exit('CTA2_EXIT');
				Enabler.requestCollapse();
			};
		}});
	}
}

	
	
	
initScrollExp();



function initScrollExp(){
	
	Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, function(){
		if(firstExpand){ 
			firstExpand = false;
			setTimeout(startAutoScrollExp, 500);
		}
	});

	//SCROLL EXPANDED
	var scrollText2 = document.getElementById("scrollContainer2");
	var textWrapper2 = document.getElementById("textWrapper2");
	var scrollBar2 = document.getElementById("scrollBar2");
	var knob2 = document.getElementById("knob2");
	var ISI2 = document.getElementById("ISI2");
	
	var scrollMask2 = document.getElementById('scrollMask2');

	
	var scrollBarTop2 = function(){ return parseInt(getComputedStyle(scrollBar2).getPropertyValue("top")) };
	var knobTop2 = function(){ return parseInt(getComputedStyle(knob2).getPropertyValue("top")) };
	var knobHeight2 = function(){ return parseInt(getComputedStyle(knob2).getPropertyValue("height")) };
	var scrollBarHeight2 = function(){ return parseInt(getComputedStyle(scrollBar2).getPropertyValue("height")) };
	var scrollTextHeight2 = function(){ return parseInt(getComputedStyle(scrollText2).getPropertyValue("height")) };
	var wholeTextHeight2 = function(){ return parseInt(getComputedStyle(textWrapper2).getPropertyValue("height")) };

	var objDiv;
	var autoScroll;
	var draggable = false;
	
	function startAutoScrollExp(){
		autoScroll = setInterval(function(){scrollText2.scrollTop += 1}, 70);
		
		scrollText2.addEventListener('mousewheel', destroyAutoScrollExp);
		scrollText2.addEventListener("DOMMouseScroll", destroyAutoScrollExp);
		scrollText2.addEventListener("touchstart", destroyAutoScrollExp);
		scrollText2.addEventListener("mousedown", destroyAutoScrollExp);

		loadScroll();
	}
			
	function destroyAutoScrollExp(){
		clearInterval(autoScroll);
	}


	function loadScroll(){
	//adjust auto knob
	scrollText2.onscroll = function(){			
			if(selected == null) knob2.style.top = (scrollBarHeight2()-knobHeight2()) * (scrollText2.scrollTop / (textWrapper2.clientHeight - scrollText2.clientHeight))  + "px";
		
		}

	/*clKnob.addEventListener('mousedown', knobDragging);
	clKnob.addEventListener('mousemove', _move_elem);
	clKnob.addEventListener('mouseup', _destroy);
	clKnob.addEventListener('mouseout', _destroy);*/
}
	
	
	
	//SCROLL
	
	var selected = null, // Object of the element to be moved
	y_pos = 0, // Stores x & y coordinates of the mouse pointer
	y_elem = 0; // Stores top, left values (edge) of the element
	
	function _drag_init(elem) {
		// Store the object of the element which needs to be moved
		selected = elem;
		y_elem = y_pos - selected.offsetTop;
	}
	
	// Will be called when user dragging an element
	function _move_elem(e) {
		
		if(knobTop2() > -1 && knobTop2() < (scrollBarHeight2() - (knobHeight2()-1))){
			y_pos = document.all ? window.event.clientY : e.pageY;
			
			if (selected !== null) {
									
				if((y_pos - y_elem) < 0){
					knob2.style.top = "0px";
				}else if((y_pos - y_elem) > (scrollBarHeight2() - knobHeight2()-1)){
					knob2.style.top = scrollBarHeight2() - 20 + "px";
				}else{
					knob2.style.top = (y_pos - y_elem)+"px";
				}
				
				var percent = knobTop2() / (scrollBarHeight2() - knobHeight2());
				
				scrollText2.scrollTop =  (wholeTextHeight2() - scrollTextHeight2()) * percent;
				
				
				//console.log("scrollText.scrollTop: " + scrollText2.scrollTop);
				//console.log("knobTop: " + knobTop2());
				//console.log("wholeTextHeight(): " + wholeTextHeight2());
			}
		}
			
	}
	
	// Destroy the object when we are done
	function _destroy() {
		selected = null;
	}
	
	
	
	// Bind the functions...


	document.getElementById('knob2').onmousedown = function () {
		_drag_init(this);
		destroyAutoScrollExp();
		return false;
	};

	document.onmousemove = _move_elem;
	document.onmouseup = _destroy;
	document.onmouseout = _destroy;







	}

