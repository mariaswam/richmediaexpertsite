var direction;
var ad_container;
var collapse_container;
var expand_container_dc;
var close_button;
var expanded_bg;
var copy1, copy2, copy3, inhaler1, inhaler2, inhaler3, inhaler4, hand, face;
var dragRail, bar;
var userTimeOut;
var noUserTimeOut;
var waitEndFrame;
var dragger;
var btn, shimmer, replay, logo;
var Anim;
var myDragger;
var  FPIbtn, FPIbtn2, clISI, footer;
var ctaContainer, activeArea;
var scrollContainer;
var clExit, wrapper;
var loading_dc;
var expand=false;

//var exExit;


// Properties
var knobLeftPosition;
var dragWidth;
var knobWidth;
var tl;

	

		
var objDiv, autoScroll;
var clScrollText, clTextWrapper, clScrollBar, clKnob, clScrollBarTop, clKnobTop, clKnobHeight, clScrollBarHeight, clScrollTextHeight, clWholeTextHeight;

		
		
window.onload = function(){
	if (Enabler.isInitialized()) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
	}
}

enablerInitHandler = function(e) {
	/*if(Enabler.isVisible()) {
		startAd();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, startAd);
	}*/
	
	if (Enabler.isPageLoaded()) {
    pageLoadedHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
    pageLoadedHandler);
  }
};

pageLoadedHandler = function(e) {

	Enabler.setExpandingPixelOffsets(0, 0, 500, 600, false, true);
	Enabler.setIsMultiDirectional(false);
	
	
	loading_dc = document.getElementById('loading_dc');
	collapse_container = document.getElementById('collapse_container_dc');
	direction = document.getElementById('direction');
	ad_container = document.getElementById('ad-container');
	close_button = document.getElementById('close-button');
	expand_container_dc = document.getElementById('expand-container-dc');
	expanded_bg = document.getElementById('expanded-bg');
    dragRail = document.getElementById('dragRail');
	bar = document.getElementById('bar');
    code = document.getElementById('code');
    copy1 = document.getElementById('copy1');
	copy2 = document.getElementById('copy2');
	copy3 = document.getElementById('copy3');
	inhaler1 = document.getElementById('inhaler1');
	inhaler2 = document.getElementById('inhaler2');
	inhaler3 = document.getElementById('inhaler3');
	inhaler4 = document.getElementById('inhaler4');
	hand = document.getElementById('hand');
	face = document.getElementById('face');
	dragger = document.getElementById('dragger');
	replay = document.getElementById('replay');
	logo = document.getElementById('logo');
	btn = document.getElementById('btn');
	Anim = document.getElementById('Anim');
	FPIbtn = document.getElementById('FPIbtn');
	FPIbtn2= document.getElementById('FPIbtn2');
	
	ctaContainer = document.getElementById('ctaContainer');
	shimmer = document.getElementById('shimmer');
	activeArea = document.getElementById('activeArea'); 
	clExit = document.getElementById('bgExit');
	//exExit = document.getElementById('bgExit2');
	footer = document.getElementById('footer');
	wrapper= document.getElementById('wrapper');
	//ISI = document.getElementById('ISI');
	//scrollContainer = document.getElementById('scrollContainer');
	//scrollBar = document.getElementById('scrollBar');
	
	//loading_dc.style.visibility = "hidden";
	
	
	//SCROLL
		clScrollText = document.getElementById("scrollContainer");
		clTextWrapper = document.getElementById("textWrapper");
		clScrollBar = document.getElementById("scrollBar");
		clKnob = document.getElementById("knob1");
		clISI = document.getElementById("ISI");
		
		clScrollBarTop = function(){ return parseInt(getComputedStyle(clScrollBar).getPropertyValue("top")) };
		clKnobTop = function(){ return parseInt(getComputedStyle(clKnob).getPropertyValue("top")) };
		clKnobHeight = function(){ return parseInt(getComputedStyle(clKnob).getPropertyValue("height")) };
		clScrollBarHeight = function(){ return parseInt(getComputedStyle(clScrollBar).getPropertyValue("height")) };
		clScrollTextHeight = function(){ return parseInt(getComputedStyle(clScrollText).getPropertyValue("height")) };
		clWholeTextHeight = function(){ return parseInt(getComputedStyle(clTextWrapper).getPropertyValue("height")) };
		
	
		
		var draggable = false;
		
		
	dragWidth = parseInt(getComputedStyle(dragRail).getPropertyValue("width"));
	knobWidth = parseInt(getComputedStyle(knob).getPropertyValue("width"));
	knobLeftPosition = parseInt(getComputedStyle(knob).getPropertyValue("left"));

	
	Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START,expandHandler);
	Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START,collapseHandler);
	Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, expandFinishHandler);
	Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH, collapseFinishHandler);
	
	FPIbtn.addEventListener('click',FPIexit);
	FPIbtn2.addEventListener('click',FPIexit);

	clExit.addEventListener('click', clexitAction);
	//exExit.addEventListener('click', clexitAction);
	//collapse_container.addEventListener('click',collapse_containerClick); 
	
	close_button.addEventListener('click',close_buttonClick); 
	//expanded_bg.addEventListener('click',expanded_bgClick); 
	 
	
	
	footer.style.zIndex = "999";
	clISI.style.zIndex = "998";
	activeArea.style.zIndex = "996";
	ctaContainer.style.zIndex = "995";
	replay.style.zIndex = "994";
	knob.style.zIndex = "997";
	dragger.style.zIndex = "997";
	clExit.style.zIndex = "900" 

	ad_container.style.display = "block";
	loading_dc.style.visibility = "hidden";
	
	
	tl = new TimelineLite({delay:0, paused:true});
	
	_userInteract = false;
	
	TweenLite.set(logo, { opacity:0});
	TweenLite.set(btn, { opacity:0});
    TweenLite.set(replay, { opacity:0});
	
	//tl.to(bar, 11.5, { left:250, ease:Linear.easeNone});
	
	TweenLite.to(knob, .2, {scaleX:1.1, scaleY:1.1, delay:.2, ease:Power1.easeOut});
	TweenLite.to(knob, .3, {scaleX:.95, scaleY:.95, delay:.4, ease:Power1.easeOut});
	TweenLite.to(knob, .2, {scaleX:1, scaleY:1, delay:.7, ease:Power1.easeOut});
	
	tl.from(copy1, 0, { opacity:0, ease:Linear.easeNone});
	tl.from(inhaler1, 0, { opacity:0, ease:Linear.easeNone});
	tl.from(inhaler2, 0.5, { opacity:0, ease:Linear.easeNone}, "+=0.5");
	tl.to(inhaler1, 0.5, { opacity:0, ease:Linear.easeNone});
	tl.from(inhaler3, 0.5, { opacity:0, ease:Linear.easeNone}, "+=0.5");
	tl.to(inhaler2, 0.5, { opacity:0, ease:Linear.easeNone}, "-=0.5");
	
	tl.from(inhaler4, 0.5, { opacity:0, ease:Linear.easeNone}, "+=0.5");
	tl.to(inhaler3, 0.5, { opacity:0, ease:Linear.easeNone}, "-=0.5");
	tl.to(copy1, 0.5, { opacity:0, ease:Linear.easeNone}, "-=0.5");
	tl.from(copy2, 0.5, { opacity:0, ease:Linear.easeNone},"-=0.5");
	
	tl.from(hand, 0.5, { opacity:0, ease:Linear.easeNone}, "+=0.5");
	tl.from(face, 0.5, { left:-130, opacity:1, ease:Linear.easeOut}, "+=0.5");
	tl.from(copy3, 0.5, { opacity:0, ease:Linear.easeNone}, "-=0.5");
		
	
	
	/*if (_userInteract == false) {
		//_userInteract = true;
		//tl.play();
	}*/
	
	activateInteraction();
	noUserTimeOut= setTimeout(playAnim, 5000);
	setTimeout(function(){startAutoScroll();}, 500);
	loadScroll();
}


function activateInteraction(){
	myDragger = Draggable.create(knob, {bounds:dragRail, type:"left", cursor:"pointer", onDrag:updateValue, onDragEnd:reasignTimer});
}

function reasignTimer(){
	//console.log('RE ASIGN TIMER');
	clearTimeout(noUserTimeOut);
	noUserTimeOut= setTimeout(playAnim, 5000);
}

function updateValue(){
	//_userInteract = true;
	clearTimeout(noUserTimeOut);
	
	
	/*if (_userInteract == true) {
		_userInteract = false;
		tl.play();
		console.log("USER");
		userTimeOut = setTimeout(goEndFrame, 5000);
	}*/
	
	knobLeftPosition = parseInt(getComputedStyle(knob).getPropertyValue("left"));
	tl.progress( knobLeftPosition / (dragWidth - knobWidth));
	
	TweenLite.to(bar, 0, { left:knobLeftPosition + "px", ease:Linear.easeNone});
	
	if((knobLeftPosition / (dragWidth - knobWidth)) == 1){
		myDragger[0].disable();
		onComplete();
	}
}

function playAnim(){
	tl.play();
	myDragger[0].disable();

	knobLeftPosition = parseInt(getComputedStyle(knob).getPropertyValue("left"));
	timePercent = 1 - (knobLeftPosition / (dragWidth - knobWidth));

	TweenLite.to(knob, 17*timePercent, { left:170, ease:Linear.easeNone, delay:.5});
	TweenLite.to(bar, 17*timePercent, { left:"170px", ease:Linear.easeNone});

	tl.eventCallback("onComplete", onComplete);
	//console.log("NO USER!!!");
		
}
function onComplete(){
	//console.log("COMPLETE!!!!");
	waitEndFrame= setTimeout(goEndFrame, 1000);
}
function goEndFrame() {
	footer.style.zIndex = "999";
	clISI.style.zIndex = "998";
	activeArea.style.zIndex = "997";
	ctaContainer.style.zIndex = "996";
	replay.style.zIndex = "995";
	knob.style.zIndex = "994";
	clExit.style.zIndex = "900";
	
	clearTimeout(noUserTimeOut);
	
	replay.addEventListener('click',rePlay); 
	activeArea.addEventListener('click',collapse_containerClick);
	
	activeArea.addEventListener('mouseover', onHover);
	activeArea.addEventListener('mouseout', onOut);
	ctaContainer.style.display = "block";
	
	TweenLite.to(inhaler1, 0.3, { opacity:0});
	
	TweenLite.to(inhaler2, 0.3, { opacity:0});
	TweenLite.to(inhaler3, 0.3, { opacity:0});
	TweenLite.to(inhaler4, 0.3, { opacity:1});
	TweenLite.to(copy1, 0.3, { opacity:0});
	TweenLite.to(copy2, 0.3, { opacity:1});
	TweenLite.to(copy3, 0.3, { opacity:1});

    TweenLite.to(copy2, 0.3, { top:100});
    TweenLite.to(code, 0.8, { opacity:1});

	TweenLite.to(hand, 0.3, { opacity:0});
	TweenLite.to(face, 0.3, { opacity:0});
	
	TweenLite.to(dragger, 0, {opacity:0, top:250});
		
	TweenLite.to(logo, 0.5, {opacity:1, delay:.5});
	TweenLite.to(btn, 0.5, {opacity:1, delay:.5});
	TweenLite.to(shimmer, 0.5, { opacity:1, delay:.7});
	TweenLite.to(replay, 0.5, {opacity:1, delay:.5});
	//console.log("timer" + getTimer());
}

function rePlay() {
		
	footer.style.zIndex = "999";
	clISI.style.zIndex = "998";
	activeArea.style.zIndex = "996";
	ctaContainer.style.zIndex = "995";
	replay.style.zIndex = "994";
	knob.style.zIndex = "1000";
	dragger.style.zIndex = "1000";
	clExit.style.zIndex = "900"
	
	tl.stop();
	tl.seek(0);
	myDragger[0].enable();
	TweenLite.to(bar, 0, { left:"0px", ease:Linear.easeNone});
	
	clearTimeout(noUserTimeOut);
	noUserTimeOut= setTimeout(playAnim, 5000);
	
	activeArea.removeEventListener('click',collapse_containerClick);
	activeArea.removeEventListener('mouseover', onHover);
	replay.removeEventListener('click', rePlay);
	
	TweenLite.to(inhaler4, 0.3, { opacity:0});
	TweenLite.to(copy2, 0.3, { opacity:0});
	TweenLite.to(copy3, 0.3, { opacity:0});
	
	TweenLite.to(dragger, 0, {opacity:1, top:267});
		
	TweenLite.to(logo, 0, {opacity:0});
	TweenLite.to(btn, 0, {opacity:0});
	TweenLite.to(replay, 0, {opacity:0});
	TweenLite.to(knob, 0, {left:0});
    TweenLite.to(code, 0, { opacity:0});
}





function collapse_containerClick() {
	Enabler.requestExpand();
}

function close_buttonClick() {
	Enabler.requestCollapse();
	Enabler.reportManualClose();
}

//*************************************************************************************************
//Exits
function clexitAction(e) {
	//Enabler.exit("main_Exit")
	e.stopImmediatePropagation();
	}		
function FPIexit(e) {
	Enabler.exit("FPI_Exit");
	e.stopImmediatePropagation();

	if(expand) close_buttonClick();
	}
function expanded_bgClick() {
	//Enabler.exit("Expanded_Clickthrough");
	Enabler.requestCollapse();
}

function showElements(){
  expand_container_dc.style.display = "block";
  collapse_container.style.display = "none";
}

function hideElements(){
  expand_container_dc.style.display = "none";
  collapse_container.style.display = "block";
}

function expandHandler() {
	//TweenLite.to(wrapper, 0, {width:"498px", ease:Power1.easeOut});
	wrapper.style.width = "318px";
	direction.innerHTML = 'Expand direction:' + Enabler.getExpandDirection();
	viewport.className = viewport.className.replace(/ dir-[trbl]{2}/, '') +
	' dir-' + Enabler.getExpandDirection();
	toggleClass('collapse', 'expand')
	showElements();
	Enabler.finishExpand();
	expand = true;
}

function expandFinishHandler() {
	
}

function collapseHandler() {
	wrapper.style.width = "158px";
	viewport.className = viewport.className.replace(/ dir-[trbl]{2}/, '');
	toggleClass('expand', 'collapse')
	direction.innerHTML = '';
	hideElements();
	Enabler.finishCollapse();
}

function collapseFinishHandler() {
}

function toggleClass(from, to) {
	viewport.className = viewport.className.replace(from, to);
}

function onHover(){
			
			TweenLite.to(shimmer, .5, {left:125, ease:Power1.easeOut});
		}
function onOut(){
			
			TweenLite.to(shimmer, .5, {left:-45, ease:Power1.easeOut});
		}


		
		
		// AUTO SCROLL
		
		function startAutoScroll(){
			//objDiv = document.getElementById("scrollContainer");
			autoScroll = setInterval(function(){clScrollText.scrollTop += 1}, 70);
			
			
			//
			clScrollText.addEventListener('mousewheel', destroyAutoScroll);
			clScrollText.addEventListener("DOMMouseScroll", destroyAutoScroll);
			clScrollText.addEventListener("touchstart", destroyAutoScroll);
			clScrollText.addEventListener("mousedown", destroyAutoScroll);
		}
		
		function destroyAutoScroll(){
			clearInterval(autoScroll);
		}
		
		
		
		function loadScroll(){
	//adjust auto knob
	clScrollText.onscroll = function(){			
			if(selected == null) clKnob.style.top = (clScrollBarHeight()-clKnobHeight()) * (clScrollText.scrollTop / (clTextWrapper.clientHeight - clScrollText.clientHeight))  + "px";
		
		}/*
	//start scroll functionality
	clOpenISI.addEventListener('click', openISI1);
	clCloseISI.addEventListener('click', closeISI1);*/
	//
	clKnob.addEventListener('mousedown', knobDragging);
	clKnob.addEventListener('mousemove', _move_elem);
	clKnob.addEventListener('mouseup', _destroy);
	clKnob.addEventListener('mouseout', _destroy);
}
		//SCROLL
		
		var selected = null, // Object of the element to be moved
		y_pos = 0, // Stores x & y coordinates of the mouse pointer
		y_elem = 0; // Stores top, left values (edge) of the element
		
		function _drag_init(elem) {
			// Store the object of the element which needs to be moved
			selected = elem;
			y_elem = y_pos - selected.offsetTop;
		}
		
		// Will be called when user dragging an element
		function _move_elem(e) {
			
			if(clKnobTop() > -1 && clKnobTop() < (clScrollBarHeight() - (clKnobHeight()-1))){
				y_pos = document.all ? window.event.clientY : e.pageY;
				
				if (selected !== null) {
										
					if((y_pos - y_elem) < 0){
						clKnob.style.top = "0px";
					}else if((y_pos - y_elem) > (clScrollBarHeight() - clKnobHeight()-1)){
						clKnob.style.top = clScrollBarHeight() - 23 + "px";
					}else{
						clKnob.style.top = (y_pos - y_elem)+"px";
					}
					
					var percent = clKnobTop() / (clScrollBarHeight() - clKnobHeight());
					
					clScrollText.scrollTop =  (clWholeTextHeight() - clScrollTextHeight()) * percent;
					
					
					//console.log("clScrollText.scrollTop: " + clScrollText.scrollTop);
					//console.log("clKnobTop: " + clKnobTop());
					//console.log("clWholeTextHeight(): " + clWholeTextHeight());
				}
			}
				
		}
		
		// Destroy the object when we are done
		function _destroy() {
			selected = null;
		}
		function knobDragging() {
			_drag_init(this);
			destroyAutoScroll();
			return false;
		};