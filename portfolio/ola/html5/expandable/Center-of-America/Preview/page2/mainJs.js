var creative = {};

/**
 * Window onload handler.
 */
function preInit() {
  setupDom();

  if (Enabler.isInitialized()) {
    init();
  } else {
    Enabler.addEventListener(
      studio.events.StudioEvent.INIT,
      init
    );
  }
}

/**
 * Initializes the ad components
 */
function setupDom() {
  creative.dom = {};
  creative.dom.mainContainer = document.getElementById('main-container');
  creative.dom.expandedExit = document.getElementById('expanded-exit');
  creative.dom.expandedContent = document.getElementById('expanded-state');
  creative.dom.collapsedExit = document.getElementById('collapsed-exit');
  creative.dom.collapsedContent = document.getElementById('collapsed-state');
  creative.dom.collapseButton = document.getElementById('collapse-button');
  creative.dom.expandButton = document.getElementById('expand-button');
  creative.dom.image0 = document.getElementById('main-img-0');
  creative.dom.image1 = document.getElementById('main-img-1');
}

/**
 * Ad initialisation.
 */
function init() {
  Enabler.setStartExpanded(false);

  addListeners();

  // Polite loading
  if (Enabler.isVisible()) {
    show();
  }
  else {
    Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, show);
  }
}

/**
 * Adds appropriate listeners at initialization time
 */
function addListeners() {
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START, expandStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, expandFinishHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START, collapseStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH, collapseFinishHandler);
  creative.dom.expandButton.addEventListener('click', onExpandHandler, false);
  creative.dom.collapseButton.addEventListener('click', onCollapseClickHandler, false);
  creative.dom.expandedExit.addEventListener('click', exitClickHandler);
  creative.dom.collapsedExit.addEventListener('click', collapsedExitClickHandler);
}

/**
 *  Shows the ad.
 */
function show() {
  creative.dom.expandedContent.style.display = 'none';
  creative.dom.expandedExit.style.display = 'none';
  creative.dom.collapseButton.style.display = 'none';

  creative.dom.collapsedContent.style.display = 'block';
  creative.dom.collapsedExit.style.display = 'block';
  creative.dom.expandButton.style.display = 'block';
  creative.dom.image0.style.visibility  = 'visible';
  creative.dom.image1.style.visibility  = 'visible';
}

// ---------------------------------------------------------------------------------
// MAIN
// ---------------------------------------------------------------------------------


function expandStartHandler() {
  // Show expanded content.
  creative.dom.expandedContent.style.display = 'block';
  creative.dom.expandedExit.style.display = 'block';
  creative.dom.collapseButton.style.display = 'block';
  creative.dom.collapsedContent.style.display = 'none';
  creative.dom.collapsedExit.style.display = 'none';
  creative.dom.expandButton.style.display = 'none';

  Enabler.finishExpand();
  metricsExpand_tlm.play();
  conter1();
}

function expandFinishHandler() {
  creative.isExpanded = true;
}

function collapseStartHandler() {
  // Perform collapse animation.
  creative.dom.expandedContent.style.display = 'none';
  creative.dom.expandedExit.style.display = 'none';
  creative.dom.collapseButton.style.display = 'none';
  creative.dom.collapsedContent.style.display = 'block';
  creative.dom.collapsedExit.style.display = 'block';
  creative.dom.expandButton.style.display = 'block';

  // When animation finished must call
  Enabler.finishCollapse();
}

function collapseFinishHandler() {
  creative.isExpanded = false;
}

function onCollapseClickHandler(){
  Enabler.requestCollapse();
  Enabler.stopTimer('Panel Expansion');
}

function onExpandHandler(){
  Enabler.requestExpand();
  Enabler.startTimer('Panel Expansion');
}

function exitClickHandler() {
  Enabler.requestCollapse();
  Enabler.stopTimer('Panel Expansion');
  Enabler.exit('BackgroundExit');
}

function collapsedExitClickHandler() {
  Enabler.exit('CollapsedExit');
}


/**
 *  Main onload handler
 */
window.addEventListener('load', preInit);




btnSaveNowMouseOver_tlm = new TimelineMax({paused: true, yoyo: true, overwrite: "true"});
metrics_tlm = new TimelineMax({paused: true, yoyo: true, overwrite: "true"});
metricsExpand_tlm = new TimelineMax({paused: true, yoyo: true, overwrite: "true"});
flug = true;

window.onload = function() {
  if (Enabler.isInitialized()) {
    initAnimation();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, initAnimation);
  }
}
function initAnimation (event) {
    Enabler.startTimer("starTimerDCS");
    btnSaveNowMouseOver_tlm.pause();
    metrics_tlm.pause();
    // var clickTagGen     = document.getElementById("clickTagGeneral"),
    var delay           = 0.5,
        title           = document.querySelector("#bgFixed"),
        object1         = document.querySelector(".object-1"),
        object2         = document.querySelector(".object-2"),
        object3         = document.querySelector(".object-3"),
        object4         = document.querySelector(".object-4"),
        object5         = document.querySelector(".object-5"),

        rollOverBTN     = document.querySelector(".btn_click-img"),
        metrics1        = document.querySelector(".metric-1");
        metrics2        = document.querySelector(".metric-2");
        metrics3        = document.querySelector(".metric-3");

        metricsCollapse1        = document.querySelector("#expanded-state .metric-1");
        metricsCollapse2        = document.querySelector("#expanded-state .metric-2");
        metricsCollapse3        = document.querySelector("#expanded-state .metric-3");
        

        TweenLite.to(title, 0.5, {css:{top: "-11px"}, ease: Power1.easeOut, delay: delay});
        delay += 0.3;
        TweenLite.to(object1, 0.5, {css:{top: "65px", opacity: "1"}, ease: Back.easeOut.config(1.7), delay: delay, onComplete:function() {metrics_tlm.play()}});
        delay += 0.3;
        TweenLite.to(object2, 0.5, {css:{left: "0"}, ease: Back.easeOut.config(1.7), delay: delay});
        delay += 0.8;
        TweenLite.to(object3, 0.5, {css:{left: "0"}, ease: Back.easeOut.config(1.7), delay: delay});
        delay += 0.8;
        TweenLite.to(object4, 0.5, {css:{left: "0"}, ease: Back.easeOut.config(1.7), delay: delay});
        delay += 0.3;
        // TweenLite.to(object5, 0.5, {css:{top: "14px"}, ease: Back.easeOut.config(1.7), delay: delay});


        metrics_tlm.to(metrics1, 0.8, {css:{width: "252px"}, ease: Sine.easeOut, delay: 0});
        metrics_tlm.to(metrics2, 0.8, {css:{width: "141px"}, ease: Sine.easeOut, delay: 0});
        metrics_tlm.to(metrics3, 0.8, {css:{width: "145px"}, ease: Sine.easeOut, delay: 0});

        metricsExpand_tlm.to(metricsCollapse1, 0.8, {css:{width: "290px"}, ease: Sine.easeOut, delay: 0});
        //metricsExpand_tlm.to(metricsCollapse2, 0.8, {css:{width: "158px"}, ease: Sine.easeOut, delay: 0});
        metricsExpand_tlm.to(metricsCollapse3, 0.8, {css:{width: "131px"}, ease: Sine.easeOut, delay: 0});
        

        rollOverBTN.addEventListener('click', bgExitHandler, false);
        /***[ BOTON ROLL-OVER DEVICES CONDITION ]***/
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        } else {
            rollOverBTN.addEventListener("mousemove", function () {
                btnSaveNowMouseOver_tlm.play();
            });
            rollOverBTN.addEventListener("mouseleave", function () {
                btnSaveNowMouseOver_tlm.reverse();
            });
        }
        btnSaveNowMouseOver_tlm.add(TweenMax.to(rollOverBTN, 0.001, {css:{className:'+=rollOver'}, repeat: 0, delay: 0, pause: true, ease: Power0.easeNone}));

};
function conter1 () {
    var i = 0;
    if (flug) {
            flug = false;
    var time1 = setInterval(function(){
        i += 1;
        document.getElementById("numbre-changes1").innerHTML = i + ".9 %";
        if (i == 87 || i > 87 ) {
            clearInterval(time1);
            setTimeout(function(){conter3()}, 350);
        }
     }, 1.8);
}
}
function conter2 () {
    var i = 0;
    var time2 = setInterval(function(){
        i += 1;
        document.getElementById("numbre-changes2").innerHTML = i + " %";
        if (i == 80 || i > 80 ) {
            clearInterval(time2);
            setTimeout(function(){conter3()}, 400);
        }
     }, 1.8);
}
function conter3 () {
    var i = 90;
    var time3 = setInterval(function(){
        i -= 1;
        document.getElementById("numbre-changes3").innerHTML = i + " %";
        if (i == 78 || i < 78 ) {
            clearInterval(time3);
        }
     }, 50);
}
function bgExitHandler(e) {
  Enabler.exitOverride('Background Exit', 'http://ctca-doctors.webflow.io/');
}
function stopTimerHandler(e) {
    Enabler.stopTimer('stopTimerDCS');
}