// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google


init = function(){
		//Declaring elements from the HTML i.e. Giving them Instance Names like in Flash - makes it easier
		var container;
		var expand_btn;
		var dcLogo;
		var add = document.addEventListener ? 'addEventListener' : 'attachEvent';
		var firstExpand = true;
		var manuallyScrolled = false;
		var manuallyScrolledExp = false;
		
		//SCROLL COLLAPSED
		var scrollText = document.getElementById("scrollContainer");
		var textWrapper = document.getElementById("textWrapper");
		var scrollBar = document.getElementById("scrollBar");
		var knob = document.getElementById("knob");
		var ISI = document.getElementById("ISI");
		var scrollBarTop = function(){ return parseInt(getComputedStyle(scrollBar).getPropertyValue("top")) };
		var knobTop = function(){ return parseInt(getComputedStyle(knob).getPropertyValue("top")) };
		var knobHeight = function(){ return parseInt(getComputedStyle(knob).getPropertyValue("height")) };
		var scrollBarHeight = function(){ return parseInt(getComputedStyle(scrollBar).getPropertyValue("height")) };
		var scrollTextHeight = function(){ return parseInt(getComputedStyle(scrollText).getPropertyValue("height")) };
		var wholeTextHeight = function(){ return parseInt(getComputedStyle(textWrapper).getPropertyValue("height")) };
		
		//SCROLL EXPANDED
		var scrollText2 = document.getElementById("scrollContainer2");
		var textWrapper2 = document.getElementById("textWrapper2");
		var scrollBar2 = document.getElementById("scrollBar2");
		var knob2 = document.getElementById("knob2");
		var ISI2 = document.getElementById("ISI2");
		var scrollBarTop2 = function(){ return parseInt(getComputedStyle(scrollBar2).getPropertyValue("top")) };
		var knobTop2 = function(){ return parseInt(getComputedStyle(knob2).getPropertyValue("top")) };
		var knobHeight2 = function(){ return parseInt(getComputedStyle(knob2).getPropertyValue("height")) };
		var scrollBarHeight2 = function(){ return parseInt(getComputedStyle(scrollBar2).getPropertyValue("height")) };
		var scrollTextHeight2 = function(){ return parseInt(getComputedStyle(scrollText2).getPropertyValue("height")) };
		var wholeTextHeight2 = function(){ return parseInt(getComputedStyle(textWrapper2).getPropertyValue("height")) };
	
	
		var objDiv;
		var autoScroll;
		//console.log(autoScroll);
		var objDivExp;
		var autoScrollExp;

		var draggable = false;

		
		//selector
		function $(id) { return document.getElementById(id)};
		
		//Assign All the elements to the element on the page
		container = document.getElementById('container_dc');
		expand_btn = document.getElementById('expand_btn');
		dcLogo = document.getElementById('doubleclick_logo_dc');
		expandedBgExit = document.getElementById('expanded_background_exit_dc');
		close_btn = document.getElementById('close_btn_dc');
		expand_content =  document.getElementById('expand_content_dc');
		tab2_btn =  document.getElementById('tab2_btn');
		tab2_btn =  document.getElementById('tab2_btn');
		learn_how =  document.getElementById('learn_how');
		learn_more =  document.getElementById('learn_more');
		visit_site =  document.getElementById('visit_site');
		
		learn_how_over =  document.getElementById('learnHow');
		learn_more_over =  document.getElementById('learnMore');

		plink1 = document.getElementById('plink1');
		plink2 = document.getElementById('plink2');



	//Show Ad
		container.style.display = "block";
		
		//Bring in listeners
		addListeners();
		showAnim();
		initScroll();
		
		//Add Event Listeners
		function addListeners() {
			//expandedBgExit.addEventListener('click', expandedBgExitHandler, false);
				//expand_content.addEventListener('mouseout', onMouseOffHandler, false);
				expand_btn.addEventListener('mouseover', onExpandHandler, false);
				close_btn.addEventListener('click',onCloseClickHandler, false);
				
				tab2_btn.addEventListener('click',tab2Visible, false);
				tab1_btn.addEventListener('click',tab1Visible, false);
				
				learn_how_over.addEventListener('mouseover', learn_how_rollOver, false);
				learn_more_over.addEventListener('mouseover', learn_more_rollOver, false);
				
				learn_how_over.addEventListener('mouseout', learn_how_rollOut, false);
				learn_more_over.addEventListener('mouseout', learn_more_rollOut, false);
                
                learn_more_over.addEventListener('click',ctaBtnClick, false);
				learn_more_over.addEventListener('click',ctaBtnClick, false);
            
                //Exits for the expand state:
                tab1.addEventListener('click',ctaBtnClick, false);
                tab2.addEventListener('click',ctaBtnClick, false);
			plink1.addEventListener('click',clickTab1, false);
			plink2.addEventListener('click',clickTab2, false);
			visit_site.addEventListener('click',clickVisitSite, false);


		}
		
		
		function showAnim(){
			//console.log("animacion")
				setTimeout(function(){$('dots').className = "fadein"}, 900);
				setTimeout(function(){$('dot6').className = "fadein"}, 800);
				setTimeout(function(){$('dot5').className = "fadein"}, 900);
				setTimeout(function(){$('dot4').className = "fadein"}, 1000);
				setTimeout(function(){$('dot3').className = "fadein"}, 1100);
				setTimeout(function(){$('dot2').className = "fadein"}, 1200);
				setTimeout(function(){$('dot1').className = "fadein"}, 1300);
				
				setTimeout(function(){$('tougeo').className = "fadein"}, 1800);
				setTimeout(function(){$('line1').className = "fadein"}, 2200);
				setTimeout(function(){$('line2').className = "fadein"}, 2200);
				
				setTimeout(function(){$('tougeo').className = "fadeout"}, 5000);
				setTimeout(function(){$('rollover').className = "fadeout"}, 5000);
				setTimeout(function(){$('line1').className = "fadeout"}, 5000);
				setTimeout(function(){$('line2').className = "fadeout"}, 5000);
				setTimeout(function(){$('dotsContainer').className = "fadeout"}, 5000);
				
				setTimeout(function(){$('logo').className = "fadein"}, 5500);
				setTimeout(function(){$('pen').className = "fadein"}, 5500);
				//setTimeout(function(){$('legal').className = "fadein"}, 5500);
				setTimeout(function(){$('footer').className = "fadein"}, 5500);
				setTimeout(function(){$('ISI').className = "fadein"}, 6000);
                setTimeout(function(){$('line3').className = "fadein"}, 6500);
				
				setTimeout(function(){$('line3').className = "fadeout"}, 9200);
				setTimeout(function(){$('line4').className = "fadein"}, 9700);

				setTimeout(function(){$('penCopy1').className = "fadeout"}, 9000);
				setTimeout(function(){$('penCopy2').className = "fadein"}, 9500);
				
				setTimeout(startAutoScroll, 15000);
		}
    
        function clickTab1(){
            Enabler.exit('Tab1_Exit');
        }
    
        function clickTab2(){
            Enabler.exit('Tab2_Exit');
        }
        
        function ctaBtnClick(){
            Enabler.exit('CTA_Exit');
        }

	function clickVisitSite (){
		Enabler.exit('Visit_Site');
	}

		// AUTO SCROLL
		function startAutoScroll(){
			if (!manuallyScrolled) {
						objDiv = document.getElementById("scrollContainer");
						autoScroll = setInterval(function(){objDiv.scrollTop += 1}, 70);
						
						objDiv[add]('mousewheel', destroyAutoScroll);
						objDiv[add]("DOMMouseScroll", destroyAutoScroll);
						objDiv[add]("touchstart", destroyAutoScroll);
						objDiv[add]("mousedown", destroyAutoScroll);
				}
		}
	
		function destroyAutoScroll(){
			clearInterval(autoScroll);
		}
		
		//AUTOSCROLL EXPANDED
		function startAutoScrollExp(){
			if (!manuallyScrolledExp) {
						objDivExp = document.getElementById("scrollContainer2");
						autoScrollExp = setInterval(function(){objDivExp.scrollTop += 1}, 70);
						
						objDivExp[add]('mousewheel', destroyAutoScrollExp);
						objDivExp[add]("DOMMouseScroll", destroyAutoScrollExp);
						objDivExp[add]("touchstart", destroyAutoScrollExp);
						objDivExp[add]("mousedown", destroyAutoScrollExp);
				}
		}
		
		function destroyAutoScrollExp(){
			clearInterval(autoScrollExp);
		}

	
		
		function onCloseClickHandler(e){
			Enabler.counter('Rich Media Manual Closes');    
						Enabler.collapse();
						Enabler.stopTimer('panel_1 Expansion');
				//Hide Expand
				expand_content.style.display = "none";	
		}
		
		
		function learn_more_rollOver(e){
			learn_more_over.style.opacity = "1";	
		}
		
		function learn_how_rollOver(e){
			learn_how_over.style.opacity = "1";	
		}
		
		
		
		function learn_more_rollOut(e){
			learn_more_over.style.opacity = "0";	
		}
		
		function learn_how_rollOut(e){
			learn_how_over.style.opacity = "0";	
		}
		
		function tab2Visible(e){
			//Hide Expand
				tab1.style.display = "none";
				tab2.style.display = "block";
				learn_how.style.display = "none";
				learn_how_over.style.opacity = "0";
				learn_more.style.display = "block";
				visit_site.style.display = "none";
				
		}
		
		function tab1Visible(e){
			//Hide Expand
			tab1.style.display = "block";
			tab2.style.display = "none";
			learn_how.style.display = "block";
			learn_more.style.display = "none";
			learn_more_over.style.opacity = "0";
			visit_site.style.display = "block";
			
		}
		
		function onMouseOffHandler(e){
			//We have to check if any images i.e. Close Button is there, otherwise we should stop it.
			evt = e || window.event;
			var tg = evt.relatedTarget;
				
			if (tg == null || tg.id == ''){ //prevents close button on roll over to blink/close the Ad
				//Hide Expand
				expand_content.style.display = "none";
				//Collapse Ad
				Enabler.collapse();
				//Stop Timer
				Enabler.stopTimer('panel_1 Expansion');
			
			}	
		}
		
		function onExpandHandler(){ 
			Enabler.expand();
			if(firstExpand){ 
				setTimeout(startAutoScrollExp, 15000);
				firstExpand = false;
			}
			
			showExpandedAssets();
			tab1Visible();
			Enabler.startTimer('panel_1 Expansion');
		}
		
		function showExpandedAssets(){
			//Show Expand over the top
			expand_content.style.display = "block";
		}
		
		// function expandedBgExitHandler(s){
		// 	//Hide Expand
		// 	expand_content.style.display = "none";
		// 	//Collapse Ad
		// 	Enabler.collapse();
		// 	//Stop Timer
		// 	Enabler.stopTimer('panel_1 Expansion');
		// 	//Call Exits
		// 	Enabler.exit('HTML5_Expanded_Clickthrough');
		// }
		
		
		function initScroll(){
			//console.log("scroll init")
		
			scrollText.onscroll = function(){			
				if(selected == null) knob.style.top = (scrollBarHeight()-knobHeight()) * (scrollContainer.scrollTop / (textWrapper.clientHeight - scrollContainer.clientHeight))  + "px";
			}
			scrollText2.onscroll = function(){			
				if(selected == null) knob2.style.top = (scrollBarHeight2()-knobHeight2()) * (scrollContainer2.scrollTop / (textWrapper2.clientHeight - scrollContainer2.clientHeight))  + "px";
			}
		
			
			//SCROLL
			var selected = null, // Object of the element to be moved
			y_pos = 0, // Stores x & y coordinates of the mouse pointer
			y_elem = 0; // Stores top, left values (edge) of the element
			
			function _drag_init(elem) {
				// Store the object of the element which needs to be moved
				selected = elem;
				y_elem = y_pos - selected.offsetTop;
			}
			
			// Will be called when user dragging an element
			function _move_elem(e) {
				if(knobTop() > -1 && knobTop() < (scrollBarHeight() - (knobHeight()-1))){
					y_pos = document.all ? window.event.clientY : e.pageY;
					
					if (selected !== null) {
											
						if((y_pos - y_elem) < 0){
							knob.style.top = "0px";
						}else if((y_pos - y_elem) > (scrollBarHeight() - knobHeight()-1)){
							knob.style.top = scrollBarHeight() - 20 + "px";
						}else{
							knob.style.top = (y_pos - y_elem)+"px";
						}
						
						var percent = knobTop() / (scrollBarHeight() - knobHeight());
						scrollText.scrollTop =  wholeTextHeight() * percent;
					}
				}
				
				if(knobTop2() > -1 && knobTop2() < (scrollBarHeight2() - (knobHeight2()-1))){
					y_pos = document.all ? window.event.clientY : e.pageY;
					
					if (selected !== null) {
											
						if((y_pos - y_elem) < 0){
							knob2.style.top = "0px";
						}else if((y_pos - y_elem) > (scrollBarHeight2() - knobHeight2()-1)){
							knob2.style.top = scrollBarHeight2() - 20 + "px";
						}else{
							knob2.style.top = (y_pos - y_elem)+"px";
						}
						
						var percent = knobTop2() / (scrollBarHeight2() - knobHeight2());
						scrollText2.scrollTop =  wholeTextHeight2() * percent;
					}
				}
					
			}
			
			// Destroy the object when we are done
			function _destroy() {
				selected = null;
			}
			
			// Bind the functions...
			document.getElementById('knob').onmousedown = function () {
				manuallyScrolled = true
				_drag_init(this);
				destroyAutoScroll();
				return false;
			};
			
			document.getElementById('knob2').onmousedown = function () {
				manuallyScrolledExp = true
				_drag_init(this);
				destroyAutoScrollExp();
				return false;
			};
		 
			
			document.onmousemove = _move_elem;
			document.onmouseup = _destroy;
			
			var scrollInterval;
			
			document.getElementById('up').onmousedown = function(e) {
				manuallyScrolled = true; //Prevents autoscroll
				destroyAutoScroll();
				scrollInterval = setInterval(function(){ scrollText.scrollTop -= 10; }, 100);
				return false;
			}
			document.getElementById('up2').onmousedown = function(e) {
				manuallyScrolledExp = true; //Prevents autoscroll
				destroyAutoScrollExp();
				scrollInterval = setInterval(function(){ scrollText2.scrollTop -= 10; }, 100);
				return false;
			}
						
			document.getElementById('up').onmouseup = document.getElementById('up').onmouseout = function(e) {
				clearInterval(scrollInterval);
				//console.log("arrow up released!");

			}
			
			document.getElementById('up2').onmouseup = document.getElementById('up2').onmouseout = function(e) {
				clearInterval(scrollInterval);
				//console.log("arrow up released!");

			}
			
			document.getElementById('down').onmousedown = function (e) {
				manuallyScrolled = true; //Prevents autoscroll
				destroyAutoScroll();
				scrollInterval = setInterval(function(){ scrollText.scrollTop += 10; }, 100);
				return false;
			}
			document.getElementById('down2').onmousedown = function (e) {
				manuallyScrolledExp = true; //Prevents autoscroll
				destroyAutoScrollExp();
				scrollInterval = setInterval(function(){ scrollText2.scrollTop += 10; }, 100);
				return false;
			}
			
			
			document.getElementById('down').onmouseup = document.getElementById('down2').onmouseup = function (e) {
				clearInterval(scrollInterval);
			}
			document.getElementById('down').onmouseout = document.getElementById('down2').onmouseout = function (e) {
				clearInterval(scrollInterval);
			}
		}
			function closeUnit(){
			// console.log('closeUnit');Enabler.collapse();
			Enabler.stopTimer('panel_1 Expansion');
			//Hide Expand
			expand_content.style.display = "none";
			Enabler.collapse();	
		}
		
		Enabler.addEventListener(studio.events.StudioEvent.EXIT, closeUnit);
		

}

