/* ################################################################################################
 * #
 * #		RED Interactive - Digital Advertising
 * #		Nike | FootballX | nike_footballx_html | live | 1.3
 * #/
 * ################################################################################################ */
trace( "------------------------------------------------------------------------------------------------" );
trace( " live_320x480_build.js[ Nike | FootballX | nike_footballx_html | live | 320x480 | 1.3 ]" );
trace( "------------------------------------------------------------------------------------------------" );



/*  */
/* -- COMPILED: 320x480_build.js-----------------------------------------------------------------------------------------
 *
 *
 *
 */
var nodes = document.querySelectorAll('div[style]');
for ( i = 0; i < nodes.length; i++ ){
if( nodes[i].style.transform ){
console.log( "Styles.setCss( '"+ nodes[i].id + "' , '-moz-transform', '" + nodes[i].style.transform.toString() + "' );" );
}
}

var View = new function() {

	this.buildMarkup = function() {
		trace( 'View.buildMarkup()' );

		
		Styles.setBackgroundColor( adData.elements.redAdContainer, 'rgba(255,255,255,1)' );

		adData.elements.stage__bg_image = Markup.addDiv({
			id: 'stage__bg_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'bg' ).src + ' ); background-size: cover;width: 320px; top: 0; height: 480px; background-color: rgba(0,0,0,0); left: 0; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__nikefootball_logo_image = Markup.addDiv({
			id: 'stage__nikefootball_logo_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'nikefootball-logo' ).src + ' ); background-size: cover;width: 300px; top: 115px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__nike_football_X_R1_g2_image = Markup.addDiv({
			id: 'stage__nike_football_X_R1_g2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'nike-football-X_R1_g2' ).src + ' ); background-size: cover;width: 300px; top: 0; height: 250px; background-color: rgba(0,0,0,0); left: 0; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__nikefootball_logo_g1_image = Markup.addDiv({
			id: 'stage__nikefootball_logo_g1_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'nikefootball-logo_g1' ).src + ' ); background-size: cover;width: 300px; top: 115px; height: 250px; background-color: rgba(0,0,0,0); left: 0; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__copy_g1_image = Markup.addDiv({
			id: 'stage__copy_g1_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'copy_g1' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; opacity: 0.37353515625; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__copy_g2_image = Markup.addDiv({
			id: 'stage__copy_g2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'copy_g2' ).src + ' ); background-size: cover;width: 300px; top: 115px; height: 250px; background-color: rgba(0,0,0,0); left: 0px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__Rectangle_rect = Markup.addDiv({
			id: 'stage__Rectangle_rect',
			styles: 'position: absolute; border-color: rgba(0,0,0,1); top: 0px; border-width: 0px; height: 480px; width: 320px; border-style: none; background-color: rgba(255,255,255,1.00); left: 1px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__shoe_line2_image = Markup.addDiv({
			id: 'stage__shoe_line2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'shoe_line' ).src + ' ); background-size: cover;width: 300px; top: 116px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; opacity: 1; -webkit-transform: rotate( -26deg ) scale( 0, 0 );  -moz-transform: rotate( -26deg ) scale( 0, 0 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__ball3_image = Markup.addDiv({
			id: 'stage__ball3_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'ball' ).src + ' ); background-size: cover;width: 85px; top: 23px; height: 85px; background-color: rgba(0,0,0,0); left: -68px; visibility: hidden; -webkit-transform: scale( 0.6, 0.6 ); -moz-transform: scale( 0.6, 0.6 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe_image = Markup.addDiv({
			id: 'stage__swipe_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 292px; top: 0px; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__skull01_image = Markup.addDiv({
			id: 'stage__skull01_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'skull01' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; opacity: 0; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__skull02_image = Markup.addDiv({
			id: 'stage__skull02_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'skull02' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__skull03_image = Markup.addDiv({
			id: 'stage__skull03_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'skull03' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; opacity: 1; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__Rectangle2_rect = Markup.addDiv({
			id: 'stage__Rectangle2_rect',
			styles: 'position: absolute; border-color: rgba(0,0,0,1); top: -9px; border-width: 0px; height: 480px; width: 320px; border-style: none; background-color: rgba(255,255,255,1.00); left: -1px; visibility: hidden; background-color: rgba(255,255,255,1.00); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__burst_image = Markup.addDiv({
			id: 'stage__burst_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'burst' ).src + ' ); background-size: cover;width: 52px; top: 175px; height: 40px; background-color: rgba(0,0,0,0); left: 75px; visibility: hidden; -webkit-transform: rotate( 90deg ) scale( 0.6, 0.6 );  -moz-transform: rotate( 90deg ) scale( 0.6, 0.6 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__ball_8_image = Markup.addDiv({
			id: 'stage__ball_8_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'ball_8' ).src + ' ); background-size: cover;width: 22px; top: -32px; height: 22px; background-color: rgba(0,0,0,0); left: -32px; visibility: hidden; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s00_image = Markup.addDiv({
			id: 'stage__s00_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's00' ).src + ' ); background-size: cover;width: 300px; top: 250px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s01_image = Markup.addDiv({
			id: 'stage__s01_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's01' ).src + ' ); background-size: cover;width: 300px; top: 240px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s03_image = Markup.addDiv({
			id: 'stage__s03_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's03' ).src + ' ); background-size: cover;width: 300px; top: 230px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s042_image = Markup.addDiv({
			id: 'stage__s042_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's042' ).src + ' ); background-size: cover;width: 300px; top: 220px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s05_image = Markup.addDiv({
			id: 'stage__s05_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's05' ).src + ' ); background-size: cover;width: 300px; top: 210px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s06_image = Markup.addDiv({
			id: 'stage__s06_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's06' ).src + ' ); background-size: cover;width: 300px; top: 200px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s07_image = Markup.addDiv({
			id: 'stage__s07_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's07' ).src + ' ); background-size: cover;width: 300px; top: 190px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s08_image = Markup.addDiv({
			id: 'stage__s08_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's08' ).src + ' ); background-size: cover;width: 300px; top: 180px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s09_image = Markup.addDiv({
			id: 'stage__s09_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's09' ).src + ' ); background-size: cover;width: 300px; top: 170px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s10_image = Markup.addDiv({
			id: 'stage__s10_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's10' ).src + ' ); background-size: cover;width: 300px; top: 160px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s11_image = Markup.addDiv({
			id: 'stage__s11_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's11' ).src + ' ); background-size: cover;width: 300px; top: 150px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s12_image = Markup.addDiv({
			id: 'stage__s12_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's12' ).src + ' ); background-size: cover;width: 300px; top: 140px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s13_image = Markup.addDiv({
			id: 'stage__s13_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's13' ).src + ' ); background-size: cover;width: 300px; top: 130px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s14_image = Markup.addDiv({
			id: 'stage__s14_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's14' ).src + ' ); background-size: cover;width: 300px; top: 120px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s15_image = Markup.addDiv({
			id: 'stage__s15_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's15' ).src + ' ); background-size: cover;width: 300px; top: 110px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s16_image = Markup.addDiv({
			id: 'stage__s16_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's16' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s16_2_image = Markup.addDiv({
			id: 'stage__s16_2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's16_2' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s17_image = Markup.addDiv({
			id: 'stage__s17_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's17' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s18_image = Markup.addDiv({
			id: 'stage__s18_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's18' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s19_image = Markup.addDiv({
			id: 'stage__s19_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's19' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s20_image = Markup.addDiv({
			id: 'stage__s20_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's20' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s21_image = Markup.addDiv({
			id: 'stage__s21_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's21' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s22_image = Markup.addDiv({
			id: 'stage__s22_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's22' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s23_image = Markup.addDiv({
			id: 'stage__s23_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's23' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s25_image = Markup.addDiv({
			id: 'stage__s25_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's25' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s26_image = Markup.addDiv({
			id: 'stage__s26_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's26' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s27_image = Markup.addDiv({
			id: 'stage__s27_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's27' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s28_image = Markup.addDiv({
			id: 'stage__s28_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's28' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s29_image = Markup.addDiv({
			id: 'stage__s29_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's29' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s30_image = Markup.addDiv({
			id: 'stage__s30_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's30' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s31_image = Markup.addDiv({
			id: 'stage__s31_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's31' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s32_image = Markup.addDiv({
			id: 'stage__s32_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's32' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s33_image = Markup.addDiv({
			id: 'stage__s33_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's33' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s34_image = Markup.addDiv({
			id: 'stage__s34_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's34' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s35_image = Markup.addDiv({
			id: 'stage__s35_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's35' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s36_image = Markup.addDiv({
			id: 'stage__s36_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's36' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s37_image = Markup.addDiv({
			id: 'stage__s37_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's37' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s38_image = Markup.addDiv({
			id: 'stage__s38_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's38' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s39_image = Markup.addDiv({
			id: 'stage__s39_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's39' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s40_image = Markup.addDiv({
			id: 'stage__s40_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's40' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s41_image = Markup.addDiv({
			id: 'stage__s41_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's41' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s42_image = Markup.addDiv({
			id: 'stage__s42_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's42' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.02, 1.02 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s43_image = Markup.addDiv({
			id: 'stage__s43_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's43' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.03, 1.03 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s44_image = Markup.addDiv({
			id: 'stage__s44_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's44' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.04, 1.04 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s45_image = Markup.addDiv({
			id: 'stage__s45_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's45' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.05, 1.05 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe2_image = Markup.addDiv({
			id: 'stage__swipe2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 320px; top: 0px; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe2Copy_image = Markup.addDiv({
			id: 'stage__swipe2Copy_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 320px; top: 0px; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s46_image = Markup.addDiv({
			id: 'stage__s46_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's46' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.06, 1.06 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s47_image = Markup.addDiv({
			id: 'stage__s47_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's47' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; opacity: 1; visibility: hidden; -webkit-transform: scale( 1.07, 1.07 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s48_image = Markup.addDiv({
			id: 'stage__s48_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's48' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.08, 1.08 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s49_image = Markup.addDiv({
			id: 'stage__s49_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's49' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.09, 1.09 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__s50_image = Markup.addDiv({
			id: 'stage__s50_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 's50' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: scale( 1.1, 1.1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00000_image = Markup.addDiv({
			id: 'stage__trans_00000_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00000' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00001_image = Markup.addDiv({
			id: 'stage__trans_00001_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00001' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00002_image = Markup.addDiv({
			id: 'stage__trans_00002_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00002' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00003_image = Markup.addDiv({
			id: 'stage__trans_00003_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00003' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00004_image = Markup.addDiv({
			id: 'stage__trans_00004_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00004' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00005_image = Markup.addDiv({
			id: 'stage__trans_00005_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00005' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00006_image = Markup.addDiv({
			id: 'stage__trans_00006_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00006' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00007_image = Markup.addDiv({
			id: 'stage__trans_00007_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00007' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00008_image = Markup.addDiv({
			id: 'stage__trans_00008_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00008' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00009_image = Markup.addDiv({
			id: 'stage__trans_00009_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00009' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00010_image = Markup.addDiv({
			id: 'stage__trans_00010_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00010' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00011_image = Markup.addDiv({
			id: 'stage__trans_00011_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00011' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00012_image = Markup.addDiv({
			id: 'stage__trans_00012_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00012' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00013_image = Markup.addDiv({
			id: 'stage__trans_00013_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00013' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00014_image = Markup.addDiv({
			id: 'stage__trans_00014_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00014' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__trans_00015_image = Markup.addDiv({
			id: 'stage__trans_00015_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'trans_00015' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe3_image = Markup.addDiv({
			id: 'stage__swipe3_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 320px; top: 0; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe3Copy_image = Markup.addDiv({
			id: 'stage__swipe3Copy_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 320px; top: 0; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; opacity: 0.53594970703125; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__swipe3Copy2_image = Markup.addDiv({
			id: 'stage__swipe3Copy2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'swipe' ).src + ' ); background-size: cover;width: 320px; top: 0; height: 480px; background-color: rgba(0,0,0,0); left: -215px; visibility: hidden; opacity: 0.53594970703125; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__endbg_image = Markup.addDiv({
			id: 'stage__endbg_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'endbg' ).src + ' ); background-size: cover;width: 320px; top: 6px; height: 480px; background-color: rgba(0,0,0,0); left: 0; visibility: hidden; opacity: 0; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__CTA_image = Markup.addDiv({
			id: 'stage__CTA_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'CTA-shop-now' ).src + ' ); background-size: cover;width: 300px; top: 110px; height: 250px; background-color: rgba(0,0,0,0); left: 5px; visibility: hidden; opacity: 0.57208251953125; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__CTA_playnow2_image = Markup.addDiv({
			id: 'stage__CTA_playnow2_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'CTA_playnow' ).src + ' ); background-size: cover;width: 300px; top: 110px; height: 250px; background-color: rgba(0,0,0,0); left: 15px; visibility: hidden; opacity: 0.45169067382812; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__x_image = Markup.addDiv({
			id: 'stage__x_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'x' ).src + ' ); background-size: cover;width: 320px; top: 0px; height: 480px; background-color: rgba(0,0,0,0); left: 0; visibility: hidden; opacity: 0.38375854492188; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__city_image = Markup.addDiv({
			id: 'stage__city_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'city' ).src + ' ); background-size: cover;width: 300px; top: 467px; height: 78px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; ',
			target: 'redAdContainer'
		});

		adData.elements.stage__frame_image = Markup.addDiv({
			id: 'stage__frame_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'frame' ).src + ' ); background-size: cover;width: 320px; top: 0; height: 480px; background-color: rgba(0,0,0,0); left: 0; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__shoe_final_image = Markup.addDiv({
			id: 'stage__shoe_final_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'shoe_final' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; -webkit-transform: rotate( 0deg ) scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__level2_goal_image = Markup.addDiv({
			id: 'stage__level2_goal_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'level2_goal' ).src + ' ); background-size: cover;width: 75px; top: -62px; height: 38px; background-color: rgba(0,0,0,0); left: 121px; visibility: hidden; -webkit-transform: scale( 0.9, 0.9 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__shoe_82_image = Markup.addDiv({
			id: 'stage__shoe_82_image',
			styles: 'position: absolute; background-image: url( ' + ImageUtils.getImage( 'shoe_8' ).src + ' ); background-size: cover;width: 300px; top: 100px; height: 250px; background-color: rgba(0,0,0,0); left: 10px; visibility: hidden; opacity: 0; -webkit-transform: scale( 1, 1 ); ',
			target: 'redAdContainer'
		});

		adData.elements.stage__Endframe_copy_1_text = Markup.addTextfield({
			id: 'stage__Endframe_copy_1_text',
			styles: 'position: absolute; width: 145px; top: 435px; height: 23px; left: 39px; visibility: hidden; opacity: 0; ',
			margin: 0,
			textStyles: ' color: rgba(255,255,255,1); font-size: 20px; line-height: normal; font-family: Lubalin_Graph_Book_Regular;',
			target: 'redAdContainer'
		}, 'playmaking', false );
		Align.left( adData.elements.stage__Endframe_copy_1_text.textfield );

		adData.elements.stage__Endframe_copy_2_text = Markup.addTextfield({
			id: 'stage__Endframe_copy_2_text',
			styles: 'position: absolute; width: 104px; top: 435px; height: 23px; left: 180px; visibility: hidden; opacity: 0; ',
			margin: 0,
			textStyles: ' color: rgba(255,255,255,1); font-size: 20px; line-height: normal; font-family: Lubalin_Graph_Demi_Regular;',
			target: 'redAdContainer'
		}, 'remixed', false );
		Align.left( adData.elements.stage__Endframe_copy_2_text.textfield );

		adData.elements.stage__CTA_play_game_text = Markup.addTextfield({
			id: 'stage__CTA_play_game_text',
			styles: 'position: absolute; width: 64px; top: 212px; height: 56px; left: 232px; text-align: center; visibility: hidden; opacity: 0.3151549994945526; ',
			margin: 0,
			textStyles: ' color: rgba(255,255,255,1); font-size: 28px; line-height: normal; font-family: VCR_OSD_MONO;',
			target: 'redAdContainer'
		}, 'PLAY<br>GAME', false );
		Align.centerHorizontal( adData.elements.stage__CTA_play_game_text.textfield );

		adData.elements.stage__CTA_shop_now_text = Markup.addTextfield({
			id: 'stage__CTA_shop_now_text',
			styles: 'position: absolute; width: 64px; top: 212px; height: 56px; left: 29px; text-align: center; visibility: hidden; opacity: 0; ',
			margin: 0,
			textStyles: ' color: rgba(255,255,255,1); font-size: 28px; line-height: normal; font-family: VCR_OSD_MONO;',
			target: 'redAdContainer'
		}, 'SHOP<br>NOW', false );
		Align.centerHorizontal( adData.elements.stage__CTA_shop_now_text.textfield );

		adData.elements.stage__intro_copy_line_1_text = Markup.addTextfield({
			id: 'stage__intro_copy_line_1_text',
			styles: 'position: absolute; width: 223px; top: 195px; height: 27px; left: 48px; text-align: center; visibility: hidden; opacity: 0.23211669921875; ',
			margin: 0,
			textStyles: ' color: rgba(236,0,68,1.00); font-size: 24px; line-height: normal; font-family: Lubalin_Graph_Book_Regular;',
			target: 'redAdContainer'
		}, 'playmaking will', false );
		Align.centerHorizontal( adData.elements.stage__intro_copy_line_1_text.textfield );

		adData.elements.stage__intro_copy_line_2_text = Markup.addTextfield({
			id: 'stage__intro_copy_line_2_text',
			styles: 'position: absolute; width: 246px; top: 222px; height: 35px; left: 38px; visibility: hidden; text-align: center; opacity: 1; ',
			margin: 0,
			textStyles: ' color: rgba(236,0,68,1); font-size: 24px; line-height: normal; font-family: Lubalin_Graph_Demi_Regular;',
			target: 'redAdContainer'
		}, 'never be the same', false );
		Align.centerHorizontal( adData.elements.stage__intro_copy_line_2_text.textfield );
		



		var tagline1 = "playmaking will"; // make sure text is lowercase!
		var tagline2 = "never be the same"; // make sure text is lowercase!
		var endframecopy1 = "playmaking"; // make sure text is lowercase!
		var endframecopy2 = "remixed"; // make sure text is lowercase!
		var ctaText1 = 'SHOP<br>NOW'; // make sure text is uppercase!
		var ctaText2 = 'PLAY<br>GAME'; // make sure text is uppercase!

		
		TextUtils.addText(stage__intro_copy_line_1_text_textfield, tagline1);
		TextUtils.addText(stage__intro_copy_line_2_text_textfield, tagline2);
		TextUtils.addText(stage__Endframe_copy_1_text_textfield, endframecopy1);
		TextUtils.addText(stage__Endframe_copy_2_text_textfield, endframecopy2);
		TextUtils.addText(stage__CTA_shop_now_text_textfield, ctaText1);
		TextUtils.addText(stage__CTA_play_game_text_textfield, ctaText2);


		Align.centerHorizontal( adData.elements.stage__intro_copy_line_1_text.textfield,-20 );
		Align.centerHorizontal( adData.elements.stage__intro_copy_line_2_text.textfield,-22);
	
		setTimeout( Control.prepareBuild, 250);
	}
}



var Control = new function() {

	this.prepareBuild = function() {
		trace( 'Control.prepareBuild()' );
		
		Align.centerHorizontal( adData.elements.stage__intro_copy_line_1_text.textfield,0);
		Align.centerHorizontal( adData.elements.stage__intro_copy_line_2_text.textfield,0);

		Gesture.addEventListener ( adData.elements.redAdContainer, GestureEvent.CLICK, ClickTag.onExit );
		Gesture.disableChildren ( adData.elements.redAdContainer );
		
		Animation.startScene();
	}



}



var Animation = new function() {

	this.startScene = function() {
		trace( 'Animation.startScene()' );

		
		global.removePreloader();
		Styles.setCss( global.adData.elements.redAdContainer, 'opacity', '1' );
		if( adParams.analytics )
			Analytics.report( Analytics.TYPE_IMPRESSION );
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.208, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.219, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.229, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.303, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.49, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 1.87, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.008, { delay: 0.2, css: { 'opacity': '0.67410278320312' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.011, { delay: 0.208, css: { 'opacity': '0.63186645507812' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.031, { delay: 0.219, css: { 'opacity': '0.34982299804688' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.042, { delay: 0.25, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.011, { delay: 0.292, css: { 'opacity': '0.44683837890625' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.007, { delay: 0.303, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.021, { delay: 0.31, css: { 'opacity': '0.51177978515625' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.043, { delay: 0.331, css: { 'opacity': '0.10051775976562' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.042, { delay: 0.374, css: { 'opacity': '0.66683959960938' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.03, { delay: 0.416, css: { 'opacity': '0.000000' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.024, { delay: 0.446, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.0, { delay: 0.49, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_image, 0.016, { delay: 1.87, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.063, { delay: 0.058, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.034, { delay: 0.121, css: { 'opacity': '0.46017456054688' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.137, { delay: 0.155, css: { 'opacity': '0.48159790039062' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.443, { delay: 1.457, css: { 'opacity': '0.92730721831322' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.058, css: { 'scaleX': '1.1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.058, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.089, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.121, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.175, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.25, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.294, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 1.886, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 1.915, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.058, css: { 'top': '103px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.089, css: { 'top': '103px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.0, { delay: 0.058, css: { 'scaleY': '1.1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.013, { delay: 0.058, css: { 'left': '-7px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nike_football_X_R1_g2_image, 0.018, { delay: 0.071, css: { 'left': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.086, { delay: 0.089, css: { 'opacity': '0.7408130168914795' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.064, { delay: 0.175, css: { 'opacity': '0.51657104492188' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.105, { delay: 0.239, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.045, { delay: 1.87, css: { 'opacity': '0.78109741210938' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 0.089, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 0.175, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 0.239, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 1.929, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 1.945, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__nikefootball_logo_g1_image, 0.0, { delay: 1.966, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 1.929, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 1.945, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 1.98, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 2.019, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 2.029, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 2.061, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 2.081, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 3.366, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 3.376, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 3.391, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 3.5, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.0, { delay: 3.525, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.021, { delay: 1.945, css: { 'opacity': '0.73147583007812' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.104, { delay: 1.966, css: { 'opacity': '0.23150634765625' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.804, { delay: 2.572, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g1_image, 0.125, { delay: 3.376, css: { 'opacity': '0.42105102539062' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 1.98, css: { 'left': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.0, css: { 'left': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.01, { delay: 2.0, css: { 'top': '111px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 1.929, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 1.98, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 1.992, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.0, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.01, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.047, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.061, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.637, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 2.675, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 3.366, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 3.391, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 3.409, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 3.451, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__copy_g2_image, 0.0, { delay: 3.5, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Rectangle_rect, 0.0, { delay: 8.175, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Rectangle_rect, 0.0, { delay: 8.25, css: { 'visibility': 'hidden' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Rectangle_rect, 0.0, { delay: 8.339, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Rectangle_rect, 0.0, { delay: 8.5, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.444, { delay: 3.366, css: { 'scaleY': '0.5' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.118, { delay: 3.81, css: { 'scaleY': '0.6' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.137, { delay: 4.484, css: { 'scaleY': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 3.582, { delay: 4.621, css: { 'scaleY': '0.49' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.0, { delay: 3.366, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.0, { delay: 3.376, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.0, { delay: 5.751, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.0, { delay: 8.203, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.0, { delay: 8.426, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.444, { delay: 3.366, css: { 'scaleX': '0.5' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.118, { delay: 3.81, css: { 'scaleX': '0.6' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.137, { delay: 4.484, css: { 'scaleX': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 3.582, { delay: 4.621, css: { 'scaleX': '0.49' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 3.582, { delay: 4.621, css: { 'left': '4px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.137, { delay: 4.484, css: { 'top': '99px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.775, { delay: 4.621, css: { 'top': '100px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 2.807, { delay: 5.396, css: { 'top': '56px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.042, { delay: 5.319, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.035, { delay: 5.361, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.045, { delay: 5.396, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.028, { delay: 5.441, css: { 'opacity': '0.673828125' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.035, { delay: 5.469, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.077, { delay: 5.504, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 2.622, { delay: 5.581, css: { 'opacity': '0.49063110351562' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.562, { delay: 3.366, css: { 'rotation': '-24' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.056, { delay: 3.928, css: { 'rotation': '-13' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.119, { delay: 3.984, css: { 'rotation': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.065, { delay: 4.103, css: { 'rotation': '-10' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.168, { delay: 4.168, css: { 'rotation': '2' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.148, { delay: 4.336, css: { 'rotation': '35' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.368, { delay: 4.484, css: { 'rotation': '339' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.211, { delay: 4.852, css: { 'rotation': '350' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_line2_image, 0.335, { delay: 5.063, css: { 'rotation': '360' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.0, { delay: 3.944, css: { 'scaleX': '0.6' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.137, { delay: 4.5, css: { 'scaleX': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.056, { delay: 3.944, css: { 'top': '188px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.119, { delay: 4.0, css: { 'top': '97px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.065, { delay: 4.119, css: { 'top': '67px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.168, { delay: 4.184, css: { 'top': '177px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.148, { delay: 4.352, css: { 'top': '83px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.137, { delay: 4.5, css: { 'top': '-34px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.07, { delay: 4.637, css: { 'top': '-65px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.161, { delay: 4.707, css: { 'top': '171px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.186, { delay: 4.868, css: { 'top': '8px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.056, { delay: 3.944, css: { 'left': '69px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.233, { delay: 4.119, css: { 'left': '75px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.148, { delay: 4.352, css: { 'left': '77px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.137, { delay: 4.5, css: { 'left': '56px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.07, { delay: 4.637, css: { 'left': '54px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.161, { delay: 4.707, css: { 'left': '46px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.186, { delay: 4.868, css: { 'left': '-196px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.0, { delay: 3.944, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.0, { delay: 5.079, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.0, { delay: 3.944, css: { 'scaleY': '0.6' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball3_image, 0.137, { delay: 4.5, css: { 'scaleY': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe_image, 0.0, { delay: 5.428, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe_image, 0.0, { delay: 5.759, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe_image, 0.322, { delay: 5.428, css: { 'left': '108px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe_image, 0.0, { delay: 5.428, css: { 'top': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull01_image, 0.0, { delay: 5.75, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull01_image, 0.0, { delay: 6.716, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull01_image, 0.779, { delay: 5.75, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull02_image, 0.0, { delay: 6.716, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull02_image, 0.0, { delay: 7.433, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull03_image, 0.648, { delay: 7.545, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull03_image, 0.0, { delay: 7.433, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__skull03_image, 0.0, { delay: 8.426, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Rectangle2_rect, 0.0, { delay: 6.716, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Rectangle2_rect, 0.0, { delay: 6.779, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Rectangle2_rect, 0.0, { delay: 7.433, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Rectangle2_rect, 0.0, { delay: 7.481, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 7.434, css: { 'rotation': '90' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.885, css: { 'scaleX': '0.6' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.716, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.779, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.826, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.885, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 7.433, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 7.481, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 7.545, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 7.605, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.0, { delay: 6.885, css: { 'scaleY': '0.6' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.548, { delay: 6.885, css: { 'left': '175px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__burst_image, 0.548, { delay: 6.885, css: { 'top': '181px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.632, { delay: 5.75, css: { 'left': '127px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.334, { delay: 6.382, css: { 'left': '79px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.393, { delay: 6.716, css: { 'left': '73px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.325, { delay: 7.109, css: { 'left': '175px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.402, { delay: 7.433, css: { 'left': '201px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.358, { delay: 7.835, css: { 'left': '139px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 5.777, css: { 'scaleX': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.632, { delay: 5.75, css: { 'top': '335px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.334, { delay: 6.382, css: { 'top': '169px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.393, { delay: 6.716, css: { 'top': '335px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.325, { delay: 7.109, css: { 'top': '180px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.402, { delay: 7.433, css: { 'top': '336px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.358, { delay: 7.835, css: { 'top': '61px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 5.75, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 8.194, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 5.777, css: { 'width': '22px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 5.777, css: { 'height': '22px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__ball_8_image, 0.0, { delay: 5.777, css: { 'scaleY': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s00_image, 0.0, { delay: 8.5, css: { 'top': '250px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s00_image, 0.0, { delay: 8.5, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s00_image, 0.0, { delay: 8.534, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s01_image, 0.0, { delay: 8.534, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s01_image, 0.0, { delay: 8.564, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s03_image, 0.0, { delay: 8.564, css: { 'top': '230px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s03_image, 0.0, { delay: 8.564, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s03_image, 0.0, { delay: 8.601, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s042_image, 0.0, { delay: 8.601, css: { 'top': '220px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s042_image, 0.0, { delay: 8.601, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s042_image, 0.0, { delay: 8.633, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s05_image, 0.0, { delay: 8.633, css: { 'top': '210px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s05_image, 0.0, { delay: 8.633, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s05_image, 0.0, { delay: 8.658, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s06_image, 0.0, { delay: 8.658, css: { 'top': '200px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s06_image, 0.0, { delay: 8.658, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s06_image, 0.0, { delay: 8.684, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s07_image, 0.0, { delay: 8.684, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s07_image, 0.0, { delay: 8.718, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s08_image, 0.0, { delay: 8.718, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s08_image, 0.0, { delay: 8.75, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s09_image, 0.0, { delay: 8.75, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s09_image, 0.0, { delay: 8.781, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s10_image, 0.0, { delay: 8.781, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s10_image, 0.0, { delay: 8.815, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s11_image, 0.0, { delay: 8.815, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s11_image, 0.0, { delay: 8.844, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s12_image, 0.0, { delay: 8.844, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s12_image, 0.0, { delay: 8.875, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s13_image, 0.0, { delay: 8.875, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s13_image, 0.0, { delay: 8.905, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s14_image, 0.0, { delay: 8.905, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s14_image, 0.0, { delay: 8.943, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s15_image, 0.0, { delay: 8.943, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s15_image, 0.0, { delay: 8.983, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s16_image, 0.0, { delay: 8.983, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s16_image, 0.0, { delay: 9.021, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s16_2_image, 0.0, { delay: 9.021, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s16_2_image, 0.0, { delay: 9.059, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s17_image, 0.0, { delay: 9.059, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s17_image, 0.0, { delay: 9.094, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s18_image, 0.0, { delay: 9.094, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s18_image, 0.0, { delay: 9.128, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s19_image, 0.0, { delay: 9.128, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s19_image, 0.0, { delay: 9.161, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s20_image, 0.0, { delay: 9.161, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s20_image, 0.0, { delay: 9.195, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s21_image, 0.0, { delay: 9.195, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s21_image, 0.0, { delay: 9.224, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s22_image, 0.0, { delay: 9.224, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s22_image, 0.0, { delay: 9.262, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s23_image, 0.0, { delay: 9.262, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s23_image, 0.0, { delay: 9.292, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s25_image, 0.0, { delay: 9.292, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s25_image, 0.0, { delay: 9.328, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s26_image, 0.0, { delay: 9.328, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s26_image, 0.0, { delay: 9.366, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s27_image, 0.0, { delay: 9.366, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s27_image, 0.0, { delay: 9.401, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s28_image, 0.0, { delay: 9.401, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s28_image, 0.0, { delay: 9.436, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s29_image, 0.0, { delay: 9.436, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s29_image, 0.0, { delay: 9.47, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s30_image, 0.0, { delay: 9.47, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s30_image, 0.0, { delay: 9.502, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s31_image, 0.0, { delay: 9.502, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s31_image, 0.0, { delay: 9.538, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s32_image, 0.0, { delay: 9.538, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s32_image, 0.0, { delay: 9.574, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s33_image, 0.0, { delay: 9.574, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s33_image, 0.0, { delay: 9.613, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s34_image, 0.0, { delay: 9.613, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s34_image, 0.0, { delay: 9.65, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s35_image, 0.0, { delay: 9.65, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s35_image, 0.0, { delay: 9.689, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s36_image, 0.0, { delay: 9.689, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s36_image, 0.0, { delay: 9.732, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s37_image, 0.0, { delay: 9.732, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s37_image, 0.0, { delay: 9.768, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s38_image, 0.0, { delay: 9.768, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s38_image, 0.0, { delay: 9.8, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s39_image, 0.0, { delay: 9.8, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s39_image, 0.0, { delay: 9.832, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s40_image, 0.0, { delay: 9.832, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s40_image, 0.0, { delay: 9.872, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s41_image, 0.0, { delay: 9.872, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s41_image, 0.0, { delay: 9.91, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s42_image, 0.0, { delay: 9.91, css: { 'scaleY': '1.02' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s42_image, 0.0, { delay: 9.91, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s42_image, 0.0, { delay: 9.944, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s42_image, 0.0, { delay: 9.91, css: { 'scaleX': '1.02' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s43_image, 0.0, { delay: 9.944, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s43_image, 0.0, { delay: 9.981, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s43_image, 0.0, { delay: 9.944, css: { 'scaleX': '1.03' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s43_image, 0.0, { delay: 9.944, css: { 'scaleY': '1.03' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s44_image, 0.0, { delay: 9.981, css: { 'scaleY': '1.04' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s44_image, 0.0, { delay: 9.981, css: { 'scaleX': '1.04' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s44_image, 0.0, { delay: 9.981, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s44_image, 0.0, { delay: 10.013, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s45_image, 0.0, { delay: 10.013, css: { 'scaleX': '1.05' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s45_image, 0.0, { delay: 10.013, css: { 'scaleY': '1.05' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s45_image, 0.0, { delay: 10.013, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s45_image, 0.0, { delay: 10.043, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe2_image, 0.0, { delay: 8.844, css: { 'top': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2_image, 0.33, { delay: 8.844, css: { 'left': '113px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2_image, 0.0, { delay: 8.844, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2_image, 0.0, { delay: 9.174, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2Copy_image, 0.0, { delay: 9.262, css: { 'top': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2Copy_image, 0.0, { delay: 9.262, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2Copy_image, 0.0, { delay: 9.591, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__swipe2Copy_image, 0.33, { delay: 9.262, css: { 'left': '113px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__s46_image, 0.0, { delay: 10.043, css: { 'scaleX': '1.06' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s46_image, 0.0, { delay: 10.043, css: { 'scaleY': '1.06' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s46_image, 0.0, { delay: 10.043, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s46_image, 0.0, { delay: 10.082, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.0, { delay: 10.082, css: { 'scaleX': '1.07' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.0, { delay: 10.082, css: { 'scaleY': '1.07' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.0, { delay: 10.082, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.0, { delay: 10.126, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.027, { delay: 10.082, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s47_image, 0.017, { delay: 10.109, css: { 'opacity': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s48_image, 0.0, { delay: 10.126, css: { 'scaleX': '1.08' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s48_image, 0.0, { delay: 10.126, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s48_image, 0.0, { delay: 10.167, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s48_image, 0.0, { delay: 10.126, css: { 'scaleY': '1.08' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s49_image, 0.0, { delay: 10.167, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s49_image, 0.0, { delay: 10.21, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s49_image, 0.0, { delay: 10.167, css: { 'scaleX': '1.09' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s49_image, 0.0, { delay: 10.167, css: { 'scaleY': '1.09' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s50_image, 0.0, { delay: 10.21, css: { 'scaleX': '1.1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s50_image, 0.0, { delay: 10.21, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s50_image, 0.0, { delay: 10.268, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__s50_image, 0.0, { delay: 10.21, css: { 'scaleY': '1.1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00000_image, 0.0, { delay: 10.268, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00000_image, 0.0, { delay: 10.294, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00001_image, 0.0, { delay: 10.33, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00001_image, 0.0, { delay: 10.359, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00002_image, 0.0, { delay: 10.359, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00002_image, 0.0, { delay: 10.394, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00003_image, 0.0, { delay: 10.394, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00003_image, 0.0, { delay: 10.431, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00004_image, 0.0, { delay: 10.431, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00004_image, 0.0, { delay: 10.467, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00005_image, 0.0, { delay: 10.467, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00005_image, 0.0, { delay: 10.503, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00006_image, 0.0, { delay: 10.503, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00006_image, 0.0, { delay: 10.532, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00007_image, 0.0, { delay: 10.532, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00007_image, 0.0, { delay: 10.566, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00008_image, 0.0, { delay: 10.566, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00008_image, 0.0, { delay: 10.601, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00009_image, 0.0, { delay: 10.601, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00009_image, 0.0, { delay: 10.638, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00010_image, 0.0, { delay: 10.638, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00010_image, 0.0, { delay: 10.671, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00011_image, 0.0, { delay: 10.671, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00011_image, 0.0, { delay: 10.71, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00012_image, 0.0, { delay: 10.71, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00012_image, 0.0, { delay: 10.745, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00013_image, 0.0, { delay: 10.742, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00013_image, 0.0, { delay: 10.779, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00014_image, 0.0, { delay: 10.779, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00014_image, 0.0, { delay: 10.811, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00015_image, 0.0, { delay: 10.811, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__trans_00015_image, 0.0, { delay: 10.847, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3_image, 0.0, { delay: 10.134, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3_image, 0.0, { delay: 10.569, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3_image, 0.0, { delay: 10.134, css: { 'scaleX': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3_image, 0.435, { delay: 10.134, css: { 'left': '114px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy_image, 0.0, { delay: 10.21, css: { 'scaleX': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy_image, 0.435, { delay: 10.21, css: { 'left': '114px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy_image, 0.0, { delay: 10.21, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy_image, 0.0, { delay: 10.645, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy_image, 0.0, { delay: 10.21, css: { 'opacity': '0.53594970703125' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy2_image, 0.0, { delay: 9.65, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy2_image, 0.0, { delay: 10.085, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy2_image, 0.435, { delay: 9.65, css: { 'left': '114px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy2_image, 0.0, { delay: 9.65, css: { 'scaleX': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__swipe3Copy2_image, 0.0, { delay: 9.65, css: { 'opacity': '0.53594970703125' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__endbg_image, 0.0, { delay: 11.049, css: { 'top': '6px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__endbg_image, 0.354, { delay: 11.049, css: { 'opacity': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__endbg_image, 0.0, { delay: 11.049, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_image, 0.05, { delay: 11.403, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_image, 0.067, { delay: 11.453, css: { 'opacity': '0.67062377929688' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_image, 0.072, { delay: 11.52, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_image, 0.075, { delay: 11.592, css: { 'opacity': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_image, 0.0, { delay: 11.403, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.0, { delay: 11.403, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.05, { delay: 11.403, css: { 'opacity': '0.77621459960938' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.067, { delay: 11.453, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.072, { delay: 11.52, css: { 'opacity': '0.79302978515625' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.075, { delay: 11.592, css: { 'opacity': '0.4066162109375' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__CTA_playnow2_image, 0.083, { delay: 11.667, css: { 'opacity': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.0, { delay: 11.049, css: { 'top': '0px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.0, { delay: 11.049, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.074, { delay: 11.049, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.068, { delay: 11.123, css: { 'opacity': '0.5980224609375' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.073, { delay: 11.191, css: { 'opacity': '0' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__x_image, 0.071, { delay: 11.264, css: { 'opacity': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__city_image, 0.0, { delay: 5.75, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__city_image, 0.0, { delay: 8.426, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__city_image, 0.763, { delay: 5.75, css: { 'top': '393px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__frame_image, 0.0, { delay: 10.376, css: { 'scaleY': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__frame_image, 0.0, { delay: 11.375, css: { 'scaleY': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__frame_image, 0.0, { delay: 10.376, css: { 'scaleX': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__frame_image, 0.0, { delay: 11.375, css: { 'scaleX': '1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.0, { delay: 10.829, css: { 'top': '100px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.384, { delay: 10.829, css: { 'scaleX': '1.1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.384, { delay: 10.829, css: { 'left': '-9px' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.384, { delay: 10.829, css: { 'rotation': '-38' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.0, { delay: 10.829, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_final_image, 0.384, { delay: 10.829, css: { 'scaleY': '1.1' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.77, { delay: 5.759, css: { 'top': '60px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.0, { delay: 6.513, css: { 'scaleY': '0.9' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.0, { delay: 6.513, css: { 'scaleX': '0.9' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.0, { delay: 5.751, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.0, { delay: 8.44, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__level2_goal_image, 0.0, { delay: 6.529, css: { 'left': '121px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 5.396, css: { 'visibility': 'visible' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 8.44, css: { 'visibility': 'hidden' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 10.071, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 10.884, css: { 'visibility': 'hidden' }, ease: 'easeInCubic'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.404, { delay: 5.75, css: { 'top': '244px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.228, { delay: 6.154, css: { 'top': '244px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.352, { delay: 6.382, css: { 'top': '244px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 7.835, css: { 'top': '244px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 1.645, { delay: 8.426, css: { 'top': '100px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.032, { delay: 5.396, css: { 'opacity': '0.25799560546875' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.041, { delay: 5.428, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.044, { delay: 5.469, css: { 'opacity': '0.53628540039062' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.051, { delay: 5.513, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.049, { delay: 5.564, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.404, { delay: 5.75, css: { 'left': '53px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.228, { delay: 6.154, css: { 'left': '15px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.352, { delay: 6.382, css: { 'left': '-70px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.375, { delay: 6.734, css: { 'left': '-38px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.727, { delay: 7.109, css: { 'left': '90px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.591, { delay: 7.835, css: { 'left': '0px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 10.071, css: { 'left': '10px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.404, { delay: 5.75, css: { 'scaleX': '0.35' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 6.154, css: { 'scaleX': '0.35' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 1.645, { delay: 8.426, css: { 'scaleX': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.404, { delay: 5.75, css: { 'scaleY': '0.35' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 6.154, css: { 'scaleY': '0.35' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 1.645, { delay: 8.426, css: { 'scaleY': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__shoe_82_image, 0.0, { delay: 10.071, css: { 'height': '250px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__Endframe_copy_1_text.container, 0.0, { delay: 11.191, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Endframe_copy_1_text.container, 0.16, { delay: 11.191, css: { 'opacity': '1' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Endframe_copy_2_text.container, 0.16, { delay: 11.191, css: { 'opacity': '1' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__Endframe_copy_2_text.container, 0.0, { delay: 11.191, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_play_game_text.container, 0.0, { delay: 11.403, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_play_game_text.container, 0.05, { delay: 11.403, css: { 'opacity': '0' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_play_game_text.container, 0.067, { delay: 11.453, css: { 'opacity': '0.73733520507812' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_play_game_text.container, 0.072, { delay: 11.52, css: { 'opacity': '0' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_play_game_text.container, 0.075, { delay: 11.592, css: { 'opacity': '1' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.0, { delay: 11.403, css: { 'visibility': 'visible' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.05, { delay: 11.403, css: { 'opacity': '0.24160766601562' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.067, { delay: 11.453, css: { 'opacity': '0' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.072, { delay: 11.52, css: { 'opacity': '0.5771484375' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.075, { delay: 11.592, css: { 'opacity': '0.007110595703125' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__CTA_shop_now_text.container, 0.083, { delay: 11.667, css: { 'opacity': '1' }, ease: 'easeNoneLinear'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 1.992, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 2.637, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 2.675, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 3.467, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.032, { delay: 2.029, css: { 'opacity': '0.50460815429688' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.049, { delay: 2.061, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.015, { delay: 2.144, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 1.992, css: { 'top': '195px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 1.992, css: { 'left': '48px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_1_text.container, 0.0, { delay: 1.992, css: { 'width': '223px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 1.992, css: { 'top': '222px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.037, { delay: 1.992, css: { 'opacity': '0.30136108398438' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.032, { delay: 2.029, css: { 'opacity': '0' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.049, { delay: 2.061, css: { 'opacity': '0.31710815429688' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.034, { delay: 2.11, css: { 'opacity': '1' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 1.992, css: { 'left': '38px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 1.992, css: { 'width': '246px' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 1.992, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 2.637, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 2.675, css: { 'visibility': 'visible' }, ease: 'easeInQuad'});
		TweenLite.to( adData.elements.stage__intro_copy_line_2_text.container, 0.0, { delay: 3.467, css: { 'visibility': 'hidden' }, ease: 'easeInQuad'});

	}
}






















