// author Piers Rueb

//var clickTag = "http://www.google.com";

//  Function to run with any animations starting on load, or bringing in images etc

init = function(){
   //  animations
   allAnimations();
}

//  CUSTOM

function allAnimations() { 

  var seq = document.getElementById('seq');

  //  copy

    setTimeout(function(){ seq.setAttribute('class', 'animation-trigger'); }, 400);
    setTimeout(function(){ seq.setAttribute('class', ''); }, 2400);
    setTimeout(function(){ seq.setAttribute('class', 'animation-trigger'); }, 2500);
    setTimeout(function(){ seq.setAttribute('class', 'animation-trigger fadeout transition'); }, 5000);
  
  //  animations end

}


