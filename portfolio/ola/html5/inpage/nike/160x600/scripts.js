// author Piers Rueb

//var clickTag = "http://www.google.com";

//  Function to run with any animations starting on load, or bringing in images etc

init = function(){
   //  animations
   allAnimations();
}

//  CUSTOM

function allAnimations() { 

    //  elements

    var frameOne = document.getElementById('main-frame-1');
    var frameTwo = document.getElementById('main-frame-2');
    var frameThree = document.getElementById('main-frame-3');
    var frameFour = document.getElementById('main-frame-4');
    var shoe = document.getElementById('shoe');

    //  timers

    setTimeout(function(){ 
        frameFour.setAttribute('class', 'fadein transition-1'); 
        shoe.setAttribute('class', 'fadein transition-1'); 
    }, 100);

    setTimeout(function(){ 
        frameOne.setAttribute('class', 'move-right transition-2'); 
        frameTwo.setAttribute('class', 'move-left transition-2'); 
    }, 800);

    setTimeout(function(){ frameThree.setAttribute('class', 'fadein transition-2'); }, 1300);

	//  animations end

}


