var banner = new Banner({
    type: "in-page",
    hotspotClose: ["#logo_collapse"],
    hotspotExpand: ["#logo_collapse"],
    
    elementsToRegister: [ {eventType: "click", element: "#overlay", functionToCall: "clickTags"},
                          {eventType: "click", element: "#logo-genzyme", functionToCall: "clickTags"},
                          {eventType: "click", element: "#footer", functionToCall: "clickTags"},
                          {eventType: "mouseover", element: "#go-button", functionToCall: "rollover"},
                          {eventType: "mouseout", element: "#go-button", functionToCall: "rollout"},
    ],
        customFunctions: {
        clickTags: function(){
            switch(this.id) {
                case "overlay":
                Enabler.exit('ClickTag1');
                break;
                case "logo-genzyme":
                Enabler.exit('ClickTag2');
                break;
                case "footer":
                Enabler.exit('ClickTag1');
                break;
                default:
                break;
            }
        },

        rollover: function () {
            document.getElementById("go-button").style.backgroundPosition = "0 -35px";
        },
        rollout: function () {
            document.getElementById("go-button").style.backgroundPosition = "0 0";
        },
    },

    animations: {

        firstFrame : function(){

            var myTimeline = new TimelineMax();
                myTimeline.to("#loading_dc", .5, {opacity:0, display:"none"})
                .to("#badge", .5, {opacity:1})
                .to("#copy_04", 1, {opacity:1, top:321, ease: Back.easeOut})
                .to("#copy_03_2", 1, {opacity:1, top:296, ease: Back.easeOut},"-=0.9")
                .to("#copy_03", 1, {opacity:1, top:271, ease: Back.easeOut},"-=0.9")
                .to("#copy_02", 1, {opacity:1, top:249, ease: Back.easeOut}, "-=0.9")
                .to("#copy_01", 1, {opacity:1, top:228, ease: Back.easeOut}, "-=0.9")
                
                .to("#copy_04", 1, {opacity:0, top:350, ease: Back.easeIn, delay:1})
                .to("#copy_03_2", 1, {opacity:0, top:320, ease: Back.easeIn}, "-=0.9")
                .to("#copy_03", 1, {opacity:0, top:300, ease: Back.easeIn}, "-=0.9")
                .to("#copy_02", 1, {opacity:0, top:290, ease: Back.easeIn},"-=0.9")
                .to("#copy_01", 1, {opacity:0, top:270, ease: Back.easeIn},"-=0.9" )
                .to("#logo_MS", 1, {opacity:1, top:241, ease: Back.easeOut})
                
                .to("#copy_come", 1, {opacity:1})
                .to(".people1", .3, {opacity:1})
                .to(".people2", .3, {opacity:1}, "-=0.3")
                .to(".people3", .3, {opacity:1})
                .to(".people4", .3, {opacity:1}, "-=0.3")
                .to(".people5", .3, {opacity:1})
                .to(".people6", .3, {opacity:1}, "-=0.3")

                .to("#copy_listen", 1, {opacity:1})
                .to(".people7", .3, {opacity:1}, "-=0.3")
                .to(".people8", .3, {opacity:1})
                .to(".people9", .3, {opacity:1})
                .to(".people10", .3, {opacity:1}, "-=0.3")
                .to(".people12", .3, {opacity:1}, "-=0.3")

                .to("#copy_learn", 1, {opacity:1})

                .to(".people13", .3, {opacity:1}, "-=0.3")
                .to(".people14", .3, {opacity:1})
                .to(".people15", .3, {opacity:1})
                .to(".people16", .3, {opacity:1}, "-=0.3")
                .to(".people17", .3, {opacity:1})
                .to(".people18", .3, {opacity:1}, "-=0.3")
                .to(".people19", .3, {opacity:1})
                .to(".people20", .3, {opacity:1})
                .to(".people21", .3, {opacity:1}, "-=0.3")
                .to(".people22", .3, {opacity:1})
                .to(".people23", .3, {opacity:1})
                .to(".people24", .3, {opacity:1}, "-=0.3")
                .to(".people25", .3, {opacity:1})
                .to(".people26", .3, {opacity:1}, "-=0.3")

                .to("#logo_MS", .5, {scale:5, left:-250, top:138, ease: Power2.easeOut, delay:.5})
                .to("#people", .3, {opacity:0, top:200}, "-=0.5")
                .to("#copy_come", 0, {opacity:0}, "-=0.3")
                .to("#copy_listen", 0, {opacity:0}, "-=0.3")
                .to("#copy_learn", 0, {opacity:0}, "-=0.3")
                .to("#ff", .5, {opacity:1}, "-=0.5")
                
                .to("#ff-header", .3, {opacity:1}, "-=0.3")
                .to("#ff-copy1", .3, {opacity:1})
                .to("#checkmark1", .3, {opacity:1})
                .to("#ff-copy2", .3, {opacity:1}, "+=0.3")
                .to("#checkmark2", .3, {opacity:1})
                .to("#ff-copy3", .3, {opacity:1})
                .to("#myform", .3, {opacity:1, display:"block"})
                .to("#footer", .3, {opacity:1}, "-=0.3")
                .to("#logo-genzyme", .3, {opacity:1}, "-=0.3");
                  
                //stats.setTimeline(myTimeline);
        },
                        
        },   
});