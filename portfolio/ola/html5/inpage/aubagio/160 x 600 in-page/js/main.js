var ScrollText, TextWrapper, ScrollBar, Knob, ScrollBarTop, KnobTop, KnobHeight, ScrollBarHeight, ScrollTextHeight, WholeTextHeight, autoScroll, expandBtn, expandTimeline;
var selected = null;
var y_pos = 0,
collapsed = false,
expandTimeline = new TimelineMax(),
y_elem = 0;

function expandScroll(){
    
    if(!collapsed){
        expandTimeline.to("#header", .3, {top:-79, ease: Power2.easeInOut})
        .to("#button-expand", .1, {opacity:0})
        .to("#bullet1", .3, {opacity:0})
        .to("#bullet2", .3, {opacity:0})
        .to("#logo-container", .5, {top:4, ease: Power2.easeInOut})
        .to("#safety-copy", .5, {top:113, ease: Power2.easeInOut}, "-=0.5")
        .to("#ISI", .5, {top:149, height:418, ease: Power2.easeInOut}, "-=0.5")
        .to("#scrollBar", .5, {top:152, height:411, ease: Power2.easeInOut}, "-=0.5")
        .to("#button-close", .3, {right:0});
        clearInterval(autoScroll);
    }
    collapsed = true;
}

function link() {
    Enabler.exit('ClickTag4');
}

function closeScroll(){
    expandTimeline.reverse();
    /*expandTimeline.to("#button-close", .3, {right:-150})   

        .to("#scrollBar", .5, {top:173, height:58, ease: Power2.easeInOut})
        .to("#ISI", .5, {top:170, height:64, ease: Power2.easeInOut}, "-=0.5")
        .to("#safety-copy", .5, {top:154, ease: Power2.easeInOut}, "-=0.5")
        .to("#logo-container", .5, {top:97, ease: Power2.easeInOut}, "-=0.5")
        .to("#bullet-endframe", .3, {opacity:1})
        .to("#button-expand", .5, {opacity:1})
        .to("#header", .3, {top:0, ease: Power2.easeInOut});
    */
    clearInterval(autoScroll);
    initScroller();
    collapsed = false;
}

function initScroller(){
    
    var draggable = false;
    expandTimeline = new TimelineMax();
    ScrollText = document.getElementById("ISI");
    TextWrapper = document.getElementById("ISItext");
    ScrollBar = document.getElementById("scrollBar");
    Knob = document.getElementById("knob");
    expand = document.getElementById("button-expand");
    close_btn = document.getElementById("button-close");
    setTimeout(function(){startAutoScroll()}, 500);
    Knob.addEventListener('mousedown', knobDragging);
    knob.addEventListener('mousemove', _move_elem);
    Knob.addEventListener('mouseup', _destroy);
    Knob.addEventListener('mouseout', _destroy);
    expand.addEventListener('click', expandScroll);
    close_btn.addEventListener('click', closeScroll);

    if(collapsed) ScrollBar.style.height = 58 +"px";
            //clISI = document.getElementById("ISI");
            
    function ScrollBarTop(){ return parseInt(getComputedStyle(ScrollBar).getPropertyValue("top")) };
    function KnobTop(){ return parseInt(getComputedStyle(Knob).getPropertyValue("top")) };
    function KnobHeight(){ return parseInt(getComputedStyle(Knob).getPropertyValue("height")) };
    function ScrollBarHeight(){ return parseInt(getComputedStyle(ScrollBar).getPropertyValue("height")) };
    function ScrollTextHeight(){ return parseInt(getComputedStyle(ScrollText).getPropertyValue("height")) };
    function WholeTextHeight(){ return parseInt(getComputedStyle(TextWrapper).getPropertyValue("height")) };

    loadScroll();

    function startAutoScroll(){
       autoScroll = setInterval(function(){ScrollText.scrollTop += 1}, 63);

        console.log("ScrollText.scrollTop", ScrollText.scrollTop)

        ScrollText.addEventListener('mousewheel', destroyAutoScroll);
        ScrollText.addEventListener("DOMMouseScroll", destroyAutoScroll);
        ScrollText.addEventListener("touchstart", destroyAutoScroll);
        ScrollText.addEventListener("mousedown", destroyAutoScroll);

    };

    function knobDragging() {
        drag_init(this);        
        destroyAutoScroll();
        return false; 
    }

    function drag_init(elem) {
        selected = elem;
        y_elem = y_pos - selected.offsetTop;
    }

    function _destroy(){
        selected = null;
    }

    function _move_elem(e) {
            
            // if(KnobTop() > -1 && KnobTop() < (ScrollBarHeight() - (KnobHeight()-1))){
                y_pos = document.all ? window.event.clientY : e.pageY;
                
                if (selected !== null) {
                                        
                    if((y_pos - y_elem) < 0){
                        Knob.style.top = "0px";
                    }else if((y_pos - y_elem) > (ScrollBarHeight() - KnobHeight()-1)){
                        Knob.style.top = ScrollBarHeight() - 15 + "px";
                    }else{
                        Knob.style.top = (y_pos - y_elem)+"px";
                    }
                    
                    var percent = KnobTop() / (ScrollBarHeight() - KnobHeight() - 3);
                    
                    ScrollText.scrollTop =  (WholeTextHeight() - ScrollTextHeight()) * percent;

                    //console.log("ScrollTextHeight: " + ScrollTextHeight());
                   // console.log("ScrollText.scrollTop: " + ScrollText.scrollTop);
                    //console.log("KnobTop: " + KnobTop());
                    //console.log("WholeTextHeight(): " + WholeTextHeight());
            // }
        }
                
    };

    function loadScroll(){

    ScrollText.onscroll = function(){         
           if(selected == null) Knob.style.top = (ScrollBarHeight()-KnobHeight()) * (ScrollText.scrollTop / (TextWrapper.clientHeight - ScrollText.clientHeight))  + "px";
        }

        Knob.addEventListener('mousedown', knobDragging);
        Knob.addEventListener('mousemove', _move_elem);
        Knob.addEventListener('mouseup', _destroy);
        Knob.addEventListener('mouseout', _destroy);
    };

    function destroyAutoScroll(){
        clearInterval(autoScroll);
    };
};


var banner = new Banner({
    type: "in-page",    
    elementsToRegister: [   {eventType: "click", element: "#prescription", functionToCall: "clicktags"},
                            {eventType: "click", element: "#medical", functionToCall: "clicktags"},
                            {eventType: "click", element: "#link-overlay", functionToCall: "clicktags"}, 
                            {eventType: "mouseover", element: "#medical", functionToCall: "rollover"},
                            {eventType: "mouseover", element: "#prescription", functionToCall: "rollover"},
                            {eventType: "mouseover", element: "#link-overlay", functionToCall: "rollover"}, 
                            {eventType: "mouseout", element: "#link-overlay", functionToCall: "rollout"}, 
                            {eventType: "mouseout", element: "#medical", functionToCall: "rollout"}, 
                            {eventType: "mouseout", element: "#prescription", functionToCall: "rollout"}, 
        ],
    customFunctions: {
        clicktags: function(){

        switch(this.id) {
            case "prescription":
            Enabler.exit('ClickTag2');
            break;
            case "medical":
            Enabler.exit('ClickTag3');
            break;
            case "link-overlay":
            Enabler.exit('ClickTag1');
            break;
            default:
            break;
        }

        },

        rollover: function () {
            
            switch(this.id) {
                case "prescription":
                TweenMax.to(this, .5, {color:"#CC6600"})
                break;
                case "medical":
                TweenMax.to(this, .5, {color:"#CC6600"})
                break;
                case "link-overlay":
                document.getElementById("button-event-container").style.backgroundPosition = "0 -26px";
                break;
                default:
                break;

            }

        },

        rollout: function () {

            switch(this.id) {
                case "prescription":
                TweenMax.to(this, .5, {color:"#4c4c4c"})
                break;
                case "medical":
                TweenMax.to(this, .5, {color:"#4c4c4c"})
                break;
                case "link-overlay":
                document.getElementById("button-event-container").style.backgroundPosition = "0 0";
                break;
                default:
                break;

            }

        },
       
    },
    animations: {

        firstFrame : function(){

            var myTimeline = new TimelineMax();
                myTimeline.to("#loading_dc", .5, {opacity:0, display:"none"})
                    .to("#header", .5, {top:0, ease: Expo.easeOut})
                    .to("#listen-container", .3, {opacity:1, left:0}, "+=0.2" )
                    .to("#listen-container", .3, {opacity:0, left:-300}, "+=2.8" )
                    .to("#learn-container", .3, {opacity: 1, left:0} )
                    .to("#learn-container", .3, {opacity: 0, left:-300}, "+=2" )
                    .to("#live-container", .3, {opacity: 1, left:0})
                    .to("#live-text", .3, {opacity: 1})
                    .to("#live-container", .3, {opacity: 0, delay:2}, "+=2")
                    .to("#live-text", .3, {opacity: 0})
                    .to("#bullet1", .3, {opacity: 1})
                    .to("#bullet2", .3, {opacity: 1})
                    .to("#logo-container", .5, {opacity: 1})
                    .to("#safety-copy", .5, {opacity: 1})
                    .to("#footer", .3, {opacity: 1}, "-=0.5")
                    .to("#ISI", .5, {opacity: 1 , display:'block'})
                    .to("#scrollBar", .5, {opacity: 1}, "-=0.4")
                    .to("#button-event-container", .5, {right: 10, ease: Expo.easeOut})
                    .to("#button-expand", .3, {opacity:1, onComplete:initScroller} , "-=0.5")
                    
                    //stats.setTimeline(myTimeline);
        },

                        
        },
});