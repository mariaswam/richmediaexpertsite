// JavaScript Document
// JavaScript Document
//HTML5 Ad Template JS from DoubleClick by Google

//Declaring elements from the HTML i.e. Giving them Instance Names like in Flash - makes it easier
var container;
var content;
var dcLogo;
var bgExit;

var woman = document.getElementById("woman");
var copy1 = document.getElementById("copy1");
var copy2 = document.getElementById("copy2");
var copy3 = document.getElementById("copy3");
var copy4 = document.getElementById("copy4");
var copy5 = document.getElementById("copy5");
var copy6 = document.getElementById("copy6");
var ctaBtn = document.getElementById("ctaBtn");
var ctaText1 = document.getElementById("ctaText1");
var ctaText2 = document.getElementById("ctaText2");
var AUB_logo = document.getElementById("AUB_logo");
var PI_exit1 = document.getElementById("PI_exit1");
var PI_exit2 = document.getElementById("PI_exit2");
var isi = document.getElementsByClassName("isi_scroll");
var isiClicktag = document.getElementById("isiClicktag");

var bg1 = document.getElementById("bg1");
var bg2 = document.getElementById("bg2");
var bg3 = document.getElementById("bg3");
var bg4 = document.getElementById("bg4");

//Function to run with any animations starting on load, or bringing in images etc
function initUnit(){
	//Assign All the elements to the element on the page
	container = document.getElementById('container_dc');
	content = document.getElementById('content_dc');
	bgExit = document.getElementById('background_exit_dc');

	//Setup Background Image (this can be done in CSS as well)
//	content.style.backgroundImage = "url("+Enabler.getFilename('DCRM_HTML5_inPage_Polite_300x250.jpg')+")";

	//Bring in listeners i.e. if a user clicks or rollovers
	addListeners();

	//Show Ad
    container.style.display = "block";
    checkTabStatus();
    frame1();
    //setTimeout (function(){isiClickPos()},26500);

}

//Add Event Listeners
function addListeners() {
	bgExit.addEventListener('click', bgExitHandler, false);
    ctaBtn.addEventListener('click', ctaClick,false);
    ctaBtn.addEventListener('mouseover', ctaOver,false);
    ctaBtn.addEventListener('mouseout', ctaOut,false);

    PI_exit1.addEventListener('click', PI_clickExit1, false);
    PI_exit2.addEventListener('click', PI_clickExit2, false);
    //isiClicktag.addEventListener('click', isiClick, false);
}

function frame1(){
    TweenLite.from(woman, .4, {opacity:0, left:-110, delay:.5});
    TweenLite.from(copy1, .4, {opacity:0, delay:.8});
    setTimeout (function(){frame1b()},500);
}

function frame1b(){
    TweenLite.to(copy1, .4, {top:25, delay:1});
    TweenLite.to(copy2, .4, {opacity:1, delay:1.25});
    TweenLite.to(copy3, .5, {opacity:1, delay:1.35});
    setTimeout (function(){frame2()},6500);
}

function frame2(){
    TweenLite.to(woman, .4, {opacity:0});
    TweenLite.to(copy1, .4, {opacity:0});
    TweenLite.to(copy2, .4, {opacity:0});
    TweenLite.to(copy3, .4, {opacity:0});
    TweenLite.to(bg1, .4, {opacity:0});

    TweenLite.to(bg2, .4, {opacity:1});
    TweenLite.to(copy4, .4, {opacity:1});
    setTimeout (function(){frame3()},5000);
}

function frame3(){
    TweenLite.to(bg2, .4, {opacity:0, delay:0});
    TweenLite.to(copy4, .4, {opacity:0, delay:0});

    TweenLite.to(bg3, .5, {opacity:1, delay:0});
    TweenLite.to(copy5, .5, {opacity:1, delay:0});
    setTimeout (function(){frame4()},6500);
}

function frame4(){
    TweenLite.to(bg3, .4, {opacity:0});
    TweenLite.to(copy5, .4, {opacity:0});

    TweenLite.to(bg4, .5, {opacity:1, delay:0});
    TweenLite.to(copy6, .5, {opacity:1, delay:0});
    TweenLite.to(ctaBtn, .5, {opacity:1, left:41, delay:1});
    TweenLite.to(AUB_logo, .5, {opacity:1, right:18, delay:1});
}

function isiClickPos(){
    TweenLite.to(isiClicktag, .5, {x:125});
}


bgExitHandler = function(e) {
	//Call Exits
	Enabler.exit('ClickTag1');
}

function PI_clickExit1(){
    Enabler.exit("ClickTag2");
    //console("exit PI1");
}

function PI_clickExit2(){
    Enabler.exit("ClickTag3");
    //console("exit PI2");
}

function isiClick(){
	//alert("CLICK isi link");
    //Enabler.exit("ClickTag4");
}

function ctaClick(){
    Enabler.exit("ClickTag4");
    ctaOut();

}


function ctaOver(){
    TweenLite.to(ctaText1, .5, {opacity:0});
    TweenLite.to(ctaText2, .5, {opacity:1});
    TweenLite.to(ctaBtn, .5, {backgroundColor:"#ffffff"});
}

function ctaOut(){
    TweenLite.to(ctaText1, .5, {opacity:1});
    TweenLite.to(ctaText2, .5, {opacity:0});
    TweenLite.to(ctaBtn, .5, {backgroundColor:"#cc6600"});
}

initUnit();

function checkTabStatus (){
    var myInt = setInterval(myFunc, 300);
    var stateKey, eventKey,
        keys = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };

    var vis = (function(){
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }

        return function(c) {
            if (c) document.addEventListener(eventKey, c);
            return !document[stateKey];
        }
    } )();

    function myFunc(){
      if(vis()){
            // before the tab gains focus again, very important!
            setTimeout(function(){
           TweenLite.ticker.useRAF(true);
            },300);
        } else{
           TweenLite.ticker.useRAF(false);
        }
    }
}

    var filesReady = false;
    var checkInterval;
    //Initialize Enabler
    if (Enabler.isInitialized()) {
        init();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, init);
    }
    //Run when Enabler is ready
    function init() {
        //politeInit();
        //Load in Javascript

        if (Enabler.isPageLoaded()) {
            politeInit();
        } else {
            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, politeInit);
        }
    }


    function politeInit() {
        //Load in Javascript
        /*var extJavascript = document.createElement('script');
            extJavascript.setAttribute('type', 'text/javascript');
            extJavascript.setAttribute('src', Enabler.getFilename('DCRM_HTML5_inPage_Polite_300x250.js'));
            document.getElementsByTagName('head')[0].appendChild(extJavascript);

            //Load in CSS

            var extCSS=document.createElement('link');
            extCSS.setAttribute("rel", "stylesheet");
            extCSS.setAttribute("type", "text/css");
            extCSS.setAttribute("href", Enabler.getFilename("DCRM_HTML5_inPage_Polite_300x250.css"));
            document.getElementsByTagName("head")[0].appendChild(extCSS);*/
        document.getElementById("container_dc").style.opacity = 1;
        document.getElementById("loading_image_dc").style.opacity = 0;
        document.getElementById("container_dc").style.display = "block";

        //filesReady = true;
    }

    function checkAssetsPolite(){
            if(filesReady){
                clearInterval(checkInterval);
                initUnit(); //call init function in js
            }
        }

        var timer1;
        var interval1;
        var api;

        window.onload = function(){
            //initUnit();
            checkInterval = setInterval(checkAssetsPolite, 500)

           /*
            *Scroll pane plugin
            * */
           /*http://jscrollpane.kelvinluck.com/*/
            $('.scroll-pane').jScrollPane({showArrows: true, verticalDragMinHeight: 25,verticalDragMaxHeight: 25, animateScroll: true, hijackInternalLinks: true,contentWidth: '0px'});
            var pane = $('.scroll-pane');
            api = pane.data('jsp');
            //--> AutoScroll <--// scrollDiv();


            $( ".jspArrowUp, .jspArrowDown, .jspDrag, #isi" ).mousedown(function(event) {
               event.preventDefault();
               clearTimeout(timer1);
               $('.jspPane, .jspDrag').stop();
            });

            $( ".jspArrowUp" ).click(function(event) {
               event.preventDefault();
                api.scrollBy(0, -10);
            });

            $( ".jspArrowDown" ).click(function(event) {
               event.preventDefault();
                api.scrollBy(0, 10);
            });

          var el = document.getElementById("isi");
          el.addEventListener("touchstart", handleTouchEvents, false);

           function handleTouchEvents(){
               event.preventDefault();
               clearTimeout(timer1);
               $('.jspPane, .jspDrag').stop();
               el.removeEventListener("touchstart", handleTouchEvents, false);
           }

            $('#isi').mousewheel(function(event) {
                event.preventDefault();
               clearTimeout(timer1);
               $('.jspPane, .jspDrag').stop();
            });

        }//END  window.onload

        function scrollDiv() {
           api.scrollBy(0, 24);
           timer1 = setTimeout("scrollDiv()", 250);

           if(api.getPercentScrolledY() >= 0.999)
           {
              clearTimeout(timer1);
              $('.jspPane, .jspDrag').stop();
              interval1 = setInterval(restartScroll, 1000);
              //api.reinitialise();
           }

        function restartScroll()
        {
            clearInterval(interval1);
            api.scrollTo(0, 0);
        }

        }

