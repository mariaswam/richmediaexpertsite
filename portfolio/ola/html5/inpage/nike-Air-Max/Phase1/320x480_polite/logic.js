//bnr.js
var bnr=function(){return{_D:{html5:"querySelector"in document&&"addEventListener"in window,ie:-1!=navigator.userAgent.toLowerCase()?parseInt(navigator.userAgent.toLowerCase().split("msie")[1]):0},detect:function(e){return this._D[e]},ready:function(e){return"complete"===document.readyState?e():(document.addEventListener("DOMContentLoaded",e,!1),this)},$:function(e){return document.getElementById(e)||document.getElementsByClassName(e)||document.getElementsByTagName(e)||document.querySelector(e)||document.querySelectorAll(e)},appendClass:function(e,t){e.className+=" "+t},setClass:function(e,t){e.className=t},on:function(e,t,n){e.addEventListener(t,n,!1)},off:function(e,t,n){e.removeEventListener(t,n,!1)},_T:{F:2e3,L:0,T:[],D:0,M:null},timeline:function(e,t){if("string"==typeof e){var n={loops:this._T.L,duration:this._T.D,timeline:this._T.M};return n[e]}if("boolean"!=typeof e||e!==!1){this._T.T=[],this._T.D=0,this._T.M=e,this._T.L++,t&&(this._T.F=t);for(var i=this,o=0;o<e.length;o++){var s=e[o][1],r=e[o][0]<10?e[o][0]*i._T.F:e[o][0]?e[o][0]:i._T.F;this._T.T.push(setTimeout(s,i._T.D+=Math.abs(r)))}}else for(var o=0;o<this._T.T.length;o++)clearTimeout(this._T.T[o])}}}();
//ANIMATION
var $banner = bnr.$('banner')[0];
var $cta = bnr.$('cta')[0];
var frameDuration = 3000;
var tl = [
	[0,function(){bnr.appendClass($banner,'scene1');}]

];
bnr.on($banner,'click', function(){ Enabler.exit('clickTag'); });

function steamToggle(val){
	var steam = document.getElementById("steam");
	var steamAnim = document.getElementById("steam-anim");
	if(val){
		steam.className = 'steam';
		steamAnim.className = 'steam-anim';
	}else{
		steam.className = '';
		steamAnim.className = '';
	}
}
var creative = {};
creative.dom = {};
function youtube(containerId, videoID, videoURL){
	if (!creative.dom.ytp) {
		creative.dom.ytp = document.createElement('gwd-youtube');
		var ytp = creative.dom.ytp;
		ytp.setAttribute('id', videoID);
	    ytp.setAttribute('video-url', videoURL);
	    ytp.setAttribute('autoplay', 'preview'); // none, standard, preview, intro.
	    ytp.setAttribute('preview-duration', '10'); // Only for preview autoplay mode.
	    // Adformat parameter required for any creative using a YouTube player.
	    ytp.setAttribute('adformat', '1_5');
	    ytp.setAttribute('controls', 'autohide'); // none, show, autohide.
	    document.getElementById(containerId).appendChild(ytp);
	    ytp.addEventListener('playpressed', function() {
	    	steamToggle(false);
	      if (creative.ytplayerEnded) {
	      	steamToggle(true);
	        creative.ytplayerEnded = false;
	        Enabler.counter(videoID + ' replay', true);
	      }
	      ytp.toggleMute();
	      Enabler.counter(videoID + ' play pressed', true);
	    }, false);
	    ytp.addEventListener('paused', function() {
	    	steamToggle(true);
	      Enabler.counter(videoID + ' paused', true);
	    }, false);
	    ytp.addEventListener('ended', function() {
	    	steamToggle(true);
	      Enabler.counter(videoID + ' ended', true);
	      creative.ytplayerEnded = true;
	    }, false);
	    ytp.addEventListener('viewed0percent', function() {
	      Enabler.counter(videoID + ' viewed 0%');
	    }, false);
	    ytp.addEventListener('viewed25percent', function() {
	      Enabler.counter(videoID + ' viewed 25%');
	    }, false);
	    ytp.addEventListener('viewed50percent', function() {
	      Enabler.counter(videoID + ' viewed 50%');
	    }, false);
	    ytp.addEventListener('viewed75percent', function() {
	      Enabler.counter(videoID + ' viewed 75%');
	    }, false);
	    ytp.addEventListener('viewed100percent', function() {
	      Enabler.counter(videoID + ' viewed 100%');
	    }, false);
	    ytp.addEventListener('previewed0percent', function() {
	      Enabler.counter(' previewed 0%');
	      if (!ytp.a.isMuted()) {
	        ytp.toggleMute();
	      }
	    }, false);
	    ytp.addEventListener('previewed25percent', function() {
	      Enabler.counter(videoID + ' previewed 25%');
	    }, false);
	    ytp.addEventListener('previewed50percent', function() {
	      Enabler.counter(videoID + ' previewed 50%');
	    }, false);
	    ytp.addEventListener('previewed75percent', function() {
	      Enabler.counter(videoID + ' previewed 75%');
	    }, false);
	    ytp.addEventListener('previewed100percent', function() {
	      Enabler.counter(videoID + ' previewed 100%');
	    }, false);
	    document.getElementById(containerId).appendChild(ytp);		
	}else{
		creative.dom.ytp.style.display = 'block';
	}
}
function adVisibilityHandler(evt){
	youtube('video', 'ytp', 'https://www.youtube.com/watch?v=ieTKOc_JaT4');
	bnr.timeline(tl,frameDuration);
}
if (Enabler.isVisible()) {
	adVisibilityHandler();
}else {
	Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler);
}