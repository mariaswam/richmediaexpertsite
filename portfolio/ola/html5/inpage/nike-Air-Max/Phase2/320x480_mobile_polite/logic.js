//bnr.js
var bnr=function(){return{_D:{html5:"querySelector"in document&&"addEventListener"in window,ie:-1!=navigator.userAgent.toLowerCase()?parseInt(navigator.userAgent.toLowerCase().split("msie")[1]):0},detect:function(e){return this._D[e]},ready:function(e){return"complete"===document.readyState?e():(document.addEventListener("DOMContentLoaded",e,!1),this)},$:function(e){return document.getElementById(e)||document.getElementsByClassName(e)||document.getElementsByTagName(e)||document.querySelector(e)||document.querySelectorAll(e)},appendClass:function(e,t){e.className+=" "+t},setClass:function(e,t){e.className=t},on:function(e,t,n){e.addEventListener(t,n,!1)},off:function(e,t,n){e.removeEventListener(t,n,!1)},_T:{F:2e3,L:0,T:[],D:0,M:null},timeline:function(e,t){if("string"==typeof e){var n={loops:this._T.L,duration:this._T.D,timeline:this._T.M};return n[e]}if("boolean"!=typeof e||e!==!1){this._T.T=[],this._T.D=0,this._T.M=e,this._T.L++,t&&(this._T.F=t);for(var i=this,o=0;o<e.length;o++){var s=e[o][1],r=e[o][0]<10?e[o][0]*i._T.F:e[o][0]?e[o][0]:i._T.F;this._T.T.push(setTimeout(s,i._T.D+=Math.abs(r)))}}else for(var o=0;o<this._T.T.length;o++)clearTimeout(this._T.T[o])}}}();
//ANIMATION
var $banner = bnr.$('banner')[0];
var $cta = bnr.$('cta')[0];
var frameDuration = 2300;
var tl = [
	[0,function(){bnr.appendClass($banner,'scene1');}],
	[,function(){bnr.appendClass($banner,'scene2');}],
	[,function(){bnr.appendClass($banner,'scene3');}],
	[,function(){bnr.appendClass($banner,'scene4');}]

];
bnr.on($banner,'click', function(){ Enabler.exit('clickTag'); });

function adVisibilityHandler(evt){
	bnr.timeline(tl,frameDuration);
}
if (Enabler.isVisible()) {
	adVisibilityHandler();
}else {
	Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisibilityHandler);
}