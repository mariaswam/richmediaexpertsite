// Variables
var clicktagArea = document.getElementById("clicktag"),
	cta = document.getElementById("cta");

clicktagArea.addEventListener( 'touchend', exitCall, false);
clicktagArea.addEventListener( 'click', exitCall, false);

cta.addEventListener( 'click', ctaClickTag, false);

// Functions

function exitCall(){
	Enabler.exit('General Exit');
	window.open("");
}

function ctaClickTag(){
	Enabler.exit('Button Exit');
	window.open("");
}

// Animations

function initAnimations(){
	TweenLite.to(["#hand-1", "#hand-2"], 0.2, {rotationZ:60, ease:Power2.easeInOut, transformOrigin:"bottom left"});
	TweenLite.to(["#hand-3", "#hand-4"], 0.2, {rotationZ:-60, ease:Power2.easeInOut, transformOrigin:"bottom right"});
	TweenLite.to(["#hand-1", "#hand-2", "#hand-3", "#hand-4"], 0.2, { autoAlpha:1, ease:Back.easeOut, delay: 1});

	TweenLite.to( "#text1", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 2});
	TweenLite.to( "#text2-1", 0.5, { left:0, right:0, ease:Power2.easeOut, delay: 2.2});
	TweenLite.to( "#text2-2", 0.5, { left:0, right:0, ease:Power2.easeOut, delay: 2.3});

	TweenLite.to( "#hands", 0.5, { top:84, ease:Power2.easeOut, delay: 3});

	TweenLite.to(["#hand-1", "#hand-2"], 0.5, {rotationZ:0, ease:Power2.easeInOut, transformOrigin:"bottom left", delay:3.1});
	TweenLite.to(["#hand-3", "#hand-4"], 0.5, {rotationZ:0, ease:Power2.easeInOut, transformOrigin:"bottom right", delay:3.1});

	TweenLite.to( "#hands", 0.5, { top:100, ease:Power2.easeOut, delay: 3.1});
	TweenLite.to( "#hands", 0.5, { top:260, ease:Power2.easeOut, delay: 3.9});

	TweenLite.to( "#text2-1", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 5});
	TweenLite.to( "#text2-2", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 5.1});

	TweenLite.to( "#text3-1", 0.5, { left:0, right:0, ease:Power2.easeOut, delay: 5.4});
	TweenLite.to( "#text3-2", 0.5, { left:0, right:0, ease:Power2.easeOut, delay: 5.5});

	TweenLite.to( "#text3-1", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 8.4});
	TweenLite.to( "#text3-2", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 8.5});
	TweenLite.to( "#devices", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 8.6});
	TweenLite.to( "#legal", 0.5, { autoAlpha:0, ease:Back.easeOut, delay: 8.7});

	TweenLite.to( "#ff-title", 0.5, { autoAlpha:1, ease:Back.easeOut, delay: 8.9});
	TweenLite.to( "#offer", 0.5, { autoAlpha:1, ease:Back.easeOut, delay: 9.1});
	TweenLite.to( "#phone", 0.5, { autoAlpha:1, ease:Back.easeOut, delay: 9.3});
	TweenLite.to( "#cta", 0.5, { autoAlpha:1, scale:1, ease:Back.easeOut, delay: 9.5});
}