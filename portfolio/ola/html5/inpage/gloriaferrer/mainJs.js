window.onload = function () {
    var clickTagGen = document.getElementById("clickTagGeneral"),
            delay = 0,
            f1 = document.querySelector("#frame_1"),
            f2 = document.querySelector("#frame_2"),
            f3 = document.querySelector("#frame_3"),
            f4 = document.querySelector("#frame_4"),
            f5 = document.querySelector("#frame_5"),
            f1_copy_1 = document.querySelector("#frame_1 .text1"),
            f2_copy_1 = document.querySelector("#frame_2 .text1"),
            f3_copy_1 = document.querySelector("#frame_3 .text1"),
            f4_copy_1 = document.querySelector("#frame_4 .text1"),
            f5_labelGloriaFerrer = document.querySelector("#frame_5 .labelGloriaFerrer"),
            f5_bottle = document.querySelector("#frame_5 .bottle"),
            f5_btnEnterNow = document.querySelector("#frame_5 .btnEnterNow");
    
    TweenLite.to([f1,f2,f3,f4,f5], 0, {opacity: 1, delay: .1});
    
    TweenLite.from(f1_copy_1, 1, {top: "5px", opacity: 0, ease: Power1.easeOut, delay: delay});
    delay += 3;
    TweenLite.to(f1_copy_1, .5, {top: "60px", opacity: 0, ease: Power1.easeIn, delay: delay});

    delay += .8;

    TweenLite.from(f2_copy_1, 1, {top: "5px", opacity: 0, ease: Power1.easeOut, delay: delay});
    delay += 3;
    TweenLite.to(f2_copy_1, .5, {top: "60px", opacity: 0, ease: Power1.easeIn, delay: delay});

    delay += .8;

    TweenLite.from(f3_copy_1, 1, {top: "5px", opacity: 0, ease: Power1.easeOut, delay: delay});
    delay += 3;
    TweenLite.to(f3_copy_1, .5, {top: "60px", opacity: 0, ease: Power1.easeIn, delay: delay});

    delay += .8;

    TweenLite.from(f4_copy_1, 1, {top: "5px", opacity: 0, ease: Power1.easeOut, delay: delay});
    delay += 3;
    TweenLite.to(f4_copy_1, .5, {top: "60px", opacity: 0, ease: Power1.easeIn, delay: delay});

    delay += .8;

    TweenLite.from(f5_labelGloriaFerrer, .5, {left: "-100px", opacity: 0, ease: Power1.easeOut, delay: delay});
    TweenLite.from(f5_bottle, .5, {right: "-100px", opacity: 0, ease: Power1.easeOut, delay: delay});
    delay += .3;
    TweenLite.from(f5_btnEnterNow, .5, {top: "-100px", opacity: 0, ease: Power1.easeOut, delay: delay});



////      // ======================CLICK TAG GENERAL=======================
//      var URL    =  'http://www.google.com/'
//      var a =  document.createElement('a');
//      a.href  = URL;
//      a.appendChild(clickTagGen);
//      a.setAttribute('target', '_blank');
//      document.getElementById('ad-300x250').appendChild(a);
//      
//
}
function init(x) {
    TweenLite.to(x, 1, {opacity: 1, delay: 0});
}