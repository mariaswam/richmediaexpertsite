contentCanon_tlm           = new TimelineMax({repeat: 0, paused: false, delay: 1, yoyo: true, overwrite: "false"});
canonStructure_tlm     = new TimelineMax({repeat: 0, paused: false, delay: 3, yoyo: true, overwrite: "false"});
canonStructureMove_tlm = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
canonWheel_tlm         = new TimelineMax({repeat: 0, paused: false, delay: 1.3, yoyo: true, overwrite: "false"});
canonSmoke_tlm         = new TimelineMax({repeat: 0, paused: true, delay: 4, yoyo: false, overwrite: "false"});
canonFire_tlm          = new TimelineMax({repeat: 0, paused: false, delay: .8, yoyo: false, overwrite: "false"});
canonFuse_tlm          = new TimelineMax({repeat: 0, paused: false, delay: .8, yoyo: false, overwrite: "false"});
cloud_tlm              = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
copys_tlm              = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
soldier_tlm            = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
soldierArm_tlm         = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
btnSaveNow_tlm         = new TimelineMax({repeat: 0, paused: false, delay: 0, yoyo: false, overwrite: "false"});
btnSaveNowMouseOver_tlm        = new TimelineMax({repeat: 0, paused: true, delay: 0, yoyo: false, overwrite: "false"});

window.onload = function () {
    var clickTagGen        = document.getElementById("clickTagGeneral"),
            contentCanon   = document.querySelector("#canon"),
            canonStructure = document.querySelector(".structure"),
            canonShadow    = document.querySelector(".shadow"),
            canonWheel     = document.querySelector(".wheel"),
            canonFire      = document.querySelector(".fire"),
            canonFuse      = document.querySelector(".fuse"),
            canonSmoke     = document.querySelector(".smoke"),
            contentSoldier = document.querySelector(".contentSoldier"),
            SoldierBody    = document.querySelector(".body"),
            SoldierArm     = document.querySelector(".arm"),
            cloudWhite     = document.querySelector(".cloudWhite"),
            copy_1         = document.querySelector(".copy_1"),
            copy_2         = document.querySelector(".copy_2"),
            copy_3         = document.querySelector(".copy_3"),
            copy_4         = document.querySelector(".copy_4"),
            btnSaveNow     = document.querySelector(".btnSaveNow"),
            addRollOver    = document.querySelector("#clickTagGeneral");

    addRollOver.addEventListener("mousemove", function () {
        btnSaveNowMouseOver_tlm.play();
        btnSaveNowMouseOver_tlm.add(TweenMax.to(btnSaveNow, .5, {scaleX: 1.1, scaleY: 1.1, delay: 0, ease: Back.easeOut.config(1.7), overwrite: "false"}));
    });
    addRollOver.addEventListener("mouseleave", function () {
        btnSaveNowMouseOver_tlm.reverse();
        btnSaveNowMouseOver_tlm.add(TweenMax.to(btnSaveNow, .5, {scaleX: 1, scaleY: 1, delay: 0, ease: Back.easeOut.config(1.7), overwrite: "false"}));
    });
    addRollOver.addEventListener('click', bgExitHandler, false);

    contentCanon_tlm.add(TweenMax.to(contentCanon, 2, {left: 0, delay: 0, ease: Back.easeOut.config(1.7), overwrite: "false"}));
    canonStructureMove_tlm.add(TweenMax.to(canonStructure, 0.3, {css: {transform: "rotate(9deg)"}, repeat: 1, yoyo: true, delay: 2, ease: Power0.easeNone, overwrite: "false"}));
    canonWheel_tlm.add(TweenMax.to(canonWheel, 1.5, {rotation: 180, ease: Back.easeOut.config(1.7), overwrite: "false"}));
    canonFire_tlm.add(TweenMax.to(canonFire, 0, {alpha: 1, delay: 2, overwrite: "false"}));
    canonFire_tlm.add(TweenMax.to(canonFire, 2, {y: 30, ease: Back.easeOut.config(1.7), delay: .3, overwrite: "false"}));
    canonFuse_tlm.add(TweenMax.to(canonFuse, 2, {y: 30, ease: Back.easeOut.config(1.7), delay: 2.3, overwrite: "false"}));
    canonFire_tlm.add(TweenMax.to(canonFire, 0, {alpha: 0, delay: 0.1, overwrite: "false"}));
    canonStructure_tlm.add(TweenMax.to(canonStructure, .1, {scaleX: 1.1, scaleY: 1.1, repeat: 3.5, yoyo: true, delay: .5, ease: Power0.easeNone, overwrite: "false", onComplete: function () {canonSmoke_tlm.restart()}}));
    canonSmoke_tlm.add(TweenMax.from(canonSmoke, .3, {x: -5, y: 3, alpha: 0, ease: Power0.easeNone}));
    canonSmoke_tlm.add(TweenMax.to(canonSmoke, .3, {x: 5, y: -3, alpha: 0, delay: 0, ease: Power0.easeNone}));

    cloud_tlm.add(TweenMax.to(cloudWhite, .8, {right: 0, delay: 4.8, ease: Back.easeOut.config(1.7), overwrite: "false"}));

    copys_tlm.add(TweenMax.to(copy_1, .5, {css:{right: "57px"}, delay: 5, ease: Back.easeOut, overwrite: "false"}));
    copys_tlm.add(TweenMax.to(copy_2, .5, {css:{right: "46px"}, delay: 0, ease: Back.easeOut, overwrite: "false"}));
    copys_tlm.add(TweenMax.to(copy_3, .5, {css:{right: "11px"}, delay: 0, ease: Back.easeOut, overwrite: "false", onComplete: function () {canonStructure_tlm.restart(), canonStructureMove()}}));
    copys_tlm.add(TweenMax.to(copy_4, .5, {css:{right: "13px"}, delay: 1.5, ease: Back.easeOut, overwrite: "false"}));

    soldier_tlm.add(TweenMax.to(contentSoldier, .5, {bottom: 0, delay: 8.5, ease: Power0.easeNone}));
    soldier_tlm.add(TweenMax.to(SoldierArm, 0.2, {css: {transform: "rotateX(-3deg) translateZ(5em)"}, delay: .8, ease: Power0.easeNone}));
    soldierArm_tlm.add(TweenMax.to(SoldierArm, 0.001, {css: {background: "url('img/arm-soldier-left-top.png')"}, delay: 9.8}));
    soldier_tlm.add(TweenMax.to(contentSoldier, .1, {scaleY: 1.05, delay: .5, ease: Power0.easeNone, repeat: 1, yoyo: true}));

    btnSaveNow_tlm.add(TweenMax.to(btnSaveNow, .5, {css:{right: "47", opacity: "1"}, delay: 11, ease: Back.easeOut.config(1.7), overwrite: "false"}));

    function canonStructureMove() {
        canonStructureMove_tlm.add(TweenMax.to(canonStructure, 0.2, {css: {transform: "rotate(9deg)"}, repeat: 0, yoyo: true, delay: 1.5, ease: Power0.easeNone, overwrite: "false"}));
        contentCanon_tlm.add(TweenMax.to(contentCanon, 0.5, {left: -200, delay: 2, ease: Power0.easeNone, overwrite: "false"}));
        canonWheel_tlm.add(TweenMax.to(canonWheel, 1, {rotation: -180, delay: 1.6, ease: Power0.easeNone, overwrite: "false"}));
    }
    function bgExitHandler(e) {
        Enabler.exit('Background Exit');
    }
    // ======================CLICK TAG GENERAL=======================
      var URL    =  'http://www.google.com/'
      var a =  document.createElement('a');
      a.href  = URL;
      a.appendChild(clickTagGen);
      a.setAttribute('target', '_blank');
      document.getElementById('ad-300x250').appendChild(a);
}