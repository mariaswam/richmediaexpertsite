(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 24,
	color: "#333333",
	manifest: [
		{src:"images/boxersbg.png?1481231316155", id:"boxersbg"},
		{src:"images/kovalev.png?1481231316156", id:"kovalev"},
		{src:"images/l0_kovalev_01.png?1481231316156", id:"l0_kovalev_01"},
		{src:"images/l0_kovalev_02.png?1481231316156", id:"l0_kovalev_02"},
		{src:"images/l0_kovalev_03.png?1481231316156", id:"l0_kovalev_03"},
		{src:"images/l0_kovalev_04.png?1481231316156", id:"l0_kovalev_04"},
		{src:"images/l0_kovalev_05.png?1481231316156", id:"l0_kovalev_05"},
		{src:"images/l0_kovalev_06.png?1481231316156", id:"l0_kovalev_06"},
		{src:"images/l0_kovalev_07.png?1481231316156", id:"l0_kovalev_07"},
		{src:"images/l0_kovalev_08.png?1481231316156", id:"l0_kovalev_08"},
		{src:"images/l0_kovalev_09.png?1481231316156", id:"l0_kovalev_09"},
		{src:"images/l0_kovalev_10.png?1481231316156", id:"l0_kovalev_10"},
		{src:"images/l0_kovalev_11.png?1481231316156", id:"l0_kovalev_11"},
		{src:"images/l0_kovalev_12.png?1481231316156", id:"l0_kovalev_12"},
		{src:"images/l0_kovalev_13.png?1481231316156", id:"l0_kovalev_13"},
		{src:"images/l0_kovalev_14.png?1481231316156", id:"l0_kovalev_14"},
		{src:"images/l0_kovalev_15.png?1481231316156", id:"l0_kovalev_15"},
		{src:"images/l0_kovalev_17.png?1481231316156", id:"l0_kovalev_17"},
		{src:"images/l0_stadium_01.png?1481231316156", id:"l0_stadium_01"},
		{src:"images/l0_stadium_02.png?1481231316156", id:"l0_stadium_02"},
		{src:"images/l0_stadium_03.png?1481231316156", id:"l0_stadium_03"},
		{src:"images/l0_stadium_04.png?1481231316156", id:"l0_stadium_04"},
		{src:"images/l0_stadium_05.png?1481231316156", id:"l0_stadium_05"},
		{src:"images/l0_stadium_06.png?1481231316156", id:"l0_stadium_06"},
		{src:"images/l0_stadium_07.png?1481231316156", id:"l0_stadium_07"},
		{src:"images/l0_stadium_08.png?1481231316156", id:"l0_stadium_08"},
		{src:"images/l0_stadium_09.png?1481231316156", id:"l0_stadium_09"},
		{src:"images/l0_stadium_10.png?1481231316156", id:"l0_stadium_10"},
		{src:"images/l0_stadium_11.png?1481231316156", id:"l0_stadium_11"},
		{src:"images/l0_stadium_12.png?1481231316156", id:"l0_stadium_12"},
		{src:"images/l0_stadium_13.png?1481231316156", id:"l0_stadium_13"},
		{src:"images/l0_stadium_14.png?1481231316156", id:"l0_stadium_14"},
		{src:"images/l0_stadium_15.png?1481231316156", id:"l0_stadium_15"},
		{src:"images/l0_stadium_16.png?1481231316156", id:"l0_stadium_16"},
		{src:"images/l0_stadium_17.png?1481231316156", id:"l0_stadium_17"},
		{src:"images/l0_stadium_18.png?1481231316156", id:"l0_stadium_18"},
		{src:"images/l0_stadium_19.png?1481231316156", id:"l0_stadium_19"},
		{src:"images/l0_stadium_20.png?1481231316156", id:"l0_stadium_20"},
		{src:"images/l0_stadium_21.png?1481231316156", id:"l0_stadium_21"},
		{src:"images/l0_stadium_22.png?1481231316156", id:"l0_stadium_22"},
		{src:"images/l0_stadium_23.png?1481231316156", id:"l0_stadium_23"},
		{src:"images/l0_stadium_24.png?1481231316156", id:"l0_stadium_24"},
		{src:"images/l0_stadium_25.png?1481231316156", id:"l0_stadium_25"},
		{src:"images/l0_stadium_26.png?1481231316156", id:"l0_stadium_26"},
		{src:"images/l0_ward_02.png?1481231316156", id:"l0_ward_02"},
		{src:"images/l0_ward_03.png?1481231316156", id:"l0_ward_03"},
		{src:"images/l0_ward_04.png?1481231316156", id:"l0_ward_04"},
		{src:"images/l0_ward_05.png?1481231316156", id:"l0_ward_05"},
		{src:"images/l0_ward_06.png?1481231316156", id:"l0_ward_06"},
		{src:"images/l0_ward_07.png?1481231316156", id:"l0_ward_07"},
		{src:"images/l0_ward_08.png?1481231316156", id:"l0_ward_08"},
		{src:"images/l0_ward_09.png?1481231316156", id:"l0_ward_09"},
		{src:"images/l0_ward_10.png?1481231316156", id:"l0_ward_10"},
		{src:"images/l0_ward_11.png?1481231316156", id:"l0_ward_11"},
		{src:"images/l0_ward_12.png?1481231316156", id:"l0_ward_12"},
		{src:"images/l0_ward_13.png?1481231316156", id:"l0_ward_13"},
		{src:"images/l0_ward_14.png?1481231316156", id:"l0_ward_14"},
		{src:"images/l0_ward_15.png?1481231316156", id:"l0_ward_15"},
		{src:"images/l0_ward_16.png?1481231316156", id:"l0_ward_16"},
		{src:"images/l0_ward_17.png?1481231316156", id:"l0_ward_17"},
		{src:"images/l0_ward_18.png?1481231316156", id:"l0_ward_18"},
		{src:"images/text.png?1481231316156", id:"text"},
		{src:"images/ward.png?1481231316156", id:"ward"}
	]
};



// symbols:



(lib.boxersbg = function() {
	this.initialize(img.boxersbg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,600,250);


(lib.kovalev = function() {
	this.initialize(img.kovalev);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,187,246);


(lib.l0_kovalev_01 = function() {
	this.initialize(img.l0_kovalev_01);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_02 = function() {
	this.initialize(img.l0_kovalev_02);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_03 = function() {
	this.initialize(img.l0_kovalev_03);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_04 = function() {
	this.initialize(img.l0_kovalev_04);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_05 = function() {
	this.initialize(img.l0_kovalev_05);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_06 = function() {
	this.initialize(img.l0_kovalev_06);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_07 = function() {
	this.initialize(img.l0_kovalev_07);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_08 = function() {
	this.initialize(img.l0_kovalev_08);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_09 = function() {
	this.initialize(img.l0_kovalev_09);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_10 = function() {
	this.initialize(img.l0_kovalev_10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_11 = function() {
	this.initialize(img.l0_kovalev_11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_12 = function() {
	this.initialize(img.l0_kovalev_12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_13 = function() {
	this.initialize(img.l0_kovalev_13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_14 = function() {
	this.initialize(img.l0_kovalev_14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_15 = function() {
	this.initialize(img.l0_kovalev_15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_kovalev_17 = function() {
	this.initialize(img.l0_kovalev_17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_01 = function() {
	this.initialize(img.l0_stadium_01);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_02 = function() {
	this.initialize(img.l0_stadium_02);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_03 = function() {
	this.initialize(img.l0_stadium_03);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_04 = function() {
	this.initialize(img.l0_stadium_04);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_05 = function() {
	this.initialize(img.l0_stadium_05);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_06 = function() {
	this.initialize(img.l0_stadium_06);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_07 = function() {
	this.initialize(img.l0_stadium_07);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_08 = function() {
	this.initialize(img.l0_stadium_08);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_09 = function() {
	this.initialize(img.l0_stadium_09);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_10 = function() {
	this.initialize(img.l0_stadium_10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_11 = function() {
	this.initialize(img.l0_stadium_11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_12 = function() {
	this.initialize(img.l0_stadium_12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_13 = function() {
	this.initialize(img.l0_stadium_13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_14 = function() {
	this.initialize(img.l0_stadium_14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_15 = function() {
	this.initialize(img.l0_stadium_15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_16 = function() {
	this.initialize(img.l0_stadium_16);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_17 = function() {
	this.initialize(img.l0_stadium_17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_18 = function() {
	this.initialize(img.l0_stadium_18);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_19 = function() {
	this.initialize(img.l0_stadium_19);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_20 = function() {
	this.initialize(img.l0_stadium_20);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_21 = function() {
	this.initialize(img.l0_stadium_21);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_22 = function() {
	this.initialize(img.l0_stadium_22);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_23 = function() {
	this.initialize(img.l0_stadium_23);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_24 = function() {
	this.initialize(img.l0_stadium_24);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_25 = function() {
	this.initialize(img.l0_stadium_25);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_stadium_26 = function() {
	this.initialize(img.l0_stadium_26);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_02 = function() {
	this.initialize(img.l0_ward_02);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_03 = function() {
	this.initialize(img.l0_ward_03);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_04 = function() {
	this.initialize(img.l0_ward_04);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_05 = function() {
	this.initialize(img.l0_ward_05);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_06 = function() {
	this.initialize(img.l0_ward_06);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_07 = function() {
	this.initialize(img.l0_ward_07);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_08 = function() {
	this.initialize(img.l0_ward_08);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_09 = function() {
	this.initialize(img.l0_ward_09);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_10 = function() {
	this.initialize(img.l0_ward_10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_11 = function() {
	this.initialize(img.l0_ward_11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_12 = function() {
	this.initialize(img.l0_ward_12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_13 = function() {
	this.initialize(img.l0_ward_13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_14 = function() {
	this.initialize(img.l0_ward_14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_15 = function() {
	this.initialize(img.l0_ward_15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_16 = function() {
	this.initialize(img.l0_ward_16);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_17 = function() {
	this.initialize(img.l0_ward_17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.l0_ward_18 = function() {
	this.initialize(img.l0_ward_18);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.text = function() {
	this.initialize(img.text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,372);


(lib.ward = function() {
	this.initialize(img.ward);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,249);


(lib.yellowBG_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A4vTiMAAAgnDMAxgAAAMAAAAnDg");
	mask.setTransform(11.5,0);

	// BG
	this.instance = new lib.boxersbg();
	this.instance.setTransform(-147,-125);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147,-125,317,250);


(lib.wardSequences_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(33).call(this.frame_33).wait(1));

	// Ward
	this.instance = new lib.l0_ward_02();
	this.instance.setTransform(0.2,-123.9);

	this.instance_1 = new lib.l0_ward_03();
	this.instance_1.setTransform(0.2,-123.9);

	this.instance_2 = new lib.l0_ward_04();
	this.instance_2.setTransform(-0.8,-123.9);

	this.instance_3 = new lib.l0_ward_05();
	this.instance_3.setTransform(0,-123.9);

	this.instance_4 = new lib.l0_ward_06();
	this.instance_4.setTransform(0,-123.9);

	this.instance_5 = new lib.l0_ward_07();
	this.instance_5.setTransform(0,-123.9);

	this.instance_6 = new lib.l0_ward_08();
	this.instance_6.setTransform(0,-123.9);

	this.instance_7 = new lib.l0_ward_09();
	this.instance_7.setTransform(0,-123.9);

	this.instance_8 = new lib.l0_ward_10();
	this.instance_8.setTransform(0,-123.9);

	this.instance_9 = new lib.l0_ward_11();
	this.instance_9.setTransform(0,-123.9);

	this.instance_10 = new lib.l0_ward_12();
	this.instance_10.setTransform(0,-123.9);

	this.instance_11 = new lib.l0_ward_13();
	this.instance_11.setTransform(0,-123.9);

	this.instance_12 = new lib.l0_ward_14();
	this.instance_12.setTransform(0,-123.9);

	this.instance_13 = new lib.l0_ward_15();
	this.instance_13.setTransform(0,-123.9);

	this.instance_14 = new lib.l0_ward_16();
	this.instance_14.setTransform(0,-123.9);

	this.instance_15 = new lib.l0_ward_17();
	this.instance_15.setTransform(0,-123.9);

	this.instance_16 = new lib.l0_ward_18();
	this.instance_16.setTransform(0.2,-123.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).to({state:[{t:this.instance_16}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.2,-123.9,300,250);


(lib.Star_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAADtIkkDXIBxlaIknjTIFsABIBulbIBvFbIFsgBIknDTIBxFag");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.5,-45.2,95.1,90.5);


(lib.poundForPound_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3kD1IAAnpMAvJAAAIAAHpg");
	mask.setTransform(-3,1.5);

	// Text
	this.instance = new lib.text();
	this.instance.setTransform(-150,-94);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-23,298,49);


(lib.lastTexts_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bIWIAAwsMAu3AAAIAAQsg");
	mask.setTransform(0,3.5);

	// Texts
	this.instance = new lib.text();
	this.instance.setTransform(-150,-287);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-50,300,107);


(lib.kovalevSequences_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_32 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(32).call(this.frame_32).wait(1));

	// Kovalev sequences
	this.instance = new lib.l0_kovalev_01();
	this.instance.setTransform(36,0);

	this.instance_1 = new lib.l0_kovalev_02();
	this.instance_1.setTransform(36,0);

	this.instance_2 = new lib.l0_kovalev_03();
	this.instance_2.setTransform(36,0);

	this.instance_3 = new lib.l0_kovalev_04();
	this.instance_3.setTransform(36,0);

	this.instance_4 = new lib.l0_kovalev_05();
	this.instance_4.setTransform(36,0);

	this.instance_5 = new lib.l0_kovalev_06();
	this.instance_5.setTransform(36,0);

	this.instance_6 = new lib.l0_kovalev_07();
	this.instance_6.setTransform(36,0);

	this.instance_7 = new lib.l0_kovalev_08();
	this.instance_7.setTransform(36,0);

	this.instance_8 = new lib.l0_kovalev_09();
	this.instance_8.setTransform(36,0);

	this.instance_9 = new lib.l0_kovalev_10();
	this.instance_9.setTransform(36,0);

	this.instance_10 = new lib.l0_kovalev_11();
	this.instance_10.setTransform(36,0);

	this.instance_11 = new lib.l0_kovalev_12();
	this.instance_11.setTransform(36,0);

	this.instance_12 = new lib.l0_kovalev_13();
	this.instance_12.setTransform(36,0);

	this.instance_13 = new lib.l0_kovalev_14();
	this.instance_13.setTransform(36,0);

	this.instance_14 = new lib.l0_kovalev_15();
	this.instance_14.setTransform(36,0);

	this.instance_15 = new lib.l0_kovalev_17();
	this.instance_15.setTransform(36,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_5}]},2).to({state:[{t:this.instance_6}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_8}]},2).to({state:[{t:this.instance_9}]},2).to({state:[{t:this.instance_10}]},2).to({state:[{t:this.instance_11}]},2).to({state:[{t:this.instance_12}]},2).to({state:[{t:this.instance_13}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_14}]},2).to({state:[{t:this.instance_15}]},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36,0,300,250);


(lib.Green_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A+GUqMAAAgpTMA8NAAAMAAAApTg");
	mask.setTransform(-4.2,-1.7);

	// Layer 1
	this.instance = new lib.boxersbg();
	this.instance.setTransform(-510,-125);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-196.9,-125,287,250);


(lib.gradienBlack_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(0,0,0,0.929)","rgba(0,0,0,0.902)","rgba(0,0,0,0.898)","rgba(0,0,0,0.8)","rgba(0,0,0,0.698)","rgba(0,0,0,0.6)","rgba(0,0,0,0)"],[0.247,0.408,0.502,0.616,0.722,0.82,1],0,125,0,-124.9).s().p("A3bThMAAAgnBMAu3AAAMAAAAnBg");
	this.shape.setTransform(0,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,0,300,250);


(lib.boxerWard_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ward();
	this.instance.setTransform(-108.5,-124.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.5,-124.5,217,249);


(lib.boxersName_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3lFZIAAqxMAvLAAAIAAKxg");
	mask.setTransform(1,1.5);

	// Texts
	this.instance = new lib.text();
	this.instance.setTransform(-150,-33);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-33,300,69.1);


(lib.boxerKovalev_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kovalev();
	this.instance.setTransform(-93.5,-123);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-93.5,-123,187,246);


(lib.bg_stadium_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{children:0});

	// timeline functions:
	this.frame_26 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(26).call(this.frame_26).wait(1));

	// Sequences
	this.instance = new lib.l0_stadium_01();

	this.instance_1 = new lib.l0_stadium_02();

	this.instance_2 = new lib.l0_stadium_03();

	this.instance_3 = new lib.l0_stadium_04();

	this.instance_4 = new lib.l0_stadium_05();

	this.instance_5 = new lib.l0_stadium_06();

	this.instance_6 = new lib.l0_stadium_07();

	this.instance_7 = new lib.l0_stadium_08();

	this.instance_8 = new lib.l0_stadium_09();

	this.instance_9 = new lib.l0_stadium_10();

	this.instance_10 = new lib.l0_stadium_11();

	this.instance_11 = new lib.l0_stadium_12();

	this.instance_12 = new lib.l0_stadium_13();

	this.instance_13 = new lib.l0_stadium_14();

	this.instance_14 = new lib.l0_stadium_15();

	this.instance_15 = new lib.l0_stadium_16();

	this.instance_16 = new lib.l0_stadium_17();

	this.instance_17 = new lib.l0_stadium_18();

	this.instance_18 = new lib.l0_stadium_19();

	this.instance_19 = new lib.l0_stadium_20();

	this.instance_20 = new lib.l0_stadium_21();

	this.instance_21 = new lib.l0_stadium_22();

	this.instance_22 = new lib.l0_stadium_23();

	this.instance_23 = new lib.l0_stadium_24();

	this.instance_24 = new lib.l0_stadium_25();

	this.instance_25 = new lib.l0_stadium_26();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).to({state:[{t:this.instance_25}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		bgStadium        = this.stadiumSequences_mc,
		kovalevSequences = this.kovalevSequences_mc,
		wardSequences    = this.wardSequences_mc,
		boxerkovalev     = this.boxerkovalev_mc,
		boxerWard        = this.boxerWard_mc,
		greenBG			 = this.greenBG_mc,
		yellowBG         = this.yellowBG_mc,
		boxersNames      = this.boxersNames_mc,
		pound            = this.pound_mc,
		lastTexts        = this.lastTexts_mc;
		gradient         = this.gradient_mc;
		
		/***[ FIRST FRAME ]**/
		tl.from(bgStadium, 0.5, {alpha: 0, ease:Power0.easeNone, onStart:function(){bgStadium.gotoAndPlay(1)}});
		tl.from(yellowBG, 0.3, {alpha: 0, x:"-=100", ease:Power1.easeOut});
		tl.from(greenBG, 0.3, {alpha: 0, x:"+=100", ease:Power1.easeOut}, "-=0.3");
		tl.from(boxersNames, 0.3, {alpha: 0, y:"+=50", ease:Power1.easeOut}, "-=0.2");
		/***[ KOVALEV ANIMATION ]**/
		tl.to([yellowBG, greenBG, boxersNames], 0.3, {x:"+=138", ease:Power1.easeOut}, "+=1");
		tl.from(kovalevSequences, 0.5, {alpha: 0, ease:Power0.easeNone, onStart:function(){kovalevSequences.gotoAndPlay(1)}}, "-=0.3");
		tl.to([kovalevSequences, bgStadium], 0.5, {alpha: 0, ease:Power0.easeNone});
		/***[ WARD ANIMATION ]***/
		tl.to([yellowBG, greenBG, boxersNames], 0.3, {x:"-=317", ease:Power1.easeOut});
		tl.from(wardSequences, 0.5, {alpha: 0, ease:Power0.easeNone, onStart:function(){wardSequences.gotoAndPlay(1)}}, "-=0.3");
		tl.to(wardSequences, 0.5, {alpha: 0, ease:Power0.easeNone});
		tl.to([yellowBG, greenBG, boxersNames], 0.3, {y:"+=100", ease:Power1.easeOut});
		/***[ POUND FOR POUD ]***/
		tl.from(boxerkovalev, 0.3, {alpha: 0, x:"-=100", ease:Power1.easeOut}, "-=0.1");
		tl.from(boxerWard, 0.3, {alpha: 0, x:"+=100", ease:Power1.easeOut}, "-=0.2");
		tl.to(boxersNames, 0.3, {y:"+=100", ease:Power1.easeOut}, "-=0.3");
		tl.from(pound, 0.3, {alpha: 0, y:"+=100", ease:Power1.easeOut}, "-=0.3");
		tl.from(gradient, 0.3, {alpha: 0, y:"+=100", ease:Power1.easeOut}, "-=0.3");
		/***[ last frame ]***/
		tl.to(boxersNames, 0, {alpha: 0, x:"+=179", y:"-=210", ease:Power1.easeOut});
		tl.to(pound, 0.3, {alpha: 0, y:"-=10", ease:Power1.easeOut}, "+=1");
		tl.from(lastTexts, 0.3, {alpha: 0, y:"-=20", ease:Power1.easeOut}, "-=0.3");
		tl.to(boxersNames, 0.3, {alpha: 1, y:"-=80", ease:Power1.easeOut}, "-=0.3");
		tl.to([boxerkovalev, boxerWard], 0.3, {scaleX: 1.2, scaleY: 1.2, ease:Power1.easeOut}, "-=0.3");
		tl.to(gradient, 0.3, { y:"-=100", ease:Power1.easeOut}, "-=0.3");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Star-left
	this.starLeft_mc = new lib.Star_mc();
	this.starLeft_mc.setTransform(11.4,124.4,0.149,0.149);

	this.timeline.addTween(cjs.Tween.get(this.starLeft_mc).wait(1));

	// Star-right
	this.starRight_mc = new lib.Star_mc();
	this.starRight_mc.setTransform(288.2,124.8,0.149,0.149);

	this.timeline.addTween(cjs.Tween.get(this.starRight_mc).wait(1));

	// Last texts
	this.lastTexts_mc = new lib.lastTexts_mc();
	this.lastTexts_mc.setTransform(150,191);

	this.timeline.addTween(cjs.Tween.get(this.lastTexts_mc).wait(1));

	// Pound
	this.pound_mc = new lib.poundForPound_mc();
	this.pound_mc.setTransform(153,208);

	this.timeline.addTween(cjs.Tween.get(this.pound_mc).wait(1));

	// Boxers Name
	this.boxersNames_mc = new lib.boxersName_mc();
	this.boxersNames_mc.setTransform(150,206.9);

	this.timeline.addTween(cjs.Tween.get(this.boxersNames_mc).wait(1));

	// Black Gradient
	this.gradient_mc = new lib.gradienBlack_mc();
	this.gradient_mc.setTransform(150,285,1,1,0,0,0,0,125);

	this.timeline.addTween(cjs.Tween.get(this.gradient_mc).wait(1));

	// Ward
	this.boxerWard_mc = new lib.boxerWard_mc();
	this.boxerWard_mc.setTransform(192,126);

	this.timeline.addTween(cjs.Tween.get(this.boxerWard_mc).wait(1));

	// Kovalev
	this.boxerkovalev_mc = new lib.boxerKovalev_mc();
	this.boxerkovalev_mc.setTransform(94,125);

	this.timeline.addTween(cjs.Tween.get(this.boxerkovalev_mc).wait(1));

	// Yellow
	this.yellowBG_mc = new lib.yellowBG_mc();
	this.yellowBG_mc.setTransform(103,244);

	this.timeline.addTween(cjs.Tween.get(this.yellowBG_mc).wait(1));

	// Green
	this.greenBG_mc = new lib.Green_mc();
	this.greenBG_mc.setTransform(223,280);

	this.timeline.addTween(cjs.Tween.get(this.greenBG_mc).wait(1));

	// Ward Sequences
	this.wardSequences_mc = new lib.wardSequences_mc();
	this.wardSequences_mc.setTransform(1800,124,1,1,0,0,0,1800,0);

	this.timeline.addTween(cjs.Tween.get(this.wardSequences_mc).wait(1));

	// Kovalev Sequences
	this.kovalevSequences_mc = new lib.kovalevSequences_mc();
	this.kovalevSequences_mc.setTransform(1764,0,1,1,0,0,0,1800,0);

	this.timeline.addTween(cjs.Tween.get(this.kovalevSequences_mc).wait(1));

	// BG Stadium Sequences
	this.stadiumSequences_mc = new lib.bg_stadium_mc();
	this.stadiumSequences_mc.setTransform(3600,0,1,1,0,0,0,3600,0);

	this.timeline.addTween(cjs.Tween.get(this.stadiumSequences_mc).wait(1));

	// BG
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-287,-96,843.1,641.9);


// stage content:
(lib._300x250HBO = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	mask.setTransform(150,125);

	// Content
	this.instance = new lib.content_mc();
	this.instance.setTransform(150,125,1,1,0,0,0,150,125);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,125,300,250);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;