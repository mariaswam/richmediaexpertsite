(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 60,
	color: "#FFFFFF",
	manifest: [
		{src:"images/cta.png?1489018345585", id:"cta"},
		{src:"images/logo.png?1489018345585", id:"logo"},
		{src:"images/shirt_1.jpg?1489018345585", id:"shirt_1"},
		{src:"images/shirt_2.jpg?1489018345585", id:"shirt_2"},
		{src:"images/shirt_3.jpg?1489018345585", id:"shirt_3"},
		{src:"images/sprite.png?1489018345585", id:"sprite"}
	]
};



// symbols:



(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,274,77);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,506,246);


(lib.shirt_1 = function() {
	this.initialize(img.shirt_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.shirt_2 = function() {
	this.initialize(img.shirt_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.shirt_3 = function() {
	this.initialize(img.shirt_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.sprite = function() {
	this.initialize(img.sprite);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,304,262);


(lib.red_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt_3();
	this.instance.setTransform(-150,-125);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-125,300,250);


(lib.orange_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt_1();
	this.instance.setTransform(-150,-125);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-125,300,250);


(lib.cta_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-69.9,-21,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.9,-21,137,38.5);


(lib.copy6_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.logo();
	this.instance.setTransform(0,2,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,2,253,123);


(lib.copy4_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A2VLLIAA2VIN2AAIAADYIB9CqIAUAAIAAAKIVJAAIAAgPIFPg/ICLRXg");
	mask.setTransform(15.5,32);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-130,-152.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-127.5,-39.5,286,143);


(lib.copy3_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApILkIhglzIVRAAIAAFzg");
	mask.setTransform(19.8,74);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-140,-3.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48.4,111,136.4,37.1);


(lib.copy2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3GitIAAs5MAuNAAAIAAM5g");
	mask.setTransform(0.5,-100);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-142,-224.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142,-200,290.5,82.5);


(lib.copy1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A2VCJIAAkRMAsrAAAIAAERg");
	mask.setTransform(-4.5,-141.2);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-142,-152);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142,-152,280.5,24.5);


(lib.blue_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt_2();
	this.instance.setTransform(-150,-125);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-125,300,250);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		cta       = this.cta_mc;
		
		
		/***[ FIRST FRAME ]**/
		
		
		tl.from(cta, 0.5, {scaleX: 2.5, scaleY: 2.5, y:"+=5",alpha: 0, ease:Power0.easeNone}, "+=8.5");
		tl.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power0.easeNone}, "+=1");
		tl.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power0.easeNone, onComplete: hovercta});
		
		function hovercta () {
		var myBtn = document.getElementById("canvas");
				
			/***[ MOUSE OVER ]***/
			myBtn.addEventListener("mouseover", function(){
				TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
			});
				
		//	/***[ MOUSE OUT ]***/
			myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
		});
		}
	}
	this.frame_80 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_177 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_241 = function() {
		this.stop();
		//var $this = this;
		
		//setTimeout(function(){
			//$this.play();
		//}, 2000);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(80).call(this.frame_80).wait(97).call(this.frame_177).wait(64).call(this.frame_241).wait(31));

	// CTA
	this.cta_mc = new lib.cta_mc();
	this.cta_mc.setTransform(151,227.3);

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(242).to({scaleX:2.33,scaleY:2.33,y:257.3},0).to({scaleX:1,scaleY:1,y:227.3},29,cjs.Ease.get(1)).wait(1));

	// copy6
	this.instance = new lib.copy6_mc();
	this.instance.setTransform(152.5,239.5,2.2,2.2,0,0,0,126.5,61.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(211).to({_off:false},0).to({scaleX:1,scaleY:1,y:139.5},30,cjs.Ease.get(1)).wait(31));

	// red
	this.orange_mc = new lib.red_mc();
	this.orange_mc.setTransform(150,125);
	this.orange_mc.alpha = 0;
	this.orange_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.orange_mc).wait(178).to({_off:false},0).to({alpha:1},33,cjs.Ease.get(1)).wait(61));

	// copy4
	this.instance_1 = new lib.copy4_mc();
	this.instance_1.setTransform(155.2,225.9,2.509,2.509,0,0,0,16.2,33);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(145).to({_off:false},0).to({scaleX:1,scaleY:1,y:166},30,cjs.Ease.get(1)).wait(3).to({alpha:0},33,cjs.Ease.get(1)).wait(61));

	// copy3
	this.instance_2 = new lib.copy3_mc();
	this.instance_2.setTransform(158.8,112.2,2.79,2.79,0,0,0,19.8,129.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(115).to({_off:false},0).to({scaleX:1,scaleY:1,x:168.8,y:112.1},30,cjs.Ease.get(1)).wait(33).to({alpha:0},33,cjs.Ease.get(1)).wait(61));

	// Blue
	this.orange_mc_1 = new lib.blue_mc();
	this.orange_mc_1.setTransform(150,125);
	this.orange_mc_1.alpha = 0;
	this.orange_mc_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.orange_mc_1).wait(82).to({_off:false},0).to({alpha:1},33,cjs.Ease.get(1)).wait(63).to({alpha:0},33,cjs.Ease.get(1)).wait(61));

	// copy2
	this.instance_3 = new lib.copy2_mc();
	this.instance_3.setTransform(161.3,182.1,2.2,2.2,0,0,0,3.2,-158.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(50).to({_off:false},0).to({scaleX:1,scaleY:1,x:152.2,y:162.7},30,cjs.Ease.get(1)).wait(2).to({regX:-4.5,regY:-175,x:144.5,y:146.5},0).to({alpha:0},33,cjs.Ease.get(1)).wait(157));

	// copy1
	this.instance_4 = new lib.copy1_mc();
	this.instance_4.setTransform(153.2,109.6,2.2,2.2,0,0,0,-1.8,-139.8);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20).to({_off:false},0).to({scaleX:1,scaleY:1,x:147.2,y:110.7},30,cjs.Ease.get(1)).wait(32).to({regX:-6.8,regY:-80.5,x:142.2,y:170},0).to({alpha:0},33,cjs.Ease.get(1)).to({_off:true},7).wait(150));

	// orange
	this.orange_mc_2 = new lib.orange_mc();
	this.orange_mc_2.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.orange_mc_2).wait(82).to({alpha:0},33,cjs.Ease.get(1)).to({_off:true},7).wait(150));

	// BG
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(271));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


// stage content:



(lib.shirt_300x250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	mask.setTransform(150,125);

	// Content
	this.content_mc = new lib.content_mc();
	this.content_mc.setTransform(150,125,1,1,0,0,0,150,125);

	this.content_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.content_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,125,300,250);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;