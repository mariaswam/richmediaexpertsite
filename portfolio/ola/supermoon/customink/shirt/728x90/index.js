(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 728,
	height: 90,
	fps: 60,
	color: "#FFFFFF",
	manifest: [
		{src:"images/cta.png?1488930246799", id:"cta"},
		{src:"images/logo.png?1488930246799", id:"logo"},
		{src:"images/shirt1.png?1488930246799", id:"shirt1"},
		{src:"images/shirt2.png?1488930246799", id:"shirt2"},
		{src:"images/shirt3.png?1488930246799", id:"shirt3"},
		{src:"images/shirt4.png?1488930246799", id:"shirt4"},
		{src:"images/shirt5.png?1488930246799", id:"shirt5"},
		{src:"images/shirt6.png?1488930246799", id:"shirt6"},
		{src:"images/shirt7.png?1488930246799", id:"shirt7"}
	]
};



// symbols:



(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,34);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,202,292);


(lib.shirt1 = function() {
	this.initialize(img.shirt1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,211,90);


(lib.shirt2 = function() {
	this.initialize(img.shirt2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,209,90);


(lib.shirt3 = function() {
	this.initialize(img.shirt3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,221,90);


(lib.shirt4 = function() {
	this.initialize(img.shirt4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,209,90);


(lib.shirt5 = function() {
	this.initialize(img.shirt5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,158,90);


(lib.shirt6 = function() {
	this.initialize(img.shirt6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,158,90);


(lib.shirt7 = function() {
	this.initialize(img.shirt7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,90);


(lib.shirt7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt7();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,157,90);


(lib.shirt6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt6();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,158,90);


(lib.shirt5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt5();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,158,90);


(lib.shirt4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt4();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,209,90);


(lib.shirt3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt3();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,221,90);


(lib.shirt2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,209,90);


(lib.shirt1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shirt1();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,211,90);


(lib.logo_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_8 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(8).call(this.frame_8).wait(12));

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AonGBIAAsBIRPAAIAAMBg");
	var mask_graphics_1 = new cjs.Graphics().p("AonGBIAAsBIRPAAIAAMBg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:102.8,y:182.5}).wait(1).to({graphics:mask_graphics_1,x:102.8,y:182.5}).wait(19));

	// Layer 1
	this.instance = new lib.logo();
	this.instance.setTransform(50.5,73,0.5,0.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:148},0).wait(7).to({_off:true},1).wait(11));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AsGHAIAAuCIXbAAIAxOCIsGACIlSABQmzAAgBgDg");
	this.shape.setTransform(102.7,181);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off:false},0).to({_off:true},8).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(50.5,144,101,75);


(lib.btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-58.5,-17);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-58.5,-17,117,34);


(lib.back3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4F7EBC").s().p("Eg43AHCIAAuDMBxvAAAIAAODg");
	this.shape.setTransform(364,45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,728,90);


(lib.back2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F28035").s().p("Eg43AHCIAAuDMBxvAAAIAAODg");
	this.shape.setTransform(364,45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,728,90);


(lib.back1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EC312F").s().p("Eg43AHCIAAuDMBxvAAAIAAODg");
	this.shape.setTransform(364,45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,728,90);


// stage content:
(lib._728x90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		cta       = this.cta_mc;
		logo      = this.logo_mc;
		
		
		
		/***[ FIRST FRAME ]**/
		
		
		
		tl.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power0.easeNone}, "+=8");
		tl.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power0.easeNone, onComplete: hovercta});
		
		function hovercta () {
		var myBtn = document.getElementById("canvas");
				
			/***[ MOUSE OVER ]***/
			myBtn.addEventListener("mouseover", function(){
				TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
				//logo.gotoAndPlay(2);
			});
				
		//	/***[ MOUSE OUT ]***/
			myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
			//logo.gotoAndStop(1);
		});
		}
	}
	this.frame_58 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_115 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_200 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(58).call(this.frame_58).wait(57).call(this.frame_115).wait(85).call(this.frame_200).wait(35));

	// logo
	this.logo_mc = new lib.logo_1();
	this.logo_mc.setTransform(76,10,1,1,0,0,0,101,146);

	this.timeline.addTween(cjs.Tween.get(this.logo_mc).wait(235));

	// cta
	this.cta_mc = new lib.btn();
	this.cta_mc.setTransform(662.5,-42);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(186).to({_off:false},0).to({y:48},14,cjs.Ease.get(1)).wait(14).to({mode:"synched",startPosition:0},0).wait(13).to({startPosition:0},0).wait(8));

	// shirt5
	this.instance = new lib.shirt5_1();
	this.instance.setTransform(229,-45,1,1,0,0,0,79,45);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(130).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(91));

	// shirt6
	this.instance_1 = new lib.shirt6_1();
	this.instance_1.setTransform(376,-45,1,1,0,0,0,79,45);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(144).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(77));

	// shirt7
	this.instance_2 = new lib.shirt7_1();
	this.instance_2.setTransform(519.5,-45,1,1,0,0,0,78.5,45);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(158).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(63));

	// back3
	this.instance_3 = new lib.back3();
	this.instance_3.setTransform(635,-45,0.255,1,0,0,0,364,45);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(172).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(49));

	// back3
	this.instance_4 = new lib.back3();
	this.instance_4.setTransform(93,-45,0.255,1,0,0,0,364,45);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(116).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(105));

	// Layer 11
	this.instance_5 = new lib.shirt3_1();
	this.instance_5.setTransform(290.5,-45,1,1,0,0,0,110.5,45);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(73).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(148));

	// Layer 12
	this.instance_6 = new lib.shirt4_1();
	this.instance_6.setTransform(489.5,-45,1,1,0,0,0,104.5,45);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(87).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(134));

	// back2
	this.instance_7 = new lib.back2();
	this.instance_7.setTransform(635.2,-45,0.255,1,0,0,0,363.9,45);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(101).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(120));

	// back2
	this.instance_8 = new lib.back2();
	this.instance_8.setTransform(92.8,-45,0.255,1,0,0,0,363.9,45);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(59).to({_off:false},0).to({x:93.2,y:45},14,cjs.Ease.get(1)).wait(162));

	// Layer 16
	this.instance_9 = new lib.shirt2_1();
	this.instance_9.setTransform(493.5,-45,1,1,0,0,0,104.5,45);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(44).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(177));

	// Layer 17
	this.instance_10 = new lib.shirt1_1();
	this.instance_10.setTransform(285.5,-45,1,1,0,0,0,105.5,45);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(30).to({_off:false},0).to({y:45},14,cjs.Ease.get(1)).wait(191));

	// back1
	this.instance_11 = new lib.back1();
	this.instance_11.setTransform(364,45,1,1,0,0,0,364,45);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(235));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(364,-18,728,153);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;