(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 600,
	fps: 60,
	color: "#FFFFFF",
	manifest: [
		{src:"images/cta.png?1489101605801", id:"cta"},
		{src:"images/imgs.jpg?1489101605801", id:"imgs"},
		{src:"images/sprite.png?1489101605801", id:"sprite"}
	]
};



// symbols:



(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,175,48);


(lib.imgs = function() {
	this.initialize(img.imgs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,900,600);


(lib.sprite = function() {
	this.initialize(img.sprite);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,250,498);


(lib.red_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("An/GSIAAsjIP/AAIAAMjg");
	mask.setTransform(151.3,40.3);

	// Layer 5
	this.instance = new lib.sprite();
	this.instance.setTransform(105.1,4.6);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask_1.setTransform(150,300);

	// Layer 4
	this.instance_1 = new lib.imgs();
	this.instance_1.setTransform(-600,0);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.orange_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("An/GSIAAsjIP/AAIAAMjg");
	mask.setTransform(151.3,40.3);

	// Layer 2
	this.instance = new lib.sprite();
	this.instance.setTransform(105.1,4.6);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask_1.setTransform(150,300);

	// Layer 5
	this.instance_1 = new lib.imgs();

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.cta_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-87.9,-24.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-87.9,-24.1,175,48);


(lib.copy5_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A1jVQIAUzsIIcjbICMzYMAgLAAAMAAAAqfg");
	mask.setTransform(136.1,206.1);

	// Layer 3
	this.instance = new lib.sprite();
	this.instance.setTransform(-55.7,95.9,1.2,1.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,95.9,246.3,246.3);


(lib.copy4_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A2aKyIAA1EILugfIEeDaIAABdIXfAAIFKkYIAAVEg");
	mask.setTransform(16.5,166.1);

	// Layer 4
	this.instance = new lib.sprite();
	this.instance.setTransform(-134.7,-185.6,1.2,1.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-127,97,287.1,138.1);


(lib.copy3_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aq6DSIAAiHIiNAAIAAkcIaPAAIAAGjg");
	mask.setTransform(39,256.6);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-144.2,-36.6,1.2,1.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45,235.6,168.1,42);


(lib.copy2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A1lELIAAtpMArLAAAIAANpg");
	mask.setTransform(10.2,-60.8);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-138,-604.8,1.2,1.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128,-121.5,276.4,87.6);


(lib.copy1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A2jDtIAAnZMAtHAAAIAAHZg");
	mask.setTransform(-0.5,-24.3);

	// Layer 1
	this.instance = new lib.sprite();
	this.instance.setTransform(-145,-482.8,1.2,1.2);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-145,-48,289,47.5);


(lib.blue_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("An/GSIAAsjIP/AAIAAMjg");
	mask.setTransform(151.3,40.3);

	// Layer 3
	this.instance = new lib.sprite();
	this.instance.setTransform(105.1,4.6);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask_1.setTransform(150,300);

	// Layer 6
	this.instance_1 = new lib.imgs();
	this.instance_1.setTransform(-300,0);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		cta       = this.cta_mc;
		
		
		/***[ FIRST FRAME ]**/
		
		
		tl.from(cta, 0.5, {y:"+=20",scaleX: 2.5, scaleY: 2.5, y:"+=5",alpha: 0, ease:Power0.easeNone}, "+=8.5");
		
		tl.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power0.easeNone}, "+=1");
		tl.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power0.easeNone, onComplete: hovercta});
		
		function hovercta () {
		var myBtn = document.getElementById("canvas");
				
			/***[ MOUSE OVER ]***/
			myBtn.addEventListener("mouseover", function(){
				TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
			});
				
		//	/***[ MOUSE OUT ]***/
			myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
		});
		}
	}
	this.frame_80 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_177 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_241 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(80).call(this.frame_80).wait(97).call(this.frame_177).wait(64).call(this.frame_241).wait(54));

	// CTA
	this.cta_mc = new lib.cta_mc();
	this.cta_mc.setTransform(147.9,498.9);

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(295));

	// copy5
	this.instance = new lib.copy5_mc();
	this.instance.setTransform(152.5,-70.5,2.2,2.2,0,0,0,126.5,61.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(211).to({_off:false},0).to({scaleX:1,scaleY:1,y:139.5},30,cjs.Ease.get(1)).wait(54));

	// red
	this.orange_mc = new lib.red_mc();
	this.orange_mc.alpha = 0;
	this.orange_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.orange_mc).wait(178).to({_off:false},0).to({alpha:1},33,cjs.Ease.get(1)).wait(84));

	// copy4
	this.instance_1 = new lib.copy4_mc();
	this.instance_1.setTransform(145.4,176,2.029,2.029,0,0,0,16.3,33);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(145).to({_off:false},0).to({regX:16.2,scaleX:1,scaleY:1,x:155.2,y:166},30,cjs.Ease.get(1)).wait(3).to({alpha:0},33,cjs.Ease.get(-1)).wait(84));

	// copy3
	this.instance_2 = new lib.copy3_mc();
	this.instance_2.setTransform(129,-196.1,2.79,2.79,0,0,0,19.8,129.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(115).to({_off:false},0).to({scaleX:1,scaleY:1,x:168.8,y:112.1},30,cjs.Ease.get(1)).wait(33).to({alpha:0},33,cjs.Ease.get(-1)).wait(84));

	// Blue
	this.orange_mc_1 = new lib.blue_mc();
	this.orange_mc_1.alpha = 0;
	this.orange_mc_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.orange_mc_1).wait(82).to({_off:false},0).to({alpha:1},33,cjs.Ease.get(1)).wait(63).to({alpha:0},33,cjs.Ease.get(-1)).wait(84));

	// copy2
	this.instance_3 = new lib.copy2_mc();
	this.instance_3.setTransform(140.7,221.2,2.2,2.2,0,0,0,3.2,-158.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(50).to({_off:false},0).to({scaleX:1,scaleY:1,x:149.8,y:226},30,cjs.Ease.get(1)).wait(2).to({alpha:0},33,cjs.Ease.get(-1)).wait(180));

	// copy1
	this.instance_4 = new lib.copy1_mc();
	this.instance_4.setTransform(1.2,65.6,2.2,2.2,0,0,0,-70.9,-75.7);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20).to({_off:false},0).to({regX:-71.1,regY:-75.8,scaleX:1,scaleY:1,x:81.9,y:186.7},30,cjs.Ease.get(1)).wait(32).to({alpha:0},33,cjs.Ease.get(-1)).to({_off:true},7).wait(173));

	// orange
	this.orange_mc_2 = new lib.orange_mc();

	this.timeline.addTween(cjs.Tween.get(this.orange_mc_2).wait(82).to({alpha:0},33,cjs.Ease.get(-1)).to({_off:true},7).wait(173));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,900,600);


// stage content:



(lib.shirt_300x600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask.setTransform(150,300);

	// Content
	this.content_mc = new lib.content_mc();
	this.content_mc.setTransform(150,125,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.content_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,300,900,600);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;