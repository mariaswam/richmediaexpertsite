(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 600,
	fps: 70,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bg.jpg?1489186331767", id:"bg"},
		{src:"images/cta.png?1489186331767", id:"cta"},
		{src:"images/hands.png?1489186331767", id:"hands"},
		{src:"images/sprite.png?1489186331767", id:"sprite"}
	]
};



// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,170,55);


(lib.hands = function() {
	this.initialize(img.hands);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,600);


(lib.sprite = function() {
	this.initialize(img.sprite);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,278,600);


(lib.Hands_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.hands();
	this.instance.setTransform(-166,-564,1.8,1.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-166,-564,468,1080);


(lib.cta_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-98.8,-26.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-98.8,-26.4,170,55);


(lib.copy_2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsUIIIAAwPIYpAAIAAQPg");
	mask.setTransform(-2.5,-0.4);

	// Layer 4
	this.instance = new lib.sprite();
	this.instance.setTransform(-82.5,-52.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A1ESpIAA6GMAqJAAAIAAaGg");
	mask_1.setTransform(1.5,119.3);

	// Layer 6
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-141,-218);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-133.4,-52.5,270,291.2);


(lib.copy_1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsUIIIAAwPIYpAAIAAQPg");
	mask.setTransform(0.5,19);

	// Layer 2
	this.instance = new lib.sprite();
	this.instance.setTransform(-81,-35);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Aw2UuIAA7pMAhtAAAIAAbpg");
	mask_1.setTransform(0.1,132.7);

	// Layer 1
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-110,-24);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-107.9,-33,216,298.4);


(lib.copu_3_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsUIIIAAwPIYpAAIAAQPg");
	mask.setTransform(-0.5,0.6);

	// Layer 3
	this.instance = new lib.sprite();
	this.instance.setTransform(-81,-53);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AxcPHIAAzgMAi5AAAIAATgg");
	mask_1.setTransform(1.2,96.8);

	// Layer 5
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-118,-388);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-110.5,-51.5,223.5,245.1);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//this.stop();
		
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		hands        = this.hands_mc;
		hands3        = this.hands3_mc;
		cta          = this.cta_mc;
	}
	this.frame_136 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2500);
	}
	this.frame_275 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2500);
	}
	this.frame_413 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 1000);
	}
	this.frame_474 = function() {
		this.stop();
		
		var myBtn = document.getElementById("canvas");
			
		/***[ MOUSE OVER ]***/
		myBtn.addEventListener("mouseover", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
		});
			
		/***[ MOUSE OUT ]***/
		myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(136).call(this.frame_136).wait(139).call(this.frame_275).wait(138).call(this.frame_413).wait(61).call(this.frame_474).wait(1));

	// Hands_3
	this.instance = new lib.Hands_1();
	this.instance.setTransform(0,-526,2.217,2.217);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(276).to({_off:false},0).to({scaleX:1,scaleY:1,x:80,y:190},59,cjs.Ease.get(1)).to({y:-324},78,cjs.Ease.get(-1)).wait(62));

	// cta
	this.cta_mc = new lib.cta_mc();
	this.cta_mc.setTransform(162.5,516.8);
	this.cta_mc.alpha = 0;
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(414).to({_off:false},0).to({y:466.8,alpha:1},35,cjs.Ease.get(1)).to({scaleX:1.15,scaleY:1.15},12,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},13,cjs.Ease.get(-1)).wait(1));

	// Mask --- f3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_335 = new cjs.Graphics().p("EgXKAhMIAAg2MAu2AAAIAAA2g");
	var mask_graphics_336 = new cjs.Graphics().p("EgXKAhMIAAg3MAu1AAAIAAA3g");
	var mask_graphics_337 = new cjs.Graphics().p("EgXKAhMIAAg5MAu2AAAIAAA5g");
	var mask_graphics_338 = new cjs.Graphics().p("EgXKAhMIAAg+MAu2AAAIAAA+g");
	var mask_graphics_339 = new cjs.Graphics().p("EgXKAhMIAAhDMAu2AAAIAABDg");
	var mask_graphics_340 = new cjs.Graphics().p("EgXKAhMIAAhLMAu2AAAIAABLg");
	var mask_graphics_341 = new cjs.Graphics().p("EgXKAhMIAAhUMAu2AAAIAABUg");
	var mask_graphics_342 = new cjs.Graphics().p("EgXKAhMIAAhfMAu2AAAIAABfg");
	var mask_graphics_343 = new cjs.Graphics().p("EgXKAhLIAAhrMAu2AAAIAABrg");
	var mask_graphics_344 = new cjs.Graphics().p("EgXKAhLIAAh5MAu2AAAIAAB5g");
	var mask_graphics_345 = new cjs.Graphics().p("EgXKAhLIAAiJMAu2AAAIAACJg");
	var mask_graphics_346 = new cjs.Graphics().p("EgXKAhLIAAiaMAu2AAAIAACag");
	var mask_graphics_347 = new cjs.Graphics().p("EgXKAhLIAAitMAu2AAAIAACtg");
	var mask_graphics_348 = new cjs.Graphics().p("EgXKAhLIAAjCMAu2AAAIAADCg");
	var mask_graphics_349 = new cjs.Graphics().p("EgXKAhKIAAjYMAu2AAAIAADYg");
	var mask_graphics_350 = new cjs.Graphics().p("EgXKAhKIAAjwMAu2AAAIAADwg");
	var mask_graphics_351 = new cjs.Graphics().p("EgXKAhKIAAkKMAu2AAAIAAEKg");
	var mask_graphics_352 = new cjs.Graphics().p("EgXKAhJIAAkkMAu2AAAIAAEkg");
	var mask_graphics_353 = new cjs.Graphics().p("EgXKAhJIAAlCMAu2AAAIAAFCg");
	var mask_graphics_354 = new cjs.Graphics().p("EgXKAhJIAAlhMAu2AAAIAAFhg");
	var mask_graphics_355 = new cjs.Graphics().p("EgXKAhJIAAmBMAu2AAAIAAGBg");
	var mask_graphics_356 = new cjs.Graphics().p("EgXKAhIIAAmiMAu2AAAIAAGig");
	var mask_graphics_357 = new cjs.Graphics().p("EgXKAhIIAAnGMAu2AAAIAAHGg");
	var mask_graphics_358 = new cjs.Graphics().p("EgXKAhHIAAnrMAu2AAAIAAHrg");
	var mask_graphics_359 = new cjs.Graphics().p("EgXKAhHIAAoSMAu2AAAIAAISg");
	var mask_graphics_360 = new cjs.Graphics().p("EgXKAhHIAAo7MAu2AAAIAAI7g");
	var mask_graphics_361 = new cjs.Graphics().p("EgXKAhGIAAplMAu2AAAIAAJlg");
	var mask_graphics_362 = new cjs.Graphics().p("EgXKAhGIAAqRMAu2AAAIAAKRg");
	var mask_graphics_363 = new cjs.Graphics().p("EgXJAhFIAAq+MAu2AAAIAAK+g");
	var mask_graphics_364 = new cjs.Graphics().p("EgXJAhFIAArtMAu2AAAIAALtg");
	var mask_graphics_365 = new cjs.Graphics().p("EgXJAhEIAAseMAu2AAAIAAMeg");
	var mask_graphics_366 = new cjs.Graphics().p("EgXJAhEIAAtQMAu2AAAIAANQg");
	var mask_graphics_367 = new cjs.Graphics().p("EgXJAhDIAAuEMAu2AAAIAAOEg");
	var mask_graphics_368 = new cjs.Graphics().p("EgXJAhCIAAu5MAu2AAAIAAO5g");
	var mask_graphics_369 = new cjs.Graphics().p("EgXJAhCIAAvxMAu2AAAIAAPxg");
	var mask_graphics_370 = new cjs.Graphics().p("EgXJAhBIAAwqMAu2AAAIAAQqg");
	var mask_graphics_371 = new cjs.Graphics().p("EgXJAhBIAAxlMAu2AAAIAARlg");
	var mask_graphics_372 = new cjs.Graphics().p("EgXJAhAIAAyhMAu2AAAIAAShg");
	var mask_graphics_373 = new cjs.Graphics().p("EgXJAg/IAAzfMAu2AAAIAATfg");
	var mask_graphics_374 = new cjs.Graphics().p("EgXJAg/IAA0fMAu2AAAIAAUfg");
	var mask_graphics_375 = new cjs.Graphics().p("EgXIAg+IAA1gMAu2AAAIAAVgg");
	var mask_graphics_376 = new cjs.Graphics().p("EgXIAg9IAA2jMAu2AAAIAAWjg");
	var mask_graphics_377 = new cjs.Graphics().p("EgXIAg9IAA3oMAu2AAAIAAXog");
	var mask_graphics_378 = new cjs.Graphics().p("EgXIAg8IAA4uMAu2AAAIAAYug");
	var mask_graphics_379 = new cjs.Graphics().p("EgXIAg7IAA52MAu2AAAIAAZ2g");
	var mask_graphics_380 = new cjs.Graphics().p("EgXIAg6IAA6/MAu2AAAIAAa/g");
	var mask_graphics_381 = new cjs.Graphics().p("EgXIAg5IAA8KMAu2AAAIAAcKg");
	var mask_graphics_382 = new cjs.Graphics().p("EgXIAg5IAA9XMAu2AAAIAAdXg");
	var mask_graphics_383 = new cjs.Graphics().p("EgXHAg4IAA+mMAu2AAAIAAemg");
	var mask_graphics_384 = new cjs.Graphics().p("EgXHAg3IAA/2MAu2AAAIAAf2g");
	var mask_graphics_385 = new cjs.Graphics().p("EgXHAg2MAAAghFMAu2AAAMAAAAhFg");
	var mask_graphics_386 = new cjs.Graphics().p("EgXHAg1MAAAgiZMAu2AAAMAAAAiZg");
	var mask_graphics_387 = new cjs.Graphics().p("EgXHAg0MAAAgjuMAu2AAAMAAAAjug");
	var mask_graphics_388 = new cjs.Graphics().p("EgXHAgzMAAAglEMAu2AAAMAAAAlEg");
	var mask_graphics_389 = new cjs.Graphics().p("EgXHAgyMAAAgmdMAu2AAAMAAAAmdg");
	var mask_graphics_390 = new cjs.Graphics().p("EgXHAgxMAAAgn3MAu2AAAMAAAAn3g");
	var mask_graphics_391 = new cjs.Graphics().p("EgXGAgxMAAAgpTMAu2AAAMAAAApTg");
	var mask_graphics_392 = new cjs.Graphics().p("EgXGAgwMAAAgqxMAu2AAAMAAAAqxg");
	var mask_graphics_393 = new cjs.Graphics().p("EgXGAgvMAAAgsQMAu2AAAMAAAAsQg");
	var mask_graphics_394 = new cjs.Graphics().p("EgXGAgtMAAAgtwMAu2AAAMAAAAtwg");
	var mask_graphics_395 = new cjs.Graphics().p("EgXGAgsMAAAgvSMAu2AAAMAAAAvSg");
	var mask_graphics_396 = new cjs.Graphics().p("EgXGAgrMAAAgw2MAu2AAAMAAAAw2g");
	var mask_graphics_397 = new cjs.Graphics().p("EgXFAgqMAAAgycMAu2AAAMAAAAycg");
	var mask_graphics_398 = new cjs.Graphics().p("EgXFAgpMAAAg0DMAu2AAAMAAAA0Dg");
	var mask_graphics_399 = new cjs.Graphics().p("EgXFAgoMAAAg1sMAu2AAAMAAAA1sg");
	var mask_graphics_400 = new cjs.Graphics().p("EgXFAgnMAAAg3XMAu2AAAMAAAA3Xg");
	var mask_graphics_401 = new cjs.Graphics().p("EgXFAgmMAAAg5DMAu2AAAMAAAA5Dg");
	var mask_graphics_402 = new cjs.Graphics().p("EgXFAglMAAAg6xMAu2AAAMAAAA6xg");
	var mask_graphics_403 = new cjs.Graphics().p("EgXEAgjMAAAg8gMAu2AAAMAAAA8gg");
	var mask_graphics_404 = new cjs.Graphics().p("EgXEAgiMAAAg+RMAu2AAAMAAAA+Rg");
	var mask_graphics_405 = new cjs.Graphics().p("EgXEAghMAAAhAEMAu2AAAMAAABAEg");
	var mask_graphics_406 = new cjs.Graphics().p("EgXEAg9MAAAhB5MAu2AAAMAAABB5g");
	var mask_graphics_407 = new cjs.Graphics().p("EgXEAh4MAAAhDvMAu2AAAMAAABDvg");
	var mask_graphics_408 = new cjs.Graphics().p("EgXDAi0MAAAhFnMAu2AAAMAAABFng");
	var mask_graphics_409 = new cjs.Graphics().p("EgXDAjxMAAAhHhMAu2AAAMAAABHhg");
	var mask_graphics_410 = new cjs.Graphics().p("EgXDAkuMAAAhJbMAu2AAAMAAABJbg");
	var mask_graphics_411 = new cjs.Graphics().p("EgXDAltMAAAhLZMAu2AAAMAAABLZg");
	var mask_graphics_412 = new cjs.Graphics().p("EgXDAmsMAAAhNXMAu2AAAMAAABNXg");
	var mask_graphics_413 = new cjs.Graphics().p("EgXDAnsMAAAhPXMAu3AAAMAAABPXg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(335).to({graphics:mask_graphics_335,x:151.6,y:212.5}).wait(1).to({graphics:mask_graphics_336,x:151.6,y:212.5}).wait(1).to({graphics:mask_graphics_337,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_338,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_339,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_340,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_341,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_342,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_343,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_344,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_345,x:151.6,y:212.4}).wait(1).to({graphics:mask_graphics_346,x:151.6,y:212.3}).wait(1).to({graphics:mask_graphics_347,x:151.6,y:212.3}).wait(1).to({graphics:mask_graphics_348,x:151.6,y:212.3}).wait(1).to({graphics:mask_graphics_349,x:151.6,y:212.3}).wait(1).to({graphics:mask_graphics_350,x:151.6,y:212.3}).wait(1).to({graphics:mask_graphics_351,x:151.6,y:212.2}).wait(1).to({graphics:mask_graphics_352,x:151.6,y:212.2}).wait(1).to({graphics:mask_graphics_353,x:151.6,y:212.2}).wait(1).to({graphics:mask_graphics_354,x:151.6,y:212.1}).wait(1).to({graphics:mask_graphics_355,x:151.7,y:212.1}).wait(1).to({graphics:mask_graphics_356,x:151.7,y:212.1}).wait(1).to({graphics:mask_graphics_357,x:151.7,y:212}).wait(1).to({graphics:mask_graphics_358,x:151.7,y:212}).wait(1).to({graphics:mask_graphics_359,x:151.7,y:211.9}).wait(1).to({graphics:mask_graphics_360,x:151.7,y:211.9}).wait(1).to({graphics:mask_graphics_361,x:151.7,y:211.9}).wait(1).to({graphics:mask_graphics_362,x:151.7,y:211.8}).wait(1).to({graphics:mask_graphics_363,x:151.7,y:211.8}).wait(1).to({graphics:mask_graphics_364,x:151.7,y:211.7}).wait(1).to({graphics:mask_graphics_365,x:151.7,y:211.7}).wait(1).to({graphics:mask_graphics_366,x:151.7,y:211.6}).wait(1).to({graphics:mask_graphics_367,x:151.7,y:211.6}).wait(1).to({graphics:mask_graphics_368,x:151.7,y:211.5}).wait(1).to({graphics:mask_graphics_369,x:151.8,y:211.4}).wait(1).to({graphics:mask_graphics_370,x:151.8,y:211.4}).wait(1).to({graphics:mask_graphics_371,x:151.8,y:211.3}).wait(1).to({graphics:mask_graphics_372,x:151.8,y:211.3}).wait(1).to({graphics:mask_graphics_373,x:151.8,y:211.2}).wait(1).to({graphics:mask_graphics_374,x:151.8,y:211.1}).wait(1).to({graphics:mask_graphics_375,x:151.8,y:211}).wait(1).to({graphics:mask_graphics_376,x:151.8,y:211}).wait(1).to({graphics:mask_graphics_377,x:151.8,y:210.9}).wait(1).to({graphics:mask_graphics_378,x:151.8,y:210.8}).wait(1).to({graphics:mask_graphics_379,x:151.9,y:210.8}).wait(1).to({graphics:mask_graphics_380,x:151.9,y:210.7}).wait(1).to({graphics:mask_graphics_381,x:151.9,y:210.6}).wait(1).to({graphics:mask_graphics_382,x:151.9,y:210.5}).wait(1).to({graphics:mask_graphics_383,x:151.9,y:210.4}).wait(1).to({graphics:mask_graphics_384,x:151.9,y:210.3}).wait(1).to({graphics:mask_graphics_385,x:151.9,y:210.3}).wait(1).to({graphics:mask_graphics_386,x:151.9,y:210.2}).wait(1).to({graphics:mask_graphics_387,x:152,y:210.1}).wait(1).to({graphics:mask_graphics_388,x:152,y:210}).wait(1).to({graphics:mask_graphics_389,x:152,y:209.9}).wait(1).to({graphics:mask_graphics_390,x:152,y:209.8}).wait(1).to({graphics:mask_graphics_391,x:152,y:209.7}).wait(1).to({graphics:mask_graphics_392,x:152,y:209.6}).wait(1).to({graphics:mask_graphics_393,x:152,y:209.5}).wait(1).to({graphics:mask_graphics_394,x:152.1,y:209.4}).wait(1).to({graphics:mask_graphics_395,x:152.1,y:209.3}).wait(1).to({graphics:mask_graphics_396,x:152.1,y:209.2}).wait(1).to({graphics:mask_graphics_397,x:152.1,y:209.1}).wait(1).to({graphics:mask_graphics_398,x:152.1,y:209}).wait(1).to({graphics:mask_graphics_399,x:152.1,y:208.9}).wait(1).to({graphics:mask_graphics_400,x:152.2,y:208.7}).wait(1).to({graphics:mask_graphics_401,x:152.2,y:208.6}).wait(1).to({graphics:mask_graphics_402,x:152.2,y:208.5}).wait(1).to({graphics:mask_graphics_403,x:152.2,y:208.4}).wait(1).to({graphics:mask_graphics_404,x:152.2,y:208.3}).wait(1).to({graphics:mask_graphics_405,x:152.2,y:208.2}).wait(1).to({graphics:mask_graphics_406,x:152.3,y:205.1}).wait(1).to({graphics:mask_graphics_407,x:152.3,y:199}).wait(1).to({graphics:mask_graphics_408,x:152.3,y:192.7}).wait(1).to({graphics:mask_graphics_409,x:152.3,y:186.4}).wait(1).to({graphics:mask_graphics_410,x:152.3,y:180}).wait(1).to({graphics:mask_graphics_411,x:152.4,y:173.5}).wait(1).to({graphics:mask_graphics_412,x:152.4,y:166.9}).wait(1).to({graphics:mask_graphics_413,x:152.4,y:160}).wait(62));

	// copy_3
	this.copy_3_mc = new lib.copu_3_mc();
	this.copy_3_mc.setTransform(151,170.2);
	this.copy_3_mc._off = true;

	this.copy_3_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.copy_3_mc).wait(335).to({_off:false},0).wait(140));

	// Hands_2
	this.instance_1 = new lib.Hands_1();
	this.instance_1.setTransform(0,1149.8,2.21,2.21,0,180,0);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(138).to({_off:false},0).to({scaleX:1,scaleY:1,x:80,y:276},59,cjs.Ease.get(1)).to({y:876},78,cjs.Ease.get(-1)).to({_off:true},118).wait(82));

	// Mask --- f2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_197 = new cjs.Graphics().p("A2WIRIAAgoMAtcAAAIAAAog");
	var mask_1_graphics_198 = new cjs.Graphics().p("A2WISIAAgpMAtcAAAIAAApg");
	var mask_1_graphics_199 = new cjs.Graphics().p("A2WITIAAgrMAtcAAAIAAArg");
	var mask_1_graphics_200 = new cjs.Graphics().p("A2WIVIAAgvMAtcAAAIAAAvg");
	var mask_1_graphics_201 = new cjs.Graphics().p("A2WIYIAAg1MAtcAAAIAAA1g");
	var mask_1_graphics_202 = new cjs.Graphics().p("A2VIbIAAg7MAtbAAAIAAA7g");
	var mask_1_graphics_203 = new cjs.Graphics().p("A2VIgIAAhEMAtbAAAIAABEg");
	var mask_1_graphics_204 = new cjs.Graphics().p("A2VIlIAAhOMAtbAAAIAABOg");
	var mask_1_graphics_205 = new cjs.Graphics().p("A2VIrIAAhaMAtbAAAIAABag");
	var mask_1_graphics_206 = new cjs.Graphics().p("A2VIyIAAhnMAtbAAAIAABng");
	var mask_1_graphics_207 = new cjs.Graphics().p("A2VI6IAAh2MAtbAAAIAAB2g");
	var mask_1_graphics_208 = new cjs.Graphics().p("A2VJCIAAiGMAtbAAAIAACGg");
	var mask_1_graphics_209 = new cjs.Graphics().p("A2VJMIAAiYMAtbAAAIAACYg");
	var mask_1_graphics_210 = new cjs.Graphics().p("A2VJWIAAisMAtbAAAIAACsg");
	var mask_1_graphics_211 = new cjs.Graphics().p("A2VJhIAAjBMAtbAAAIAADBg");
	var mask_1_graphics_212 = new cjs.Graphics().p("A2VJtIAAjYMAtaAAAIAADYg");
	var mask_1_graphics_213 = new cjs.Graphics().p("A2VJ5IAAjvMAtaAAAIAADvg");
	var mask_1_graphics_214 = new cjs.Graphics().p("A2VKHIAAkJMAtaAAAIAAEJg");
	var mask_1_graphics_215 = new cjs.Graphics().p("A2VKVIAAklMAtaAAAIAAElg");
	var mask_1_graphics_216 = new cjs.Graphics().p("A2UKkIAAlBMAtZAAAIAAFBg");
	var mask_1_graphics_217 = new cjs.Graphics().p("A2UK0IAAlgMAtZAAAIAAFgg");
	var mask_1_graphics_218 = new cjs.Graphics().p("A2ULEIAAl/MAtZAAAIAAF/g");
	var mask_1_graphics_219 = new cjs.Graphics().p("A2ULWIAAmhMAtZAAAIAAGhg");
	var mask_1_graphics_220 = new cjs.Graphics().p("A2ULoIAAnEMAtYAAAIAAHEg");
	var mask_1_graphics_221 = new cjs.Graphics().p("A2UL7IAAnoMAtYAAAIAAHog");
	var mask_1_graphics_222 = new cjs.Graphics().p("A2TMPIAAoPMAtXAAAIAAIPg");
	var mask_1_graphics_223 = new cjs.Graphics().p("A2TMkIAAo3MAtXAAAIAAI3g");
	var mask_1_graphics_224 = new cjs.Graphics().p("A2TM5IAApgMAtXAAAIAAJgg");
	var mask_1_graphics_225 = new cjs.Graphics().p("A2TNQIAAqLMAtXAAAIAAKLg");
	var mask_1_graphics_226 = new cjs.Graphics().p("A2TNnIAAq3MAtWAAAIAAK3g");
	var mask_1_graphics_227 = new cjs.Graphics().p("A2TN/IAArlMAtWAAAIAALlg");
	var mask_1_graphics_228 = new cjs.Graphics().p("A2SOYIAAsVMAtVAAAIAAMVg");
	var mask_1_graphics_229 = new cjs.Graphics().p("A2SOxIAAtGMAtVAAAIAANGg");
	var mask_1_graphics_230 = new cjs.Graphics().p("A2SPMIAAt5MAtVAAAIAAN5g");
	var mask_1_graphics_231 = new cjs.Graphics().p("A2SPnIAAutMAtUAAAIAAOtg");
	var mask_1_graphics_232 = new cjs.Graphics().p("A2RQDIAAviMAtTAAAIAAPig");
	var mask_1_graphics_233 = new cjs.Graphics().p("A2RQgIAAwaMAtTAAAIAAQag");
	var mask_1_graphics_234 = new cjs.Graphics().p("A2RQ+IAAxRMAtTAAAIAARRg");
	var mask_1_graphics_235 = new cjs.Graphics().p("A2RRcIAAyLMAtTAAAIAASLg");
	var mask_1_graphics_236 = new cjs.Graphics().p("A2QR7IAAzHMAtRAAAIAATHg");
	var mask_1_graphics_237 = new cjs.Graphics().p("A2QSbIAA0EMAtRAAAIAAUEg");
	var mask_1_graphics_238 = new cjs.Graphics().p("A2QS8IAA1EMAtRAAAIAAVEg");
	var mask_1_graphics_239 = new cjs.Graphics().p("A2QTeIAA2EMAtRAAAIAAWEg");
	var mask_1_graphics_240 = new cjs.Graphics().p("A2PUBIAA3HMAtPAAAIAAXHg");
	var mask_1_graphics_241 = new cjs.Graphics().p("A2PUkIAA4LMAtPAAAIAAYLg");
	var mask_1_graphics_242 = new cjs.Graphics().p("A2PVIIAA5QMAtPAAAIAAZQg");
	var mask_1_graphics_243 = new cjs.Graphics().p("A2PVtIAA6XMAtOAAAIAAaXg");
	var mask_1_graphics_244 = new cjs.Graphics().p("A2OWTIAA7fMAtNAAAIAAbfg");
	var mask_1_graphics_245 = new cjs.Graphics().p("A2OW5IAA8pMAtNAAAIAAcpg");
	var mask_1_graphics_246 = new cjs.Graphics().p("A2OXhIAA91MAtNAAAIAAd1g");
	var mask_1_graphics_247 = new cjs.Graphics().p("A2NYJIAA/CMAtLAAAIAAfCg");
	var mask_1_graphics_248 = new cjs.Graphics().p("A2NYyMAAAggRMAtLAAAMAAAAgRg");
	var mask_1_graphics_249 = new cjs.Graphics().p("A2NZcMAAAghhMAtLAAAMAAAAhhg");
	var mask_1_graphics_250 = new cjs.Graphics().p("A2MaHMAAAgizMAtJAAAMAAAAizg");
	var mask_1_graphics_251 = new cjs.Graphics().p("A2MayMAAAgkGMAtJAAAMAAAAkGg");
	var mask_1_graphics_252 = new cjs.Graphics().p("A2LbeMAAAglbMAtIAAAMAAAAlbg");
	var mask_1_graphics_253 = new cjs.Graphics().p("A2LcLMAAAgmxMAtHAAAMAAAAmxg");
	var mask_1_graphics_254 = new cjs.Graphics().p("A2Lc5MAAAgoJMAtHAAAMAAAAoJg");
	var mask_1_graphics_255 = new cjs.Graphics().p("A2KdoMAAAgpjMAtFAAAMAAAApjg");
	var mask_1_graphics_256 = new cjs.Graphics().p("A2KeYMAAAgq/MAtFAAAMAAAAq/g");
	var mask_1_graphics_257 = new cjs.Graphics().p("A2KfIMAAAgsbMAtFAAAMAAAAsbg");
	var mask_1_graphics_258 = new cjs.Graphics().p("A2Jf5MAAAgt5MAtDAAAMAAAAt5g");
	var mask_1_graphics_259 = new cjs.Graphics().p("EgWJAgrMAAAgvZMAtDAAAMAAAAvZg");
	var mask_1_graphics_260 = new cjs.Graphics().p("EgWIAheMAAAgw7MAtBAAAMAAAAw7g");
	var mask_1_graphics_261 = new cjs.Graphics().p("EgWIAiSMAAAgyeMAtBAAAMAAAAyeg");
	var mask_1_graphics_262 = new cjs.Graphics().p("EgWHAjGMAAAg0CMAtAAAAMAAAA0Cg");
	var mask_1_graphics_263 = new cjs.Graphics().p("EgWHAj7MAAAg1oMAs/AAAMAAAA1og");
	var mask_1_graphics_264 = new cjs.Graphics().p("EgWHAkxMAAAg3QMAs/AAAMAAAA3Qg");
	var mask_1_graphics_265 = new cjs.Graphics().p("EgWGAloMAAAg45MAs9AAAMAAAA45g");
	var mask_1_graphics_266 = new cjs.Graphics().p("EgWGAmgMAAAg6kMAs9AAAMAAAA6kg");
	var mask_1_graphics_267 = new cjs.Graphics().p("EgWFAnYMAAAg8QMAs8AAAMAAAA8Qg");
	var mask_1_graphics_268 = new cjs.Graphics().p("EgWFAoSMAAAg9+MAs7AAAMAAAA9+g");
	var mask_1_graphics_269 = new cjs.Graphics().p("EgWEApMMAAAg/uMAs6AAAMAAAA/ug");
	var mask_1_graphics_270 = new cjs.Graphics().p("EgWEAqHMAAAhBfMAs5AAAMAAABBfg");
	var mask_1_graphics_271 = new cjs.Graphics().p("EgWDArCMAAAhDRMAs4AAAMAAABDRg");
	var mask_1_graphics_272 = new cjs.Graphics().p("EgWDAr/MAAAhFFMAs3AAAMAAABFFg");
	var mask_1_graphics_273 = new cjs.Graphics().p("EgWCAs8MAAAhG6MAs2AAAMAAABG6g");
	var mask_1_graphics_274 = new cjs.Graphics().p("EgWCAt6MAAAhIyMAs1AAAMAAABIyg");
	var mask_1_graphics_275 = new cjs.Graphics().p("EgWCAu4MAAAhKrMAs0AAAMAAABKrg");
	var mask_1_graphics_335 = new cjs.Graphics().p("EgWCAlWMAAAhKrMAs0AAAMAAABKrg");
	var mask_1_graphics_336 = new cjs.Graphics().p("EgWCAlVMAAAhKpMAs0AAAMAAABKpg");
	var mask_1_graphics_337 = new cjs.Graphics().p("EgWCAlUMAAAhKnMAs0AAAMAAABKng");
	var mask_1_graphics_338 = new cjs.Graphics().p("EgWCAlSMAAAhKjMAs0AAAMAAABKjg");
	var mask_1_graphics_339 = new cjs.Graphics().p("EgWCAlQMAAAhKfMAs0AAAMAAABKfg");
	var mask_1_graphics_340 = new cjs.Graphics().p("EgWCAlMMAAAhKXMAs0AAAMAAABKXg");
	var mask_1_graphics_341 = new cjs.Graphics().p("EgWCAlIMAAAhKPMAs0AAAMAAABKPg");
	var mask_1_graphics_342 = new cjs.Graphics().p("EgWCAlDMAAAhKFMAs0AAAMAAABKFg");
	var mask_1_graphics_343 = new cjs.Graphics().p("EgWCAk9MAAAhJ5MAs0AAAMAAABJ5g");
	var mask_1_graphics_344 = new cjs.Graphics().p("EgWCAk2MAAAhJrMAs0AAAMAAABJrg");
	var mask_1_graphics_345 = new cjs.Graphics().p("EgWCAkvMAAAhJdMAs1AAAMAAABJdg");
	var mask_1_graphics_346 = new cjs.Graphics().p("EgWCAknMAAAhJNMAs1AAAMAAABJNg");
	var mask_1_graphics_347 = new cjs.Graphics().p("EgWCAkeMAAAhI7MAs1AAAMAAABI7g");
	var mask_1_graphics_348 = new cjs.Graphics().p("EgWCAkUMAAAhInMAs1AAAMAAABIng");
	var mask_1_graphics_349 = new cjs.Graphics().p("EgWCAkJMAAAhIRMAs1AAAMAAABIRg");
	var mask_1_graphics_350 = new cjs.Graphics().p("EgWCAj+MAAAhH7MAs1AAAMAAABH7g");
	var mask_1_graphics_351 = new cjs.Graphics().p("EgWDAjyMAAAhHjMAs2AAAMAAABHjg");
	var mask_1_graphics_352 = new cjs.Graphics().p("EgWDAjlMAAAhHJMAs2AAAMAAABHJg");
	var mask_1_graphics_353 = new cjs.Graphics().p("EgWDAjYMAAAhGvMAs2AAAMAAABGvg");
	var mask_1_graphics_354 = new cjs.Graphics().p("EgWDAjJMAAAhGRMAs2AAAMAAABGRg");
	var mask_1_graphics_355 = new cjs.Graphics().p("EgWDAi6MAAAhFzMAs3AAAMAAABFzg");
	var mask_1_graphics_356 = new cjs.Graphics().p("EgWDAiqMAAAhFTMAs3AAAMAAABFTg");
	var mask_1_graphics_357 = new cjs.Graphics().p("EgWDAiZMAAAhExMAs3AAAMAAABExg");
	var mask_1_graphics_358 = new cjs.Graphics().p("EgWDAiIMAAAhEPMAs3AAAMAAABEPg");
	var mask_1_graphics_359 = new cjs.Graphics().p("EgWEAh1MAAAhDpMAs4AAAMAAABDpg");
	var mask_1_graphics_360 = new cjs.Graphics().p("EgWEAhiMAAAhDDMAs4AAAMAAABDDg");
	var mask_1_graphics_361 = new cjs.Graphics().p("EgWEAhOMAAAhCbMAs4AAAMAAABCbg");
	var mask_1_graphics_362 = new cjs.Graphics().p("EgWEAg6MAAAhBzMAs5AAAMAAABBzg");
	var mask_1_graphics_363 = new cjs.Graphics().p("EgWEAgkMAAAhBHMAs5AAAMAAABBHg");
	var mask_1_graphics_364 = new cjs.Graphics().p("EgWFAgOMAAAhAbMAs6AAAMAAABAbg");
	var mask_1_graphics_365 = new cjs.Graphics().p("A2Ff3MAAAg/tMAs6AAAMAAAA/tg");
	var mask_1_graphics_366 = new cjs.Graphics().p("A2FffMAAAg+9MAs6AAAMAAAA+9g");
	var mask_1_graphics_367 = new cjs.Graphics().p("A2FfHMAAAg+NMAs7AAAMAAAA+Ng");
	var mask_1_graphics_368 = new cjs.Graphics().p("A2FeuMAAAg9bMAs7AAAMAAAA9bg");
	var mask_1_graphics_369 = new cjs.Graphics().p("A2GeTMAAAg8lMAs8AAAMAAAA8lg");
	var mask_1_graphics_370 = new cjs.Graphics().p("A2Gd5MAAAg7xMAs8AAAMAAAA7xg");
	var mask_1_graphics_371 = new cjs.Graphics().p("A2GddMAAAg65MAs8AAAMAAAA65g");
	var mask_1_graphics_372 = new cjs.Graphics().p("A2GdAMAAAg5/MAs9AAAMAAAA5/g");
	var mask_1_graphics_373 = new cjs.Graphics().p("A2HcjMAAAg5FMAs+AAAMAAAA5Fg");
	var mask_1_graphics_374 = new cjs.Graphics().p("A2HcFMAAAg4JMAs+AAAMAAAA4Jg");
	var mask_1_graphics_375 = new cjs.Graphics().p("A2HbmMAAAg3LMAs+AAAMAAAA3Lg");
	var mask_1_graphics_376 = new cjs.Graphics().p("A2HbHMAAAg2NMAs/AAAMAAAA2Ng");
	var mask_1_graphics_377 = new cjs.Graphics().p("A2IanMAAAg1NMAtAAAAMAAAA1Ng");
	var mask_1_graphics_378 = new cjs.Graphics().p("A2IaFMAAAg0JMAtAAAAMAAAA0Jg");
	var mask_1_graphics_379 = new cjs.Graphics().p("A2IZkMAAAgzHMAtAAAAMAAAAzHg");
	var mask_1_graphics_380 = new cjs.Graphics().p("A2IZBMAAAgyBMAtBAAAMAAAAyBg");
	var mask_1_graphics_381 = new cjs.Graphics().p("A2JYdMAAAgw5MAtCAAAMAAAAw5g");
	var mask_1_graphics_382 = new cjs.Graphics().p("A2JX5MAAAgvxMAtCAAAMAAAAvxg");
	var mask_1_graphics_383 = new cjs.Graphics().p("A2JXUMAAAgunMAtDAAAMAAAAung");
	var mask_1_graphics_384 = new cjs.Graphics().p("A2KWuMAAAgtbMAtEAAAMAAAAtbg");
	var mask_1_graphics_385 = new cjs.Graphics().p("A2KWIMAAAgsPMAtEAAAMAAAAsPg");
	var mask_1_graphics_386 = new cjs.Graphics().p("A2KVgMAAAgq/MAtFAAAMAAAAq/g");
	var mask_1_graphics_387 = new cjs.Graphics().p("A2LU4MAAAgpvMAtGAAAMAAAApvg");
	var mask_1_graphics_388 = new cjs.Graphics().p("A2LUPMAAAgodMAtGAAAMAAAAodg");
	var mask_1_graphics_389 = new cjs.Graphics().p("A2LTmMAAAgnLMAtHAAAMAAAAnLg");
	var mask_1_graphics_390 = new cjs.Graphics().p("A2MS7MAAAgl1MAtIAAAMAAAAl1g");
	var mask_1_graphics_391 = new cjs.Graphics().p("A2MSQMAAAgkfMAtIAAAMAAAAkfg");
	var mask_1_graphics_392 = new cjs.Graphics().p("A2NRkMAAAgjHMAtKAAAMAAAAjHg");
	var mask_1_graphics_393 = new cjs.Graphics().p("A2NQ3MAAAghtMAtKAAAMAAAAhtg");
	var mask_1_graphics_394 = new cjs.Graphics().p("A2NQKMAAAggTMAtKAAAMAAAAgTg");
	var mask_1_graphics_395 = new cjs.Graphics().p("A2OPbIAA+1MAtMAAAIAAe1g");
	var mask_1_graphics_396 = new cjs.Graphics().p("A2OOsIAA9XMAtMAAAIAAdXg");
	var mask_1_graphics_397 = new cjs.Graphics().p("A2PN8IAA73MAtOAAAIAAb3g");
	var mask_1_graphics_398 = new cjs.Graphics().p("A2PNLIAA6VMAtOAAAIAAaVg");
	var mask_1_graphics_399 = new cjs.Graphics().p("A2PMaIAA4zMAtOAAAIAAYzg");
	var mask_1_graphics_400 = new cjs.Graphics().p("A2QLoIAA3PMAtQAAAIAAXPg");
	var mask_1_graphics_401 = new cjs.Graphics().p("A2QK1IAA1pMAtQAAAIAAVpg");
	var mask_1_graphics_402 = new cjs.Graphics().p("A2RKBIAA0BMAtSAAAIAAUBg");
	var mask_1_graphics_403 = new cjs.Graphics().p("A2RJMIAAyXMAtSAAAIAASXg");
	var mask_1_graphics_404 = new cjs.Graphics().p("A2SIXIAAwtMAtTAAAIAAQtg");
	var mask_1_graphics_405 = new cjs.Graphics().p("A2SHhIAAvBMAtUAAAIAAPBg");
	var mask_1_graphics_406 = new cjs.Graphics().p("A2TGLIAAtTMAtVAAAIAANTg");
	var mask_1_graphics_407 = new cjs.Graphics().p("A2TEXIAArkMAtWAAAIAALkg");
	var mask_1_graphics_408 = new cjs.Graphics().p("A2UChIAApyMAtXAAAIAAJyg");
	var mask_1_graphics_409 = new cjs.Graphics().p("A2UAqIAAoAMAtYAAAIAAIAg");
	var mask_1_graphics_410 = new cjs.Graphics().p("A2VhNIAAmNMAtZAAAIAAGNg");
	var mask_1_graphics_411 = new cjs.Graphics().p("A2VjHIAAkYMAtaAAAIAAEYg");
	var mask_1_graphics_412 = new cjs.Graphics().p("A2WlDIAAihMAtbAAAIAAChg");
	var mask_1_graphics_413 = new cjs.Graphics().p("A2WnBIAAgoMAtcAAAIAAAog");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(197).to({graphics:mask_1_graphics_197,x:147.9,y:53}).wait(1).to({graphics:mask_1_graphics_198,x:147.9,y:53}).wait(1).to({graphics:mask_1_graphics_199,x:147.9,y:53.1}).wait(1).to({graphics:mask_1_graphics_200,x:147.9,y:53.3}).wait(1).to({graphics:mask_1_graphics_201,x:147.9,y:53.6}).wait(1).to({graphics:mask_1_graphics_202,x:147.9,y:54}).wait(1).to({graphics:mask_1_graphics_203,x:147.9,y:54.4}).wait(1).to({graphics:mask_1_graphics_204,x:147.9,y:55}).wait(1).to({graphics:mask_1_graphics_205,x:147.8,y:55.6}).wait(1).to({graphics:mask_1_graphics_206,x:147.8,y:56.3}).wait(1).to({graphics:mask_1_graphics_207,x:147.8,y:57}).wait(1).to({graphics:mask_1_graphics_208,x:147.8,y:57.9}).wait(1).to({graphics:mask_1_graphics_209,x:147.8,y:58.8}).wait(1).to({graphics:mask_1_graphics_210,x:147.8,y:59.8}).wait(1).to({graphics:mask_1_graphics_211,x:147.8,y:60.9}).wait(1).to({graphics:mask_1_graphics_212,x:147.8,y:62.1}).wait(1).to({graphics:mask_1_graphics_213,x:147.8,y:63.4}).wait(1).to({graphics:mask_1_graphics_214,x:147.8,y:64.7}).wait(1).to({graphics:mask_1_graphics_215,x:147.8,y:66.1}).wait(1).to({graphics:mask_1_graphics_216,x:147.8,y:67.6}).wait(1).to({graphics:mask_1_graphics_217,x:147.7,y:69.2}).wait(1).to({graphics:mask_1_graphics_218,x:147.7,y:70.9}).wait(1).to({graphics:mask_1_graphics_219,x:147.7,y:72.6}).wait(1).to({graphics:mask_1_graphics_220,x:147.7,y:74.5}).wait(1).to({graphics:mask_1_graphics_221,x:147.7,y:76.4}).wait(1).to({graphics:mask_1_graphics_222,x:147.7,y:78.4}).wait(1).to({graphics:mask_1_graphics_223,x:147.7,y:80.4}).wait(1).to({graphics:mask_1_graphics_224,x:147.6,y:82.6}).wait(1).to({graphics:mask_1_graphics_225,x:147.6,y:84.8}).wait(1).to({graphics:mask_1_graphics_226,x:147.6,y:87.1}).wait(1).to({graphics:mask_1_graphics_227,x:147.6,y:89.5}).wait(1).to({graphics:mask_1_graphics_228,x:147.6,y:92}).wait(1).to({graphics:mask_1_graphics_229,x:147.5,y:94.6}).wait(1).to({graphics:mask_1_graphics_230,x:147.5,y:97.2}).wait(1).to({graphics:mask_1_graphics_231,x:147.5,y:99.9}).wait(1).to({graphics:mask_1_graphics_232,x:147.5,y:102.7}).wait(1).to({graphics:mask_1_graphics_233,x:147.5,y:105.6}).wait(1).to({graphics:mask_1_graphics_234,x:147.4,y:108.6}).wait(1).to({graphics:mask_1_graphics_235,x:147.4,y:111.6}).wait(1).to({graphics:mask_1_graphics_236,x:147.4,y:114.8}).wait(1).to({graphics:mask_1_graphics_237,x:147.4,y:118}).wait(1).to({graphics:mask_1_graphics_238,x:147.3,y:121.3}).wait(1).to({graphics:mask_1_graphics_239,x:147.3,y:124.7}).wait(1).to({graphics:mask_1_graphics_240,x:147.3,y:128.1}).wait(1).to({graphics:mask_1_graphics_241,x:147.2,y:131.6}).wait(1).to({graphics:mask_1_graphics_242,x:147.2,y:135.3}).wait(1).to({graphics:mask_1_graphics_243,x:147.2,y:139}).wait(1).to({graphics:mask_1_graphics_244,x:147.2,y:142.7}).wait(1).to({graphics:mask_1_graphics_245,x:147.1,y:146.6}).wait(1).to({graphics:mask_1_graphics_246,x:147.1,y:150.5}).wait(1).to({graphics:mask_1_graphics_247,x:147.1,y:154.6}).wait(1).to({graphics:mask_1_graphics_248,x:147,y:158.7}).wait(1).to({graphics:mask_1_graphics_249,x:147,y:162.8}).wait(1).to({graphics:mask_1_graphics_250,x:147,y:167.1}).wait(1).to({graphics:mask_1_graphics_251,x:146.9,y:171.5}).wait(1).to({graphics:mask_1_graphics_252,x:146.9,y:175.9}).wait(1).to({graphics:mask_1_graphics_253,x:146.9,y:180.4}).wait(1).to({graphics:mask_1_graphics_254,x:146.8,y:185}).wait(1).to({graphics:mask_1_graphics_255,x:146.8,y:189.7}).wait(1).to({graphics:mask_1_graphics_256,x:146.8,y:194.4}).wait(1).to({graphics:mask_1_graphics_257,x:146.7,y:199.3}).wait(1).to({graphics:mask_1_graphics_258,x:146.7,y:204.2}).wait(1).to({graphics:mask_1_graphics_259,x:146.6,y:209.2}).wait(1).to({graphics:mask_1_graphics_260,x:146.6,y:214.2}).wait(1).to({graphics:mask_1_graphics_261,x:146.6,y:219.4}).wait(1).to({graphics:mask_1_graphics_262,x:146.5,y:224.6}).wait(1).to({graphics:mask_1_graphics_263,x:146.5,y:230}).wait(1).to({graphics:mask_1_graphics_264,x:146.4,y:235.4}).wait(1).to({graphics:mask_1_graphics_265,x:146.4,y:240.9}).wait(1).to({graphics:mask_1_graphics_266,x:146.3,y:246.4}).wait(1).to({graphics:mask_1_graphics_267,x:146.3,y:252.1}).wait(1).to({graphics:mask_1_graphics_268,x:146.3,y:257.8}).wait(1).to({graphics:mask_1_graphics_269,x:146.2,y:263.6}).wait(1).to({graphics:mask_1_graphics_270,x:146.2,y:269.5}).wait(1).to({graphics:mask_1_graphics_271,x:146.1,y:275.5}).wait(1).to({graphics:mask_1_graphics_272,x:146.1,y:281.5}).wait(1).to({graphics:mask_1_graphics_273,x:146,y:287.7}).wait(1).to({graphics:mask_1_graphics_274,x:146,y:293.9}).wait(1).to({graphics:mask_1_graphics_275,x:145.9,y:300}).wait(60).to({graphics:mask_1_graphics_335,x:145.9,y:179.1}).wait(1).to({graphics:mask_1_graphics_336,x:145.9,y:179}).wait(1).to({graphics:mask_1_graphics_337,x:145.9,y:178.9}).wait(1).to({graphics:mask_1_graphics_338,x:145.9,y:178.6}).wait(1).to({graphics:mask_1_graphics_339,x:145.9,y:178.3}).wait(1).to({graphics:mask_1_graphics_340,x:145.9,y:177.9}).wait(1).to({graphics:mask_1_graphics_341,x:145.9,y:177.4}).wait(1).to({graphics:mask_1_graphics_342,x:145.9,y:176.8}).wait(1).to({graphics:mask_1_graphics_343,x:145.9,y:176.2}).wait(1).to({graphics:mask_1_graphics_344,x:145.9,y:175.4}).wait(1).to({graphics:mask_1_graphics_345,x:145.9,y:174.5}).wait(1).to({graphics:mask_1_graphics_346,x:145.9,y:173.6}).wait(1).to({graphics:mask_1_graphics_347,x:145.9,y:172.5}).wait(1).to({graphics:mask_1_graphics_348,x:145.9,y:171.4}).wait(1).to({graphics:mask_1_graphics_349,x:145.9,y:170.2}).wait(1).to({graphics:mask_1_graphics_350,x:145.9,y:168.9}).wait(1).to({graphics:mask_1_graphics_351,x:146,y:167.5}).wait(1).to({graphics:mask_1_graphics_352,x:146,y:166}).wait(1).to({graphics:mask_1_graphics_353,x:146,y:164.4}).wait(1).to({graphics:mask_1_graphics_354,x:146,y:162.7}).wait(1).to({graphics:mask_1_graphics_355,x:146,y:161}).wait(1).to({graphics:mask_1_graphics_356,x:146,y:159.1}).wait(1).to({graphics:mask_1_graphics_357,x:146,y:157.2}).wait(1).to({graphics:mask_1_graphics_358,x:146,y:155.1}).wait(1).to({graphics:mask_1_graphics_359,x:146.1,y:153}).wait(1).to({graphics:mask_1_graphics_360,x:146.1,y:150.8}).wait(1).to({graphics:mask_1_graphics_361,x:146.1,y:148.5}).wait(1).to({graphics:mask_1_graphics_362,x:146.1,y:146.1}).wait(1).to({graphics:mask_1_graphics_363,x:146.1,y:143.6}).wait(1).to({graphics:mask_1_graphics_364,x:146.1,y:141}).wait(1).to({graphics:mask_1_graphics_365,x:146.2,y:138.4}).wait(1).to({graphics:mask_1_graphics_366,x:146.2,y:135.6}).wait(1).to({graphics:mask_1_graphics_367,x:146.2,y:132.7}).wait(1).to({graphics:mask_1_graphics_368,x:146.2,y:129.8}).wait(1).to({graphics:mask_1_graphics_369,x:146.2,y:126.8}).wait(1).to({graphics:mask_1_graphics_370,x:146.3,y:123.7}).wait(1).to({graphics:mask_1_graphics_371,x:146.3,y:120.4}).wait(1).to({graphics:mask_1_graphics_372,x:146.3,y:117.1}).wait(1).to({graphics:mask_1_graphics_373,x:146.3,y:113.8}).wait(1).to({graphics:mask_1_graphics_374,x:146.4,y:110.3}).wait(1).to({graphics:mask_1_graphics_375,x:146.4,y:106.7}).wait(1).to({graphics:mask_1_graphics_376,x:146.4,y:103}).wait(1).to({graphics:mask_1_graphics_377,x:146.4,y:99.3}).wait(1).to({graphics:mask_1_graphics_378,x:146.5,y:95.4}).wait(1).to({graphics:mask_1_graphics_379,x:146.5,y:91.5}).wait(1).to({graphics:mask_1_graphics_380,x:146.5,y:87.5}).wait(1).to({graphics:mask_1_graphics_381,x:146.6,y:83.4}).wait(1).to({graphics:mask_1_graphics_382,x:146.6,y:79.2}).wait(1).to({graphics:mask_1_graphics_383,x:146.6,y:74.9}).wait(1).to({graphics:mask_1_graphics_384,x:146.6,y:70.5}).wait(1).to({graphics:mask_1_graphics_385,x:146.7,y:66}).wait(1).to({graphics:mask_1_graphics_386,x:146.7,y:61.4}).wait(1).to({graphics:mask_1_graphics_387,x:146.7,y:56.8}).wait(1).to({graphics:mask_1_graphics_388,x:146.8,y:52}).wait(1).to({graphics:mask_1_graphics_389,x:146.8,y:47.2}).wait(1).to({graphics:mask_1_graphics_390,x:146.8,y:42.3}).wait(1).to({graphics:mask_1_graphics_391,x:146.9,y:37.2}).wait(1).to({graphics:mask_1_graphics_392,x:146.9,y:32.1}).wait(1).to({graphics:mask_1_graphics_393,x:147,y:26.9}).wait(1).to({graphics:mask_1_graphics_394,x:147,y:21.6}).wait(1).to({graphics:mask_1_graphics_395,x:147,y:16.3}).wait(1).to({graphics:mask_1_graphics_396,x:147.1,y:10.8}).wait(1).to({graphics:mask_1_graphics_397,x:147.1,y:5.2}).wait(1).to({graphics:mask_1_graphics_398,x:147.1,y:-0.4}).wait(1).to({graphics:mask_1_graphics_399,x:147.2,y:-6.2}).wait(1).to({graphics:mask_1_graphics_400,x:147.2,y:-12}).wait(1).to({graphics:mask_1_graphics_401,x:147.3,y:-17.9}).wait(1).to({graphics:mask_1_graphics_402,x:147.3,y:-23.9}).wait(1).to({graphics:mask_1_graphics_403,x:147.4,y:-30}).wait(1).to({graphics:mask_1_graphics_404,x:147.4,y:-36.2}).wait(1).to({graphics:mask_1_graphics_405,x:147.4,y:-42.5}).wait(1).to({graphics:mask_1_graphics_406,x:147.5,y:-45.8}).wait(1).to({graphics:mask_1_graphics_407,x:147.5,y:-46.2}).wait(1).to({graphics:mask_1_graphics_408,x:147.6,y:-46.7}).wait(1).to({graphics:mask_1_graphics_409,x:147.6,y:-47.1}).wait(1).to({graphics:mask_1_graphics_410,x:147.7,y:-47.6}).wait(1).to({graphics:mask_1_graphics_411,x:147.7,y:-48.1}).wait(1).to({graphics:mask_1_graphics_412,x:147.8,y:-48.5}).wait(1).to({graphics:mask_1_graphics_413,x:147.9,y:-49}).wait(62));

	// copy_2
	this.copy_2_mc = new lib.copy_2_mc();
	this.copy_2_mc.setTransform(151.8,169.5);
	this.copy_2_mc._off = true;

	this.copy_2_mc.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.copy_2_mc).wait(197).to({_off:false},0).wait(138).to({_off:true},78).wait(62));

	// HandsNew
	this.instance_2 = new lib.Hands_1();
	this.instance_2.setTransform(0,-566,2.217,2.217);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:1,scaleY:1,x:80,y:226},59,cjs.Ease.get(1)).to({y:-264},78,cjs.Ease.get(-1)).to({_off:true},256).wait(82));

	// Mask --- f1 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_59 = new cjs.Graphics().p("EgXKAjiIAAg2MAu2AAAIAAA2g");
	var mask_2_graphics_60 = new cjs.Graphics().p("EgXKAjiIAAg3MAu1AAAIAAA3g");
	var mask_2_graphics_61 = new cjs.Graphics().p("EgXKAjiIAAg6MAu2AAAIAAA6g");
	var mask_2_graphics_62 = new cjs.Graphics().p("EgXKAjiIAAg+MAu2AAAIAAA+g");
	var mask_2_graphics_63 = new cjs.Graphics().p("EgXKAjhIAAhDMAu2AAAIAABDg");
	var mask_2_graphics_64 = new cjs.Graphics().p("EgXKAjhIAAhLMAu2AAAIAABLg");
	var mask_2_graphics_65 = new cjs.Graphics().p("EgXKAjhIAAhUMAu2AAAIAABUg");
	var mask_2_graphics_66 = new cjs.Graphics().p("EgXKAjgIAAheMAu2AAAIAABeg");
	var mask_2_graphics_67 = new cjs.Graphics().p("EgXKAjgIAAhrMAu2AAAIAABrg");
	var mask_2_graphics_68 = new cjs.Graphics().p("EgXKAjfIAAh5MAu2AAAIAAB5g");
	var mask_2_graphics_69 = new cjs.Graphics().p("EgXKAjfIAAiJMAu2AAAIAACJg");
	var mask_2_graphics_70 = new cjs.Graphics().p("EgXKAjeIAAiaMAu2AAAIAACag");
	var mask_2_graphics_71 = new cjs.Graphics().p("EgXKAjdIAAitMAu2AAAIAACtg");
	var mask_2_graphics_72 = new cjs.Graphics().p("EgXKAjcIAAjBMAu2AAAIAADBg");
	var mask_2_graphics_73 = new cjs.Graphics().p("EgXKAjbIAAjYMAu2AAAIAADYg");
	var mask_2_graphics_74 = new cjs.Graphics().p("EgXKAjbIAAjxMAu2AAAIAADxg");
	var mask_2_graphics_75 = new cjs.Graphics().p("EgXKAjZIAAkJMAu2AAAIAAEJg");
	var mask_2_graphics_76 = new cjs.Graphics().p("EgXKAjYIAAklMAu2AAAIAAElg");
	var mask_2_graphics_77 = new cjs.Graphics().p("EgXKAjXIAAlCMAu2AAAIAAFCg");
	var mask_2_graphics_78 = new cjs.Graphics().p("EgXKAjWIAAlgMAu2AAAIAAFgg");
	var mask_2_graphics_79 = new cjs.Graphics().p("EgXKAjVIAAmBMAu2AAAIAAGBg");
	var mask_2_graphics_80 = new cjs.Graphics().p("EgXKAjTIAAmiMAu2AAAIAAGig");
	var mask_2_graphics_81 = new cjs.Graphics().p("EgXKAjSIAAnGMAu2AAAIAAHGg");
	var mask_2_graphics_82 = new cjs.Graphics().p("EgXKAjQIAAnrMAu2AAAIAAHrg");
	var mask_2_graphics_83 = new cjs.Graphics().p("EgXKAjPIAAoSMAu2AAAIAAISg");
	var mask_2_graphics_84 = new cjs.Graphics().p("EgXKAjNIAAo6MAu2AAAIAAI6g");
	var mask_2_graphics_85 = new cjs.Graphics().p("EgXKAjLIAApkMAu2AAAIAAJkg");
	var mask_2_graphics_86 = new cjs.Graphics().p("EgXKAjKIAAqRMAu2AAAIAAKRg");
	var mask_2_graphics_87 = new cjs.Graphics().p("EgXJAjIIAAq+MAu2AAAIAAK+g");
	var mask_2_graphics_88 = new cjs.Graphics().p("EgXJAjGIAArtMAu2AAAIAALtg");
	var mask_2_graphics_89 = new cjs.Graphics().p("EgXJAjEIAAseMAu2AAAIAAMeg");
	var mask_2_graphics_90 = new cjs.Graphics().p("EgXJAjCIAAtQMAu2AAAIAANQg");
	var mask_2_graphics_91 = new cjs.Graphics().p("EgXJAjAIAAuEMAu2AAAIAAOEg");
	var mask_2_graphics_92 = new cjs.Graphics().p("EgXJAi+IAAu6MAu2AAAIAAO6g");
	var mask_2_graphics_93 = new cjs.Graphics().p("EgXJAi7IAAvxMAu2AAAIAAPxg");
	var mask_2_graphics_94 = new cjs.Graphics().p("EgXJAi5IAAwqMAu2AAAIAAQqg");
	var mask_2_graphics_95 = new cjs.Graphics().p("EgXJAi3IAAxlMAu2AAAIAARlg");
	var mask_2_graphics_96 = new cjs.Graphics().p("EgXJAi0IAAyhMAu2AAAIAAShg");
	var mask_2_graphics_97 = new cjs.Graphics().p("EgXJAiyIAAzfMAu2AAAIAATfg");
	var mask_2_graphics_98 = new cjs.Graphics().p("EgXJAivIAA0fMAu2AAAIAAUfg");
	var mask_2_graphics_99 = new cjs.Graphics().p("EgXIAitIAA1gMAu2AAAIAAVgg");
	var mask_2_graphics_100 = new cjs.Graphics().p("EgXIAiqIAA2jMAu2AAAIAAWjg");
	var mask_2_graphics_101 = new cjs.Graphics().p("EgXIAinIAA3nMAu2AAAIAAXng");
	var mask_2_graphics_102 = new cjs.Graphics().p("EgXIAikIAA4uMAu2AAAIAAYug");
	var mask_2_graphics_103 = new cjs.Graphics().p("EgXIAihIAA51MAu2AAAIAAZ1g");
	var mask_2_graphics_104 = new cjs.Graphics().p("EgXIAieIAA6/MAu2AAAIAAa/g");
	var mask_2_graphics_105 = new cjs.Graphics().p("EgXIAibIAA8KMAu2AAAIAAcKg");
	var mask_2_graphics_106 = new cjs.Graphics().p("EgXIAiYIAA9XMAu2AAAIAAdXg");
	var mask_2_graphics_107 = new cjs.Graphics().p("EgXHAiVIAA+mMAu2AAAIAAemg");
	var mask_2_graphics_108 = new cjs.Graphics().p("EgXHAiSIAA/2MAu2AAAIAAf2g");
	var mask_2_graphics_109 = new cjs.Graphics().p("EgXHAiOMAAAghHMAu2AAAMAAAAhHg");
	var mask_2_graphics_110 = new cjs.Graphics().p("EgXHAiLMAAAgiZMAu2AAAMAAAAiZg");
	var mask_2_graphics_111 = new cjs.Graphics().p("EgXHAiIMAAAgjuMAu2AAAMAAAAjug");
	var mask_2_graphics_112 = new cjs.Graphics().p("EgXHAiEMAAAglFMAu2AAAMAAAAlFg");
	var mask_2_graphics_113 = new cjs.Graphics().p("EgXHAiBMAAAgmeMAu2AAAMAAAAmeg");
	var mask_2_graphics_114 = new cjs.Graphics().p("EgXHAh9MAAAgn3MAu2AAAMAAAAn3g");
	var mask_2_graphics_115 = new cjs.Graphics().p("EgXGAh5MAAAgpTMAu2AAAMAAAApTg");
	var mask_2_graphics_116 = new cjs.Graphics().p("EgXGAh1MAAAgqwMAu2AAAMAAAAqwg");
	var mask_2_graphics_117 = new cjs.Graphics().p("EgXGAhyMAAAgsQMAu2AAAMAAAAsQg");
	var mask_2_graphics_118 = new cjs.Graphics().p("EgXGAhuMAAAgtwMAu2AAAMAAAAtwg");
	var mask_2_graphics_119 = new cjs.Graphics().p("EgXGAhqMAAAgvTMAu2AAAMAAAAvTg");
	var mask_2_graphics_120 = new cjs.Graphics().p("EgXGAhmMAAAgw3MAu2AAAMAAAAw3g");
	var mask_2_graphics_121 = new cjs.Graphics().p("EgXFAhiMAAAgycMAu2AAAMAAAAycg");
	var mask_2_graphics_122 = new cjs.Graphics().p("EgXFAhdMAAAg0DMAu2AAAMAAAA0Dg");
	var mask_2_graphics_123 = new cjs.Graphics().p("EgXFAhZMAAAg1sMAu2AAAMAAAA1sg");
	var mask_2_graphics_124 = new cjs.Graphics().p("EgXFAhVMAAAg3XMAu2AAAMAAAA3Xg");
	var mask_2_graphics_125 = new cjs.Graphics().p("EgXFAhQMAAAg5DMAu2AAAMAAAA5Dg");
	var mask_2_graphics_126 = new cjs.Graphics().p("EgXFAhMMAAAg6xMAu2AAAMAAAA6xg");
	var mask_2_graphics_127 = new cjs.Graphics().p("EgXEAhHMAAAg8gMAu2AAAMAAAA8gg");
	var mask_2_graphics_128 = new cjs.Graphics().p("EgXEAhDMAAAg+SMAu2AAAMAAAA+Sg");
	var mask_2_graphics_129 = new cjs.Graphics().p("EgXEAg+MAAAhAEMAu2AAAMAAABAEg");
	var mask_2_graphics_130 = new cjs.Graphics().p("EgXEAg9MAAAhB5MAu2AAAMAAABB5g");
	var mask_2_graphics_131 = new cjs.Graphics().p("EgXEAh4MAAAhDvMAu2AAAMAAABDvg");
	var mask_2_graphics_132 = new cjs.Graphics().p("EgXDAi0MAAAhFnMAu2AAAMAAABFng");
	var mask_2_graphics_133 = new cjs.Graphics().p("EgXDAjxMAAAhHhMAu2AAAMAAABHhg");
	var mask_2_graphics_134 = new cjs.Graphics().p("EgXDAkuMAAAhJbMAu2AAAMAAABJbg");
	var mask_2_graphics_135 = new cjs.Graphics().p("EgXDAltMAAAhLZMAu2AAAMAAABLZg");
	var mask_2_graphics_136 = new cjs.Graphics().p("EgXDAmsMAAAhNXMAu2AAAMAAABNXg");
	var mask_2_graphics_137 = new cjs.Graphics().p("EgXDAnsMAAAhPXMAu3AAAMAAABPXg");
	var mask_2_graphics_197 = new cjs.Graphics().p("EgXDArSMAAAhPXMAu3AAAMAAABPXg");
	var mask_2_graphics_198 = new cjs.Graphics().p("EgXDArSMAAAhPWMAu3AAAMAAABPWg");
	var mask_2_graphics_199 = new cjs.Graphics().p("EgXDArSMAAAhPTMAu3AAAMAAABPTg");
	var mask_2_graphics_200 = new cjs.Graphics().p("EgXDArTMAAAhPQMAu3AAAMAAABPQg");
	var mask_2_graphics_201 = new cjs.Graphics().p("EgXDArUMAAAhPLMAu3AAAMAAABPLg");
	var mask_2_graphics_202 = new cjs.Graphics().p("EgXDArVMAAAhPEMAu3AAAMAAABPEg");
	var mask_2_graphics_203 = new cjs.Graphics().p("EgXDArWMAAAhO8MAu3AAAMAAABO8g");
	var mask_2_graphics_204 = new cjs.Graphics().p("EgXDArXMAAAhOxMAu3AAAMAAABOxg");
	var mask_2_graphics_205 = new cjs.Graphics().p("EgXDArZMAAAhOmMAu3AAAMAAABOmg");
	var mask_2_graphics_206 = new cjs.Graphics().p("EgXDArbMAAAhOaMAu3AAAMAAABOag");
	var mask_2_graphics_207 = new cjs.Graphics().p("EgXDArdMAAAhOLMAu3AAAMAAABOLg");
	var mask_2_graphics_208 = new cjs.Graphics().p("EgXDArgMAAAhN8MAu3AAAMAAABN8g");
	var mask_2_graphics_209 = new cjs.Graphics().p("EgXDAriMAAAhNqMAu3AAAMAAABNqg");
	var mask_2_graphics_210 = new cjs.Graphics().p("EgXDArlMAAAhNXMAu3AAAMAAABNXg");
	var mask_2_graphics_211 = new cjs.Graphics().p("EgXDAroMAAAhNDMAu3AAAMAAABNDg");
	var mask_2_graphics_212 = new cjs.Graphics().p("EgXDArsMAAAhMtMAu3AAAMAAABMtg");
	var mask_2_graphics_213 = new cjs.Graphics().p("EgXDArvMAAAhMVMAu3AAAMAAABMVg");
	var mask_2_graphics_214 = new cjs.Graphics().p("EgXDArzMAAAhL9MAu3AAAMAAABL9g");
	var mask_2_graphics_215 = new cjs.Graphics().p("EgXDAr3MAAAhLiMAu3AAAMAAABLig");
	var mask_2_graphics_216 = new cjs.Graphics().p("EgXDAr8MAAAhLHMAu3AAAMAAABLHg");
	var mask_2_graphics_217 = new cjs.Graphics().p("EgXDAsAMAAAhKpMAu3AAAMAAABKpg");
	var mask_2_graphics_218 = new cjs.Graphics().p("EgXDAsFMAAAhKKMAu3AAAMAAABKKg");
	var mask_2_graphics_219 = new cjs.Graphics().p("EgXDAsKMAAAhJqMAu3AAAMAAABJqg");
	var mask_2_graphics_220 = new cjs.Graphics().p("EgXDAsPMAAAhJIMAu3AAAMAAABJIg");
	var mask_2_graphics_221 = new cjs.Graphics().p("EgXDAsVMAAAhIlMAu3AAAMAAABIlg");
	var mask_2_graphics_222 = new cjs.Graphics().p("EgXDAsaMAAAhIAMAu3AAAMAAABIAg");
	var mask_2_graphics_223 = new cjs.Graphics().p("EgXDAsgMAAAhHZMAu3AAAMAAABHZg");
	var mask_2_graphics_224 = new cjs.Graphics().p("EgXDAsnMAAAhGyMAu3AAAMAAABGyg");
	var mask_2_graphics_225 = new cjs.Graphics().p("EgXDAstMAAAhGIMAu3AAAMAAABGIg");
	var mask_2_graphics_226 = new cjs.Graphics().p("EgXDAs0MAAAhFeMAu3AAAMAAABFeg");
	var mask_2_graphics_227 = new cjs.Graphics().p("EgXDAs7MAAAhExMAu3AAAMAAABExg");
	var mask_2_graphics_228 = new cjs.Graphics().p("EgXDAtCMAAAhEDMAu3AAAMAAABEDg");
	var mask_2_graphics_229 = new cjs.Graphics().p("EgXDAtJMAAAhDUMAu3AAAMAAABDUg");
	var mask_2_graphics_230 = new cjs.Graphics().p("EgXDAtRMAAAhCjMAu3AAAMAAABCjg");
	var mask_2_graphics_231 = new cjs.Graphics().p("EgXDAtYMAAAhBwMAu3AAAMAAABBwg");
	var mask_2_graphics_232 = new cjs.Graphics().p("EgXDAtgMAAAhA8MAu3AAAMAAABA8g");
	var mask_2_graphics_233 = new cjs.Graphics().p("EgXDAtpMAAAhAHMAu3AAAMAAABAHg");
	var mask_2_graphics_234 = new cjs.Graphics().p("EgXDAtxMAAAg/QMAu3AAAMAAAA/Qg");
	var mask_2_graphics_235 = new cjs.Graphics().p("EgXDAt6MAAAg+XMAu3AAAMAAAA+Xg");
	var mask_2_graphics_236 = new cjs.Graphics().p("EgXDAuDMAAAg9dMAu3AAAMAAAA9dg");
	var mask_2_graphics_237 = new cjs.Graphics().p("EgXDAuMMAAAg8iMAu3AAAMAAAA8ig");
	var mask_2_graphics_238 = new cjs.Graphics().p("EgXDAuWMAAAg7lMAu3AAAMAAAA7lg");
	var mask_2_graphics_239 = new cjs.Graphics().p("EgXDAufMAAAg6mMAu3AAAMAAAA6mg");
	var mask_2_graphics_240 = new cjs.Graphics().p("EgXDAupMAAAg5mMAu3AAAMAAAA5mg");
	var mask_2_graphics_241 = new cjs.Graphics().p("EgXDAuzMAAAg4lMAu3AAAMAAAA4lg");
	var mask_2_graphics_242 = new cjs.Graphics().p("EgXDAu+MAAAg3iMAu3AAAMAAAA3ig");
	var mask_2_graphics_243 = new cjs.Graphics().p("EgXDAvIMAAAg2dMAu3AAAMAAAA2dg");
	var mask_2_graphics_244 = new cjs.Graphics().p("EgXDAvTMAAAg1XMAu3AAAMAAAA1Xg");
	var mask_2_graphics_245 = new cjs.Graphics().p("EgXDAveMAAAg0QMAu3AAAMAAAA0Qg");
	var mask_2_graphics_246 = new cjs.Graphics().p("EgXDAvpMAAAgzGMAu3AAAMAAAAzGg");
	var mask_2_graphics_247 = new cjs.Graphics().p("EgXDAv1MAAAgx8MAu3AAAMAAAAx8g");
	var mask_2_graphics_248 = new cjs.Graphics().p("EgXDAwBMAAAgwwMAu3AAAMAAAAwwg");
	var mask_2_graphics_249 = new cjs.Graphics().p("EgXDAwNMAAAgvlMAu3AAAMAAAAvlg");
	var mask_2_graphics_250 = new cjs.Graphics().p("EgXDAwZMAAAguWMAu3AAAMAAAAuWg");
	var mask_2_graphics_251 = new cjs.Graphics().p("EgXDAwlMAAAgtFMAu3AAAMAAAAtFg");
	var mask_2_graphics_252 = new cjs.Graphics().p("EgXDAwyMAAAgrzMAu3AAAMAAAArzg");
	var mask_2_graphics_253 = new cjs.Graphics().p("EgXDAw/MAAAgqfMAu3AAAMAAAAqfg");
	var mask_2_graphics_254 = new cjs.Graphics().p("EgXDAxMMAAAgpKMAu3AAAMAAAApKg");
	var mask_2_graphics_255 = new cjs.Graphics().p("EgXDAxaMAAAgn0MAu3AAAMAAAAn0g");
	var mask_2_graphics_256 = new cjs.Graphics().p("EgXDAxnMAAAgmbMAu3AAAMAAAAmbg");
	var mask_2_graphics_257 = new cjs.Graphics().p("EgXDAx1MAAAglCMAu3AAAMAAAAlCg");
	var mask_2_graphics_258 = new cjs.Graphics().p("EgXDAyDMAAAgjnMAu3AAAMAAAAjng");
	var mask_2_graphics_259 = new cjs.Graphics().p("EgXDAySMAAAgiLMAu3AAAMAAAAiLg");
	var mask_2_graphics_260 = new cjs.Graphics().p("EgXDAygMAAAggsMAu3AAAMAAAAgsg");
	var mask_2_graphics_261 = new cjs.Graphics().p("EgXDAyvIAA/NMAu3AAAIAAfNg");
	var mask_2_graphics_262 = new cjs.Graphics().p("EgXDAy+IAA9sMAu3AAAIAAdsg");
	var mask_2_graphics_263 = new cjs.Graphics().p("EgXDAzNIAA8JMAu3AAAIAAcJg");
	var mask_2_graphics_264 = new cjs.Graphics().p("EgXDAzdIAA6lMAu3AAAIAAalg");
	var mask_2_graphics_265 = new cjs.Graphics().p("EgXDAzsIAA4/MAu3AAAIAAY/g");
	var mask_2_graphics_266 = new cjs.Graphics().p("EgXDAz8IAA3XMAu3AAAIAAXXg");
	var mask_2_graphics_267 = new cjs.Graphics().p("EgXDA0NIAA1wMAu3AAAIAAVwg");
	var mask_2_graphics_268 = new cjs.Graphics().p("EgXDA0dIAA0FMAu3AAAIAAUFg");
	var mask_2_graphics_269 = new cjs.Graphics().p("EgXDA0uIAAyaMAu3AAAIAASag");
	var mask_2_graphics_270 = new cjs.Graphics().p("EgXDA0/IAAwtMAu3AAAIAAQtg");
	var mask_2_graphics_271 = new cjs.Graphics().p("EgXDA1QIAAu+MAu3AAAIAAO+g");
	var mask_2_graphics_272 = new cjs.Graphics().p("EgXDA1hIAAtNMAu3AAAIAANNg");
	var mask_2_graphics_273 = new cjs.Graphics().p("EgXDA1zIAArcMAu3AAAIAALcg");
	var mask_2_graphics_274 = new cjs.Graphics().p("EgXDA2FIAAppMAu3AAAIAAJpg");
	var mask_2_graphics_275 = new cjs.Graphics().p("EgXDA2XIAAn0MAu3AAAIAAH0g");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(59).to({graphics:mask_2_graphics_59,x:151.6,y:227.5}).wait(1).to({graphics:mask_2_graphics_60,x:151.6,y:227.4}).wait(1).to({graphics:mask_2_graphics_61,x:151.6,y:227.4}).wait(1).to({graphics:mask_2_graphics_62,x:151.6,y:227.4}).wait(1).to({graphics:mask_2_graphics_63,x:151.6,y:227.4}).wait(1).to({graphics:mask_2_graphics_64,x:151.6,y:227.4}).wait(1).to({graphics:mask_2_graphics_65,x:151.6,y:227.3}).wait(1).to({graphics:mask_2_graphics_66,x:151.6,y:227.3}).wait(1).to({graphics:mask_2_graphics_67,x:151.6,y:227.2}).wait(1).to({graphics:mask_2_graphics_68,x:151.6,y:227.2}).wait(1).to({graphics:mask_2_graphics_69,x:151.6,y:227.1}).wait(1).to({graphics:mask_2_graphics_70,x:151.6,y:227}).wait(1).to({graphics:mask_2_graphics_71,x:151.6,y:227}).wait(1).to({graphics:mask_2_graphics_72,x:151.6,y:226.9}).wait(1).to({graphics:mask_2_graphics_73,x:151.6,y:226.8}).wait(1).to({graphics:mask_2_graphics_74,x:151.6,y:226.7}).wait(1).to({graphics:mask_2_graphics_75,x:151.6,y:226.6}).wait(1).to({graphics:mask_2_graphics_76,x:151.6,y:226.5}).wait(1).to({graphics:mask_2_graphics_77,x:151.6,y:226.4}).wait(1).to({graphics:mask_2_graphics_78,x:151.6,y:226.2}).wait(1).to({graphics:mask_2_graphics_79,x:151.7,y:226.1}).wait(1).to({graphics:mask_2_graphics_80,x:151.7,y:226}).wait(1).to({graphics:mask_2_graphics_81,x:151.7,y:225.8}).wait(1).to({graphics:mask_2_graphics_82,x:151.7,y:225.7}).wait(1).to({graphics:mask_2_graphics_83,x:151.7,y:225.5}).wait(1).to({graphics:mask_2_graphics_84,x:151.7,y:225.4}).wait(1).to({graphics:mask_2_graphics_85,x:151.7,y:225.2}).wait(1).to({graphics:mask_2_graphics_86,x:151.7,y:225}).wait(1).to({graphics:mask_2_graphics_87,x:151.7,y:224.8}).wait(1).to({graphics:mask_2_graphics_88,x:151.7,y:224.6}).wait(1).to({graphics:mask_2_graphics_89,x:151.7,y:224.4}).wait(1).to({graphics:mask_2_graphics_90,x:151.7,y:224.2}).wait(1).to({graphics:mask_2_graphics_91,x:151.7,y:224}).wait(1).to({graphics:mask_2_graphics_92,x:151.7,y:223.8}).wait(1).to({graphics:mask_2_graphics_93,x:151.8,y:223.6}).wait(1).to({graphics:mask_2_graphics_94,x:151.8,y:223.4}).wait(1).to({graphics:mask_2_graphics_95,x:151.8,y:223.1}).wait(1).to({graphics:mask_2_graphics_96,x:151.8,y:222.9}).wait(1).to({graphics:mask_2_graphics_97,x:151.8,y:222.6}).wait(1).to({graphics:mask_2_graphics_98,x:151.8,y:222.4}).wait(1).to({graphics:mask_2_graphics_99,x:151.8,y:222.1}).wait(1).to({graphics:mask_2_graphics_100,x:151.8,y:221.8}).wait(1).to({graphics:mask_2_graphics_101,x:151.8,y:221.6}).wait(1).to({graphics:mask_2_graphics_102,x:151.8,y:221.3}).wait(1).to({graphics:mask_2_graphics_103,x:151.9,y:221}).wait(1).to({graphics:mask_2_graphics_104,x:151.9,y:220.7}).wait(1).to({graphics:mask_2_graphics_105,x:151.9,y:220.4}).wait(1).to({graphics:mask_2_graphics_106,x:151.9,y:220.1}).wait(1).to({graphics:mask_2_graphics_107,x:151.9,y:219.8}).wait(1).to({graphics:mask_2_graphics_108,x:151.9,y:219.4}).wait(1).to({graphics:mask_2_graphics_109,x:151.9,y:219.1}).wait(1).to({graphics:mask_2_graphics_110,x:151.9,y:218.8}).wait(1).to({graphics:mask_2_graphics_111,x:152,y:218.4}).wait(1).to({graphics:mask_2_graphics_112,x:152,y:218.1}).wait(1).to({graphics:mask_2_graphics_113,x:152,y:217.7}).wait(1).to({graphics:mask_2_graphics_114,x:152,y:217.3}).wait(1).to({graphics:mask_2_graphics_115,x:152,y:217}).wait(1).to({graphics:mask_2_graphics_116,x:152,y:216.6}).wait(1).to({graphics:mask_2_graphics_117,x:152,y:216.2}).wait(1).to({graphics:mask_2_graphics_118,x:152.1,y:215.8}).wait(1).to({graphics:mask_2_graphics_119,x:152.1,y:215.4}).wait(1).to({graphics:mask_2_graphics_120,x:152.1,y:215}).wait(1).to({graphics:mask_2_graphics_121,x:152.1,y:214.6}).wait(1).to({graphics:mask_2_graphics_122,x:152.1,y:214.2}).wait(1).to({graphics:mask_2_graphics_123,x:152.1,y:213.8}).wait(1).to({graphics:mask_2_graphics_124,x:152.2,y:213.3}).wait(1).to({graphics:mask_2_graphics_125,x:152.2,y:212.9}).wait(1).to({graphics:mask_2_graphics_126,x:152.2,y:212.4}).wait(1).to({graphics:mask_2_graphics_127,x:152.2,y:212}).wait(1).to({graphics:mask_2_graphics_128,x:152.2,y:211.5}).wait(1).to({graphics:mask_2_graphics_129,x:152.2,y:211.1}).wait(1).to({graphics:mask_2_graphics_130,x:152.3,y:210.3}).wait(1).to({graphics:mask_2_graphics_131,x:152.3,y:203.4}).wait(1).to({graphics:mask_2_graphics_132,x:152.3,y:196.4}).wait(1).to({graphics:mask_2_graphics_133,x:152.3,y:189.4}).wait(1).to({graphics:mask_2_graphics_134,x:152.3,y:182.2}).wait(1).to({graphics:mask_2_graphics_135,x:152.4,y:175}).wait(1).to({graphics:mask_2_graphics_136,x:152.4,y:167.6}).wait(1).to({graphics:mask_2_graphics_137,x:152.4,y:160}).wait(60).to({graphics:mask_2_graphics_197,x:152.4,y:277}).wait(1).to({graphics:mask_2_graphics_198,x:152.4,y:277}).wait(1).to({graphics:mask_2_graphics_199,x:152.4,y:277.1}).wait(1).to({graphics:mask_2_graphics_200,x:152.4,y:277.1}).wait(1).to({graphics:mask_2_graphics_201,x:152.4,y:277.2}).wait(1).to({graphics:mask_2_graphics_202,x:152.4,y:277.3}).wait(1).to({graphics:mask_2_graphics_203,x:152.4,y:277.4}).wait(1).to({graphics:mask_2_graphics_204,x:152.4,y:277.6}).wait(1).to({graphics:mask_2_graphics_205,x:152.4,y:277.8}).wait(1).to({graphics:mask_2_graphics_206,x:152.4,y:278}).wait(1).to({graphics:mask_2_graphics_207,x:152.4,y:278.2}).wait(1).to({graphics:mask_2_graphics_208,x:152.4,y:278.4}).wait(1).to({graphics:mask_2_graphics_209,x:152.4,y:278.7}).wait(1).to({graphics:mask_2_graphics_210,x:152.4,y:279}).wait(1).to({graphics:mask_2_graphics_211,x:152.4,y:279.3}).wait(1).to({graphics:mask_2_graphics_212,x:152.4,y:279.6}).wait(1).to({graphics:mask_2_graphics_213,x:152.4,y:280}).wait(1).to({graphics:mask_2_graphics_214,x:152.4,y:280.4}).wait(1).to({graphics:mask_2_graphics_215,x:152.4,y:280.8}).wait(1).to({graphics:mask_2_graphics_216,x:152.4,y:281.2}).wait(1).to({graphics:mask_2_graphics_217,x:152.4,y:281.7}).wait(1).to({graphics:mask_2_graphics_218,x:152.4,y:282.2}).wait(1).to({graphics:mask_2_graphics_219,x:152.4,y:282.7}).wait(1).to({graphics:mask_2_graphics_220,x:152.4,y:283.2}).wait(1).to({graphics:mask_2_graphics_221,x:152.4,y:283.7}).wait(1).to({graphics:mask_2_graphics_222,x:152.4,y:284.3}).wait(1).to({graphics:mask_2_graphics_223,x:152.4,y:284.9}).wait(1).to({graphics:mask_2_graphics_224,x:152.4,y:285.5}).wait(1).to({graphics:mask_2_graphics_225,x:152.4,y:286.1}).wait(1).to({graphics:mask_2_graphics_226,x:152.4,y:286.8}).wait(1).to({graphics:mask_2_graphics_227,x:152.4,y:287.5}).wait(1).to({graphics:mask_2_graphics_228,x:152.4,y:288.2}).wait(1).to({graphics:mask_2_graphics_229,x:152.4,y:288.9}).wait(1).to({graphics:mask_2_graphics_230,x:152.4,y:289.7}).wait(1).to({graphics:mask_2_graphics_231,x:152.4,y:290.5}).wait(1).to({graphics:mask_2_graphics_232,x:152.4,y:291.3}).wait(1).to({graphics:mask_2_graphics_233,x:152.4,y:292.1}).wait(1).to({graphics:mask_2_graphics_234,x:152.4,y:293}).wait(1).to({graphics:mask_2_graphics_235,x:152.4,y:293.8}).wait(1).to({graphics:mask_2_graphics_236,x:152.4,y:294.7}).wait(1).to({graphics:mask_2_graphics_237,x:152.4,y:295.7}).wait(1).to({graphics:mask_2_graphics_238,x:152.4,y:296.6}).wait(1).to({graphics:mask_2_graphics_239,x:152.4,y:297.6}).wait(1).to({graphics:mask_2_graphics_240,x:152.4,y:298.6}).wait(1).to({graphics:mask_2_graphics_241,x:152.4,y:299.6}).wait(1).to({graphics:mask_2_graphics_242,x:152.4,y:300.6}).wait(1).to({graphics:mask_2_graphics_243,x:152.4,y:301.7}).wait(1).to({graphics:mask_2_graphics_244,x:152.4,y:302.8}).wait(1).to({graphics:mask_2_graphics_245,x:152.4,y:303.9}).wait(1).to({graphics:mask_2_graphics_246,x:152.4,y:305}).wait(1).to({graphics:mask_2_graphics_247,x:152.4,y:306.1}).wait(1).to({graphics:mask_2_graphics_248,x:152.4,y:307.3}).wait(1).to({graphics:mask_2_graphics_249,x:152.4,y:308.5}).wait(1).to({graphics:mask_2_graphics_250,x:152.4,y:309.7}).wait(1).to({graphics:mask_2_graphics_251,x:152.4,y:311}).wait(1).to({graphics:mask_2_graphics_252,x:152.4,y:312.3}).wait(1).to({graphics:mask_2_graphics_253,x:152.4,y:313.6}).wait(1).to({graphics:mask_2_graphics_254,x:152.4,y:314.9}).wait(1).to({graphics:mask_2_graphics_255,x:152.4,y:316.2}).wait(1).to({graphics:mask_2_graphics_256,x:152.4,y:317.6}).wait(1).to({graphics:mask_2_graphics_257,x:152.4,y:319}).wait(1).to({graphics:mask_2_graphics_258,x:152.4,y:320.4}).wait(1).to({graphics:mask_2_graphics_259,x:152.4,y:321.8}).wait(1).to({graphics:mask_2_graphics_260,x:152.4,y:323.3}).wait(1).to({graphics:mask_2_graphics_261,x:152.4,y:324.7}).wait(1).to({graphics:mask_2_graphics_262,x:152.4,y:326.2}).wait(1).to({graphics:mask_2_graphics_263,x:152.4,y:327.8}).wait(1).to({graphics:mask_2_graphics_264,x:152.4,y:329.3}).wait(1).to({graphics:mask_2_graphics_265,x:152.4,y:330.9}).wait(1).to({graphics:mask_2_graphics_266,x:152.4,y:332.5}).wait(1).to({graphics:mask_2_graphics_267,x:152.4,y:334.1}).wait(1).to({graphics:mask_2_graphics_268,x:152.4,y:335.8}).wait(1).to({graphics:mask_2_graphics_269,x:152.4,y:337.4}).wait(1).to({graphics:mask_2_graphics_270,x:152.4,y:339.1}).wait(1).to({graphics:mask_2_graphics_271,x:152.4,y:340.8}).wait(1).to({graphics:mask_2_graphics_272,x:152.4,y:342.6}).wait(1).to({graphics:mask_2_graphics_273,x:152.4,y:344.3}).wait(1).to({graphics:mask_2_graphics_274,x:152.4,y:346.1}).wait(1).to({graphics:mask_2_graphics_275,x:152.4,y:347.9}).wait(200));

	// copy_1
	this.copy_1_mc = new lib.copy_1_mc();
	this.copy_1_mc.setTransform(152.4,152.8);
	this.copy_1_mc._off = true;

	this.copy_1_mc.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.copy_1_mc).wait(59).to({_off:false},0).wait(138).to({_off:true},196).wait(82));

	// BG
	this.instance_3 = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(475));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-368,-1816.4,1037.6,2416.5);


// stage content:
(lib.squeegee_300x600 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Content
	this.content_mc = new lib.content_mc();
	this.content_mc.setTransform(150,125,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.content_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-218,-1516.4,1037.6,2416.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;