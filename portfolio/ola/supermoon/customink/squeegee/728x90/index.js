(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 728,
	height: 90,
	fps: 70,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bg.jpg?1489014854090", id:"bg"},
		{src:"images/cta.png?1489014854090", id:"cta"},
		{src:"images/hands.png?1489014854090", id:"hands"},
		{src:"images/sprite.png?1489014854091", id:"sprite"}
	]
};



// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,728,90);


(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,238,83);


(lib.hands = function() {
	this.initialize(img.hands);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,600);


(lib.sprite = function() {
	this.initialize(img.sprite);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,526,168);


(lib.Hands_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.hands();
	this.instance.setTransform(-65,-147.9,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-65,-147.9,130,300);


(lib.cta_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-61.3,-19.2,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-61.3,-19.2,119,41.5);


(lib.copy_2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ao5FJIAAqRIRzAAIAAKRg");
	mask.setTransform(510.4,-123.4);

	// Layer 4
	this.instance = new lib.sprite();
	this.instance.setTransform(464.5,-153.5);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgosACLIAAkWMBRZAAAIAAEWg");
	mask_1.setTransform(166.8,-120.4);

	// Layer 6
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-99,-230);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-93.7,-153.5,661.2,63.1);


(lib.copy_1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aa6gXIAAqyIRNAAIAAKyg");
	mask.setTransform(282.3,-71.4);

	// Layer 6
	this.instance = new lib.sprite();
	this.instance.setTransform(464,-137);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Egj2AClIAAlJMBHtAAAIAAFJg");
	mask_1.setTransform(167.2,-104.3);

	// Layer 1
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-65,-184);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-62.4,-137,627.1,63.2);


(lib.copu_3_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnWEmIAApLIOtAAIAAJLg");
	mask.setTransform(512.1,-124.6);

	// Layer 3
	this.instance = new lib.sprite();
	this.instance.setTransform(465,-154);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 4 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Egh+ACLIAAkWMBD9AAAIAAEWg");
	mask_1.setTransform(91.6,-122.1);

	// Layer 5
	this.instance_1 = new lib.sprite();
	this.instance_1.setTransform(-124,-261);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-124,-154,683.3,58.9);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//this.stop();
		
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		hands        = this.hands_mc;
		hands3        = this.hands3_mc;
		cta          = this.cta_mc;
	}
	this.frame_177 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2500);
	}
	this.frame_355 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2500);
	}
	this.frame_533 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 1000);
	}
	this.frame_594 = function() {
		this.stop();
		
		var myBtn = document.getElementById("canvas");
			
		/***[ MOUSE OVER ]***/
		myBtn.addEventListener("mouseover", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
		});
			
		/***[ MOUSE OUT ]***/
		myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(177).call(this.frame_177).wait(178).call(this.frame_355).wait(178).call(this.frame_533).wait(61).call(this.frame_594).wait(1));

	// cta
	this.cta_mc = new lib.cta_mc();
	this.cta_mc.setTransform(535.5,64.8);
	this.cta_mc.alpha = 0;
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(534).to({_off:false},0).to({x:536.5,y:49.8,alpha:1},35,cjs.Ease.get(1)).to({scaleX:1.15,scaleY:1.15},12,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},13,cjs.Ease.get(-1)).wait(1));

	// Hands_3
	this.instance = new lib.Hands_1();
	this.instance.setTransform(880,46,2.217,2.217,90);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(356).to({_off:false},0).to({scaleX:1,scaleY:1,x:100},79,cjs.Ease.get(1)).to({x:820},98,cjs.Ease.get(-1)).wait(62));

	// Mask --- f3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_435 = new cjs.Graphics().p("ABnLpIAA3RIBQAAIAAXRg");
	var mask_graphics_436 = new cjs.Graphics().p("ABnLpIAA3RIBRAAIAAXRg");
	var mask_graphics_437 = new cjs.Graphics().p("ABmLpIAA3RIBTAAIAAXRg");
	var mask_graphics_438 = new cjs.Graphics().p("ABkLpIAA3RIBXAAIAAXRg");
	var mask_graphics_439 = new cjs.Graphics().p("ABhLpIAA3RIBcAAIAAXRg");
	var mask_graphics_440 = new cjs.Graphics().p("ABeLpIAA3RIBjAAIAAXRg");
	var mask_graphics_441 = new cjs.Graphics().p("ABaLpIAA3RIBrAAIAAXRg");
	var mask_graphics_442 = new cjs.Graphics().p("ABVLpIAA3RIB1AAIAAXRg");
	var mask_graphics_443 = new cjs.Graphics().p("ABPLpIAA3RICAAAIAAXRg");
	var mask_graphics_444 = new cjs.Graphics().p("ABJLpIAA3RICNAAIAAXRg");
	var mask_graphics_445 = new cjs.Graphics().p("ABCLpIAA3RICbAAIAAXRg");
	var mask_graphics_446 = new cjs.Graphics().p("AA6LpIAA3RICrAAIAAXRg");
	var mask_graphics_447 = new cjs.Graphics().p("AAxLpIAA3RIC8AAIAAXRg");
	var mask_graphics_448 = new cjs.Graphics().p("AAoLpIAA3RIDPAAIAAXRg");
	var mask_graphics_449 = new cjs.Graphics().p("AAeLpIAA3RIDjAAIAAXRg");
	var mask_graphics_450 = new cjs.Graphics().p("AATLpIAA3RID5AAIAAXRg");
	var mask_graphics_451 = new cjs.Graphics().p("AAHLpIAA3RIEQAAIAAXRg");
	var mask_graphics_452 = new cjs.Graphics().p("AgDLpIAA3RIEnAAIAAXRg");
	var mask_graphics_453 = new cjs.Graphics().p("AgRLpIAA3RIFCAAIAAXRg");
	var mask_graphics_454 = new cjs.Graphics().p("AgfLpIAA3RIFeAAIAAXRg");
	var mask_graphics_455 = new cjs.Graphics().p("AgtLpIAA3RIF6AAIAAXRg");
	var mask_graphics_456 = new cjs.Graphics().p("Ag9LpIAA3RIGaAAIAAXRg");
	var mask_graphics_457 = new cjs.Graphics().p("AhNLpIAA3RIG6AAIAAXRg");
	var mask_graphics_458 = new cjs.Graphics().p("AheLpIAA3RIHcAAIAAXRg");
	var mask_graphics_459 = new cjs.Graphics().p("AhwLpIAA3RIH/AAIAAXRg");
	var mask_graphics_460 = new cjs.Graphics().p("AiCLpIAA3RIIkAAIAAXRg");
	var mask_graphics_461 = new cjs.Graphics().p("AiVLpIAA3RIJKAAIAAXRg");
	var mask_graphics_462 = new cjs.Graphics().p("AipLpIAA3RIJyAAIAAXRg");
	var mask_graphics_463 = new cjs.Graphics().p("Ai+LpIAA3RIKbAAIAAXRg");
	var mask_graphics_464 = new cjs.Graphics().p("AjULpIAA3RILHAAIAAXRg");
	var mask_graphics_465 = new cjs.Graphics().p("AjqLpIAA3RILzAAIAAXRg");
	var mask_graphics_466 = new cjs.Graphics().p("AkBLpIAA3RIMhAAIAAXRg");
	var mask_graphics_467 = new cjs.Graphics().p("AkZLpIAA3RINQAAIAAXRg");
	var mask_graphics_468 = new cjs.Graphics().p("AkxLpIAA3RIOBAAIAAXRg");
	var mask_graphics_469 = new cjs.Graphics().p("AlKLpIAA3RIOzAAIAAXRg");
	var mask_graphics_470 = new cjs.Graphics().p("AlkLpIAA3RIPnAAIAAXRg");
	var mask_graphics_471 = new cjs.Graphics().p("Al/LpIAA3RIQcAAIAAXRg");
	var mask_graphics_472 = new cjs.Graphics().p("AmbLpIAA3RIRUAAIAAXRg");
	var mask_graphics_473 = new cjs.Graphics().p("Am3LpIAA3RISMAAIAAXRg");
	var mask_graphics_474 = new cjs.Graphics().p("AnULpIAA3RITGAAIAAXRg");
	var mask_graphics_475 = new cjs.Graphics().p("AnyLpIAA3RIUBAAIAAXRg");
	var mask_graphics_476 = new cjs.Graphics().p("AoQLpIAA3RIU+AAIAAXRg");
	var mask_graphics_477 = new cjs.Graphics().p("AowLpIAA3RIV9AAIAAXRg");
	var mask_graphics_478 = new cjs.Graphics().p("ApQroIW9AAIAAXRI29AAg");
	var mask_graphics_479 = new cjs.Graphics().p("ApwroIX9AAIAAXRI39AAg");
	var mask_graphics_480 = new cjs.Graphics().p("AqSroIZBAAIAAXRI5BAAg");
	var mask_graphics_481 = new cjs.Graphics().p("Aq0roIaFAAIAAXRI6FAAg");
	var mask_graphics_482 = new cjs.Graphics().p("ArXroIbLAAIAAXRI7LAAg");
	var mask_graphics_483 = new cjs.Graphics().p("Ar7roIcSAAIAAXRI8SAAg");
	var mask_graphics_484 = new cjs.Graphics().p("AsgroIdcAAIAAXRI9cAAg");
	var mask_graphics_485 = new cjs.Graphics().p("AtFroIemAAIAAXRI+mAAg");
	var mask_graphics_486 = new cjs.Graphics().p("AtrroIfyAAIAAXRI/yAAg");
	var mask_graphics_487 = new cjs.Graphics().p("AuSroMAg/AAAIAAXRMgg/AAAg");
	var mask_graphics_488 = new cjs.Graphics().p("Au5roMAiOAAAIAAXRMgiOAAAg");
	var mask_graphics_489 = new cjs.Graphics().p("AviroMAjfAAAIAAXRMgjfAAAg");
	var mask_graphics_490 = new cjs.Graphics().p("AwLroMAkxAAAIAAXRMgkxAAAg");
	var mask_graphics_491 = new cjs.Graphics().p("Aw1roMAmEAAAIABXRMgmFAAAg");
	var mask_graphics_492 = new cjs.Graphics().p("AxfroMAnZAAAIAAXRMgnZAAAg");
	var mask_graphics_493 = new cjs.Graphics().p("AyLroMAowAAAIAAXRMgowAAAg");
	var mask_graphics_494 = new cjs.Graphics().p("Ay3roMAqIAAAIAAXRMgqIAAAg");
	var mask_graphics_495 = new cjs.Graphics().p("AzkroMAriAAAIAAXRMgriAAAg");
	var mask_graphics_496 = new cjs.Graphics().p("A0RroMAs8AAAIAAXRMgs8AAAg");
	var mask_graphics_497 = new cjs.Graphics().p("A1AroMAuZAAAIAAXRMguZAAAg");
	var mask_graphics_498 = new cjs.Graphics().p("A1vroMAv3AAAIAAXRMgv3AAAg");
	var mask_graphics_499 = new cjs.Graphics().p("A2froMAxXAAAIAAXRMgxXAAAg");
	var mask_graphics_500 = new cjs.Graphics().p("A3ProMAy3AAAIAAXRMgy3AAAg");
	var mask_graphics_501 = new cjs.Graphics().p("A4BroMA0aAAAIAAXRMg0aAAAg");
	var mask_graphics_502 = new cjs.Graphics().p("A4zroMA1+AAAIAAXRMg1+AAAg");
	var mask_graphics_503 = new cjs.Graphics().p("A5mroMA3kAAAIAAXRMg3kAAAg");
	var mask_graphics_504 = new cjs.Graphics().p("A6ZroMA5KAAAIAAXRMg5KAAAg");
	var mask_graphics_505 = new cjs.Graphics().p("A7OroMA6zAAAIAAXRMg6zAAAg");
	var mask_graphics_506 = new cjs.Graphics().p("A8DroMA8dAAAIAAXRMg8dAAAg");
	var mask_graphics_507 = new cjs.Graphics().p("A85roMA+JAAAIAAXRMg+JAAAg");
	var mask_graphics_508 = new cjs.Graphics().p("A9vroMA/1AAAIAAXRMg/1AAAg");
	var mask_graphics_509 = new cjs.Graphics().p("A+nroMBBkAAAIAAXRMhBkAAAg");
	var mask_graphics_510 = new cjs.Graphics().p("A/froMBDUAAAIAAXRMhDUAAAg");
	var mask_graphics_511 = new cjs.Graphics().p("EggYgLoMBFGAAAIAAXRMhFGAAAg");
	var mask_graphics_512 = new cjs.Graphics().p("EghSgLoMBG5AAAIAAXRMhG5AAAg");
	var mask_graphics_513 = new cjs.Graphics().p("EgiMgLoMBItAAAIAAXRMhItAAAg");
	var mask_graphics_514 = new cjs.Graphics().p("EgjHgLoMBKjAAAIAAXRMhKjAAAg");
	var mask_graphics_515 = new cjs.Graphics().p("EgkDgLoMBMbAAAIAAXRMhMbAAAg");
	var mask_graphics_516 = new cjs.Graphics().p("EglAgLoMBOUAAAIAAXRMhOUAAAg");
	var mask_graphics_517 = new cjs.Graphics().p("Egl9gLoMBQOAAAIAAXRMhQOAAAg");
	var mask_graphics_518 = new cjs.Graphics().p("Egm8gLoMBSLAAAIAAXRMhSLAAAg");
	var mask_graphics_519 = new cjs.Graphics().p("Egn7gLoMBUJAAAIAAXRMhUJAAAg");
	var mask_graphics_520 = new cjs.Graphics().p("Ego6gLoMBWHAAAIAAXRMhWHAAAg");
	var mask_graphics_521 = new cjs.Graphics().p("Egp7gLoMBYIAAAIAAXRMhYIAAAg");
	var mask_graphics_522 = new cjs.Graphics().p("Egq8gLoMBaKAAAIAAXRMhaKAAAg");
	var mask_graphics_523 = new cjs.Graphics().p("Egr+gLoMBcOAAAIAAXRMhcOAAAg");
	var mask_graphics_524 = new cjs.Graphics().p("EgtBgLoMBeTAAAIAAXRMheTAAAg");
	var mask_graphics_525 = new cjs.Graphics().p("EguEgLoMBgZAAAIABXRMhgaAAAg");
	var mask_graphics_526 = new cjs.Graphics().p("EgvIgLoMBihAAAIAAXRMhihAAAg");
	var mask_graphics_527 = new cjs.Graphics().p("EgwNgLoMBkrAAAIAAXRMhkrAAAg");
	var mask_graphics_528 = new cjs.Graphics().p("EgxTgLoMBm2AAAIAAXRMhm2AAAg");
	var mask_graphics_529 = new cjs.Graphics().p("EgyagLoMBpEAAAIAAXRMhpEAAAg");
	var mask_graphics_530 = new cjs.Graphics().p("EgzhgLoMBrRAAAIABXRMhrSAAAg");
	var mask_graphics_531 = new cjs.Graphics().p("Eg0pgLoMBthAAAIAAXRMhthAAAg");
	var mask_graphics_532 = new cjs.Graphics().p("Eg1ygLoMBvzAAAIAAXRMhvzAAAg");
	var mask_graphics_533 = new cjs.Graphics().p("Eg27gLoMByFAAAIAAXRMhyFAAAg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(435).to({graphics:mask_graphics_435,x:18.4,y:36.5}).wait(1).to({graphics:mask_graphics_436,x:18.4,y:36.5}).wait(1).to({graphics:mask_graphics_437,x:18.5,y:36.5}).wait(1).to({graphics:mask_graphics_438,x:18.7,y:36.5}).wait(1).to({graphics:mask_graphics_439,x:19,y:36.5}).wait(1).to({graphics:mask_graphics_440,x:19.3,y:36.5}).wait(1).to({graphics:mask_graphics_441,x:19.7,y:36.5}).wait(1).to({graphics:mask_graphics_442,x:20.2,y:36.5}).wait(1).to({graphics:mask_graphics_443,x:20.8,y:36.5}).wait(1).to({graphics:mask_graphics_444,x:21.4,y:36.5}).wait(1).to({graphics:mask_graphics_445,x:22.1,y:36.5}).wait(1).to({graphics:mask_graphics_446,x:22.9,y:36.5}).wait(1).to({graphics:mask_graphics_447,x:23.8,y:36.5}).wait(1).to({graphics:mask_graphics_448,x:24.7,y:36.5}).wait(1).to({graphics:mask_graphics_449,x:25.7,y:36.5}).wait(1).to({graphics:mask_graphics_450,x:26.8,y:36.5}).wait(1).to({graphics:mask_graphics_451,x:28,y:36.5}).wait(1).to({graphics:mask_graphics_452,x:29.2,y:36.5}).wait(1).to({graphics:mask_graphics_453,x:30.5,y:36.5}).wait(1).to({graphics:mask_graphics_454,x:31.9,y:36.5}).wait(1).to({graphics:mask_graphics_455,x:33.4,y:36.5}).wait(1).to({graphics:mask_graphics_456,x:34.9,y:36.5}).wait(1).to({graphics:mask_graphics_457,x:36.5,y:36.5}).wait(1).to({graphics:mask_graphics_458,x:38.2,y:36.5}).wait(1).to({graphics:mask_graphics_459,x:40,y:36.5}).wait(1).to({graphics:mask_graphics_460,x:41.8,y:36.5}).wait(1).to({graphics:mask_graphics_461,x:43.7,y:36.5}).wait(1).to({graphics:mask_graphics_462,x:45.7,y:36.5}).wait(1).to({graphics:mask_graphics_463,x:47.8,y:36.5}).wait(1).to({graphics:mask_graphics_464,x:49.9,y:36.5}).wait(1).to({graphics:mask_graphics_465,x:52.1,y:36.5}).wait(1).to({graphics:mask_graphics_466,x:54.4,y:36.5}).wait(1).to({graphics:mask_graphics_467,x:56.8,y:36.5}).wait(1).to({graphics:mask_graphics_468,x:59.2,y:36.5}).wait(1).to({graphics:mask_graphics_469,x:61.7,y:36.5}).wait(1).to({graphics:mask_graphics_470,x:64.3,y:36.5}).wait(1).to({graphics:mask_graphics_471,x:67,y:36.5}).wait(1).to({graphics:mask_graphics_472,x:69.7,y:36.5}).wait(1).to({graphics:mask_graphics_473,x:72.5,y:36.5}).wait(1).to({graphics:mask_graphics_474,x:75.4,y:36.5}).wait(1).to({graphics:mask_graphics_475,x:78.4,y:36.5}).wait(1).to({graphics:mask_graphics_476,x:81.4,y:36.5}).wait(1).to({graphics:mask_graphics_477,x:84.5,y:36.5}).wait(1).to({graphics:mask_graphics_478,x:87.7,y:36.5}).wait(1).to({graphics:mask_graphics_479,x:91,y:36.5}).wait(1).to({graphics:mask_graphics_480,x:94.3,y:36.5}).wait(1).to({graphics:mask_graphics_481,x:97.7,y:36.5}).wait(1).to({graphics:mask_graphics_482,x:101.2,y:36.5}).wait(1).to({graphics:mask_graphics_483,x:104.8,y:36.5}).wait(1).to({graphics:mask_graphics_484,x:108.4,y:36.5}).wait(1).to({graphics:mask_graphics_485,x:112.1,y:36.5}).wait(1).to({graphics:mask_graphics_486,x:115.9,y:36.5}).wait(1).to({graphics:mask_graphics_487,x:119.8,y:36.5}).wait(1).to({graphics:mask_graphics_488,x:123.7,y:36.5}).wait(1).to({graphics:mask_graphics_489,x:127.7,y:36.5}).wait(1).to({graphics:mask_graphics_490,x:131.8,y:36.5}).wait(1).to({graphics:mask_graphics_491,x:136,y:36.5}).wait(1).to({graphics:mask_graphics_492,x:140.2,y:36.5}).wait(1).to({graphics:mask_graphics_493,x:144.6,y:36.5}).wait(1).to({graphics:mask_graphics_494,x:148.9,y:36.5}).wait(1).to({graphics:mask_graphics_495,x:153.4,y:36.5}).wait(1).to({graphics:mask_graphics_496,x:157.9,y:36.5}).wait(1).to({graphics:mask_graphics_497,x:162.6,y:36.5}).wait(1).to({graphics:mask_graphics_498,x:167.2,y:36.5}).wait(1).to({graphics:mask_graphics_499,x:172,y:36.5}).wait(1).to({graphics:mask_graphics_500,x:176.9,y:36.5}).wait(1).to({graphics:mask_graphics_501,x:181.8,y:36.5}).wait(1).to({graphics:mask_graphics_502,x:186.8,y:36.5}).wait(1).to({graphics:mask_graphics_503,x:191.8,y:36.5}).wait(1).to({graphics:mask_graphics_504,x:197,y:36.5}).wait(1).to({graphics:mask_graphics_505,x:202.2,y:36.5}).wait(1).to({graphics:mask_graphics_506,x:207.5,y:36.5}).wait(1).to({graphics:mask_graphics_507,x:212.8,y:36.5}).wait(1).to({graphics:mask_graphics_508,x:218.3,y:36.5}).wait(1).to({graphics:mask_graphics_509,x:223.8,y:36.5}).wait(1).to({graphics:mask_graphics_510,x:229.4,y:36.5}).wait(1).to({graphics:mask_graphics_511,x:235,y:36.5}).wait(1).to({graphics:mask_graphics_512,x:240.8,y:36.5}).wait(1).to({graphics:mask_graphics_513,x:246.6,y:36.5}).wait(1).to({graphics:mask_graphics_514,x:252.5,y:36.5}).wait(1).to({graphics:mask_graphics_515,x:258.4,y:36.5}).wait(1).to({graphics:mask_graphics_516,x:264.5,y:36.5}).wait(1).to({graphics:mask_graphics_517,x:270.6,y:36.5}).wait(1).to({graphics:mask_graphics_518,x:276.8,y:36.5}).wait(1).to({graphics:mask_graphics_519,x:283,y:36.5}).wait(1).to({graphics:mask_graphics_520,x:289.4,y:36.5}).wait(1).to({graphics:mask_graphics_521,x:295.8,y:36.5}).wait(1).to({graphics:mask_graphics_522,x:302.3,y:36.5}).wait(1).to({graphics:mask_graphics_523,x:308.9,y:36.5}).wait(1).to({graphics:mask_graphics_524,x:315.5,y:36.5}).wait(1).to({graphics:mask_graphics_525,x:322.2,y:36.5}).wait(1).to({graphics:mask_graphics_526,x:329,y:36.5}).wait(1).to({graphics:mask_graphics_527,x:335.9,y:36.5}).wait(1).to({graphics:mask_graphics_528,x:342.8,y:36.5}).wait(1).to({graphics:mask_graphics_529,x:349.8,y:36.5}).wait(1).to({graphics:mask_graphics_530,x:356.9,y:36.5}).wait(1).to({graphics:mask_graphics_531,x:364.1,y:36.5}).wait(1).to({graphics:mask_graphics_532,x:371.3,y:36.5}).wait(1).to({graphics:mask_graphics_533,x:378.6,y:36.5}).wait(62));

	// copy_3
	this.copy_3_mc = new lib.copu_3_mc();
	this.copy_3_mc.setTransform(151,170.2);
	this.copy_3_mc._off = true;

	this.copy_3_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.copy_3_mc).wait(435).to({_off:false},0).wait(160));

	// Hands_2
	this.instance_1 = new lib.Hands_1();
	this.instance_1.setTransform(-180,46,2.217,2.217,-90);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(178).to({_off:false},0).to({scaleX:1,scaleY:1,x:640},79,cjs.Ease.get(1)).to({x:-80},98,cjs.Ease.get(-1)).to({_off:true},158).wait(82));

	// Mask --- f2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_257 = new cjs.Graphics().p("EA2bALpIAA3RIBQAAIAAXRg");
	var mask_1_graphics_258 = new cjs.Graphics().p("EA2bALpIAA3RIBQAAIAAXRg");
	var mask_1_graphics_259 = new cjs.Graphics().p("EA2YALpIAA3RIBTAAIAAXRg");
	var mask_1_graphics_260 = new cjs.Graphics().p("EA2UALpIAA3RIBXAAIAAXRg");
	var mask_1_graphics_261 = new cjs.Graphics().p("EA2PALpIAA3RIBdAAIAAXRg");
	var mask_1_graphics_262 = new cjs.Graphics().p("EA2IALpIAA3RIBkAAIAAXRg");
	var mask_1_graphics_263 = new cjs.Graphics().p("EA2AALpIgB3RIBtAAIAAXRg");
	var mask_1_graphics_264 = new cjs.Graphics().p("EA11ALpIAA3RIB3AAIAAXRg");
	var mask_1_graphics_265 = new cjs.Graphics().p("EA1qALpIAA3RICDAAIAAXRg");
	var mask_1_graphics_266 = new cjs.Graphics().p("EA1dALpIAA3RICQAAIAAXRg");
	var mask_1_graphics_267 = new cjs.Graphics().p("EA1OALpIAA3RICfAAIAAXRg");
	var mask_1_graphics_268 = new cjs.Graphics().p("EA09ALpIAA3RICxAAIAAXRg");
	var mask_1_graphics_269 = new cjs.Graphics().p("EA0sALpIAA3RIDCAAIABXRg");
	var mask_1_graphics_270 = new cjs.Graphics().p("EA0YALpIAA3RIDXAAIAAXRg");
	var mask_1_graphics_271 = new cjs.Graphics().p("EA0DALpIAA3RIDtAAIAAXRg");
	var mask_1_graphics_272 = new cjs.Graphics().p("EAztALpIAA3RIEDAAIAAXRg");
	var mask_1_graphics_273 = new cjs.Graphics().p("EAzVALpIAA3RIEcAAIAAXRg");
	var mask_1_graphics_274 = new cjs.Graphics().p("EAy7ALpIAA3RIE3AAIAAXRg");
	var mask_1_graphics_275 = new cjs.Graphics().p("EAygALpIAA3RIFTAAIAAXRg");
	var mask_1_graphics_276 = new cjs.Graphics().p("EAyDALpIAA3RIFxAAIAAXRg");
	var mask_1_graphics_277 = new cjs.Graphics().p("EAxlALpIAA3RIGPAAIAAXRg");
	var mask_1_graphics_278 = new cjs.Graphics().p("EAxFALpIAA3RIGwAAIAAXRg");
	var mask_1_graphics_279 = new cjs.Graphics().p("EAwjALpIAA3RIHTAAIAAXRg");
	var mask_1_graphics_280 = new cjs.Graphics().p("EAwAALpIAA3RIH3AAIAAXRg");
	var mask_1_graphics_281 = new cjs.Graphics().p("EAvcALpIAA3RIIcAAIABXRg");
	var mask_1_graphics_282 = new cjs.Graphics().p("EAu2ALpIAA3RIJEAAIAAXRg");
	var mask_1_graphics_283 = new cjs.Graphics().p("EAuOALpIAA3RIJtAAIAAXRg");
	var mask_1_graphics_284 = new cjs.Graphics().p("EAtlALpIAA3RIKXAAIAAXRg");
	var mask_1_graphics_285 = new cjs.Graphics().p("EAs6ALpIAA3RILDAAIAAXRg");
	var mask_1_graphics_286 = new cjs.Graphics().p("EAsOALpIAA3RILxAAIAAXRg");
	var mask_1_graphics_287 = new cjs.Graphics().p("EArgALpIAA3RIMgAAIAAXRg");
	var mask_1_graphics_288 = new cjs.Graphics().p("EAqxALpIAA3RINQAAIAAXRg");
	var mask_1_graphics_289 = new cjs.Graphics().p("EAqAALpIAA3RIODAAIAAXRg");
	var mask_1_graphics_290 = new cjs.Graphics().p("EApNALpIAA3RIO3AAIAAXRg");
	var mask_1_graphics_291 = new cjs.Graphics().p("EAoZALpIAA3RIPtAAIAAXRg");
	var mask_1_graphics_292 = new cjs.Graphics().p("EAnkALpIAA3RIQkAAIAAXRg");
	var mask_1_graphics_293 = new cjs.Graphics().p("EAmsALpIAA3RIRdAAIAAXRg");
	var mask_1_graphics_294 = new cjs.Graphics().p("EAl0ALpIAA3RISXAAIAAXRg");
	var mask_1_graphics_295 = new cjs.Graphics().p("EAk5ALpIAA3RITUAAIAAXRg");
	var mask_1_graphics_296 = new cjs.Graphics().p("EAj+ALpIAA3RIUQAAIAAXRg");
	var mask_1_graphics_297 = new cjs.Graphics().p("EAjAgLoIVQAAIAAXRI1QAAg");
	var mask_1_graphics_298 = new cjs.Graphics().p("EAiBgLoIWRAAIAAXRI2RAAg");
	var mask_1_graphics_299 = new cjs.Graphics().p("EAhBgLoIXTAAIAAXRI3TAAg");
	var mask_1_graphics_300 = new cjs.Graphics().p("Af/roIYXAAIAAXRI4XAAg");
	var mask_1_graphics_301 = new cjs.Graphics().p("Ae7roIZdAAIAAXRI5dAAg");
	var mask_1_graphics_302 = new cjs.Graphics().p("Ad2roIakAAIAAXRI6kAAg");
	var mask_1_graphics_303 = new cjs.Graphics().p("AcvroIbtAAIAAXRI7tAAg");
	var mask_1_graphics_304 = new cjs.Graphics().p("AbnroIc3AAIAAXRI83AAg");
	var mask_1_graphics_305 = new cjs.Graphics().p("AadroIeEAAIAAXRI+EAAg");
	var mask_1_graphics_306 = new cjs.Graphics().p("AZSroIfRAAIAAXRI/RAAg");
	var mask_1_graphics_307 = new cjs.Graphics().p("AYFroMAggAAAIAAXRMgggAAAg");
	var mask_1_graphics_308 = new cjs.Graphics().p("AW2roMAhxAAAIAAXRMghwAAAg");
	var mask_1_graphics_309 = new cjs.Graphics().p("AVmroMAjEAAAIAAXRMgjEAAAg");
	var mask_1_graphics_310 = new cjs.Graphics().p("AUVroMAkXAAAIAAXRMgkXAAAg");
	var mask_1_graphics_311 = new cjs.Graphics().p("ATCroMAltAAAIAAXRMgltAAAg");
	var mask_1_graphics_312 = new cjs.Graphics().p("ARtroMAnEAAAIAAXRMgnEAAAg");
	var mask_1_graphics_313 = new cjs.Graphics().p("AQXroMAodAAAIAAXRMgodAAAg");
	var mask_1_graphics_314 = new cjs.Graphics().p("AO/roMAp3AAAIAAXRMgp3AAAg");
	var mask_1_graphics_315 = new cjs.Graphics().p("ANmroMArTAAAIAAXRMgrTAAAg");
	var mask_1_graphics_316 = new cjs.Graphics().p("AMLroMAsxAAAIAAXRMgsxAAAg");
	var mask_1_graphics_317 = new cjs.Graphics().p("AKuroMAuRAAAIAAXRMguRAAAg");
	var mask_1_graphics_318 = new cjs.Graphics().p("AJQroMAvxAAAIAAXRMgvxAAAg");
	var mask_1_graphics_319 = new cjs.Graphics().p("AHxroMAxTAAAIAAXRMgxTAAAg");
	var mask_1_graphics_320 = new cjs.Graphics().p("AGQroMAy3AAAIAAXRMgy3AAAg");
	var mask_1_graphics_321 = new cjs.Graphics().p("AEtroMA0dAAAIAAXRMg0dAAAg");
	var mask_1_graphics_322 = new cjs.Graphics().p("ADJroMA2EAAAIAAXRMg2EAAAg");
	var mask_1_graphics_323 = new cjs.Graphics().p("ABjroMA3tAAAIAAXRMg3tAAAg");
	var mask_1_graphics_324 = new cjs.Graphics().p("AgDroMA5WAAAIAAXRMg5VAAAg");
	var mask_1_graphics_325 = new cjs.Graphics().p("AhrroMA7BAAAIAAXRMg7BAAAg");
	var mask_1_graphics_326 = new cjs.Graphics().p("AjWroMA8vAAAIAAXRMg8vAAAg");
	var mask_1_graphics_327 = new cjs.Graphics().p("AlCroMA+fAAAIAAXRMg+fAAAg");
	var mask_1_graphics_328 = new cjs.Graphics().p("AmvroMBAPAAAIAAXRMhAPAAAg");
	var mask_1_graphics_329 = new cjs.Graphics().p("AofroMBCCAAAIAAXRMhCBAAAg");
	var mask_1_graphics_330 = new cjs.Graphics().p("AqProMBD2AAAIAAXRMhD2AAAg");
	var mask_1_graphics_331 = new cjs.Graphics().p("AsBroMBFrAAAIAAXRMhFrAAAg");
	var mask_1_graphics_332 = new cjs.Graphics().p("At1roMBHiAAAIAAXRMhHiAAAg");
	var mask_1_graphics_333 = new cjs.Graphics().p("AvrroMBJcAAAIAAXRMhJbAAAg");
	var mask_1_graphics_334 = new cjs.Graphics().p("AxhroMBLVAAAIABXRMhLWAAAg");
	var mask_1_graphics_335 = new cjs.Graphics().p("AzaroMBNSAAAIAAXRMhNSAAAg");
	var mask_1_graphics_336 = new cjs.Graphics().p("A1UroMBPQAAAIAAXRMhPQAAAg");
	var mask_1_graphics_337 = new cjs.Graphics().p("A3ProMBROAAAIAAXRMhROAAAg");
	var mask_1_graphics_338 = new cjs.Graphics().p("A5MroMBTPAAAIAAXRMhTPAAAg");
	var mask_1_graphics_339 = new cjs.Graphics().p("A7LroMBVSAAAIAAXRMhVSAAAg");
	var mask_1_graphics_340 = new cjs.Graphics().p("A9LroMBXWAAAIAAXRMhXWAAAg");
	var mask_1_graphics_341 = new cjs.Graphics().p("A/NroMBZcAAAIAAXRMhZcAAAg");
	var mask_1_graphics_342 = new cjs.Graphics().p("EghQgLoMBbiAAAIABXRMhbjAAAg");
	var mask_1_graphics_343 = new cjs.Graphics().p("EgjVgLoMBdrAAAIAAXRMhdrAAAg");
	var mask_1_graphics_344 = new cjs.Graphics().p("EglcgLoMBf2AAAIAAXRMhf2AAAg");
	var mask_1_graphics_345 = new cjs.Graphics().p("EgnkgLoMBiDAAAIAAXRMhiDAAAg");
	var mask_1_graphics_346 = new cjs.Graphics().p("EgptgLoMBkQAAAIAAXRMhkQAAAg");
	var mask_1_graphics_347 = new cjs.Graphics().p("Egr4gLoMBmfAAAIAAXRMhmfAAAg");
	var mask_1_graphics_348 = new cjs.Graphics().p("EguFgLoMBowAAAIAAXRMhowAAAg");
	var mask_1_graphics_349 = new cjs.Graphics().p("EgwTgLoMBrCAAAIAAXRMhrCAAAg");
	var mask_1_graphics_350 = new cjs.Graphics().p("EgyjgLoMBtWAAAIABXRMhtXAAAg");
	var mask_1_graphics_351 = new cjs.Graphics().p("Eg00gLoMBvsAAAIAAXRMhvsAAAg");
	var mask_1_graphics_352 = new cjs.Graphics().p("Eg3HgLoMByDAAAIAAXRMhyDAAAg");
	var mask_1_graphics_353 = new cjs.Graphics().p("Eg5bgLoMB0cAAAIAAXRMh0cAAAg");
	var mask_1_graphics_354 = new cjs.Graphics().p("Eg7bgLoMB23AAAIAAXRMh23AAAg");
	var mask_1_graphics_355 = new cjs.Graphics().p("Eg8pgLoMB5TAAAIAAXRMh5TAAAg");
	var mask_1_graphics_435 = new cjs.Graphics().p("EhAhgLoMCFMAAAIAAXRMiFMAAAg");
	var mask_1_graphics_436 = new cjs.Graphics().p("EhAggLoMCFLAAAIAAXRMiFLAAAg");
	var mask_1_graphics_437 = new cjs.Graphics().p("EhAegLoMCFJAAAIAAXRMiFJAAAg");
	var mask_1_graphics_438 = new cjs.Graphics().p("EhAagLoMCFFAAAIAAXRMiFFAAAg");
	var mask_1_graphics_439 = new cjs.Graphics().p("EhAUgLoMCE+AAAIABXRMiE/AAAg");
	var mask_1_graphics_440 = new cjs.Graphics().p("EhANgLoMCE3AAAIAAXRMiE3AAAg");
	var mask_1_graphics_441 = new cjs.Graphics().p("EhAEgLoMCEtAAAIAAXRMiEtAAAg");
	var mask_1_graphics_442 = new cjs.Graphics().p("Eg/5gLoMCEiAAAIAAXRMiEiAAAg");
	var mask_1_graphics_443 = new cjs.Graphics().p("Eg/tgLoMCEVAAAIAAXRMiEVAAAg");
	var mask_1_graphics_444 = new cjs.Graphics().p("Eg/fgLoMCEGAAAIAAXRMiEGAAAg");
	var mask_1_graphics_445 = new cjs.Graphics().p("Eg/PgLoMCD1AAAIAAXRMiD1AAAg");
	var mask_1_graphics_446 = new cjs.Graphics().p("Eg++gLoMCDjAAAIAAXRMiDjAAAg");
	var mask_1_graphics_447 = new cjs.Graphics().p("Eg+rgLoMCDOAAAIAAXRMiDOAAAg");
	var mask_1_graphics_448 = new cjs.Graphics().p("Eg+WgLoMCC4AAAIAAXRMiC4AAAg");
	var mask_1_graphics_449 = new cjs.Graphics().p("Eg+AgLoMCCgAAAIAAXRMiCgAAAg");
	var mask_1_graphics_450 = new cjs.Graphics().p("Eg9ogLoMCCHAAAIAAXRMiCHAAAg");
	var mask_1_graphics_451 = new cjs.Graphics().p("Eg9OgLoMCBrAAAIAAXRMiBrAAAg");
	var mask_1_graphics_452 = new cjs.Graphics().p("Eg8zgLoMCBOAAAIAAXRMiBOAAAg");
	var mask_1_graphics_453 = new cjs.Graphics().p("Eg8WgLoMCAvAAAIAAXRMiAvAAAg");
	var mask_1_graphics_454 = new cjs.Graphics().p("Eg74gLoMCAPAAAIAAXRMiAPAAAg");
	var mask_1_graphics_455 = new cjs.Graphics().p("Eg7YgLoMB/tAAAIAAXRMh/tAAAg");
	var mask_1_graphics_456 = new cjs.Graphics().p("Eg62gLoMB/JAAAIAAXRMh/JAAAg");
	var mask_1_graphics_457 = new cjs.Graphics().p("Eg6TgLoMB+jAAAIAAXRMh+jAAAg");
	var mask_1_graphics_458 = new cjs.Graphics().p("Eg5ugLoMB98AAAIAAXRMh98AAAg");
	var mask_1_graphics_459 = new cjs.Graphics().p("Eg5HgLoMB9SAAAIAAXRMh9SAAAg");
	var mask_1_graphics_460 = new cjs.Graphics().p("Eg4fgLoMB8nAAAIAAXRMh8nAAAg");
	var mask_1_graphics_461 = new cjs.Graphics().p("Eg31gLoMB76AAAIAAXRMh76AAAg");
	var mask_1_graphics_462 = new cjs.Graphics().p("Eg3JgLoMB7LAAAIABXRMh7MAAAg");
	var mask_1_graphics_463 = new cjs.Graphics().p("Eg2cgLoMB6bAAAIAAXRMh6bAAAg");
	var mask_1_graphics_464 = new cjs.Graphics().p("Eg1tgLoMB5pAAAIAAXRMh5pAAAg");
	var mask_1_graphics_465 = new cjs.Graphics().p("Eg08gLoMB41AAAIAAXRMh41AAAg");
	var mask_1_graphics_466 = new cjs.Graphics().p("Eg0KgLoMB3/AAAIAAXRMh3/AAAg");
	var mask_1_graphics_467 = new cjs.Graphics().p("EgzWgLoMB3IAAAIAAXRMh3IAAAg");
	var mask_1_graphics_468 = new cjs.Graphics().p("EgyhgLoMB2PAAAIAAXRMh2PAAAg");
	var mask_1_graphics_469 = new cjs.Graphics().p("EgxpgLoMB1UAAAIAAXRMh1UAAAg");
	var mask_1_graphics_470 = new cjs.Graphics().p("EgwxgLoMB0YAAAIAAXRMh0YAAAg");
	var mask_1_graphics_471 = new cjs.Graphics().p("Egv2gLoMBzZAAAIAAXRMhzZAAAg");
	var mask_1_graphics_472 = new cjs.Graphics().p("Egu6gLoMByZAAAIAAXRMhyZAAAg");
	var mask_1_graphics_473 = new cjs.Graphics().p("Egt8gLoMBxWAAAIAAXRMhxWAAAg");
	var mask_1_graphics_474 = new cjs.Graphics().p("Egs9gLoMBwTAAAIAAXRMhwTAAAg");
	var mask_1_graphics_475 = new cjs.Graphics().p("Egr8gLoMBvOAAAIAAXRMhvOAAAg");
	var mask_1_graphics_476 = new cjs.Graphics().p("Egq5gLoMBuGAAAIAAXRMhuGAAAg");
	var mask_1_graphics_477 = new cjs.Graphics().p("Egp1gLoMBs9AAAIAAXRMhs9AAAg");
	var mask_1_graphics_478 = new cjs.Graphics().p("EgovgLoMBrzAAAIAAXRMhrzAAAg");
	var mask_1_graphics_479 = new cjs.Graphics().p("EgnngLoMBqmAAAIAAXRMhqmAAAg");
	var mask_1_graphics_480 = new cjs.Graphics().p("EgmegLoMBpYAAAIAAXRMhpYAAAg");
	var mask_1_graphics_481 = new cjs.Graphics().p("EglTgLoMBoIAAAIAAXRMhoIAAAg");
	var mask_1_graphics_482 = new cjs.Graphics().p("EgkGgLoMBm1AAAIAAXRMhm1AAAg");
	var mask_1_graphics_483 = new cjs.Graphics().p("Egi4gLoMBliAAAIAAXRMhliAAAg");
	var mask_1_graphics_484 = new cjs.Graphics().p("EghogLoMBkNAAAIAAXRMhkNAAAg");
	var mask_1_graphics_485 = new cjs.Graphics().p("EggXgLoMBi2AAAIAAXRMhi2AAAg");
	var mask_1_graphics_486 = new cjs.Graphics().p("A/EroMBhdAAAIAAXRMhhdAAAg");
	var mask_1_graphics_487 = new cjs.Graphics().p("A9vroMBgDAAAIAAXRMhgDAAAg");
	var mask_1_graphics_488 = new cjs.Graphics().p("A8YroMBemAAAIAAXRMhemAAAg");
	var mask_1_graphics_489 = new cjs.Graphics().p("A7AroMBdIAAAIAAXRMhdIAAAg");
	var mask_1_graphics_490 = new cjs.Graphics().p("A5mroMBboAAAIAAXRMhboAAAg");
	var mask_1_graphics_491 = new cjs.Graphics().p("A4LroMBaGAAAIAAXRMhaGAAAg");
	var mask_1_graphics_492 = new cjs.Graphics().p("A2uroMBYjAAAIAAXRMhYjAAAg");
	var mask_1_graphics_493 = new cjs.Graphics().p("A1ProMBW+AAAIAAXRMhW+AAAg");
	var mask_1_graphics_494 = new cjs.Graphics().p("AzvroMBVXAAAIAAXRMhVXAAAg");
	var mask_1_graphics_495 = new cjs.Graphics().p("AyNroMBTuAAAIAAXRMhTuAAAg");
	var mask_1_graphics_496 = new cjs.Graphics().p("AwproMBSEAAAIAAXRMhSEAAAg");
	var mask_1_graphics_497 = new cjs.Graphics().p("AvEroMBQYAAAIAAXRMhQYAAAg");
	var mask_1_graphics_498 = new cjs.Graphics().p("AtdroMBOqAAAIAAXRMhOqAAAg");
	var mask_1_graphics_499 = new cjs.Graphics().p("Ar1roMBM6AAAIABXRMhM7AAAg");
	var mask_1_graphics_500 = new cjs.Graphics().p("AqKroMBLIAAAIAAXRMhLIAAAg");
	var mask_1_graphics_501 = new cjs.Graphics().p("AoeroMBJVAAAIAAXRMhJVAAAg");
	var mask_1_graphics_502 = new cjs.Graphics().p("AmxroMBHgAAAIAAXRMhHgAAAg");
	var mask_1_graphics_503 = new cjs.Graphics().p("AlCroMBFqAAAIAAXRMhFqAAAg");
	var mask_1_graphics_504 = new cjs.Graphics().p("AjRroMBDxAAAIAAXRMhDxAAAg");
	var mask_1_graphics_505 = new cjs.Graphics().p("AhfroMBB3AAAIAAXRMhB2AAAg");
	var mask_1_graphics_506 = new cjs.Graphics().p("AAUroMA/8AAAIAAXRMg/8AAAg");
	var mask_1_graphics_507 = new cjs.Graphics().p("ACJroMA9/AAAIAAXRMg9/AAAg");
	var mask_1_graphics_508 = new cjs.Graphics().p("AEBroMA7/AAAIAAXRMg7/AAAg");
	var mask_1_graphics_509 = new cjs.Graphics().p("AF6roMA5+AAAIAAXRMg5+AAAg");
	var mask_1_graphics_510 = new cjs.Graphics().p("AH0roMA38AAAIAAXRMg38AAAg");
	var mask_1_graphics_511 = new cjs.Graphics().p("AJxroMA12AAAIAAXRMg12AAAg");
	var mask_1_graphics_512 = new cjs.Graphics().p("ALvroMAzwAAAIAAXRMgzwAAAg");
	var mask_1_graphics_513 = new cjs.Graphics().p("ANuroMAxoAAAIAAXRMgxoAAAg");
	var mask_1_graphics_514 = new cjs.Graphics().p("APwroMAvdAAAIAAXRMgvdAAAg");
	var mask_1_graphics_515 = new cjs.Graphics().p("ARzroMAtRAAAIAAXRMgtRAAAg");
	var mask_1_graphics_516 = new cjs.Graphics().p("AT3roMArEAAAIAAXRMgrEAAAg");
	var mask_1_graphics_517 = new cjs.Graphics().p("AV9roMAo1AAAIAAXRMgo1AAAg");
	var mask_1_graphics_518 = new cjs.Graphics().p("AYFroMAmkAAAIAAXRMgmkAAAg");
	var mask_1_graphics_519 = new cjs.Graphics().p("AaProMAkQAAAIAAXRMgkQAAAg");
	var mask_1_graphics_520 = new cjs.Graphics().p("AcaroMAh8AAAIAAXRMgh8AAAg");
	var mask_1_graphics_521 = new cjs.Graphics().p("AenroIflAAIAAXRI/lAAg");
	var mask_1_graphics_522 = new cjs.Graphics().p("EAg1gLoIdOAAIAAXRI9OAAg");
	var mask_1_graphics_523 = new cjs.Graphics().p("EAjFgLoIa0AAIAAXRI60AAg");
	var mask_1_graphics_524 = new cjs.Graphics().p("EAlXgLoIYYAAIAAXRI4YAAg");
	var mask_1_graphics_525 = new cjs.Graphics().p("EAnqALpIAA3RIV7AAIAAXRg");
	var mask_1_graphics_526 = new cjs.Graphics().p("EAp/ALpIAA3RITcAAIAAXRg");
	var mask_1_graphics_527 = new cjs.Graphics().p("EAsWALpIAA3RIQ6AAIAAXRg");
	var mask_1_graphics_528 = new cjs.Graphics().p("EAuvALpIgB3RIOYAAIAAXRg");
	var mask_1_graphics_529 = new cjs.Graphics().p("EAxIALpIAA3RILzAAIAAXRg");
	var mask_1_graphics_530 = new cjs.Graphics().p("EAzkALpIAA3RIJNAAIAAXRg");
	var mask_1_graphics_531 = new cjs.Graphics().p("EA2BALpIAA3RIGlAAIAAXRg");
	var mask_1_graphics_532 = new cjs.Graphics().p("EA4gALpIAA3RID7AAIAAXRg");
	var mask_1_graphics_533 = new cjs.Graphics().p("EA7BALpIAA3RIBPAAIAAXRg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(257).to({graphics:mask_1_graphics_257,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_258,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_259,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_260,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_261,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_262,x:356.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_263,x:356.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_264,x:356.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_265,x:356.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_266,x:356.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_267,x:356.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_268,x:356.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_269,x:356.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_270,x:356.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_271,x:356.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_272,x:356.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_273,x:357,y:36.5}).wait(1).to({graphics:mask_1_graphics_274,x:357,y:36.5}).wait(1).to({graphics:mask_1_graphics_275,x:357.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_276,x:357.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_277,x:357.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_278,x:357.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_279,x:357.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_280,x:357.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_281,x:357.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_282,x:357.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_283,x:357.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_284,x:358.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_285,x:358.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_286,x:358.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_287,x:358.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_288,x:358.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_289,x:358.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_290,x:358.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_291,x:359,y:36.5}).wait(1).to({graphics:mask_1_graphics_292,x:359.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_293,x:359.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_294,x:359.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_295,x:359.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_296,x:359.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_297,x:360.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_298,x:360.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_299,x:360.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_300,x:360.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_301,x:360.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_302,x:361.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_303,x:361.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_304,x:361.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_305,x:361.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_306,x:361.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_307,x:362.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_308,x:362.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_309,x:362.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_310,x:362.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_311,x:363.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_312,x:363.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_313,x:363.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_314,x:363.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_315,x:364.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_316,x:364.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_317,x:364.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_318,x:365,y:36.5}).wait(1).to({graphics:mask_1_graphics_319,x:365.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_320,x:365.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_321,x:365.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_322,x:366.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_323,x:366.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_324,x:366.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_325,x:367.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_326,x:367.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_327,x:367.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_328,x:368,y:36.5}).wait(1).to({graphics:mask_1_graphics_329,x:368.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_330,x:368.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_331,x:369.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_332,x:369.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_333,x:369.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_334,x:370.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_335,x:370.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_336,x:370.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_337,x:371.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_338,x:371.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_339,x:371.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_340,x:372.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_341,x:372.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_342,x:373.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_343,x:373.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_344,x:373.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_345,x:374.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_346,x:374.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_347,x:375.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_348,x:375.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_349,x:376,y:36.5}).wait(1).to({graphics:mask_1_graphics_350,x:376.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_351,x:376.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_352,x:377.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_353,x:377.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_354,x:375.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_355,x:369,y:36.5}).wait(80).to({graphics:mask_1_graphics_435,x:439.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_436,x:439.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_437,x:439.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_438,x:439.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_439,x:439.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_440,x:439.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_441,x:439.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_442,x:439.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_443,x:439.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_444,x:439.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_445,x:439,y:36.5}).wait(1).to({graphics:mask_1_graphics_446,x:438.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_447,x:438.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_448,x:438.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_449,x:438.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_450,x:438.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_451,x:438.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_452,x:438,y:36.5}).wait(1).to({graphics:mask_1_graphics_453,x:437.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_454,x:437.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_455,x:437.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_456,x:437.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_457,x:436.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_458,x:436.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_459,x:436.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_460,x:436.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_461,x:435.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_462,x:435.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_463,x:435.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_464,x:434.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_465,x:434.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_466,x:434.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_467,x:433.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_468,x:433.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_469,x:433.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_470,x:432.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_471,x:432.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_472,x:431.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_473,x:431.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_474,x:431.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_475,x:430.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_476,x:430.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_477,x:429.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_478,x:429.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_479,x:428.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_480,x:428.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_481,x:427.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_482,x:427.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_483,x:426.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_484,x:426.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_485,x:425.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_486,x:425,y:36.5}).wait(1).to({graphics:mask_1_graphics_487,x:424.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_488,x:423.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_489,x:423.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_490,x:422.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_491,x:422,y:36.5}).wait(1).to({graphics:mask_1_graphics_492,x:421.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_493,x:420.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_494,x:420.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_495,x:419.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_496,x:418.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_497,x:418,y:36.5}).wait(1).to({graphics:mask_1_graphics_498,x:417.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_499,x:416.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_500,x:415.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_501,x:415.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_502,x:414.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_503,x:413.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_504,x:412.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_505,x:412.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_506,x:411.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_507,x:410.5,y:36.5}).wait(1).to({graphics:mask_1_graphics_508,x:409.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_509,x:408.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_510,x:408,y:36.5}).wait(1).to({graphics:mask_1_graphics_511,x:407.2,y:36.5}).wait(1).to({graphics:mask_1_graphics_512,x:406.3,y:36.5}).wait(1).to({graphics:mask_1_graphics_513,x:405.4,y:36.5}).wait(1).to({graphics:mask_1_graphics_514,x:404.6,y:36.5}).wait(1).to({graphics:mask_1_graphics_515,x:403.7,y:36.5}).wait(1).to({graphics:mask_1_graphics_516,x:402.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_517,x:401.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_518,x:400.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_519,x:400,y:36.5}).wait(1).to({graphics:mask_1_graphics_520,x:399,y:36.5}).wait(1).to({graphics:mask_1_graphics_521,x:398.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_522,x:397.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_523,x:396.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_524,x:395.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_525,x:394.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_526,x:393.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_527,x:392.1,y:36.5}).wait(1).to({graphics:mask_1_graphics_528,x:391,y:36.5}).wait(1).to({graphics:mask_1_graphics_529,x:390,y:36.5}).wait(1).to({graphics:mask_1_graphics_530,x:388.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_531,x:387.9,y:36.5}).wait(1).to({graphics:mask_1_graphics_532,x:386.8,y:36.5}).wait(1).to({graphics:mask_1_graphics_533,x:385.7,y:36.5}).wait(62));

	// copy_2
	this.copy_2_mc = new lib.copy_2_mc();
	this.copy_2_mc.setTransform(151.8,169.5);
	this.copy_2_mc._off = true;

	this.copy_2_mc.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.copy_2_mc).wait(257).to({_off:false},0).wait(178).to({_off:true},99).wait(61));

	// HandsNew
	this.instance_2 = new lib.Hands_1();
	this.instance_2.setTransform(880,46,2.217,2.217,90);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:1,scaleY:1,x:160},79,cjs.Ease.get(1)).to({x:840},98,cjs.Ease.get(-1)).to({_off:true},336).wait(82));

	// Mask --- f1 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_79 = new cjs.Graphics().p("AF/LpIAA3RIBQAAIAAXRg");
	var mask_2_graphics_80 = new cjs.Graphics().p("AF/LpIAA3RIBQAAIABXRg");
	var mask_2_graphics_81 = new cjs.Graphics().p("AF+LpIAA3RIBTAAIAAXRg");
	var mask_2_graphics_82 = new cjs.Graphics().p("AF8LpIAA3RIBWAAIAAXRg");
	var mask_2_graphics_83 = new cjs.Graphics().p("AF6LpIAA3RIBbAAIAAXRg");
	var mask_2_graphics_84 = new cjs.Graphics().p("AF3LpIAA3RIBhAAIAAXRg");
	var mask_2_graphics_85 = new cjs.Graphics().p("AFzLpIAA3RIBpAAIAAXRg");
	var mask_2_graphics_86 = new cjs.Graphics().p("AFvLpIgB3RIByAAIAAXRg");
	var mask_2_graphics_87 = new cjs.Graphics().p("AFpLpIAA3RIB8AAIAAXRg");
	var mask_2_graphics_88 = new cjs.Graphics().p("AFjLpIAA3RICIAAIAAXRg");
	var mask_2_graphics_89 = new cjs.Graphics().p("AFdLpIAA3RICVAAIAAXRg");
	var mask_2_graphics_90 = new cjs.Graphics().p("AFWLpIAA3RICjAAIAAXRg");
	var mask_2_graphics_91 = new cjs.Graphics().p("AFOLpIAA3RICzAAIAAXRg");
	var mask_2_graphics_92 = new cjs.Graphics().p("AFFLpIAA3RIDFAAIAAXRg");
	var mask_2_graphics_93 = new cjs.Graphics().p("AE8LpIAA3RIDXAAIAAXRg");
	var mask_2_graphics_94 = new cjs.Graphics().p("AEyLpIAA3RIDrAAIAAXRg");
	var mask_2_graphics_95 = new cjs.Graphics().p("AEnLpIAA3RIEBAAIAAXRg");
	var mask_2_graphics_96 = new cjs.Graphics().p("AEcLpIgB3RIEYAAIAAXRg");
	var mask_2_graphics_97 = new cjs.Graphics().p("AEPLpIAA3RIEwAAIAAXRg");
	var mask_2_graphics_98 = new cjs.Graphics().p("AEDLpIAA3RIFJAAIAAXRg");
	var mask_2_graphics_99 = new cjs.Graphics().p("AD1LpIAA3RIFlAAIAAXRg");
	var mask_2_graphics_100 = new cjs.Graphics().p("ADnLpIAA3RIGBAAIAAXRg");
	var mask_2_graphics_101 = new cjs.Graphics().p("ADYLpIAA3RIGfAAIAAXRg");
	var mask_2_graphics_102 = new cjs.Graphics().p("ADILpIAA3RIG+AAIAAXRg");
	var mask_2_graphics_103 = new cjs.Graphics().p("AC4LpIAA3RIHeAAIAAXRg");
	var mask_2_graphics_104 = new cjs.Graphics().p("ACnLpIAA3RIIAAAIAAXRg");
	var mask_2_graphics_105 = new cjs.Graphics().p("ACWLpIAA3RIIjAAIAAXRg");
	var mask_2_graphics_106 = new cjs.Graphics().p("ACDLpIAA3RIJIAAIAAXRg");
	var mask_2_graphics_107 = new cjs.Graphics().p("ABwLpIAA3RIJuAAIAAXRg");
	var mask_2_graphics_108 = new cjs.Graphics().p("ABdLpIAA3RIKVAAIAAXRg");
	var mask_2_graphics_109 = new cjs.Graphics().p("ABILpIAA3RIK+AAIABXRg");
	var mask_2_graphics_110 = new cjs.Graphics().p("AAzLpIAA3RILpAAIAAXRg");
	var mask_2_graphics_111 = new cjs.Graphics().p("AAdLpIAA3RIMUAAIAAXRg");
	var mask_2_graphics_112 = new cjs.Graphics().p("AAHLpIAA3RINBAAIAAXRg");
	var mask_2_graphics_113 = new cjs.Graphics().p("AgOLpIAA3RINtAAIAAXRg");
	var mask_2_graphics_114 = new cjs.Graphics().p("AgmLpIAA3RIOdAAIAAXRg");
	var mask_2_graphics_115 = new cjs.Graphics().p("Ag/LpIAA3RIPOAAIABXRg");
	var mask_2_graphics_116 = new cjs.Graphics().p("AhYLpIAA3RIQBAAIAAXRg");
	var mask_2_graphics_117 = new cjs.Graphics().p("AhyLpIAA3RIQ1AAIAAXRg");
	var mask_2_graphics_118 = new cjs.Graphics().p("AiNLpIAA3RIRqAAIAAXRg");
	var mask_2_graphics_119 = new cjs.Graphics().p("AioLpIAA3RIShAAIAAXRg");
	var mask_2_graphics_120 = new cjs.Graphics().p("AjELpIAA3RITZAAIAAXRg");
	var mask_2_graphics_121 = new cjs.Graphics().p("AjhLpIAA3RIUSAAIAAXRg");
	var mask_2_graphics_122 = new cjs.Graphics().p("Aj+roIVNAAIAAXRI1NAAg");
	var mask_2_graphics_123 = new cjs.Graphics().p("AkcroIWJAAIAAXRI2JAAg");
	var mask_2_graphics_124 = new cjs.Graphics().p("Ak7roIXHAAIAAXRI3HAAg");
	var mask_2_graphics_125 = new cjs.Graphics().p("AlbroIYGAAIAAXRI4GAAg");
	var mask_2_graphics_126 = new cjs.Graphics().p("Al7roIZGAAIAAXRI5GAAg");
	var mask_2_graphics_127 = new cjs.Graphics().p("AmcroIaIAAIAAXRI6IAAg");
	var mask_2_graphics_128 = new cjs.Graphics().p("Am9roIbLAAIAAXRI7LAAg");
	var mask_2_graphics_129 = new cjs.Graphics().p("AnfroIcPAAIAAXRI8PAAg");
	var mask_2_graphics_130 = new cjs.Graphics().p("AoCroIdVAAIAAXRI9VAAg");
	var mask_2_graphics_131 = new cjs.Graphics().p("AomroIedAAIAAXRI+dAAg");
	var mask_2_graphics_132 = new cjs.Graphics().p("ApKroIflAAIAAXRI/lAAg");
	var mask_2_graphics_133 = new cjs.Graphics().p("ApvroMAgvAAAIAAXRMggvAAAg");
	var mask_2_graphics_134 = new cjs.Graphics().p("AqVroMAh7AAAIAAXRMgh7AAAg");
	var mask_2_graphics_135 = new cjs.Graphics().p("Aq7roMAjHAAAIAAXRMgjHAAAg");
	var mask_2_graphics_136 = new cjs.Graphics().p("ArjroMAkWAAAIAAXRMgkVAAAg");
	var mask_2_graphics_137 = new cjs.Graphics().p("AsKroMAllAAAIAAXRMgllAAAg");
	var mask_2_graphics_138 = new cjs.Graphics().p("AszroMAm2AAAIAAXRMgm2AAAg");
	var mask_2_graphics_139 = new cjs.Graphics().p("AtcroMAoJAAAIAAXRMgoJAAAg");
	var mask_2_graphics_140 = new cjs.Graphics().p("AuGroMApcAAAIAAXRMgpcAAAg");
	var mask_2_graphics_141 = new cjs.Graphics().p("AuwroMAqxAAAIAAXRMgqxAAAg");
	var mask_2_graphics_142 = new cjs.Graphics().p("AvcroMAsIAAAIAAXRMgsIAAAg");
	var mask_2_graphics_143 = new cjs.Graphics().p("AwIroMAtgAAAIAAXRMgtfAAAg");
	var mask_2_graphics_144 = new cjs.Graphics().p("Aw0roMAu5AAAIAAXRMgu5AAAg");
	var mask_2_graphics_145 = new cjs.Graphics().p("AxhroMAwTAAAIAAXRMgwTAAAg");
	var mask_2_graphics_146 = new cjs.Graphics().p("AyProMAxvAAAIAAXRMgxvAAAg");
	var mask_2_graphics_147 = new cjs.Graphics().p("Ay+roMAzNAAAIAAXRMgzNAAAg");
	var mask_2_graphics_148 = new cjs.Graphics().p("AzuroMA0sAAAIAAXRMg0sAAAg");
	var mask_2_graphics_149 = new cjs.Graphics().p("A0eroMA2MAAAIAAXRMg2MAAAg");
	var mask_2_graphics_150 = new cjs.Graphics().p("A1OroMA3tAAAIAAXRMg3tAAAg");
	var mask_2_graphics_151 = new cjs.Graphics().p("A2AroMA5RAAAIAAXRMg5RAAAg");
	var mask_2_graphics_152 = new cjs.Graphics().p("A2yroMA61AAAIAAXRMg61AAAg");
	var mask_2_graphics_153 = new cjs.Graphics().p("A3lroMA8bAAAIAAXRMg8bAAAg");
	var mask_2_graphics_154 = new cjs.Graphics().p("A4YroMA+BAAAIAAXRMg+BAAAg");
	var mask_2_graphics_155 = new cjs.Graphics().p("A5NroMA/qAAAIAAXRMg/qAAAg");
	var mask_2_graphics_156 = new cjs.Graphics().p("A6CroMBBUAAAIAAXRMhBUAAAg");
	var mask_2_graphics_157 = new cjs.Graphics().p("A63roMBC/AAAIAAXRMhC/AAAg");
	var mask_2_graphics_158 = new cjs.Graphics().p("A7uroMBEsAAAIAAXRMhEsAAAg");
	var mask_2_graphics_159 = new cjs.Graphics().p("A8lroMBGaAAAIAAXRMhGaAAAg");
	var mask_2_graphics_160 = new cjs.Graphics().p("A9croMBIJAAAIAAXRMhIJAAAg");
	var mask_2_graphics_161 = new cjs.Graphics().p("A+VroMBJ6AAAIAAXRMhJ6AAAg");
	var mask_2_graphics_162 = new cjs.Graphics().p("A/OroMBLsAAAIAAXRMhLsAAAg");
	var mask_2_graphics_163 = new cjs.Graphics().p("EggIgLoMBNgAAAIAAXRMhNfAAAg");
	var mask_2_graphics_164 = new cjs.Graphics().p("EghCgLoMBPVAAAIAAXRMhPVAAAg");
	var mask_2_graphics_165 = new cjs.Graphics().p("Egh9gLoMBRLAAAIAAXRMhRLAAAg");
	var mask_2_graphics_166 = new cjs.Graphics().p("Egi5gLoMBTDAAAIAAXRMhTDAAAg");
	var mask_2_graphics_167 = new cjs.Graphics().p("Egj2gLoMBU8AAAIAAXRMhU7AAAg");
	var mask_2_graphics_168 = new cjs.Graphics().p("EgkzgLoMBW2AAAIAAXRMhW2AAAg");
	var mask_2_graphics_169 = new cjs.Graphics().p("EglxgLoMBYyAAAIAAXRMhYyAAAg");
	var mask_2_graphics_170 = new cjs.Graphics().p("EgmvgLoMBavAAAIAAXRMhavAAAg");
	var mask_2_graphics_171 = new cjs.Graphics().p("EgnvgLoMBcuAAAIAAXRMhcuAAAg");
	var mask_2_graphics_172 = new cjs.Graphics().p("EgovgLoMBeuAAAIAAXRMheuAAAg");
	var mask_2_graphics_173 = new cjs.Graphics().p("EgpvgLoMBgvAAAIAAXRMhgvAAAg");
	var mask_2_graphics_174 = new cjs.Graphics().p("EgqxgLoMBiyAAAIAAXRMhiyAAAg");
	var mask_2_graphics_175 = new cjs.Graphics().p("EgrzgLoMBk2AAAIAAXRMhk2AAAg");
	var mask_2_graphics_176 = new cjs.Graphics().p("Egs2gLoMBm8AAAIAAXRMhm7AAAg");
	var mask_2_graphics_177 = new cjs.Graphics().p("Egt5gLoMBpDAAAIAAXRMhpDAAAg");
	var mask_2_graphics_257 = new cjs.Graphics().p("EhCmgLoMCFNAAAIAAXRMiFNAAAg");
	var mask_2_graphics_258 = new cjs.Graphics().p("EhClgLoMCFLAAAIAAXRMiFLAAAg");
	var mask_2_graphics_259 = new cjs.Graphics().p("EhCkgLoMCFJAAAIAAXRMiFJAAAg");
	var mask_2_graphics_260 = new cjs.Graphics().p("EhCigLoMCFFAAAIAAXRMiFFAAAg");
	var mask_2_graphics_261 = new cjs.Graphics().p("EhCfgLoMCE/AAAIAAXRMiE/AAAg");
	var mask_2_graphics_262 = new cjs.Graphics().p("EhCbgLoMCE3AAAIAAXRMiE3AAAg");
	var mask_2_graphics_263 = new cjs.Graphics().p("EhCWgLoMCEtAAAIAAXRMiEtAAAg");
	var mask_2_graphics_264 = new cjs.Graphics().p("EhCQgLoMCEhAAAIAAXRMiEhAAAg");
	var mask_2_graphics_265 = new cjs.Graphics().p("EhCKgLoMCEVAAAIAAXRMiEVAAAg");
	var mask_2_graphics_266 = new cjs.Graphics().p("EhCCgLoMCEFAAAIAAXRMiEFAAAg");
	var mask_2_graphics_267 = new cjs.Graphics().p("EhB6gLoMCD1AAAIAAXRMiD1AAAg");
	var mask_2_graphics_268 = new cjs.Graphics().p("EhBxgLoMCDjAAAIAAXRMiDjAAAg");
	var mask_2_graphics_269 = new cjs.Graphics().p("EhBngLoMCDOAAAIABXRMiDOAAAg");
	var mask_2_graphics_270 = new cjs.Graphics().p("EhBcgLoMCC4AAAIABXRMiC4AAAg");
	var mask_2_graphics_271 = new cjs.Graphics().p("EhBQgLoMCChAAAIAAXRMiChAAAg");
	var mask_2_graphics_272 = new cjs.Graphics().p("EhBDgLoMCCHAAAIAAXRMiCHAAAg");
	var mask_2_graphics_273 = new cjs.Graphics().p("EhA1gLoMCBrAAAIAAXRMiBrAAAg");
	var mask_2_graphics_274 = new cjs.Graphics().p("EhAngLoMCBPAAAIAAXRMiBPAAAg");
	var mask_2_graphics_275 = new cjs.Graphics().p("EhAXgLoMCAvAAAIAAXRMiAvAAAg");
	var mask_2_graphics_276 = new cjs.Graphics().p("EhAHgLoMCAPAAAIAAXRMiAPAAAg");
	var mask_2_graphics_277 = new cjs.Graphics().p("Eg/2gLoMB/tAAAIAAXRMh/tAAAg");
	var mask_2_graphics_278 = new cjs.Graphics().p("Eg/kgLoMB/JAAAIAAXRMh/JAAAg");
	var mask_2_graphics_279 = new cjs.Graphics().p("Eg/RgLoMB+jAAAIAAXRMh+jAAAg");
	var mask_2_graphics_280 = new cjs.Graphics().p("Eg+9gLoMB97AAAIAAXRMh97AAAg");
	var mask_2_graphics_281 = new cjs.Graphics().p("Eg+pgLoMB9TAAAIAAXRMh9TAAAg");
	var mask_2_graphics_282 = new cjs.Graphics().p("Eg+TgLoMB8nAAAIAAXRMh8nAAAg");
	var mask_2_graphics_283 = new cjs.Graphics().p("Eg99gLoMB77AAAIAAXRMh77AAAg");
	var mask_2_graphics_284 = new cjs.Graphics().p("Eg9lgLoMB7LAAAIAAXRMh7LAAAg");
	var mask_2_graphics_285 = new cjs.Graphics().p("Eg9NgLoMB6bAAAIAAXRMh6bAAAg");
	var mask_2_graphics_286 = new cjs.Graphics().p("Eg80gLoMB5pAAAIAAXRMh5pAAAg");
	var mask_2_graphics_287 = new cjs.Graphics().p("Eg8agLoMB41AAAIAAXRMh41AAAg");
	var mask_2_graphics_288 = new cjs.Graphics().p("Eg7/gLoMB3/AAAIAAXRMh3/AAAg");
	var mask_2_graphics_289 = new cjs.Graphics().p("Eg7kgLoMB3IAAAIABXRMh3IAAAg");
	var mask_2_graphics_290 = new cjs.Graphics().p("Eg7HgLoMB2PAAAIAAXRMh2PAAAg");
	var mask_2_graphics_291 = new cjs.Graphics().p("Eg6pgLoMB1TAAAIAAXRMh1TAAAg");
	var mask_2_graphics_292 = new cjs.Graphics().p("Eg6LgLoMB0XAAAIAAXRMh0XAAAg");
	var mask_2_graphics_293 = new cjs.Graphics().p("Eg5sgLoMBzZAAAIAAXRMhzZAAAg");
	var mask_2_graphics_294 = new cjs.Graphics().p("Eg5MgLoMByZAAAIAAXRMhyZAAAg");
	var mask_2_graphics_295 = new cjs.Graphics().p("Eg4rgLoMBxXAAAIAAXRMhxXAAAg");
	var mask_2_graphics_296 = new cjs.Graphics().p("Eg4JgLoMBwTAAAIAAXRMhwTAAAg");
	var mask_2_graphics_297 = new cjs.Graphics().p("Eg3mgLoMBvNAAAIAAXRMhvNAAAg");
	var mask_2_graphics_298 = new cjs.Graphics().p("Eg3DgLoMBuHAAAIAAXRMhuHAAAg");
	var mask_2_graphics_299 = new cjs.Graphics().p("Eg2egLoMBs9AAAIAAXRMhs9AAAg");
	var mask_2_graphics_300 = new cjs.Graphics().p("Eg15gLoMBrzAAAIAAXRMhrzAAAg");
	var mask_2_graphics_301 = new cjs.Graphics().p("Eg1SgLoMBqlAAAIAAXRMhqlAAAg");
	var mask_2_graphics_302 = new cjs.Graphics().p("Eg0rgLoMBpXAAAIAAXRMhpXAAAg");
	var mask_2_graphics_303 = new cjs.Graphics().p("Eg0DgLoMBoHAAAIAAXRMhoHAAAg");
	var mask_2_graphics_304 = new cjs.Graphics().p("EgzagLoMBm1AAAIAAXRMhm1AAAg");
	var mask_2_graphics_305 = new cjs.Graphics().p("EgyxgLoMBljAAAIAAXRMhljAAAg");
	var mask_2_graphics_306 = new cjs.Graphics().p("EgyGgLoMBkNAAAIAAXRMhkNAAAg");
	var mask_2_graphics_307 = new cjs.Graphics().p("EgxagLoMBi1AAAIAAXRMhi1AAAg");
	var mask_2_graphics_308 = new cjs.Graphics().p("EgwugLoMBhdAAAIAAXRMhhdAAAg");
	var mask_2_graphics_309 = new cjs.Graphics().p("EgwBgLoMBgDAAAIAAXRMhgDAAAg");
	var mask_2_graphics_310 = new cjs.Graphics().p("EgvTgLoMBemAAAIABXRMhemAAAg");
	var mask_2_graphics_311 = new cjs.Graphics().p("EgujgLoMBdHAAAIAAXRMhdHAAAg");
	var mask_2_graphics_312 = new cjs.Graphics().p("Egt0gLoMBbpAAAIAAXRMhbpAAAg");
	var mask_2_graphics_313 = new cjs.Graphics().p("EgtDgLoMBaHAAAIAAXRMhaHAAAg");
	var mask_2_graphics_314 = new cjs.Graphics().p("EgsRgLoMBYjAAAIAAXRMhYjAAAg");
	var mask_2_graphics_315 = new cjs.Graphics().p("EgregLoMBW9AAAIAAXRMhW9AAAg");
	var mask_2_graphics_316 = new cjs.Graphics().p("EgqrgLoMBVXAAAIAAXRMhVXAAAg");
	var mask_2_graphics_317 = new cjs.Graphics().p("Egp3gLoMBTvAAAIAAXRMhTvAAAg");
	var mask_2_graphics_318 = new cjs.Graphics().p("EgpBgLoMBSDAAAIAAXRMhSDAAAg");
	var mask_2_graphics_319 = new cjs.Graphics().p("EgoLgLoMBQXAAAIAAXRMhQXAAAg");
	var mask_2_graphics_320 = new cjs.Graphics().p("EgnUgLoMBOpAAAIAAXRMhOpAAAg");
	var mask_2_graphics_321 = new cjs.Graphics().p("EgmdgLoMBM7AAAIAAXRMhM7AAAg");
	var mask_2_graphics_322 = new cjs.Graphics().p("EglkgLoMBLJAAAIAAXRMhLJAAAg");
	var mask_2_graphics_323 = new cjs.Graphics().p("EgkqgLoMBJVAAAIAAXRMhJVAAAg");
	var mask_2_graphics_324 = new cjs.Graphics().p("EgjwgLoMBHhAAAIAAXRMhHhAAAg");
	var mask_2_graphics_325 = new cjs.Graphics().p("Egi0gLoMBFpAAAIAAXRMhFpAAAg");
	var mask_2_graphics_326 = new cjs.Graphics().p("Egh4gLoMBDxAAAIAAXRMhDxAAAg");
	var mask_2_graphics_327 = new cjs.Graphics().p("Egg7gLoMBB3AAAIAAXRMhB3AAAg");
	var mask_2_graphics_328 = new cjs.Graphics().p("A/9roMA/7AAAIAAXRMg/7AAAg");
	var mask_2_graphics_329 = new cjs.Graphics().p("A++roMA99AAAIAAXRMg99AAAg");
	var mask_2_graphics_330 = new cjs.Graphics().p("A9+roMA79AAAIAAXRMg79AAAg");
	var mask_2_graphics_331 = new cjs.Graphics().p("A8+roMA59AAAIAAXRMg59AAAg");
	var mask_2_graphics_332 = new cjs.Graphics().p("A78roMA35AAAIAAXRMg35AAAg");
	var mask_2_graphics_333 = new cjs.Graphics().p("A66roMA11AAAIAAXRMg11AAAg");
	var mask_2_graphics_334 = new cjs.Graphics().p("A52roMAztAAAIAAXRMgztAAAg");
	var mask_2_graphics_335 = new cjs.Graphics().p("A4yroMAxlAAAIAAXRMgxlAAAg");
	var mask_2_graphics_336 = new cjs.Graphics().p("A3troMAvbAAAIAAXRMgvbAAAg");
	var mask_2_graphics_337 = new cjs.Graphics().p("A2nroMAtPAAAIAAXRMgtPAAAg");
	var mask_2_graphics_338 = new cjs.Graphics().p("A1hroMArCAAAIABXRMgrCAAAg");
	var mask_2_graphics_339 = new cjs.Graphics().p("A0ZroMAozAAAIAAXRMgozAAAg");
	var mask_2_graphics_340 = new cjs.Graphics().p("AzQroMAmhAAAIAAXRMgmhAAAg");
	var mask_2_graphics_341 = new cjs.Graphics().p("AyHroMAkPAAAIAAXRMgkPAAAg");
	var mask_2_graphics_342 = new cjs.Graphics().p("Aw8roMAh5AAAIAAXRMgh5AAAg");
	var mask_2_graphics_343 = new cjs.Graphics().p("AvxroIfjAAIAAXRI/jAAg");
	var mask_2_graphics_344 = new cjs.Graphics().p("AulroIdLAAIAAXRI9LAAg");
	var mask_2_graphics_345 = new cjs.Graphics().p("AtYroIaxAAIAAXRI6xAAg");
	var mask_2_graphics_346 = new cjs.Graphics().p("AsKroIYVAAIAAXRI4VAAg");
	var mask_2_graphics_347 = new cjs.Graphics().p("Aq8LpIAA3RIV5AAIAAXRg");
	var mask_2_graphics_348 = new cjs.Graphics().p("ApsLpIAA3RITZAAIAAXRg");
	var mask_2_graphics_349 = new cjs.Graphics().p("AocLpIAA3RIQ5AAIAAXRg");
	var mask_2_graphics_350 = new cjs.Graphics().p("AnKLpIAA3RIOVAAIAAXRg");
	var mask_2_graphics_351 = new cjs.Graphics().p("Al4LpIAA3RILxAAIAAXRg");
	var mask_2_graphics_352 = new cjs.Graphics().p("AklLpIAA3RIJLAAIAAXRg");
	var mask_2_graphics_353 = new cjs.Graphics().p("AjRLpIAA3RIGjAAIAAXRg");
	var mask_2_graphics_354 = new cjs.Graphics().p("AjALpIAA3RID5AAIAAXRg");
	var mask_2_graphics_355 = new cjs.Graphics().p("Ai1LpIAA3RIBPAAIAAXRg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(79).to({graphics:mask_2_graphics_79,x:46.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_80,x:46.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_81,x:46.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_82,x:46.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_83,x:46.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_84,x:47.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_85,x:47.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_86,x:48.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_87,x:48.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_88,x:49.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_89,x:49.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_90,x:50.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_91,x:51.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_92,x:52.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_93,x:53.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_94,x:54.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_95,x:55.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_96,x:56.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_97,x:57.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_98,x:58.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_99,x:60.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_100,x:61.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_101,x:63.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_102,x:64.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_103,x:66.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_104,x:68,y:36.5}).wait(1).to({graphics:mask_2_graphics_105,x:69.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_106,x:71.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_107,x:73.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_108,x:75.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_109,x:77.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_110,x:79.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_111,x:81.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_112,x:84,y:36.5}).wait(1).to({graphics:mask_2_graphics_113,x:86.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_114,x:88.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_115,x:91.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_116,x:93.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_117,x:96.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_118,x:99,y:36.5}).wait(1).to({graphics:mask_2_graphics_119,x:101.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_120,x:104.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_121,x:107.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_122,x:110.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_123,x:113.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_124,x:116.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_125,x:119.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_126,x:122.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_127,x:126.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_128,x:129.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_129,x:132.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_130,x:136.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_131,x:139.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_132,x:143.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_133,x:147.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_134,x:151,y:36.5}).wait(1).to({graphics:mask_2_graphics_135,x:154.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_136,x:158.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_137,x:162.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_138,x:166.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_139,x:170.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_140,x:175.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_141,x:179.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_142,x:183.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_143,x:188.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_144,x:192.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_145,x:197.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_146,x:201.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_147,x:206.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_148,x:211.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_149,x:215.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_150,x:220.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_151,x:225.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_152,x:230.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_153,x:235.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_154,x:241,y:36.5}).wait(1).to({graphics:mask_2_graphics_155,x:246.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_156,x:251.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_157,x:256.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_158,x:262.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_159,x:267.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_160,x:273.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_161,x:279,y:36.5}).wait(1).to({graphics:mask_2_graphics_162,x:284.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_163,x:290.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_164,x:296.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_165,x:302.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_166,x:308.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_167,x:314.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_168,x:320.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_169,x:326.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_170,x:332.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_171,x:339.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_172,x:345.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_173,x:352,y:36.5}).wait(1).to({graphics:mask_2_graphics_174,x:358.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_175,x:365.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_176,x:371.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_177,x:378.6,y:36.5}).wait(80).to({graphics:mask_2_graphics_257,x:284.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_258,x:284.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_259,x:284.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_260,x:284.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_261,x:284.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_262,x:284,y:36.5}).wait(1).to({graphics:mask_2_graphics_263,x:283.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_264,x:283.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_265,x:282.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_266,x:282.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_267,x:281.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_268,x:280.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_269,x:280.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_270,x:279.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_271,x:278.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_272,x:277.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_273,x:276.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_274,x:275.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_275,x:274.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_276,x:272.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_277,x:271.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_278,x:270.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_279,x:268.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_280,x:267.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_281,x:265.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_282,x:264.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_283,x:262.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_284,x:260.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_285,x:258.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_286,x:257.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_287,x:255.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_288,x:253.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_289,x:251,y:36.5}).wait(1).to({graphics:mask_2_graphics_290,x:248.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_291,x:246.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_292,x:244.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_293,x:242,y:36.5}).wait(1).to({graphics:mask_2_graphics_294,x:239.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_295,x:237.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_296,x:234.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_297,x:232,y:36.5}).wait(1).to({graphics:mask_2_graphics_298,x:229.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_299,x:226.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_300,x:223.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_301,x:220.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_302,x:217.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_303,x:214.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_304,x:211.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_305,x:208.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_306,x:205.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_307,x:202.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_308,x:198.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_309,x:195.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_310,x:192,y:36.5}).wait(1).to({graphics:mask_2_graphics_311,x:188.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_312,x:184.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_313,x:181.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_314,x:177.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_315,x:173.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_316,x:169.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_317,x:165.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_318,x:161.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_319,x:157.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_320,x:153.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_321,x:149.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_322,x:145.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_323,x:140.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_324,x:136.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_325,x:132,y:36.5}).wait(1).to({graphics:mask_2_graphics_326,x:127.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_327,x:122.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_328,x:118.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_329,x:113.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_330,x:108.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_331,x:103.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_332,x:98.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_333,x:93.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_334,x:88.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_335,x:83.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_336,x:78.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_337,x:73.3,y:36.5}).wait(1).to({graphics:mask_2_graphics_338,x:68,y:36.5}).wait(1).to({graphics:mask_2_graphics_339,x:62.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_340,x:57.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_341,x:51.6,y:36.5}).wait(1).to({graphics:mask_2_graphics_342,x:46,y:36.5}).wait(1).to({graphics:mask_2_graphics_343,x:40.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_344,x:34.7,y:36.5}).wait(1).to({graphics:mask_2_graphics_345,x:28.9,y:36.5}).wait(1).to({graphics:mask_2_graphics_346,x:23,y:36.5}).wait(1).to({graphics:mask_2_graphics_347,x:17.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_348,x:11.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_349,x:5.1,y:36.5}).wait(1).to({graphics:mask_2_graphics_350,x:-1,y:36.5}).wait(1).to({graphics:mask_2_graphics_351,x:-7.2,y:36.5}).wait(1).to({graphics:mask_2_graphics_352,x:-13.5,y:36.5}).wait(1).to({graphics:mask_2_graphics_353,x:-19.8,y:36.5}).wait(1).to({graphics:mask_2_graphics_354,x:-19.4,y:36.5}).wait(1).to({graphics:mask_2_graphics_355,x:-18.3,y:36.5}).wait(240));

	// copy_1
	this.copy_1_mc = new lib.copy_1_mc();
	this.copy_1_mc.setTransform(152.4,152.8);
	this.copy_1_mc._off = true;

	this.copy_1_mc.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.copy_1_mc).wait(79).to({_off:false},0).wait(178).to({_off:true},257).wait(81));

	// BG
	this.instance_3 = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(595));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-98.1,1208,288.2);


// stage content:
(lib.squeegee_728x90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Eg43AHCIAAuDMBxvAAAIAAODg");
	mask.setTransform(364,45);

	// Content
	this.content_mc = new lib.content_mc();
	this.content_mc.setTransform(150,125,1,1,0,0,0,150,125);

	this.content_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.content_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(364,45,728,90);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;