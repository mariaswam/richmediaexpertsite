(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 70,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bg.jpg?1489012538578", id:"bg"},
		{src:"images/cta.png?1489012538578", id:"cta"},
		{src:"images/hands.png?1489012538578", id:"hands"},
		{src:"images/logo.png?1489012538578", id:"logo"},
		{src:"images/logobig.png?1489012538578", id:"logobig"},
		{src:"images/thejoy.png?1489012538578", id:"thejoy"},
		{src:"images/whatever.png?1489012538578", id:"whatever"}
	]
};



// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,274,77);


(lib.hands = function() {
	this.initialize(img.hands);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,915,746);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,71,48);


(lib.logobig = function() {
	this.initialize(img.logobig);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,506,246);


(lib.thejoy = function() {
	this.initialize(img.thejoy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,60);


(lib.whatever = function() {
	this.initialize(img.whatever);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,206,94);


(lib.hands_animation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.hands();
	this.instance.setTransform(31.9,-335);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(31.9,-335,915,746);


(lib.cta_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.cta();
	this.instance.setTransform(-68.5,-19.2,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-19.2,137,38.5);


(lib.copy_2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.logo();
	this.instance.setTransform(-35,-113);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.whatever();
	this.instance_1.setTransform(-103,-47);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-103,-113,206,160);


(lib.copy_1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.logo();
	this.instance.setTransform(-37,-96);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.thejoy();
	this.instance_1.setTransform(-109,-31);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-109,-96,217,125);


(lib.copu_3_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.logobig();
	this.instance.setTransform(-125.5,-109.5,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-125.5,-109.5,253,123);


(lib.hands2_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_50 = function() {
		this.parent.play();
		
		trace("TEST");
	}
	this.frame_150 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(50).call(this.frame_50).wait(100).call(this.frame_150).wait(1));

	// Hands
	this.instance = new lib.hands_animation();
	this.instance.setTransform(-1498.1,1.4,1.541,1.541);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:-238.2},50,cjs.Ease.get(1)).to({scaleX:0.71,x:-1024.8,y:1.9},100,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1449,-514.8,1409.7,1149.4);


(lib.hands1_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 2000);
	}
	this.frame_50 = function() {
		this.parent.play();
	}
	this.frame_150 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(50).call(this.frame_50).wait(100).call(this.frame_150).wait(1));

	// Hands
	this.instance = new lib.hands_animation();
	this.instance.setTransform(-1498.1,1.4,1.541,1.541);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:-238.2},50,cjs.Ease.get(1)).to({scaleX:0.71,x:-1024.8,y:1.9},100,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1449,-514.8,1409.7,1149.4);


(lib.hands_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_44 = function() {
		this.parent.play();
	}
	this.frame_144 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(44).call(this.frame_44).wait(100).call(this.frame_144).wait(1));

	// Hands
	this.instance = new lib.hands_animation();
	this.instance.setTransform(-1498.1,1.4,1.541,1.541);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:-476.1},44,cjs.Ease.get(1)).to({scaleX:0.71,x:-1024.8,y:1.9},100,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1449,-514.8,1409.7,1149.4);


(lib.content_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		
		var tl = new TimelineMax();
		//tl.timeScale( 0.5 );
		
		hands        = this.hands_mc;
		hands3        = this.hands3_mc;
		cta          = this.cta_mc;
	}
	this.frame_79 = function() {
		this.stop();
		
		setTimeout(function(){
			hands1.play();
		}, 2500);
	}
	this.frame_174 = function() {
		this.stop();
		
		setTimeout(function(){
			hands.play();
		}, 2500);
	}
	this.frame_257 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 1000);
	}
	this.frame_292 = function() {
		this.stop();
		var $this = this;
		
		setTimeout(function(){
			$this.play();
		}, 1000);
	}
	this.frame_317 = function() {
		this.stop();
		
		var myBtn = document.getElementById("canvas");
			
		/***[ MOUSE OVER ]***/
		myBtn.addEventListener("mouseover", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1.1, scaleY: 1.1, ease:Power1.easeOut});
		});
			
		/***[ MOUSE OUT ]***/
		myBtn.addEventListener("mouseout", function(){
			TweenMax.to(cta, 0.3, {scaleX: 1, scaleY: 1, ease:Power1.easeOut});
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(79).call(this.frame_79).wait(95).call(this.frame_174).wait(83).call(this.frame_257).wait(35).call(this.frame_292).wait(25).call(this.frame_317).wait(1));

	// Hands 1
	this.hands_mc = new lib.hands_mc();
	this.hands_mc.setTransform(157.3,10.3,0.495,0.495,90,0,0,-0.7,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.hands_mc).to({_off:true},79).wait(239));

	// Hands
	this.hands1 = new lib.hands1_mc();
	this.hands1.setTransform(302.1,131.5,0.41,0.41,180,0,0,-0.6,-0.1);

	this.hands_mc_1 = new lib.hands2_mc();
	this.hands_mc_1.setTransform(9.3,131.5,0.41,0.41,0,-180,0,-0.6,-0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.hands1}]},79).to({state:[{t:this.hands_mc_1}]},95).to({state:[{t:this.hands_mc_1}]},143).wait(1));

	// Mask --- f3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_81 = new cjs.Graphics().p("A3aTiMAAAgnDMAu2AAAMAAAAnDg");
	var mask_graphics_174 = new cjs.Graphics().p("AW1TiMAAAgnDIAEAAMAAAAnDg");
	var mask_graphics_175 = new cjs.Graphics().p("AWQTiMAAAgnDIApAAMAAAAnDg");
	var mask_graphics_176 = new cjs.Graphics().p("AVsTiMAAAgnDIBNAAMAAAAnDg");
	var mask_graphics_177 = new cjs.Graphics().p("AVITiMAAAgnDIBxAAMAAAAnDg");
	var mask_graphics_178 = new cjs.Graphics().p("AUkTiMAAAgnDICVAAMAAAAnDg");
	var mask_graphics_179 = new cjs.Graphics().p("AT/TiMAAAgnDIC6AAMAAAAnDg");
	var mask_graphics_180 = new cjs.Graphics().p("ATbTiMAAAgnDIDeAAMAAAAnDg");
	var mask_graphics_181 = new cjs.Graphics().p("AS3TiMAAAgnDIECAAMAAAAnDg");
	var mask_graphics_182 = new cjs.Graphics().p("ASTTiMAAAgnDIElAAMAAAAnDg");
	var mask_graphics_183 = new cjs.Graphics().p("ARvTiMAAAgnDIFJAAMAAAAnDg");
	var mask_graphics_184 = new cjs.Graphics().p("ARKTiMAAAgnDIFuAAMAAAAnDg");
	var mask_graphics_185 = new cjs.Graphics().p("AQmTiMAAAgnDIGSAAMAAAAnDg");
	var mask_graphics_186 = new cjs.Graphics().p("AQCTiMAAAgnDIG2AAMAAAAnDg");
	var mask_graphics_187 = new cjs.Graphics().p("APeTiMAAAgnDIHaAAMAAAAnDg");
	var mask_graphics_188 = new cjs.Graphics().p("AO6TiMAAAgnDIH+AAMAAAAnDg");
	var mask_graphics_189 = new cjs.Graphics().p("AOVTiMAAAgnDIIjAAMAAAAnDg");
	var mask_graphics_190 = new cjs.Graphics().p("ANxTiMAAAgnDIJGAAMAAAAnDg");
	var mask_graphics_191 = new cjs.Graphics().p("ANNTiMAAAgnDIJqAAMAAAAnDg");
	var mask_graphics_192 = new cjs.Graphics().p("AMpTiMAAAgnDIKOAAMAAAAnDg");
	var mask_graphics_193 = new cjs.Graphics().p("AMETiMAAAgnDIKzAAMAAAAnDg");
	var mask_graphics_194 = new cjs.Graphics().p("ALgTiMAAAgnDILXAAMAAAAnDg");
	var mask_graphics_195 = new cjs.Graphics().p("AK8TiMAAAgnDIL7AAMAAAAnDg");
	var mask_graphics_196 = new cjs.Graphics().p("AKYTiMAAAgnDIMfAAMAAAAnDg");
	var mask_graphics_197 = new cjs.Graphics().p("AJ0TiMAAAgnDINDAAMAAAAnDg");
	var mask_graphics_198 = new cjs.Graphics().p("AJPTiMAAAgnDINnAAMAAAAnDg");
	var mask_graphics_199 = new cjs.Graphics().p("AIrTiMAAAgnDIOLAAMAAAAnDg");
	var mask_graphics_200 = new cjs.Graphics().p("AIHTiMAAAgnDIOvAAMAAAAnDg");
	var mask_graphics_201 = new cjs.Graphics().p("AHjTiMAAAgnDIPTAAMAAAAnDg");
	var mask_graphics_202 = new cjs.Graphics().p("AG/TiMAAAgnDIP3AAMAAAAnDg");
	var mask_graphics_203 = new cjs.Graphics().p("AGaTiMAAAgnDIQcAAMAAAAnDg");
	var mask_graphics_204 = new cjs.Graphics().p("AF2TiMAAAgnDIRAAAMAAAAnDg");
	var mask_graphics_205 = new cjs.Graphics().p("AFSTiMAAAgnDIRjAAMAAAAnDg");
	var mask_graphics_206 = new cjs.Graphics().p("AEuTiMAAAgnDISHAAMAAAAnDg");
	var mask_graphics_207 = new cjs.Graphics().p("AEJTiMAAAgnDISsAAMAAAAnDg");
	var mask_graphics_208 = new cjs.Graphics().p("ADlTiMAAAgnDITQAAMAAAAnDg");
	var mask_graphics_209 = new cjs.Graphics().p("ADBTiMAAAgnDIT0AAMAAAAnDg");
	var mask_graphics_210 = new cjs.Graphics().p("ACdTiMAAAgnDIUYAAMAAAAnDg");
	var mask_graphics_211 = new cjs.Graphics().p("AB5TiMAAAgnDIU8AAMAAAAnDg");
	var mask_graphics_212 = new cjs.Graphics().p("ABUTiMAAAgnDIVhAAMAAAAnDg");
	var mask_graphics_213 = new cjs.Graphics().p("AAwTiMAAAgnDIWEAAMAAAAnDg");
	var mask_graphics_214 = new cjs.Graphics().p("AAMTiMAAAgnDIWoAAMAAAAnDg");
	var mask_graphics_215 = new cjs.Graphics().p("AgWTiMAAAgnDIXKAAMAAAAnDg");
	var mask_graphics_216 = new cjs.Graphics().p("Ag6TiMAAAgnDIXuAAMAAAAnDg");
	var mask_graphics_217 = new cjs.Graphics().p("AhfTiMAAAgnDIYTAAMAAAAnDg");
	var mask_graphics_218 = new cjs.Graphics().p("AiDTiMAAAgnDIY3AAMAAAAnDg");
	var mask_graphics_219 = new cjs.Graphics().p("AinTiMAAAgnDIZbAAMAAAAnDg");
	var mask_graphics_220 = new cjs.Graphics().p("AjLTiMAAAgnDIZ/AAMAAAAnDg");
	var mask_graphics_221 = new cjs.Graphics().p("AjwTiMAAAgnDIajAAMAAAAnDg");
	var mask_graphics_222 = new cjs.Graphics().p("AkUTiMAAAgnDIbHAAMAAAAnDg");
	var mask_graphics_223 = new cjs.Graphics().p("Ak4TiMAAAgnDIbrAAMAAAAnDg");
	var mask_graphics_224 = new cjs.Graphics().p("AlcTiMAAAgnDIcPAAMAAAAnDg");
	var mask_graphics_225 = new cjs.Graphics().p("AmATiMAAAgnDIczAAMAAAAnDg");
	var mask_graphics_226 = new cjs.Graphics().p("AmlTiMAAAgnDIdYAAMAAAAnDg");
	var mask_graphics_227 = new cjs.Graphics().p("AnJTiMAAAgnDId8AAMAAAAnDg");
	var mask_graphics_228 = new cjs.Graphics().p("AntTiMAAAgnDIegAAMAAAAnDg");
	var mask_graphics_229 = new cjs.Graphics().p("AoRTiMAAAgnDIfDAAMAAAAnDg");
	var mask_graphics_230 = new cjs.Graphics().p("Ao1TiMAAAgnDIfnAAMAAAAnDg");
	var mask_graphics_231 = new cjs.Graphics().p("ApaTiMAAAgnDMAgMAAAMAAAAnDg");
	var mask_graphics_232 = new cjs.Graphics().p("Ap+TiMAAAgnDMAgwAAAMAAAAnDg");
	var mask_graphics_233 = new cjs.Graphics().p("AqiTiMAAAgnDMAhUAAAMAAAAnDg");
	var mask_graphics_234 = new cjs.Graphics().p("ArGTiMAAAgnDMAh4AAAMAAAAnDg");
	var mask_graphics_235 = new cjs.Graphics().p("ArrTiMAAAgnDMAidAAAMAAAAnDg");
	var mask_graphics_236 = new cjs.Graphics().p("AsPTiMAAAgnDMAjAAAAMAAAAnDg");
	var mask_graphics_237 = new cjs.Graphics().p("AszTiMAAAgnDMAjkAAAMAAAAnDg");
	var mask_graphics_238 = new cjs.Graphics().p("AtXTiMAAAgnDMAkIAAAMAAAAnDg");
	var mask_graphics_239 = new cjs.Graphics().p("At7TiMAAAgnDMAksAAAMAAAAnDg");
	var mask_graphics_240 = new cjs.Graphics().p("AugTiMAAAgnDMAlRAAAMAAAAnDg");
	var mask_graphics_241 = new cjs.Graphics().p("AvETiMAAAgnDMAl1AAAMAAAAnDg");
	var mask_graphics_242 = new cjs.Graphics().p("AvoTiMAAAgnDMAmZAAAMAAAAnDg");
	var mask_graphics_243 = new cjs.Graphics().p("AwMTiMAAAgnDMAm9AAAMAAAAnDg");
	var mask_graphics_244 = new cjs.Graphics().p("AwwTiMAAAgnDMAngAAAMAAAAnDg");
	var mask_graphics_245 = new cjs.Graphics().p("AxVTiMAAAgnDMAoFAAAMAAAAnDg");
	var mask_graphics_246 = new cjs.Graphics().p("Ax5TiMAAAgnDMAopAAAMAAAAnDg");
	var mask_graphics_247 = new cjs.Graphics().p("AydTiMAAAgnDMApNAAAMAAAAnDg");
	var mask_graphics_248 = new cjs.Graphics().p("AzBTiMAAAgnDMApxAAAMAAAAnDg");
	var mask_graphics_249 = new cjs.Graphics().p("AzmTiMAAAgnDMAqWAAAMAAAAnDg");
	var mask_graphics_250 = new cjs.Graphics().p("A0KTiMAAAgnDMAq6AAAMAAAAnDg");
	var mask_graphics_251 = new cjs.Graphics().p("A0uTiMAAAgnDMAreAAAMAAAAnDg");
	var mask_graphics_252 = new cjs.Graphics().p("A1STiMAAAgnDMAsBAAAMAAAAnDg");
	var mask_graphics_253 = new cjs.Graphics().p("A12TiMAAAgnDMAslAAAMAAAAnDg");
	var mask_graphics_254 = new cjs.Graphics().p("A2bTiMAAAgnDMAtKAAAMAAAAnDg");
	var mask_graphics_255 = new cjs.Graphics().p("A22TiMAAAgnDMAttAAAMAAAAnDg");
	var mask_graphics_256 = new cjs.Graphics().p("A3ITiMAAAgnDMAuRAAAMAAAAnDg");
	var mask_graphics_257 = new cjs.Graphics().p("A3aTiMAAAgnDMAu2AAAMAAAAnDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(81).to({graphics:mask_graphics_81,x:150,y:125}).wait(93).to({graphics:mask_graphics_174,x:146.6,y:125}).wait(1).to({graphics:mask_graphics_175,x:146.6,y:125}).wait(1).to({graphics:mask_graphics_176,x:146.6,y:125}).wait(1).to({graphics:mask_graphics_177,x:146.6,y:125}).wait(1).to({graphics:mask_graphics_178,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_179,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_180,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_181,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_182,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_183,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_184,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_185,x:146.5,y:125}).wait(1).to({graphics:mask_graphics_186,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_187,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_188,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_189,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_190,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_191,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_192,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_193,x:146.4,y:125}).wait(1).to({graphics:mask_graphics_194,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_195,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_196,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_197,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_198,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_199,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_200,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_201,x:146.3,y:125}).wait(1).to({graphics:mask_graphics_202,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_203,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_204,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_205,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_206,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_207,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_208,x:146.2,y:125}).wait(1).to({graphics:mask_graphics_209,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_210,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_211,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_212,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_213,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_214,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_215,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_216,x:146.1,y:125}).wait(1).to({graphics:mask_graphics_217,x:146,y:125}).wait(1).to({graphics:mask_graphics_218,x:146,y:125}).wait(1).to({graphics:mask_graphics_219,x:146,y:125}).wait(1).to({graphics:mask_graphics_220,x:146,y:125}).wait(1).to({graphics:mask_graphics_221,x:146,y:125}).wait(1).to({graphics:mask_graphics_222,x:146,y:125}).wait(1).to({graphics:mask_graphics_223,x:146,y:125}).wait(1).to({graphics:mask_graphics_224,x:146,y:125}).wait(1).to({graphics:mask_graphics_225,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_226,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_227,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_228,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_229,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_230,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_231,x:145.9,y:125}).wait(1).to({graphics:mask_graphics_232,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_233,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_234,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_235,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_236,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_237,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_238,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_239,x:145.8,y:125}).wait(1).to({graphics:mask_graphics_240,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_241,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_242,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_243,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_244,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_245,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_246,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_247,x:145.7,y:125}).wait(1).to({graphics:mask_graphics_248,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_249,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_250,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_251,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_252,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_253,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_254,x:145.6,y:125}).wait(1).to({graphics:mask_graphics_255,x:144.7,y:125}).wait(1).to({graphics:mask_graphics_256,x:142.9,y:125}).wait(1).to({graphics:mask_graphics_257,x:141.1,y:125}).wait(61));

	// copy_3
	this.copy_3_mc = new lib.copu_3_mc();
	this.copy_3_mc.setTransform(151,170.2);
	this.copy_3_mc._off = true;

	this.copy_3_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.copy_3_mc).wait(174).to({_off:false},0).wait(144));

	// cta
	this.cta_mc = new lib.cta_mc();
	this.cta_mc.setTransform(151,210.3);
	this.cta_mc.alpha = 0;
	this.cta_mc._off = true;

	this.cta_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(174).to({_off:false},0).wait(83).to({y:220.3},0).to({y:210.3,alpha:1},35,cjs.Ease.get(1)).to({scaleX:1.15,scaleY:1.15},12,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},13,cjs.Ease.get(-1)).wait(1));

	// Mask --- f2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_81 = new cjs.Graphics().p("ACrTiMAAAgnDIAbAAMAAAAnDg");
	var mask_1_graphics_82 = new cjs.Graphics().p("ACVTiMAAAgnDIBCAAMAAAAnDg");
	var mask_1_graphics_83 = new cjs.Graphics().p("AB/TiMAAAgnDIBqAAMAAAAnDg");
	var mask_1_graphics_84 = new cjs.Graphics().p("ABpTiMAAAgnDICRAAMAAAAnDg");
	var mask_1_graphics_85 = new cjs.Graphics().p("ABSTiMAAAgnDIC5AAMAAAAnDg");
	var mask_1_graphics_86 = new cjs.Graphics().p("AA8TiMAAAgnDIDhAAMAAAAnDg");
	var mask_1_graphics_87 = new cjs.Graphics().p("AAmTiMAAAgnDIEIAAMAAAAnDg");
	var mask_1_graphics_88 = new cjs.Graphics().p("AAPTiMAAAgnDIExAAMAAAAnDg");
	var mask_1_graphics_89 = new cjs.Graphics().p("AgFTiMAAAgnDIFWAAMAAAAnDg");
	var mask_1_graphics_90 = new cjs.Graphics().p("AgbTiMAAAgnDIF9AAMAAAAnDg");
	var mask_1_graphics_91 = new cjs.Graphics().p("AgxTiMAAAgnDIGlAAMAAAAnDg");
	var mask_1_graphics_92 = new cjs.Graphics().p("AhITiMAAAgnDIHNAAMAAAAnDg");
	var mask_1_graphics_93 = new cjs.Graphics().p("AheTiMAAAgnDIH1AAMAAAAnDg");
	var mask_1_graphics_94 = new cjs.Graphics().p("Ah0TiMAAAgnDIIcAAMAAAAnDg");
	var mask_1_graphics_95 = new cjs.Graphics().p("AiKTiMAAAgnDIJDAAMAAAAnDg");
	var mask_1_graphics_96 = new cjs.Graphics().p("AihTiMAAAgnDIJsAAMAAAAnDg");
	var mask_1_graphics_97 = new cjs.Graphics().p("Ai3TiMAAAgnDIKTAAMAAAAnDg");
	var mask_1_graphics_98 = new cjs.Graphics().p("AjNTiMAAAgnDIK6AAMAAAAnDg");
	var mask_1_graphics_99 = new cjs.Graphics().p("AjjTiMAAAgnDILiAAMAAAAnDg");
	var mask_1_graphics_100 = new cjs.Graphics().p("Aj6TiMAAAgnDIMKAAMAAAAnDg");
	var mask_1_graphics_101 = new cjs.Graphics().p("AkQTiMAAAgnDIMyAAMAAAAnDg");
	var mask_1_graphics_102 = new cjs.Graphics().p("AkmTiMAAAgnDINZAAMAAAAnDg");
	var mask_1_graphics_103 = new cjs.Graphics().p("Ak8TiMAAAgnDIOAAAMAAAAnDg");
	var mask_1_graphics_104 = new cjs.Graphics().p("AlTTiMAAAgnDIOpAAMAAAAnDg");
	var mask_1_graphics_105 = new cjs.Graphics().p("AlpTiMAAAgnDIPQAAMAAAAnDg");
	var mask_1_graphics_106 = new cjs.Graphics().p("Al/TiMAAAgnDIP4AAMAAAAnDg");
	var mask_1_graphics_107 = new cjs.Graphics().p("AmVTiMAAAgnDIQfAAMAAAAnDg");
	var mask_1_graphics_108 = new cjs.Graphics().p("AmsTiMAAAgnDIRHAAMAAAAnDg");
	var mask_1_graphics_109 = new cjs.Graphics().p("AnCTiMAAAgnDIRvAAMAAAAnDg");
	var mask_1_graphics_110 = new cjs.Graphics().p("AnYTiMAAAgnDISWAAMAAAAnDg");
	var mask_1_graphics_111 = new cjs.Graphics().p("AnuTiMAAAgnDIS9AAMAAAAnDg");
	var mask_1_graphics_112 = new cjs.Graphics().p("AoFTiMAAAgnDITmAAMAAAAnDg");
	var mask_1_graphics_113 = new cjs.Graphics().p("AobTiMAAAgnDIUNAAMAAAAnDg");
	var mask_1_graphics_114 = new cjs.Graphics().p("AoxTiMAAAgnDIU1AAMAAAAnDg");
	var mask_1_graphics_115 = new cjs.Graphics().p("ApITiMAAAgnDIVdAAMAAAAnDg");
	var mask_1_graphics_116 = new cjs.Graphics().p("ApeTiMAAAgnDIWEAAMAAAAnDg");
	var mask_1_graphics_117 = new cjs.Graphics().p("Ap0TiMAAAgnDIWsAAMAAAAnDg");
	var mask_1_graphics_118 = new cjs.Graphics().p("AqKTiMAAAgnDIXTAAMAAAAnDg");
	var mask_1_graphics_119 = new cjs.Graphics().p("AqhTiMAAAgnDIX8AAMAAAAnDg");
	var mask_1_graphics_120 = new cjs.Graphics().p("Aq3TiMAAAgnDIYjAAMAAAAnDg");
	var mask_1_graphics_121 = new cjs.Graphics().p("ArNTiMAAAgnDIZKAAMAAAAnDg");
	var mask_1_graphics_122 = new cjs.Graphics().p("ArjTiMAAAgnDIZyAAMAAAAnDg");
	var mask_1_graphics_123 = new cjs.Graphics().p("Ar6TiMAAAgnDIaaAAMAAAAnDg");
	var mask_1_graphics_124 = new cjs.Graphics().p("AsQTiMAAAgnDIbBAAMAAAAnDg");
	var mask_1_graphics_125 = new cjs.Graphics().p("AsmTiMAAAgnDIbpAAMAAAAnDg");
	var mask_1_graphics_126 = new cjs.Graphics().p("As8TiMAAAgnDIcQAAMAAAAnDg");
	var mask_1_graphics_127 = new cjs.Graphics().p("AtTTiMAAAgnDIc5AAMAAAAnDg");
	var mask_1_graphics_128 = new cjs.Graphics().p("AtpTiMAAAgnDIdgAAMAAAAnDg");
	var mask_1_graphics_129 = new cjs.Graphics().p("At/TiMAAAgnDIeHAAMAAAAnDg");
	var mask_1_graphics_130 = new cjs.Graphics().p("AuVTiMAAAgnDIevAAMAAAAnDg");
	var mask_1_graphics_131 = new cjs.Graphics().p("AusTiMAAAgnDIfXAAMAAAAnDg");
	var mask_1_graphics_132 = new cjs.Graphics().p("AvCTiMAAAgnDIf/AAMAAAAnDg");
	var mask_1_graphics_133 = new cjs.Graphics().p("AvYTiMAAAgnDMAgmAAAMAAAAnDg");
	var mask_1_graphics_134 = new cjs.Graphics().p("AvuTiMAAAgnDMAhNAAAMAAAAnDg");
	var mask_1_graphics_135 = new cjs.Graphics().p("AwFTiMAAAgnDMAh2AAAMAAAAnDg");
	var mask_1_graphics_136 = new cjs.Graphics().p("AwbTiMAAAgnDMAidAAAMAAAAnDg");
	var mask_1_graphics_137 = new cjs.Graphics().p("AwxTiMAAAgnDMAjFAAAMAAAAnDg");
	var mask_1_graphics_138 = new cjs.Graphics().p("AxHTiMAAAgnDMAjsAAAMAAAAnDg");
	var mask_1_graphics_139 = new cjs.Graphics().p("AxeTiMAAAgnDMAkUAAAMAAAAnDg");
	var mask_1_graphics_140 = new cjs.Graphics().p("Ax0TiMAAAgnDMAk8AAAMAAAAnDg");
	var mask_1_graphics_141 = new cjs.Graphics().p("AyKTiMAAAgnDMAljAAAMAAAAnDg");
	var mask_1_graphics_142 = new cjs.Graphics().p("AyhTiMAAAgnDMAmLAAAMAAAAnDg");
	var mask_1_graphics_143 = new cjs.Graphics().p("Ay3TiMAAAgnDMAmzAAAMAAAAnDg");
	var mask_1_graphics_144 = new cjs.Graphics().p("AzNTiMAAAgnDMAnaAAAMAAAAnDg");
	var mask_1_graphics_145 = new cjs.Graphics().p("AzjTiMAAAgnDMAoCAAAMAAAAnDg");
	var mask_1_graphics_146 = new cjs.Graphics().p("Az6TiMAAAgnDMAoqAAAMAAAAnDg");
	var mask_1_graphics_147 = new cjs.Graphics().p("A0QTiMAAAgnDMApRAAAMAAAAnDg");
	var mask_1_graphics_148 = new cjs.Graphics().p("A0mTiMAAAgnDMAp5AAAMAAAAnDg");
	var mask_1_graphics_149 = new cjs.Graphics().p("A08TiMAAAgnDMAqgAAAMAAAAnDg");
	var mask_1_graphics_150 = new cjs.Graphics().p("A1TTiMAAAgnDMArJAAAMAAAAnDg");
	var mask_1_graphics_151 = new cjs.Graphics().p("A1pTiMAAAgnDMArwAAAMAAAAnDg");
	var mask_1_graphics_152 = new cjs.Graphics().p("A1/TiMAAAgnDMAsXAAAMAAAAnDg");
	var mask_1_graphics_153 = new cjs.Graphics().p("A2VTiMAAAgnDMAs/AAAMAAAAnDg");
	var mask_1_graphics_154 = new cjs.Graphics().p("A2sTiMAAAgnDMAtnAAAMAAAAnDg");
	var mask_1_graphics_155 = new cjs.Graphics().p("A3CTiMAAAgnDMAuOAAAMAAAAnDg");
	var mask_1_graphics_156 = new cjs.Graphics().p("A3YTiMAAAgnDMAu2AAAMAAAAnDg");
	var mask_1_graphics_173 = new cjs.Graphics().p("A3YTiMAAAgnDMAu2AAAMAAAAnDg");
	var mask_1_graphics_174 = new cjs.Graphics().p("A3aTiMAAAgnDMAu1AAAMAAAAnDg");
	var mask_1_graphics_175 = new cjs.Graphics().p("A3GTiMAAAgnDMAuNAAAMAAAAnDg");
	var mask_1_graphics_176 = new cjs.Graphics().p("A2xTiMAAAgnDMAtjAAAMAAAAnDg");
	var mask_1_graphics_177 = new cjs.Graphics().p("A2cTiMAAAgnDMAs5AAAMAAAAnDg");
	var mask_1_graphics_178 = new cjs.Graphics().p("A2ITiMAAAgnDMAsRAAAMAAAAnDg");
	var mask_1_graphics_179 = new cjs.Graphics().p("A1zTiMAAAgnDMArnAAAMAAAAnDg");
	var mask_1_graphics_180 = new cjs.Graphics().p("A1eTiMAAAgnDMAq9AAAMAAAAnDg");
	var mask_1_graphics_181 = new cjs.Graphics().p("A1JTiMAAAgnDMAqTAAAMAAAAnDg");
	var mask_1_graphics_182 = new cjs.Graphics().p("A01TiMAAAgnDMAprAAAMAAAAnDg");
	var mask_1_graphics_183 = new cjs.Graphics().p("A0gTiMAAAgnDMApBAAAMAAAAnDg");
	var mask_1_graphics_184 = new cjs.Graphics().p("A0LTiMAAAgnDMAoXAAAMAAAAnDg");
	var mask_1_graphics_185 = new cjs.Graphics().p("Az2TiMAAAgnDMAntAAAMAAAAnDg");
	var mask_1_graphics_186 = new cjs.Graphics().p("AziTiMAAAgnDMAnFAAAMAAAAnDg");
	var mask_1_graphics_187 = new cjs.Graphics().p("AzNTiMAAAgnDMAmbAAAMAAAAnDg");
	var mask_1_graphics_188 = new cjs.Graphics().p("Ay4TiMAAAgnDMAlxAAAMAAAAnDg");
	var mask_1_graphics_189 = new cjs.Graphics().p("AyjTiMAAAgnDMAlHAAAMAAAAnDg");
	var mask_1_graphics_190 = new cjs.Graphics().p("AyPTiMAAAgnDMAkfAAAMAAAAnDg");
	var mask_1_graphics_191 = new cjs.Graphics().p("Ax6TiMAAAgnDMAj1AAAMAAAAnDg");
	var mask_1_graphics_192 = new cjs.Graphics().p("AxlTiMAAAgnDMAjLAAAMAAAAnDg");
	var mask_1_graphics_193 = new cjs.Graphics().p("AxRTiMAAAgnDMAijAAAMAAAAnDg");
	var mask_1_graphics_194 = new cjs.Graphics().p("Aw8TiMAAAgnDMAh5AAAMAAAAnDg");
	var mask_1_graphics_195 = new cjs.Graphics().p("AwnTiMAAAgnDMAhPAAAMAAAAnDg");
	var mask_1_graphics_196 = new cjs.Graphics().p("AwSTiMAAAgnDMAglAAAMAAAAnDg");
	var mask_1_graphics_197 = new cjs.Graphics().p("Av+TiMAAAgnDIf9AAMAAAAnDg");
	var mask_1_graphics_198 = new cjs.Graphics().p("AvpTiMAAAgnDIfTAAMAAAAnDg");
	var mask_1_graphics_199 = new cjs.Graphics().p("AvUTiMAAAgnDIepAAMAAAAnDg");
	var mask_1_graphics_200 = new cjs.Graphics().p("Au/TiMAAAgnDId/AAMAAAAnDg");
	var mask_1_graphics_201 = new cjs.Graphics().p("AurTiMAAAgnDIdXAAMAAAAnDg");
	var mask_1_graphics_202 = new cjs.Graphics().p("AuWTiMAAAgnDIctAAMAAAAnDg");
	var mask_1_graphics_203 = new cjs.Graphics().p("AuBTiMAAAgnDIcDAAMAAAAnDg");
	var mask_1_graphics_204 = new cjs.Graphics().p("AtsTiMAAAgnDIbZAAMAAAAnDg");
	var mask_1_graphics_205 = new cjs.Graphics().p("AtYTiMAAAgnDIaxAAMAAAAnDg");
	var mask_1_graphics_206 = new cjs.Graphics().p("AtDTiMAAAgnDIaHAAMAAAAnDg");
	var mask_1_graphics_207 = new cjs.Graphics().p("AsuTiMAAAgnDIZdAAMAAAAnDg");
	var mask_1_graphics_208 = new cjs.Graphics().p("AsZTiMAAAgnDIYzAAMAAAAnDg");
	var mask_1_graphics_209 = new cjs.Graphics().p("AsFTiMAAAgnDIYLAAMAAAAnDg");
	var mask_1_graphics_210 = new cjs.Graphics().p("ArwTiMAAAgnDIXhAAMAAAAnDg");
	var mask_1_graphics_211 = new cjs.Graphics().p("ArbTiMAAAgnDIW3AAMAAAAnDg");
	var mask_1_graphics_212 = new cjs.Graphics().p("ArHTiMAAAgnDIWPAAMAAAAnDg");
	var mask_1_graphics_213 = new cjs.Graphics().p("AqyTiMAAAgnDIVlAAMAAAAnDg");
	var mask_1_graphics_214 = new cjs.Graphics().p("AqdTiMAAAgnDIU7AAMAAAAnDg");
	var mask_1_graphics_215 = new cjs.Graphics().p("AqITiMAAAgnDIURAAMAAAAnDg");
	var mask_1_graphics_216 = new cjs.Graphics().p("Ap0TiMAAAgnDITpAAMAAAAnDg");
	var mask_1_graphics_217 = new cjs.Graphics().p("ApfTiMAAAgnDIS/AAMAAAAnDg");
	var mask_1_graphics_218 = new cjs.Graphics().p("ApKTiMAAAgnDISVAAMAAAAnDg");
	var mask_1_graphics_219 = new cjs.Graphics().p("Ao1TiMAAAgnDIRrAAMAAAAnDg");
	var mask_1_graphics_220 = new cjs.Graphics().p("AohTiMAAAgnDIRDAAMAAAAnDg");
	var mask_1_graphics_221 = new cjs.Graphics().p("AoMTiMAAAgnDIQZAAMAAAAnDg");
	var mask_1_graphics_222 = new cjs.Graphics().p("An3TiMAAAgnDIPvAAMAAAAnDg");
	var mask_1_graphics_223 = new cjs.Graphics().p("AniTiMAAAgnDIPFAAMAAAAnDg");
	var mask_1_graphics_224 = new cjs.Graphics().p("AnOTiMAAAgnDIOdAAMAAAAnDg");
	var mask_1_graphics_225 = new cjs.Graphics().p("Am5TiMAAAgnDINzAAMAAAAnDg");
	var mask_1_graphics_226 = new cjs.Graphics().p("AmkTiMAAAgnDINJAAMAAAAnDg");
	var mask_1_graphics_227 = new cjs.Graphics().p("AmQTiMAAAgnDIMhAAMAAAAnDg");
	var mask_1_graphics_228 = new cjs.Graphics().p("Al7TiMAAAgnDIL3AAMAAAAnDg");
	var mask_1_graphics_229 = new cjs.Graphics().p("AlmTiMAAAgnDILNAAMAAAAnDg");
	var mask_1_graphics_230 = new cjs.Graphics().p("AlRTiMAAAgnDIKjAAMAAAAnDg");
	var mask_1_graphics_231 = new cjs.Graphics().p("Ak9TiMAAAgnDIJ7AAMAAAAnDg");
	var mask_1_graphics_232 = new cjs.Graphics().p("AkoTiMAAAgnDIJRAAMAAAAnDg");
	var mask_1_graphics_233 = new cjs.Graphics().p("AkTTiMAAAgnDIInAAMAAAAnDg");
	var mask_1_graphics_234 = new cjs.Graphics().p("Aj+TiMAAAgnDIH9AAMAAAAnDg");
	var mask_1_graphics_235 = new cjs.Graphics().p("AjqTiMAAAgnDIHVAAMAAAAnDg");
	var mask_1_graphics_236 = new cjs.Graphics().p("AjVTiMAAAgnDIGrAAMAAAAnDg");
	var mask_1_graphics_237 = new cjs.Graphics().p("AjATiMAAAgnDIGBAAMAAAAnDg");
	var mask_1_graphics_238 = new cjs.Graphics().p("AirTiMAAAgnDIFXAAMAAAAnDg");
	var mask_1_graphics_239 = new cjs.Graphics().p("AiXTiMAAAgnDIEvAAMAAAAnDg");
	var mask_1_graphics_240 = new cjs.Graphics().p("AiCTiMAAAgnDIEFAAMAAAAnDg");
	var mask_1_graphics_241 = new cjs.Graphics().p("AhtTiMAAAgnDIDbAAMAAAAnDg");
	var mask_1_graphics_242 = new cjs.Graphics().p("AhYTiMAAAgnDICxAAMAAAAnDg");
	var mask_1_graphics_243 = new cjs.Graphics().p("AhETiMAAAgnDICJAAMAAAAnDg");
	var mask_1_graphics_244 = new cjs.Graphics().p("AgvTiMAAAgnDIBfAAMAAAAnDg");
	var mask_1_graphics_245 = new cjs.Graphics().p("AgaTiMAAAgnDIA1AAMAAAAnDg");
	var mask_1_graphics_246 = new cjs.Graphics().p("AgKTiMAAAgnDIAMAAMAAAAnDg");
	var mask_1_graphics_317 = new cjs.Graphics().p("AgKTiMAAAgnDIAMAAMAAAAnDg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(81).to({graphics:mask_1_graphics_81,x:19.8,y:125}).wait(1).to({graphics:mask_1_graphics_82,x:21.6,y:125}).wait(1).to({graphics:mask_1_graphics_83,x:23.3,y:125}).wait(1).to({graphics:mask_1_graphics_84,x:25.1,y:125}).wait(1).to({graphics:mask_1_graphics_85,x:26.8,y:125}).wait(1).to({graphics:mask_1_graphics_86,x:28.5,y:125}).wait(1).to({graphics:mask_1_graphics_87,x:30.3,y:125}).wait(1).to({graphics:mask_1_graphics_88,x:32,y:125}).wait(1).to({graphics:mask_1_graphics_89,x:33.7,y:125}).wait(1).to({graphics:mask_1_graphics_90,x:35.5,y:125}).wait(1).to({graphics:mask_1_graphics_91,x:37.2,y:125}).wait(1).to({graphics:mask_1_graphics_92,x:39,y:125}).wait(1).to({graphics:mask_1_graphics_93,x:40.7,y:125}).wait(1).to({graphics:mask_1_graphics_94,x:42.4,y:125}).wait(1).to({graphics:mask_1_graphics_95,x:44.2,y:125}).wait(1).to({graphics:mask_1_graphics_96,x:45.9,y:125}).wait(1).to({graphics:mask_1_graphics_97,x:47.7,y:125}).wait(1).to({graphics:mask_1_graphics_98,x:49.4,y:125}).wait(1).to({graphics:mask_1_graphics_99,x:51.1,y:125}).wait(1).to({graphics:mask_1_graphics_100,x:52.9,y:125}).wait(1).to({graphics:mask_1_graphics_101,x:54.6,y:125}).wait(1).to({graphics:mask_1_graphics_102,x:56.4,y:125}).wait(1).to({graphics:mask_1_graphics_103,x:58.1,y:125}).wait(1).to({graphics:mask_1_graphics_104,x:59.8,y:125}).wait(1).to({graphics:mask_1_graphics_105,x:61.6,y:125}).wait(1).to({graphics:mask_1_graphics_106,x:63.3,y:125}).wait(1).to({graphics:mask_1_graphics_107,x:65,y:125}).wait(1).to({graphics:mask_1_graphics_108,x:66.8,y:125}).wait(1).to({graphics:mask_1_graphics_109,x:68.5,y:125}).wait(1).to({graphics:mask_1_graphics_110,x:70.3,y:125}).wait(1).to({graphics:mask_1_graphics_111,x:72,y:125}).wait(1).to({graphics:mask_1_graphics_112,x:73.7,y:125}).wait(1).to({graphics:mask_1_graphics_113,x:75.5,y:125}).wait(1).to({graphics:mask_1_graphics_114,x:77.2,y:125}).wait(1).to({graphics:mask_1_graphics_115,x:79,y:125}).wait(1).to({graphics:mask_1_graphics_116,x:80.7,y:125}).wait(1).to({graphics:mask_1_graphics_117,x:82.4,y:125}).wait(1).to({graphics:mask_1_graphics_118,x:84.2,y:125}).wait(1).to({graphics:mask_1_graphics_119,x:85.9,y:125}).wait(1).to({graphics:mask_1_graphics_120,x:87.6,y:125}).wait(1).to({graphics:mask_1_graphics_121,x:89.4,y:125}).wait(1).to({graphics:mask_1_graphics_122,x:91.1,y:125}).wait(1).to({graphics:mask_1_graphics_123,x:92.9,y:125}).wait(1).to({graphics:mask_1_graphics_124,x:94.6,y:125}).wait(1).to({graphics:mask_1_graphics_125,x:96.3,y:125}).wait(1).to({graphics:mask_1_graphics_126,x:98.1,y:125}).wait(1).to({graphics:mask_1_graphics_127,x:99.8,y:125}).wait(1).to({graphics:mask_1_graphics_128,x:101.6,y:125}).wait(1).to({graphics:mask_1_graphics_129,x:103.3,y:125}).wait(1).to({graphics:mask_1_graphics_130,x:105,y:125}).wait(1).to({graphics:mask_1_graphics_131,x:106.8,y:125}).wait(1).to({graphics:mask_1_graphics_132,x:108.5,y:125}).wait(1).to({graphics:mask_1_graphics_133,x:110.2,y:125}).wait(1).to({graphics:mask_1_graphics_134,x:112,y:125}).wait(1).to({graphics:mask_1_graphics_135,x:113.7,y:125}).wait(1).to({graphics:mask_1_graphics_136,x:115.5,y:125}).wait(1).to({graphics:mask_1_graphics_137,x:117.2,y:125}).wait(1).to({graphics:mask_1_graphics_138,x:118.9,y:125}).wait(1).to({graphics:mask_1_graphics_139,x:120.7,y:125}).wait(1).to({graphics:mask_1_graphics_140,x:122.4,y:125}).wait(1).to({graphics:mask_1_graphics_141,x:124.2,y:125}).wait(1).to({graphics:mask_1_graphics_142,x:125.9,y:125}).wait(1).to({graphics:mask_1_graphics_143,x:127.6,y:125}).wait(1).to({graphics:mask_1_graphics_144,x:129.4,y:125}).wait(1).to({graphics:mask_1_graphics_145,x:131.1,y:125}).wait(1).to({graphics:mask_1_graphics_146,x:132.8,y:125}).wait(1).to({graphics:mask_1_graphics_147,x:134.6,y:125}).wait(1).to({graphics:mask_1_graphics_148,x:136.3,y:125}).wait(1).to({graphics:mask_1_graphics_149,x:138.1,y:125}).wait(1).to({graphics:mask_1_graphics_150,x:139.8,y:125}).wait(1).to({graphics:mask_1_graphics_151,x:141.5,y:125}).wait(1).to({graphics:mask_1_graphics_152,x:143.3,y:125}).wait(1).to({graphics:mask_1_graphics_153,x:145,y:125}).wait(1).to({graphics:mask_1_graphics_154,x:146.8,y:125}).wait(1).to({graphics:mask_1_graphics_155,x:148.5,y:125}).wait(1).to({graphics:mask_1_graphics_156,x:150.2,y:125}).wait(17).to({graphics:mask_1_graphics_173,x:150.2,y:125}).wait(1).to({graphics:mask_1_graphics_174,x:112.9,y:125}).wait(1).to({graphics:mask_1_graphics_175,x:111.3,y:125}).wait(1).to({graphics:mask_1_graphics_176,x:109.7,y:125}).wait(1).to({graphics:mask_1_graphics_177,x:108.1,y:125}).wait(1).to({graphics:mask_1_graphics_178,x:106.5,y:125}).wait(1).to({graphics:mask_1_graphics_179,x:104.9,y:125}).wait(1).to({graphics:mask_1_graphics_180,x:103.3,y:125}).wait(1).to({graphics:mask_1_graphics_181,x:101.7,y:125}).wait(1).to({graphics:mask_1_graphics_182,x:100.2,y:125}).wait(1).to({graphics:mask_1_graphics_183,x:98.6,y:125}).wait(1).to({graphics:mask_1_graphics_184,x:97,y:125}).wait(1).to({graphics:mask_1_graphics_185,x:95.4,y:125}).wait(1).to({graphics:mask_1_graphics_186,x:93.8,y:125}).wait(1).to({graphics:mask_1_graphics_187,x:92.2,y:125}).wait(1).to({graphics:mask_1_graphics_188,x:90.6,y:125}).wait(1).to({graphics:mask_1_graphics_189,x:89,y:125}).wait(1).to({graphics:mask_1_graphics_190,x:87.4,y:125}).wait(1).to({graphics:mask_1_graphics_191,x:85.9,y:125}).wait(1).to({graphics:mask_1_graphics_192,x:84.3,y:125}).wait(1).to({graphics:mask_1_graphics_193,x:82.7,y:125}).wait(1).to({graphics:mask_1_graphics_194,x:81.1,y:125}).wait(1).to({graphics:mask_1_graphics_195,x:79.5,y:125}).wait(1).to({graphics:mask_1_graphics_196,x:77.9,y:125}).wait(1).to({graphics:mask_1_graphics_197,x:76.3,y:125}).wait(1).to({graphics:mask_1_graphics_198,x:74.7,y:125}).wait(1).to({graphics:mask_1_graphics_199,x:73.1,y:125}).wait(1).to({graphics:mask_1_graphics_200,x:71.6,y:125}).wait(1).to({graphics:mask_1_graphics_201,x:70,y:125}).wait(1).to({graphics:mask_1_graphics_202,x:68.4,y:125}).wait(1).to({graphics:mask_1_graphics_203,x:66.8,y:125}).wait(1).to({graphics:mask_1_graphics_204,x:65.2,y:125}).wait(1).to({graphics:mask_1_graphics_205,x:63.6,y:125}).wait(1).to({graphics:mask_1_graphics_206,x:62,y:125}).wait(1).to({graphics:mask_1_graphics_207,x:60.4,y:125}).wait(1).to({graphics:mask_1_graphics_208,x:58.8,y:125}).wait(1).to({graphics:mask_1_graphics_209,x:57.2,y:125}).wait(1).to({graphics:mask_1_graphics_210,x:55.7,y:125}).wait(1).to({graphics:mask_1_graphics_211,x:54.1,y:125}).wait(1).to({graphics:mask_1_graphics_212,x:52.5,y:125}).wait(1).to({graphics:mask_1_graphics_213,x:50.9,y:125}).wait(1).to({graphics:mask_1_graphics_214,x:49.3,y:125}).wait(1).to({graphics:mask_1_graphics_215,x:47.7,y:125}).wait(1).to({graphics:mask_1_graphics_216,x:46.1,y:125}).wait(1).to({graphics:mask_1_graphics_217,x:44.5,y:125}).wait(1).to({graphics:mask_1_graphics_218,x:42.9,y:125}).wait(1).to({graphics:mask_1_graphics_219,x:41.4,y:125}).wait(1).to({graphics:mask_1_graphics_220,x:39.8,y:125}).wait(1).to({graphics:mask_1_graphics_221,x:38.2,y:125}).wait(1).to({graphics:mask_1_graphics_222,x:36.6,y:125}).wait(1).to({graphics:mask_1_graphics_223,x:35,y:125}).wait(1).to({graphics:mask_1_graphics_224,x:33.4,y:125}).wait(1).to({graphics:mask_1_graphics_225,x:31.8,y:125}).wait(1).to({graphics:mask_1_graphics_226,x:30.2,y:125}).wait(1).to({graphics:mask_1_graphics_227,x:28.6,y:125}).wait(1).to({graphics:mask_1_graphics_228,x:27.1,y:125}).wait(1).to({graphics:mask_1_graphics_229,x:25.5,y:125}).wait(1).to({graphics:mask_1_graphics_230,x:23.9,y:125}).wait(1).to({graphics:mask_1_graphics_231,x:22.3,y:125}).wait(1).to({graphics:mask_1_graphics_232,x:20.7,y:125}).wait(1).to({graphics:mask_1_graphics_233,x:19.1,y:125}).wait(1).to({graphics:mask_1_graphics_234,x:17.5,y:125}).wait(1).to({graphics:mask_1_graphics_235,x:15.9,y:125}).wait(1).to({graphics:mask_1_graphics_236,x:14.3,y:125}).wait(1).to({graphics:mask_1_graphics_237,x:12.8,y:125}).wait(1).to({graphics:mask_1_graphics_238,x:11.2,y:125}).wait(1).to({graphics:mask_1_graphics_239,x:9.6,y:125}).wait(1).to({graphics:mask_1_graphics_240,x:8,y:125}).wait(1).to({graphics:mask_1_graphics_241,x:6.4,y:125}).wait(1).to({graphics:mask_1_graphics_242,x:4.8,y:125}).wait(1).to({graphics:mask_1_graphics_243,x:3.2,y:125}).wait(1).to({graphics:mask_1_graphics_244,x:1.6,y:125}).wait(1).to({graphics:mask_1_graphics_245,x:0,y:125}).wait(1).to({graphics:mask_1_graphics_246,x:-1.1,y:125}).wait(71).to({graphics:mask_1_graphics_317,x:-1.1,y:125}).wait(1));

	// copy_2
	this.copy_2_mc = new lib.copy_2_mc();
	this.copy_2_mc.setTransform(150.8,169.5);
	this.copy_2_mc._off = true;

	this.copy_2_mc.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.copy_2_mc).wait(81).to({_off:false},0).wait(237));

	// Mask --- f1 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("A3KSCIAAg2MAu2AAAIAAA2g");
	var mask_2_graphics_1 = new cjs.Graphics().p("A3KSCIAAhVMAu2AAAIAABVg");
	var mask_2_graphics_2 = new cjs.Graphics().p("A3KSCIAAhzMAu2AAAIAABzg");
	var mask_2_graphics_3 = new cjs.Graphics().p("A3KSDIAAiTMAu2AAAIAACTg");
	var mask_2_graphics_4 = new cjs.Graphics().p("A3KSDIAAiyMAu2AAAIAACyg");
	var mask_2_graphics_5 = new cjs.Graphics().p("A3KSDIAAjQMAu2AAAIAADQg");
	var mask_2_graphics_6 = new cjs.Graphics().p("A3KSDIAAjvMAu2AAAIAADvg");
	var mask_2_graphics_7 = new cjs.Graphics().p("A3KSDIAAkOMAu2AAAIAAEOg");
	var mask_2_graphics_8 = new cjs.Graphics().p("A3KSEIAAktMAu2AAAIAAEtg");
	var mask_2_graphics_9 = new cjs.Graphics().p("A3KSEIAAlMMAu2AAAIAAFMg");
	var mask_2_graphics_10 = new cjs.Graphics().p("A3JSEIAAlrMAu2AAAIAAFrg");
	var mask_2_graphics_11 = new cjs.Graphics().p("A3JSEIAAmJMAu2AAAIAAGJg");
	var mask_2_graphics_12 = new cjs.Graphics().p("A3JSEIAAmoMAu2AAAIAAGog");
	var mask_2_graphics_13 = new cjs.Graphics().p("A3JSFIAAnHMAu2AAAIAAHHg");
	var mask_2_graphics_14 = new cjs.Graphics().p("A3JSFIAAnmMAu2AAAIAAHmg");
	var mask_2_graphics_15 = new cjs.Graphics().p("A3JSFIAAoFMAu2AAAIAAIFg");
	var mask_2_graphics_16 = new cjs.Graphics().p("A3JSFIAAojMAu2AAAIAAIjg");
	var mask_2_graphics_17 = new cjs.Graphics().p("A3JSFIAApCMAu2AAAIAAJCg");
	var mask_2_graphics_18 = new cjs.Graphics().p("A3JSGIAAphMAu2AAAIAAJhg");
	var mask_2_graphics_19 = new cjs.Graphics().p("A3JSGIAAqAMAu2AAAIAAKAg");
	var mask_2_graphics_20 = new cjs.Graphics().p("A3ISGIAAqeMAu2AAAIAAKeg");
	var mask_2_graphics_21 = new cjs.Graphics().p("A3ISGIAAq9MAu2AAAIAAK9g");
	var mask_2_graphics_22 = new cjs.Graphics().p("A3ISGIAArbMAu2AAAIAALbg");
	var mask_2_graphics_23 = new cjs.Graphics().p("A3ISHIAAr7MAu2AAAIAAL7g");
	var mask_2_graphics_24 = new cjs.Graphics().p("A3ISHIAAsZMAu2AAAIAAMZg");
	var mask_2_graphics_25 = new cjs.Graphics().p("A3ISHIAAs4MAu2AAAIAAM4g");
	var mask_2_graphics_26 = new cjs.Graphics().p("A3ISHIAAtWMAu2AAAIAANWg");
	var mask_2_graphics_27 = new cjs.Graphics().p("A3ISHIAAt1MAu2AAAIAAN1g");
	var mask_2_graphics_28 = new cjs.Graphics().p("A3ISHIAAuTMAu2AAAIAAOTg");
	var mask_2_graphics_29 = new cjs.Graphics().p("A3ISIIAAuzMAu2AAAIAAOzg");
	var mask_2_graphics_30 = new cjs.Graphics().p("A3HSIIAAvRMAu2AAAIAAPRg");
	var mask_2_graphics_31 = new cjs.Graphics().p("A3HSIIAAvwMAu2AAAIAAPwg");
	var mask_2_graphics_32 = new cjs.Graphics().p("A3HSIIAAwOMAu2AAAIAAQOg");
	var mask_2_graphics_33 = new cjs.Graphics().p("A3HSIIAAwtMAu2AAAIAAQtg");
	var mask_2_graphics_34 = new cjs.Graphics().p("A3HSJIAAxMMAu2AAAIAARMg");
	var mask_2_graphics_35 = new cjs.Graphics().p("A3HSJIAAxqMAu2AAAIAARqg");
	var mask_2_graphics_36 = new cjs.Graphics().p("A3HSJIAAyJMAu2AAAIAASJg");
	var mask_2_graphics_37 = new cjs.Graphics().p("A3HSJIAAylMAu2AAAIAASlg");
	var mask_2_graphics_38 = new cjs.Graphics().p("A3HSJIAAzEMAu2AAAIAATEg");
	var mask_2_graphics_39 = new cjs.Graphics().p("A3HSKIAAzjMAu2AAAIAATjg");
	var mask_2_graphics_40 = new cjs.Graphics().p("A3GSKIAA0BMAu2AAAIAAUBg");
	var mask_2_graphics_41 = new cjs.Graphics().p("A3GSKIAA0gMAu2AAAIAAUgg");
	var mask_2_graphics_42 = new cjs.Graphics().p("A3GSKIAA0+MAu2AAAIAAU+g");
	var mask_2_graphics_43 = new cjs.Graphics().p("A3GSKIAA1cMAu2AAAIAAVcg");
	var mask_2_graphics_44 = new cjs.Graphics().p("A3GSLIAA18MAu2AAAIAAV8g");
	var mask_2_graphics_45 = new cjs.Graphics().p("A3GSLIAA2aMAu2AAAIAAWag");
	var mask_2_graphics_46 = new cjs.Graphics().p("A3GSLIAA25MAu2AAAIAAW5g");
	var mask_2_graphics_47 = new cjs.Graphics().p("A3GSLIAA3XMAu2AAAIAAXXg");
	var mask_2_graphics_48 = new cjs.Graphics().p("A3GSLIAA31MAu2AAAIAAX1g");
	var mask_2_graphics_49 = new cjs.Graphics().p("A3GSMIAA4UMAu2AAAIAAYUg");
	var mask_2_graphics_50 = new cjs.Graphics().p("A3FSMIAA4zMAu2AAAIAAYzg");
	var mask_2_graphics_51 = new cjs.Graphics().p("A3FSMIAA5RMAu2AAAIAAZRg");
	var mask_2_graphics_52 = new cjs.Graphics().p("A3FSMIAA5vMAu2AAAIAAZvg");
	var mask_2_graphics_53 = new cjs.Graphics().p("A3FSMIAA6OMAu2AAAIAAaOg");
	var mask_2_graphics_54 = new cjs.Graphics().p("A3FSNIAA6tMAu2AAAIAAatg");
	var mask_2_graphics_55 = new cjs.Graphics().p("A3FSNIAA7LMAu2AAAIAAbLg");
	var mask_2_graphics_56 = new cjs.Graphics().p("A3FSNIAA7pMAu2AAAIAAbpg");
	var mask_2_graphics_57 = new cjs.Graphics().p("A3FSNIAA8IMAu2AAAIAAcIg");
	var mask_2_graphics_58 = new cjs.Graphics().p("A3FSNIAA8mMAu2AAAIAAcmg");
	var mask_2_graphics_59 = new cjs.Graphics().p("A3FSNIAA9EMAu2AAAIAAdEg");
	var mask_2_graphics_60 = new cjs.Graphics().p("A3ESOIAA9jMAu2AAAIAAdjg");
	var mask_2_graphics_61 = new cjs.Graphics().p("A3ESOIAA+CMAu2AAAIAAeCg");
	var mask_2_graphics_62 = new cjs.Graphics().p("A3ESOIAA+gMAu2AAAIAAegg");
	var mask_2_graphics_63 = new cjs.Graphics().p("A3ESOIAA++MAu2AAAIAAe+g");
	var mask_2_graphics_64 = new cjs.Graphics().p("A3ESOIAA/cMAu2AAAIAAfcg");
	var mask_2_graphics_65 = new cjs.Graphics().p("A3ESPIAA/7MAu2AAAIAAf7g");
	var mask_2_graphics_66 = new cjs.Graphics().p("A3ESPMAAAggaMAu2AAAMAAAAgag");
	var mask_2_graphics_67 = new cjs.Graphics().p("A3ESPMAAAgg4MAu2AAAMAAAAg4g");
	var mask_2_graphics_68 = new cjs.Graphics().p("A3ESPMAAAghWMAu2AAAMAAAAhWg");
	var mask_2_graphics_69 = new cjs.Graphics().p("A3ESPMAAAgh0MAu2AAAMAAAAh0g");
	var mask_2_graphics_70 = new cjs.Graphics().p("A3DSQMAAAgiTMAu2AAAMAAAAiTg");
	var mask_2_graphics_71 = new cjs.Graphics().p("A3DSQMAAAgiyMAu2AAAMAAAAiyg");
	var mask_2_graphics_72 = new cjs.Graphics().p("A3DSQMAAAgjQMAu2AAAMAAAAjQg");
	var mask_2_graphics_73 = new cjs.Graphics().p("A3DSQMAAAgjuMAu2AAAMAAAAjug");
	var mask_2_graphics_74 = new cjs.Graphics().p("A3DSQMAAAgkMMAu2AAAMAAAAkMg");
	var mask_2_graphics_75 = new cjs.Graphics().p("A3DSWMAAAgkrMAu2AAAMAAAAkrg");
	var mask_2_graphics_76 = new cjs.Graphics().p("A3DSlMAAAglJMAu2AAAMAAAAlJg");
	var mask_2_graphics_77 = new cjs.Graphics().p("A3DS0MAAAglnMAu2AAAMAAAAlng");
	var mask_2_graphics_78 = new cjs.Graphics().p("A3DTDMAAAgmFMAu2AAAMAAAAmFg");
	var mask_2_graphics_79 = new cjs.Graphics().p("A3DTSMAAAgmjMAu2AAAMAAAAmjg");
	var mask_2_graphics_80 = new cjs.Graphics().p("A3DTiMAAAgnDMAu3AAAMAAAAnDg");
	var mask_2_graphics_81 = new cjs.Graphics().p("AzxTiMAAAgnDMAu2AAAMAAAAnDg");
	var mask_2_graphics_82 = new cjs.Graphics().p("AzMTiMAAAgnDMAuPAAAMAAAAnDg");
	var mask_2_graphics_83 = new cjs.Graphics().p("AynTiMAAAgnDMAtnAAAMAAAAnDg");
	var mask_2_graphics_84 = new cjs.Graphics().p("AyCTiMAAAgnDMAs/AAAMAAAAnDg");
	var mask_2_graphics_85 = new cjs.Graphics().p("AxeTiMAAAgnDMAsYAAAMAAAAnDg");
	var mask_2_graphics_86 = new cjs.Graphics().p("Aw5TiMAAAgnDMArwAAAMAAAAnDg");
	var mask_2_graphics_87 = new cjs.Graphics().p("AwUTiMAAAgnDMArJAAAMAAAAnDg");
	var mask_2_graphics_88 = new cjs.Graphics().p("AvwTiMAAAgnDMAqiAAAMAAAAnDg");
	var mask_2_graphics_89 = new cjs.Graphics().p("AvLTiMAAAgnDMAp6AAAMAAAAnDg");
	var mask_2_graphics_90 = new cjs.Graphics().p("AumTiMAAAgnDMApSAAAMAAAAnDg");
	var mask_2_graphics_91 = new cjs.Graphics().p("AuCTiMAAAgnDMAorAAAMAAAAnDg");
	var mask_2_graphics_92 = new cjs.Graphics().p("AtdTiMAAAgnDMAoEAAAMAAAAnDg");
	var mask_2_graphics_93 = new cjs.Graphics().p("As5TiMAAAgnDMAndAAAMAAAAnDg");
	var mask_2_graphics_94 = new cjs.Graphics().p("AsUTiMAAAgnDMAm1AAAMAAAAnDg");
	var mask_2_graphics_95 = new cjs.Graphics().p("ArvTiMAAAgnDMAmNAAAMAAAAnDg");
	var mask_2_graphics_96 = new cjs.Graphics().p("ArLTiMAAAgnDMAlmAAAMAAAAnDg");
	var mask_2_graphics_97 = new cjs.Graphics().p("AqmTiMAAAgnDMAk/AAAMAAAAnDg");
	var mask_2_graphics_98 = new cjs.Graphics().p("AqBTiMAAAgnDMAkXAAAMAAAAnDg");
	var mask_2_graphics_99 = new cjs.Graphics().p("ApdTiMAAAgnDMAjwAAAMAAAAnDg");
	var mask_2_graphics_100 = new cjs.Graphics().p("Ao4TiMAAAgnDMAjIAAAMAAAAnDg");
	var mask_2_graphics_101 = new cjs.Graphics().p("AoTTiMAAAgnDMAihAAAMAAAAnDg");
	var mask_2_graphics_102 = new cjs.Graphics().p("AnvTiMAAAgnDMAh6AAAMAAAAnDg");
	var mask_2_graphics_103 = new cjs.Graphics().p("AnKTiMAAAgnDMAhSAAAMAAAAnDg");
	var mask_2_graphics_104 = new cjs.Graphics().p("AmlTiMAAAgnDMAgqAAAMAAAAnDg");
	var mask_2_graphics_105 = new cjs.Graphics().p("AmBTiMAAAgnDMAgDAAAMAAAAnDg");
	var mask_2_graphics_106 = new cjs.Graphics().p("AlcTiMAAAgnDIfcAAMAAAAnDg");
	var mask_2_graphics_107 = new cjs.Graphics().p("Ak3TiMAAAgnDIe0AAMAAAAnDg");
	var mask_2_graphics_108 = new cjs.Graphics().p("AkTTiMAAAgnDIeNAAMAAAAnDg");
	var mask_2_graphics_109 = new cjs.Graphics().p("AjuTiMAAAgnDIdlAAMAAAAnDg");
	var mask_2_graphics_110 = new cjs.Graphics().p("AjJTiMAAAgnDIc9AAMAAAAnDg");
	var mask_2_graphics_111 = new cjs.Graphics().p("AikTiMAAAgnDIcWAAMAAAAnDg");
	var mask_2_graphics_112 = new cjs.Graphics().p("AiATiMAAAgnDIbvAAMAAAAnDg");
	var mask_2_graphics_113 = new cjs.Graphics().p("AhbTiMAAAgnDIbHAAMAAAAnDg");
	var mask_2_graphics_114 = new cjs.Graphics().p("Ag2TiMAAAgnDIafAAMAAAAnDg");
	var mask_2_graphics_115 = new cjs.Graphics().p("AgSTiMAAAgnDIZ4AAMAAAAnDg");
	var mask_2_graphics_116 = new cjs.Graphics().p("AARTiMAAAgnDIZTAAMAAAAnDg");
	var mask_2_graphics_117 = new cjs.Graphics().p("AA1TiMAAAgnDIYsAAMAAAAnDg");
	var mask_2_graphics_118 = new cjs.Graphics().p("ABaTiMAAAgnDIYEAAMAAAAnDg");
	var mask_2_graphics_119 = new cjs.Graphics().p("AB/TiMAAAgnDIXcAAMAAAAnDg");
	var mask_2_graphics_120 = new cjs.Graphics().p("ACjTiMAAAgnDIW1AAMAAAAnDg");
	var mask_2_graphics_121 = new cjs.Graphics().p("ADITiMAAAgnDIWOAAMAAAAnDg");
	var mask_2_graphics_122 = new cjs.Graphics().p("ADtTiMAAAgnDIVmAAMAAAAnDg");
	var mask_2_graphics_123 = new cjs.Graphics().p("AERTiMAAAgnDIU/AAMAAAAnDg");
	var mask_2_graphics_124 = new cjs.Graphics().p("AE2TiMAAAgnDIUXAAMAAAAnDg");
	var mask_2_graphics_125 = new cjs.Graphics().p("AFbTiMAAAgnDITvAAMAAAAnDg");
	var mask_2_graphics_126 = new cjs.Graphics().p("AF/TiMAAAgnDITJAAMAAAAnDg");
	var mask_2_graphics_127 = new cjs.Graphics().p("AGkTiMAAAgnDIShAAMAAAAnDg");
	var mask_2_graphics_128 = new cjs.Graphics().p("AHJTiMAAAgnDIR5AAMAAAAnDg");
	var mask_2_graphics_129 = new cjs.Graphics().p("AHtTiMAAAgnDIRSAAMAAAAnDg");
	var mask_2_graphics_130 = new cjs.Graphics().p("AISTiMAAAgnDIQqAAMAAAAnDg");
	var mask_2_graphics_131 = new cjs.Graphics().p("AI3TiMAAAgnDIQDAAMAAAAnDg");
	var mask_2_graphics_132 = new cjs.Graphics().p("AJbTiMAAAgnDIPcAAMAAAAnDg");
	var mask_2_graphics_133 = new cjs.Graphics().p("AKATiMAAAgnDIO0AAMAAAAnDg");
	var mask_2_graphics_134 = new cjs.Graphics().p("AKlTiMAAAgnDIOMAAMAAAAnDg");
	var mask_2_graphics_135 = new cjs.Graphics().p("ALJTiMAAAgnDINlAAMAAAAnDg");
	var mask_2_graphics_136 = new cjs.Graphics().p("ALuTiMAAAgnDIM9AAMAAAAnDg");
	var mask_2_graphics_137 = new cjs.Graphics().p("AMTTiMAAAgnDIMWAAMAAAAnDg");
	var mask_2_graphics_138 = new cjs.Graphics().p("AM3TiMAAAgnDILvAAMAAAAnDg");
	var mask_2_graphics_139 = new cjs.Graphics().p("ANcTiMAAAgnDILHAAMAAAAnDg");
	var mask_2_graphics_140 = new cjs.Graphics().p("AOBTiMAAAgnDIKfAAMAAAAnDg");
	var mask_2_graphics_141 = new cjs.Graphics().p("AOlTiMAAAgnDIJ4AAMAAAAnDg");
	var mask_2_graphics_142 = new cjs.Graphics().p("APKTiMAAAgnDIJRAAMAAAAnDg");
	var mask_2_graphics_143 = new cjs.Graphics().p("APvTiMAAAgnDIIpAAMAAAAnDg");
	var mask_2_graphics_144 = new cjs.Graphics().p("AQTTiMAAAgnDIICAAMAAAAnDg");
	var mask_2_graphics_145 = new cjs.Graphics().p("AQ4TiMAAAgnDIHaAAMAAAAnDg");
	var mask_2_graphics_146 = new cjs.Graphics().p("ARdTiMAAAgnDIGyAAMAAAAnDg");
	var mask_2_graphics_147 = new cjs.Graphics().p("ASBTiMAAAgnDIGMAAMAAAAnDg");
	var mask_2_graphics_148 = new cjs.Graphics().p("ASmTiMAAAgnDIFkAAMAAAAnDg");
	var mask_2_graphics_149 = new cjs.Graphics().p("ATLTiMAAAgnDIE8AAMAAAAnDg");
	var mask_2_graphics_150 = new cjs.Graphics().p("ATvTiMAAAgnDIEVAAMAAAAnDg");
	var mask_2_graphics_151 = new cjs.Graphics().p("AUUTiMAAAgnDIDtAAMAAAAnDg");
	var mask_2_graphics_152 = new cjs.Graphics().p("AU5TiMAAAgnDIDGAAMAAAAnDg");
	var mask_2_graphics_153 = new cjs.Graphics().p("AVdTiMAAAgnDICfAAMAAAAnDg");
	var mask_2_graphics_154 = new cjs.Graphics().p("AWCTiMAAAgnDIB3AAMAAAAnDg");
	var mask_2_graphics_155 = new cjs.Graphics().p("AWnTiMAAAgnDIBPAAMAAAAnDg");
	var mask_2_graphics_156 = new cjs.Graphics().p("AXLTiMAAAgnDIApAAMAAAAnDg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:151.6,y:115.5}).wait(1).to({graphics:mask_2_graphics_1,x:151.6,y:115.5}).wait(1).to({graphics:mask_2_graphics_2,x:151.6,y:115.5}).wait(1).to({graphics:mask_2_graphics_3,x:151.6,y:115.5}).wait(1).to({graphics:mask_2_graphics_4,x:151.6,y:115.5}).wait(1).to({graphics:mask_2_graphics_5,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_6,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_7,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_8,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_9,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_10,x:151.7,y:115.6}).wait(1).to({graphics:mask_2_graphics_11,x:151.7,y:115.7}).wait(1).to({graphics:mask_2_graphics_12,x:151.7,y:115.7}).wait(1).to({graphics:mask_2_graphics_13,x:151.7,y:115.7}).wait(1).to({graphics:mask_2_graphics_14,x:151.7,y:115.7}).wait(1).to({graphics:mask_2_graphics_15,x:151.8,y:115.7}).wait(1).to({graphics:mask_2_graphics_16,x:151.8,y:115.8}).wait(1).to({graphics:mask_2_graphics_17,x:151.8,y:115.8}).wait(1).to({graphics:mask_2_graphics_18,x:151.8,y:115.8}).wait(1).to({graphics:mask_2_graphics_19,x:151.8,y:115.8}).wait(1).to({graphics:mask_2_graphics_20,x:151.8,y:115.8}).wait(1).to({graphics:mask_2_graphics_21,x:151.8,y:115.9}).wait(1).to({graphics:mask_2_graphics_22,x:151.8,y:115.9}).wait(1).to({graphics:mask_2_graphics_23,x:151.8,y:115.9}).wait(1).to({graphics:mask_2_graphics_24,x:151.8,y:115.9}).wait(1).to({graphics:mask_2_graphics_25,x:151.9,y:115.9}).wait(1).to({graphics:mask_2_graphics_26,x:151.9,y:116}).wait(1).to({graphics:mask_2_graphics_27,x:151.9,y:116}).wait(1).to({graphics:mask_2_graphics_28,x:151.9,y:116}).wait(1).to({graphics:mask_2_graphics_29,x:151.9,y:116}).wait(1).to({graphics:mask_2_graphics_30,x:151.9,y:116}).wait(1).to({graphics:mask_2_graphics_31,x:151.9,y:116.1}).wait(1).to({graphics:mask_2_graphics_32,x:151.9,y:116.1}).wait(1).to({graphics:mask_2_graphics_33,x:151.9,y:116.1}).wait(1).to({graphics:mask_2_graphics_34,x:151.9,y:116.1}).wait(1).to({graphics:mask_2_graphics_35,x:152,y:116.1}).wait(1).to({graphics:mask_2_graphics_36,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_37,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_38,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_39,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_40,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_41,x:152,y:116.2}).wait(1).to({graphics:mask_2_graphics_42,x:152,y:116.3}).wait(1).to({graphics:mask_2_graphics_43,x:152,y:116.3}).wait(1).to({graphics:mask_2_graphics_44,x:152,y:116.3}).wait(1).to({graphics:mask_2_graphics_45,x:152.1,y:116.3}).wait(1).to({graphics:mask_2_graphics_46,x:152.1,y:116.3}).wait(1).to({graphics:mask_2_graphics_47,x:152.1,y:116.4}).wait(1).to({graphics:mask_2_graphics_48,x:152.1,y:116.4}).wait(1).to({graphics:mask_2_graphics_49,x:152.1,y:116.4}).wait(1).to({graphics:mask_2_graphics_50,x:152.1,y:116.4}).wait(1).to({graphics:mask_2_graphics_51,x:152.1,y:116.4}).wait(1).to({graphics:mask_2_graphics_52,x:152.1,y:116.5}).wait(1).to({graphics:mask_2_graphics_53,x:152.1,y:116.5}).wait(1).to({graphics:mask_2_graphics_54,x:152.1,y:116.5}).wait(1).to({graphics:mask_2_graphics_55,x:152.2,y:116.5}).wait(1).to({graphics:mask_2_graphics_56,x:152.2,y:116.5}).wait(1).to({graphics:mask_2_graphics_57,x:152.2,y:116.6}).wait(1).to({graphics:mask_2_graphics_58,x:152.2,y:116.6}).wait(1).to({graphics:mask_2_graphics_59,x:152.2,y:116.6}).wait(1).to({graphics:mask_2_graphics_60,x:152.2,y:116.6}).wait(1).to({graphics:mask_2_graphics_61,x:152.2,y:116.6}).wait(1).to({graphics:mask_2_graphics_62,x:152.2,y:116.7}).wait(1).to({graphics:mask_2_graphics_63,x:152.2,y:116.7}).wait(1).to({graphics:mask_2_graphics_64,x:152.2,y:116.7}).wait(1).to({graphics:mask_2_graphics_65,x:152.3,y:116.7}).wait(1).to({graphics:mask_2_graphics_66,x:152.3,y:116.7}).wait(1).to({graphics:mask_2_graphics_67,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_68,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_69,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_70,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_71,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_72,x:152.3,y:116.8}).wait(1).to({graphics:mask_2_graphics_73,x:152.3,y:116.9}).wait(1).to({graphics:mask_2_graphics_74,x:152.3,y:116.9}).wait(1).to({graphics:mask_2_graphics_75,x:152.4,y:116.4}).wait(1).to({graphics:mask_2_graphics_76,x:152.4,y:114.9}).wait(1).to({graphics:mask_2_graphics_77,x:152.4,y:113.4}).wait(1).to({graphics:mask_2_graphics_78,x:152.4,y:112}).wait(1).to({graphics:mask_2_graphics_79,x:152.4,y:110.5}).wait(1).to({graphics:mask_2_graphics_80,x:152.4,y:109}).wait(1).to({graphics:mask_2_graphics_81,x:173.4,y:109}).wait(1).to({graphics:mask_2_graphics_82,x:173.1,y:109}).wait(1).to({graphics:mask_2_graphics_83,x:172.8,y:109}).wait(1).to({graphics:mask_2_graphics_84,x:172.6,y:109}).wait(1).to({graphics:mask_2_graphics_85,x:172.3,y:109}).wait(1).to({graphics:mask_2_graphics_86,x:172,y:109}).wait(1).to({graphics:mask_2_graphics_87,x:171.7,y:109}).wait(1).to({graphics:mask_2_graphics_88,x:171.4,y:109}).wait(1).to({graphics:mask_2_graphics_89,x:171.2,y:109}).wait(1).to({graphics:mask_2_graphics_90,x:170.9,y:109}).wait(1).to({graphics:mask_2_graphics_91,x:170.6,y:109}).wait(1).to({graphics:mask_2_graphics_92,x:170.3,y:109}).wait(1).to({graphics:mask_2_graphics_93,x:170,y:109}).wait(1).to({graphics:mask_2_graphics_94,x:169.8,y:109}).wait(1).to({graphics:mask_2_graphics_95,x:169.5,y:109}).wait(1).to({graphics:mask_2_graphics_96,x:169.2,y:109}).wait(1).to({graphics:mask_2_graphics_97,x:168.9,y:109}).wait(1).to({graphics:mask_2_graphics_98,x:168.6,y:109}).wait(1).to({graphics:mask_2_graphics_99,x:168.4,y:109}).wait(1).to({graphics:mask_2_graphics_100,x:168.1,y:109}).wait(1).to({graphics:mask_2_graphics_101,x:167.8,y:109}).wait(1).to({graphics:mask_2_graphics_102,x:167.5,y:109}).wait(1).to({graphics:mask_2_graphics_103,x:167.2,y:109}).wait(1).to({graphics:mask_2_graphics_104,x:167,y:109}).wait(1).to({graphics:mask_2_graphics_105,x:166.7,y:109}).wait(1).to({graphics:mask_2_graphics_106,x:166.4,y:109}).wait(1).to({graphics:mask_2_graphics_107,x:166.1,y:109}).wait(1).to({graphics:mask_2_graphics_108,x:165.8,y:109}).wait(1).to({graphics:mask_2_graphics_109,x:165.6,y:109}).wait(1).to({graphics:mask_2_graphics_110,x:165.3,y:109}).wait(1).to({graphics:mask_2_graphics_111,x:165,y:109}).wait(1).to({graphics:mask_2_graphics_112,x:164.7,y:109}).wait(1).to({graphics:mask_2_graphics_113,x:164.4,y:109}).wait(1).to({graphics:mask_2_graphics_114,x:164.2,y:109}).wait(1).to({graphics:mask_2_graphics_115,x:163.9,y:109}).wait(1).to({graphics:mask_2_graphics_116,x:163.6,y:109}).wait(1).to({graphics:mask_2_graphics_117,x:163.3,y:109}).wait(1).to({graphics:mask_2_graphics_118,x:163,y:109}).wait(1).to({graphics:mask_2_graphics_119,x:162.8,y:109}).wait(1).to({graphics:mask_2_graphics_120,x:162.5,y:109}).wait(1).to({graphics:mask_2_graphics_121,x:162.2,y:109}).wait(1).to({graphics:mask_2_graphics_122,x:161.9,y:109}).wait(1).to({graphics:mask_2_graphics_123,x:161.6,y:109}).wait(1).to({graphics:mask_2_graphics_124,x:161.4,y:109}).wait(1).to({graphics:mask_2_graphics_125,x:161.1,y:109}).wait(1).to({graphics:mask_2_graphics_126,x:160.8,y:109}).wait(1).to({graphics:mask_2_graphics_127,x:160.5,y:109}).wait(1).to({graphics:mask_2_graphics_128,x:160.2,y:109}).wait(1).to({graphics:mask_2_graphics_129,x:160,y:109}).wait(1).to({graphics:mask_2_graphics_130,x:159.7,y:109}).wait(1).to({graphics:mask_2_graphics_131,x:159.4,y:109}).wait(1).to({graphics:mask_2_graphics_132,x:159.1,y:109}).wait(1).to({graphics:mask_2_graphics_133,x:158.8,y:109}).wait(1).to({graphics:mask_2_graphics_134,x:158.6,y:109}).wait(1).to({graphics:mask_2_graphics_135,x:158.3,y:109}).wait(1).to({graphics:mask_2_graphics_136,x:158,y:109}).wait(1).to({graphics:mask_2_graphics_137,x:157.7,y:109}).wait(1).to({graphics:mask_2_graphics_138,x:157.4,y:109}).wait(1).to({graphics:mask_2_graphics_139,x:157.2,y:109}).wait(1).to({graphics:mask_2_graphics_140,x:156.9,y:109}).wait(1).to({graphics:mask_2_graphics_141,x:156.6,y:109}).wait(1).to({graphics:mask_2_graphics_142,x:156.3,y:109}).wait(1).to({graphics:mask_2_graphics_143,x:156,y:109}).wait(1).to({graphics:mask_2_graphics_144,x:155.8,y:109}).wait(1).to({graphics:mask_2_graphics_145,x:155.5,y:109}).wait(1).to({graphics:mask_2_graphics_146,x:155.2,y:109}).wait(1).to({graphics:mask_2_graphics_147,x:154.9,y:109}).wait(1).to({graphics:mask_2_graphics_148,x:154.6,y:109}).wait(1).to({graphics:mask_2_graphics_149,x:154.4,y:109}).wait(1).to({graphics:mask_2_graphics_150,x:154.1,y:109}).wait(1).to({graphics:mask_2_graphics_151,x:153.8,y:109}).wait(1).to({graphics:mask_2_graphics_152,x:153.5,y:109}).wait(1).to({graphics:mask_2_graphics_153,x:153.2,y:109}).wait(1).to({graphics:mask_2_graphics_154,x:153,y:109}).wait(1).to({graphics:mask_2_graphics_155,x:152.7,y:109}).wait(1).to({graphics:mask_2_graphics_156,x:152.4,y:109}).wait(162));

	// copy_1
	this.copy_1_mc = new lib.copy_1_mc();
	this.copy_1_mc.setTransform(152.4,152.8);

	this.copy_1_mc.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.copy_1_mc).wait(318));

	// BG
	this.instance = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(318));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-156.9,-706.6,569,956.7);


// stage content:
(lib.squeegee_300x250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	mask.setTransform(150,125);

	// Content
	this.content_mc = new lib.content_mc();
	this.content_mc.setTransform(150,125,1,1,0,0,0,150,125);

	this.content_mc.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.content_mc).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,125,300,250);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;