
   /*-----------------------------
               SCRIPTS
   -----------------------------*/
    var scroll;

    (function() {
       'use strict';

      /*--------------------------
                 LOADER
       --------------------------*/

      $('.page-loading').fadeOut();
      $('.loader').delay(350).fadeOut('slow');

      $(".ham-menu").on('click', function() {
        $(this).toggleClass('open-menu');
        $('.menu').toggleClass('open-menu');
      });

      $('.button').on('click', function() {
         $(".ham-menu").removeClass('open-menu');
         $('.menu').removeClass('open-menu');
      });

      /*--------------------------
             SRCOLL TO TOP
       ---------------------------*/

    //  var offset              = 300,
    //      offset_opacity      = 1200,
    //      scroll_top_duration = 700,
    //      $back_to_top        = $('.cd-top');
     //
    //   $(window).on('scroll',function() {
    //     scroll = $(this).scrollTop();
    //      ($(this).scrollTop() > offset) ?
    //      $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
    //      if ($(this).scrollTop() > offset_opacity) {
    //         $back_to_top.addClass('cd-fade-out');
    //      }
    //      if ($(this).scrollTop() > 195) {
    //       $('.header').addClass('scroll');
    //      };
    //      if ($(this).scrollTop() < 196) {
    //       $('.header').removeClass('scroll');
    //      };
    //   });
     //
    //   $back_to_top.on('click', function(event) {
    //      event.preventDefault();
    //      $('body,html').animate({
    //         scrollTop: 0,
    //      }, scroll_top_duration);
    //   });


      /*--------------------------
                ISOTOPE
       ---------------------------*/


      var clients = {
        ola: {
            1: {
                client: 'audi',
                link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/audi/',
                class: 'RichMediaAds all',
                title: 'audi',
                description: 'Video banner'
            },
            2: {client: 'Nike', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/nike/', class: 'RichMediaAds all', title: 'nike', description: 'image sequence banner'},
            3: {client: 'nike', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/nike-red-football/', class: 'RichMediaAds all', title: 'nike', description: 'image sequence banner'},
            4: {client: 'Nike', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/nike-air-max/', class: 'RichMediaAds all', title: 'nike', description: 'video banner'},

            5: {client: 'Freeman__supercell-kinhgVsKing__AR', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/supercell-clash-royale/', class: 'RichMediaAds all', title: 'supercell', description: 'responsive banner'},
            6: {client: 'Freeman__supercell-kinhgVsKing__AR', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/supercell-clash-royale-2/', class: 'RichMediaAds all', title: 'supercell', description: 'responsive banner'},

            7: {client: 'EASportFiFA', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/ea-sports-fifa-2016/', class: 'RichMediaAds all', title: 'es sports', description: 'expandable video banner'},
            8: {client: 'SWFT_standard', link: 'http://wamdigital.com/wp_preview-banner/2017/06/21/ea-star-wars/', class: 'RichMediaAds all', title: 'ea', description: 'html5 banner'},
            9: {client: 'lavazza', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/lavazza/', class: 'RichMediaAds all', title: 'lavazza', description: 'video banner'},
            10: {client: 'hbo', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/hbo-kvsw/', class: 'RichMediaAds all', title: 'HBO', description: 'image sequence banner'},

            11: {client: 'Jose-Cuervo__reserva', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/jose-cuervo-reserva/', class: 'RichMediaAds all', title: 'jose cuervo', description: 'html5 banner'},
            12: {client: 'Jose-Cuervo__rolling-stones', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/jose-cuervo-rolling-stones/', class: 'RichMediaAds all', title: 'jose cuervo', description: 'html5 banner'},

            13: {client: '3RC__map', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/intuit-quickbooks/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},

            14: {client: 'Duncan', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/tahoe-winter-2017/', class: 'RichMediaAds all', title: 'tahoe', description: 'html5 banner'},
            15: {client: 'Duncan', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/tahoe-winter-2017-2/', class: 'RichMediaAds all', title: 'tahoe', description: 'html5 banner'},

            16: {client: 'Duncan', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/tahoe-winter-night-2017/', class: 'RichMediaAds all', title: 'tahoe', description: 'html5 banner'},
            17: {client: 'Duncan', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/tahoe-winter-2017-snowball/', class: 'RichMediaAds all', title: 'tahoe', description: 'html5 banner'},
            18: {client: 'toujeo', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/toujeo/', class: 'RichMediaAds all', title: 'toujeo', description: 'expandable banner'},


            19: {client: 'sims', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/sims-4/', class: 'RichMediaAds all', title: 'sims', description: 'html5 banner'},

            20: {client: 'Maestro Dobell', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/dobel-maestro/', class: 'RichMediaAds all', title: 'dobel', description: 'html5 banner'},

            21: {client: 'Silk__Better smoothie', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/silk-better-smothie/', class: 'RichMediaAds all', title: 'silk', description: 'html5 banner'},
            22: {client: 'Silk__Hack', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/silk-hack-your-smothie/', class: 'RichMediaAds all', title: 'silk', description: 'html5 banner'},
            23: {client: 'verizon', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/verizon-dt-motorola/', class: 'RichMediaAds all', title: 'verizon', description: 'expandable banner'},


            24: {client: '3RC__1099', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/intuit-turbotax/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            25: {client: '3RC__coffeShop', link: 'hhttp://wamdigital.com/wp_preview-banner/2017/06/22/intuit-qb-se/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            26: {client: '3RC__freelancers_love', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/intuit-freelancers-love/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            27: {client: 'supermoon', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/customink-squeegee/', class: 'RichMediaAds all', title: 'customink', description: 'html5 banner'},

            28: {client: '3RC__designer', link: 'http://wamdigital.com/wp_preview-banner/2017/06/22/intuit-designer/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},

            29: {client: '3RC__photographer', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/intuit-photographer/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            30: {client: '3RC__real_state', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/intuit-real-state/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            31: {client: '3RC__intuit', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/intuit-zen/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},
            32: {client: '3RC__Canada-BOF', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/intuit-quickbooks-40/', class: 'RichMediaAds all', title: 'intuit', description: 'html5 banner'},

            33: {client: 'Event & Summer', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/tahoe-summer-2016/', class: 'RichMediaAds all', title: 'tahoe', description: 'html5 banner'},
            34: {client: 'AZ-Helps', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/armor-az-helps/', class: 'RichMediaAds all', title: 'astrazeneca', description: 'html5 banner'},
            35: {client: 'Amor shire', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/armor-lialda/', class: 'RichMediaAds all', title: 'lialda', description: 'html5 banner'},
            36: {client: 'Freedman', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/twd-static/', class: 'all', title: 'The walking dead', description: 'staic banner transcreation'},

            37: {client: 'Freeman__supercell', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/supercell-trasncreation/', class: 'RichMediaAds all transcreation', title: 'supercell', description: 'transcreation'},

            38: {client: 'HR-SURGE-Plum', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/fitbit-surge/', class: 'RichMediaAds all', title: 'fitbit', description: 'html5 banner'},
            39: {client: 'disney', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/disney-wbg/', class: 'RichMediaAds all', title: 'disney', description: 'html5 banner'},
            40: {client: 'sedona', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/sedona-arizona/', class: 'RichMediaAds all', title: 'sedona', description: 'html5 banner'},
            41: {client: 'Gloria Ferrer', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/gloria-ferrer-be-glorius/', class: 'RichMediaAds all', title: 'gloria ferrer', description: 'html5 banner'},
            42: {client: 'schnucks', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/schnukcs-ss/', class: 'RichMediaAds all', title: 'schnucks', description: 'html5 banner'},
            43: {client: 'gloria ferrer', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/gloria-ferrer-challenge/', class: 'RichMediaAds all', title: 'gloria ferrer', description: 'html5 banner'},
            44: {client: 'kraft', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/kraft/', class: 'RichMediaAds all', title: 'kraft', description: 'multitab banner'},
            45: {client: 'lego', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/lego-duplo/', class: 'RichMediaAds all', title: 'lego', description: 'richmedia banner'},
            46: {client: 'new balance', link: 'http://www.wamdigital.com/ourwork/ola/newbalance/demo1/index.html', class: 'RichMediaAds all', title: 'new balance', description: 'html5 banner'},
            47: {client: 'lego duplo', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/lego-duplo-2/', class: 'RichMediaAds all', title: 'lego', description: 'richmedia banner'},
            48: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/zmED-U1uRvOjoND5mUjEfQ', class: 'RichMediaAds all', title: 'Mcdonalds', description: 'html5 video banner'},
            49: {client: 'gatorade', link: 'http://platform.mediamind.com/Eyeblaster.Preview.Web/Default.aspx?previewID=%2Fwx5sM3KHQTb%2FbY0MHYX%2FespSp%2B8tm7F89bGTP7SLuAXxztUavAb3A%3D%3D&AdID=25778696&lang=en-US', class: 'RichMediaAds all', title: 'gatorade', description: 'DESCRIPTION'},
            // 50: {client: 'blackberry', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/K7nAXLyXSL6V1IAtLYEwgA?creativeId=23091493', class: 'RichMediaAds all', title: 'TITLE', description: 'DESCRIPTION'},
            50: {client: 'supermoon', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/customink-shirt/', class: 'RichMediaAds all', title: 'customink', description: 'html5 banner'},
            51: {client: 'TD Bank', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/td-bank-goodbye/', class: 'RichMediaAds all', title: 'td bank', description: 'html5 banner'},
            52: {client: 'xfiniti', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/xfinity/', class: 'RichMediaAds all', title: 'xfinity', description: 'html5 banner'},
            53: {client: 'TD Bank', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/td-bank-payment/', class: 'RichMediaAds all', title: 'td bank', description: 'html5 banner'},
            54: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/xvE2eufFRDKOr9qFj_jbiQ?creativeId=38269891', class: 'RichMediaAds all', title: 'mcdonalds', description: 'vido banner'},
            55: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/8x5UA--nRwiELgh8ER7L8w?creativeId=38458493', class: 'RichMediaAds all', title: 'mcdonalds', description: 'html5 banner'},
            56: {client: 'verizon', link: 'http://wamdigital.com/wp_preview-banner/2017/06/23/verizon-motherday/', class: 'RichMediaAds all', title: 'verizon', description: 'expandable banner'}
        },
        apps: {
            1: {client: 'Liquor Cabinet', link: 'http://wamdigital.com/portfolio/Preview/3RC/liquor_Cabinet/preview.html', class: 'apps all', title: 'Liquor cabinet', description: 'app'},
            2: {client: 'pfizer > xalkori products', link: 'https://www.behance.net/gallery/16497681/XALKORI-APP', class: 'apps all', title: 'xalkori', description: 'medical app'},
            3: {client: 'pfizer > tygacil', link: 'https://www.behance.net/gallery/17060351/TYGACIL-APP-FOR-TABLET', class: 'apps all', title: 'tygacil', description: 'medical app'},
            4: {client: 'bestbrands > corporate app', link: 'https://play.google.com/store/apps/details?id=com.bbapp&hl=en', class: 'apps all', title: 'best brands', description: 'app'},
            // 5: {client: 'builder home sites > sales arquitecture', link: 'https://docs.google.com/document/d/1bN2OtFf32aJJe9ty2Tn146airUy1Regjvg3vM5GC9_g/edit?pli=1', class: 'apps all', title: 'TITLE', description: 'DESCRIPTION'},
            // 6: {client: 'perimercado > CostaRican supermarket > Trineo Aventura', link: 'http://stagingwamdigital.com/TrineoAventura/', class: 'game all', title: 'TITLE', description: 'DESCRIPTION'}
        },
        websites: {
            1: {client: 'Capris', link: 'http://capris.cr/', class: 'website all', title: 'capris', description: 'website'},
            2: {client: 'nuncasolas', link: 'http://nuncasolas.com/', class: 'website all', title: 'nunca solas', description: 'website'},
            3: {client: 'bestbrands / web site / 2014', link: 'http://bestbrands.cr/', class: 'website all', title: 'best brands', description: 'website'},
            4: {client: 'samsung', link: 'http://www.wamdigital.com/ourwork/web/Samsung/', class: 'website all', title: 'samsung', description: 'website'},
            5: {client: 'toyota', link: 'http://www.wamdigital.com/ourwork/web/Toyota/', class: 'website all', title: 'toyota', description: 'website'},
            6: {client: 'mc-donalds', link: 'https://www.behance.net/gallery/17118315/MC-DONALDS-CAMPAIGN-COSTA-RICA', class: 'website all', title: 'mcdonalds', description: 'website'},
            7: {client: 'vizi', link: 'http://vizihealthsolutions.com/', class: 'website all', title: 'vizi', description: 'websites'}
        },
        transcreation: {
            1: {client: 'supercell', link: 'http://www.wamdigital.com/ourwork/videos/supercell/video-bb.html', class: 'all transcreation' , title: 'supercell', description: 'transcreation'},
            2: {client: 'supercell', link: 'http://www.wamdigital.com/ourwork/img/transcreation/clashroyale.jpg', class: 'all transcreation', title: 'supercell', description: 'transcreation'},
            3: {client: 'KingVsKing', link: 'http://wamdigital.com/portfolio/Preview/Freedman/KingVsKing/preview.jpg', class: 'transcreation all', title: 'supercell', description: 'transcreation'}

            // 1: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/ourwork/peri-futbol.html', class: 'game all', title: 'TITLE', description: 'DESCRIPTION'},
            // 2: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/ourwork/peri-navidad.html', class: 'game all', title: 'TITLE', description: 'DESCRIPTION'},
            // 3: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/work/RM/Nintendo/', class: 'game all', title: 'TITLE', description: 'DESCRIPTION'}
        },
        emails: {
            1: {client: 'Kinetix', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_1/Email1_Alcon-Interactive-Email-1.23.16_comic.html', class: 'email_newsletter all', title: 'Kinetix', description: 'email'},
            2: {client: 'Alcon', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_2/Email2_Alcon-FollowUpEmail_2_-1.15.16.html', class: 'email_newsletter all', title: 'Alcon', description: 'email'},
            3: {client: 'Alcon', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_3/Email3_Alcon-FollowUpEmail_1_-1.14.16.html', class: 'email_newsletter all', title: 'Alcon', description: 'email'},
            4: {client: 'citi bank > newsletter', link: 'http://www.wamdigital.com/ourwork/emails/Holiday_Sweepstakes_Newsletter/', class: 'email_newsletter all', title: 'city bank', description: 'email'},
            5: {client: 'citi bank > newsletter', link: 'http://www.wamdigital.com/ourwork/emails/July_Cardmember_Benefits_Newsletter/', class: 'email_newsletter all', title: 'city bank', description: 'email'},
            6: {client: 'wharton university', link: 'http://www.wamdigital.com/ourwork/emails/wharton-email/HTML_Template/SVC_14A.html', class: 'email_newsletter all', title: 'wharton university', description: 'email'},
            7: {client: 'hilton hhonors', link: 'http://www.wamdigital.com/ourwork/emails/Citi_July_Hilton/', class: 'email_newsletter all', title: 'hilton honors', description: 'email'},
        }
    };

      var service;
      var count = 1;
      var objChildren = 1;
      var allWork = 1;
      var title;
      var description;

      for (var indice = 1; indice < 6; indice++) {
          switch (indice) {
              case 1:
                  service = 'ola';
                  break;
              case 2:
                  service = 'apps';
                  break;
              case 3:
                  service = 'websites';
                  break;
              case 4:
                  service = 'transcreation';
                  break;
              case 5:
                  service = 'emails';
                  break;
          }
          for (var i = 1; i <= Object.keys(clients[service]).length; i++) {
              $("main ul").append( '<li class="' + clients[service][i]["class"] + '"><a target="_blank" href="' + clients[service][i]["link"] + '" class="button"><img src="img/' + service + '/mini_' + i + '.jpg"> <figcaption class="info_banner"><h2>' + clients[service][i]["title"]  + '</h2><p>' + clients[service][i]["description"]  + '</p></figcaption></a></li>' );
              count++;
          }
      }

      /*--------------------------
                ANIMATED
       ---------------------------*/

         new WOW().init();

      })();


      // portfolio filter
      $(window).on('load', function() {
         'use strict';
      });

$(window).load(function () {
  var $container = $('main ul');
  $container.isotope({
      filter: '*',
      animationOptions: {
          duration: 750,
          easing: 'linear',
          queue: false
      }
  });
  $('.content_nav nav ul li .button').on('click', function(e) {
      e.preventDefault();
      if (scroll > 196) {
        $('body,html').animate({
          scrollTop: 200
        }, 300);
      }

      $('nav ul li .button').removeClass('current');
      $('nav ul li').removeClass('current');
      $(this).addClass('current');
      $(this).parent().addClass('current');

      var selector = $(this).attr('data-filter');
      $container.isotope({
          filter: selector,
          animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false
          }
      });
      return false;
  });
});

// var clients = {
//   ola: {
//     1: {client: 'supermoon', link: 'http://wamdigital.com/portfolio/ola/supermoon/customink/shirt/preview.html', class: 'RichMediaAds all'},
//     2: {client: 'supermoon', link: 'http://wamdigital.com/portfolio/ola/supermoon/customink/squeegee/preview.html', class: 'RichMediaAds all'},
//     3: {client: 'Duncan', link: 'http://wamdigital.com/portfolio/Preview/Duncan/Tahoe-Winter-2017/Dogsled/preview.html', class: 'RichMediaAds all'},
//     4: {client: 'Duncan', link: 'http://wamdigital.com/portfolio/Preview/Duncan/Tahoe-Winter-2017/Ski/preview.html', class: 'RichMediaAds all'},
//     5: {client: 'Duncan', link: 'http://wamdigital.com/portfolio/Preview/Duncan/Tahoe-Winter-2017/Night/preview.html', class: 'RichMediaAds all'},
//     6: {client: 'Duncan', link: 'http://wamdigital.com/portfolio/Preview/Duncan/Tahoe-Winter-2017/SnowBall/preview.html', class: 'RichMediaAds all'},
//     7: {client: 'Nike', link: 'http://wamdigital.com/portfolio/ola/zerozero3/HBO_KvsW/300x250/animate/index.html', class: 'RichMediaAds all'},
//     8: {client: 'Nike', link: 'http://wamdigital.com/clients/freedmaninternational/SuperCell/september/cadence/facebook/carousel/CR_September_Carousel_Pekka_TCH_Fr1.mp4', class: 'RichMediaAds all'},
//     9: {client: 'Nike', link: 'https://wamdigital.com/portfolio/Preview/SIMS4_EP2_GO2_Concept_2/index.html', class: 'RichMediaAds all'},
//     10: {client: 'Nike', link: 'https://wamdigital.com/portfolio/Preview/The_Walking_Dead/Armory_Glenn_Offer/index.html', class: 'RichMediaAds all'},
//     11: {client: 'Nike', link: 'http://wamdigital.com/clients/freedmaninternational/SuperCell/August/cadence/facebook/carousel/concept-1/CR_Carousel_August_FRAME_4.mp4', class: 'RichMediaAds all'},
//     12: {client: 'Nike', link: 'https://wamdigital.com/portfolio/Preview/lavazza/index.html', class: 'RichMediaAds all'},
//
//     13: {client: 'Nike', link: 'https://wamdigital.com/portfolio/ola/html5/inpage/nike/preview.html', class: 'RichMediaAds all'},
//     14: {client: 'Nike', link: 'https://wamdigital.com/portfolio/ola/html5/inpage/nike-Air-Max/preview.html', class: 'RichMediaAds all'},
//     15: {client: 'nike', link: 'http://wamdigital.com/portfolio/ola/html5/inpage/nike/320x480/live_320x480_index.html', class: 'RichMediaAds all'},
//     16: {client: 'Freeman__supercell-kinhgVsKing__AR', link: 'http://wamdigital.com/portfolio/Preview/Freedman/june/preview.html', class: 'RichMediaAds all'},
//     17: {client: 'Freeman__supercell-kinhgVsKing__AR', link: 'http://wamdigital.com/portfolio/Preview/Freedman/june/Embers/v0/', class: 'RichMediaAds all'},
//     18: {client: 'Jose-Cuervo__reserva', link: 'http://wamdigital.com/portfolio/Preview/Armor/Jose_cuervo/reserva/preview.html', class: 'RichMediaAds all'},
//     19: {client: 'Jose-Cuervo__rolling-stones', link: 'http://wamdigital.com/portfolio/Preview/Armor/Jose_cuervo/rolling_stones/preview.html', class: 'RichMediaAds all'},
//
//     20: {client: 'Maestro Dobell', link: 'http://wamdigital.com/portfolio/Preview/Armor/Maestro_dobell/preview.html', class: 'RichMediaAds all'},
//
//     21: {client: 'Silk__Better smoothie', link: 'http://wamdigital.com/portfolio/Preview/Nomadic/silk/better-smothie/preview.html', class: 'RichMediaAds all'},
//     22: {client: 'Silk__Hack', link: 'http://wamdigital.com/portfolio/Preview/Nomadic/silk/hack/preview.html', class: 'RichMediaAds all'},
//     23: {client: 'KingVsKing', link: 'http://wamdigital.com/portfolio/Preview/Freedman/KingVsKing/preview.jpg', class: 'RichMediaAds all'},
//     24: {client: '3RC__1099', link: 'http://wamdigital.com/portfolio/Preview/3RC/1099/preview.html', class: 'RichMediaAds all'},
//     25: {client: '3RC__coffeShop', link: 'http://wamdigital.com/portfolio/Preview/3RC/coffeShop/preview.html', class: 'RichMediaAds all'},
//     26: {client: '3RC__freelancers_love', link: 'http://wamdigital.com/portfolio/Preview/3RC/freelancers_love/preview.html', class: 'RichMediaAds all'},
//     27: {client: '3RC__map', link: 'http://wamdigital.com/portfolio/Preview/3RC/map/preview.html', class: 'RichMediaAds all'},
//     28: {client: '3RC__designer', link: 'http://wamdigital.com/portfolio/Preview/3RC/vertical/designer/preview.html', class: 'RichMediaAds all'},
//     29: {client: '3RC__photographer', link: 'http://wamdigital.com/portfolio/Preview/3RC/vertical/photographer/preview.html', class: 'RichMediaAds all'},
//     30: {client: '3RC__real_state', link: 'http://wamdigital.com/portfolio/Preview/3RC/vertical/real_state/preview.html', class: 'RichMediaAds all'},
//     31: {client: '3RC__intuit', link: 'http://wamdigital.com/clients/3RC/intuit/preview.html', class: 'RichMediaAds all'},
//     32: {client: '3RC__Canada-BOF', link: 'http://wamdigital.com/clients/3RC/Canada-BOF/v-3/300x250/index.html', class: 'RichMediaAds all'},
//
//     33: {client: 'Event & Summer', link: 'http://wamdigital.com/portfolio/Preview/Duncan/tahoe_summer2016/summer/preview.html', class: 'RichMediaAds all'},
//     34: {client: 'AZ-Helps', link: 'http://wamdigital.com/portfolio/Preview/Armor/AZ-helps/preview.html', class: 'RichMediaAds all'},
//     35: {client: 'Amor shire', link: 'http://wamdigital.com/portfolio/Preview/Armor/Shire/preview.html', class: 'RichMediaAds all'},
//
//     36: {client: 'EASportFiFA', link: 'http://wamdigital.com/clients/fmi/ola/EASportFiFA/index.html', class: 'RichMediaAds all'},
//     37: {client: 'SWFT_standard', link: 'http://wamdigital.com/clients/fmi/ola/SWFT_standard/index.html', class: 'RichMediaAds all'},
//     38: {client: 'HR-SURGE-Plum', link: 'http://wamdigital.com/clients/fmi/ola/HR_SURGE_Plum_EN_160x600/index.html', class: 'RichMediaAds all'},
//     39: {client: 'disney', link: 'https://wamdigital.com/ourwork/ola/html5/inpage/disney/300x250/AltaCalidad/GrandparentsUGC_Banner_300x250.html', class: 'RichMediaAds all'},
//     40: {client: 'Gloria Ferrer', link: 'https://wamdigital.com/ourwork/ola/html5/inpage/sedona/preview.html', class: 'RichMediaAds all'},
//     41: {client: 'sedona', link: 'https://wamdigital.com/ourwork/ola/html5/inpage/gloriaferrer/preview.html', class: 'RichMediaAds all'},
//     42: {client: 'schnucks', link: 'https://wamdigital.com/ourwork/ola/html5/inpage/schnucks/index.html', class: 'RichMediaAds all'},
//     43: {client: 'gloria ferrer', link: 'http://www.wamdigital.com/ourwork/ola/html5/inpage/gloriaferrer/', class: 'RichMediaAds all'},
//     44: {client: 'kraft', link: 'http://www.wamdigital.com/ourwork/ola/kraft/K7CCR14_RM_banner.html', class: 'RichMediaAds all'},
//     45: {client: 'lego', link: 'http://www.wamdigital.com/ourwork/ola/lego/demo1/Lego_Duplo_FiremanTakeover_Demo.html', class: 'RichMediaAds all'},
//     46: {client: 'new balance', link: 'http://www.wamdigital.com/ourwork/ola/newbalance/demo1/index.html', class: 'RichMediaAds all'},
//     47: {client: 'lego duplo', link: 'http://www.wamdigital.com/ourwork/ola/RM/demos2/Duplo_lego_stencil_728x250.html', class: 'RichMediaAds all'},
//     48: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/zmED-U1uRvOjoND5mUjEfQ', class: 'RichMediaAds all'},
//     49: {client: 'gatorade', link: 'http://platform.mediamind.com/Eyeblaster.Preview.Web/Default.aspx?previewID=%2Fwx5sM3KHQTb%2FbY0MHYX%2FespSp%2B8tm7F89bGTP7SLuAXxztUavAb3A%3D%3D&AdID=25778696&lang=en-US', class: 'RichMediaAds all'},
//     50: {client: 'blackberry', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/K7nAXLyXSL6V1IAtLYEwgA?creativeId=23091493', class: 'RichMediaAds all'},
//     51: {client: 'audi', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/Q0lhr3trSSicuwQgq-8EoA', class: 'RichMediaAds all'},
//     52: {client: 'TD Bank', link: 'http://wamdigital.com/ourwork/ola/html5/inpage/tdbank/inpage/GoodbyeMonthlyFee/preview.html', class: 'RichMediaAds all'},
//     53: {client: 'xfiniti', link: 'http://wamdigital.com/ourwork/ola/html5/inpage/wifi_multi-device/728x90/', class: 'RichMediaAds all'},
//     54: {client: 'TD Bank', link: 'http://wamdigital.com/ourwork/ola/html5/inpage/tdbank/inpage/Payment/', class: 'RichMediaAds all'},
//     55: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/xvE2eufFRDKOr9qFj_jbiQ?creativeId=38269891', class: 'RichMediaAds all'},
//     56: {client: 'mc donalds', link: 'https://www.google.com/doubleclick/studio/externalpreview/#/8x5UA--nRwiELgh8ER7L8w?creativeId=38458493', class: 'RichMediaAds all'},
//     57: {client: 'verizon', link: 'http://www.wamdigital.com/ourwork/ola/html5/expandable/mothers_day/Fixed%20Size/728%20x%2090%20tablet%20expanding/', class: 'RichMediaAds all'},
//     58: {client: 'Youjeo', link: 'http://www.wamdigital.com/ourwork/ola/html5/expandable/tougeo/300%20x%20600%20expanding/', class: 'RichMediaAds all'},
//     59: {client: 'verizon', link: 'http://wamdigital.com/ourwork/ola/html5/expandable/dads%20&%20Grads/300%20x%2050%20expanding/', class: 'RichMediaAds all'}
//   },
//   apps: {
//     1: {client: 'Liquor Cabinet', link: 'http://wamdigital.com/portfolio/Preview/3RC/liquor_Cabinet/preview.html', class: 'apps all'},
//     2: {client: 'pfizer > xalkori products', link: 'https://www.behance.net/gallery/16497681/XALKORI-APP', class: 'apps all'},
//     3: {client: 'pfizer > tygacil', link: 'https://www.behance.net/gallery/17060351/TYGACIL-APP-FOR-TABLET', class: 'apps all'},
//     4: {client: 'bestbrands > corporate app', link: 'https://play.google.com/store/apps/details?id=com.bbapp&hl=en', class: 'apps all'},
//     5: {client: 'builder home sites > sales arquitecture', link: 'https://docs.google.com/document/d/1bN2OtFf32aJJe9ty2Tn146airUy1Regjvg3vM5GC9_g/edit?pli=1', class: 'apps all'},
//     6: {client: 'perimercado > CostaRican supermarket > Trineo Aventura', link: 'http://stagingwamdigital.com/TrineoAventura/', class: 'game all'}
//   },
//   websites: {
//     1: {client: 'Capris', link: 'http://capris.cr/', class: 'website all'},
//     2: {client: 'MyBravity', link: 'https://mybravity.com/', class: 'website all'},
//     3: {client: 'Americainmobiliaria', link: 'http://www.americainmobiliaria.com/', class: 'website all'},
//     4: {client: 'bestbrands / web site / 2014', link: 'http://bestbrands.cr/', class: 'website all'},
//     5: {client: 'samsung', link: 'http://www.wamdigital.com/ourwork/web/Samsung/', class: 'website all'},
//     6: {client: 'toyota', link: 'http://www.wamdigital.com/ourwork/web/Toyota/', class: 'website all'},
//     7: {client: 'mc-donalds', link: 'https://www.behance.net/gallery/17118315/MC-DONALDS-CAMPAIGN-COSTA-RICA', class: 'website all'},
//     8: {client: 'wikol', link: 'http://stagingwamdigital.com/wikol/', class: 'website all'},
//     9: {client: 'vizi', link: 'http://vizihealthsolutions.com/', class: 'website all'}
//   },
//   games: {
//     1: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/ourwork/peri-futbol.html', class: 'game all'},
//     2: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/ourwork/peri-navidad.html', class: 'game all'},
//     // 3: {client: 'perimercado > costarican supermarket > promo game', link: 'http://www.wamdigital.com/work/RM/Nintendo/', class: 'game all'}
//   },
//   emails: {
//     1: {client: 'Kinetix', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_1/Email1_Alcon-Interactive-Email-1.23.16_comic.html', class: 'email_newsletter all'},
//     2: {client: 'Alcon', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_1/Email1_Alcon-Interactive-Email-1.23.16_comic.html', class: 'email_newsletter all'},
//     3: {client: 'Alcon', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_2/Email2_Alcon-FollowUpEmail_2_-1.15.16.html', class: 'email_newsletter all'},
//     4: {client: 'Alcon', link: 'http://www.wamdigital.com/portfolio/emails/Alcon/email_3/Email3_Alcon-FollowUpEmail_1_-1.14.16.html', class: 'email_newsletter all'},
//     5: {client: 'citi bank > newsletter', link: 'http://www.wamdigital.com/ourwork/emails/Holiday_Sweepstakes_Newsletter/', class: 'email_newsletter all'},
//     6: {client: 'citi bank > newsletter', link: 'http://www.wamdigital.com/ourwork/emails/July_Cardmember_Benefits_Newsletter/', class: 'email_newsletter all'},
//     7: {client: 'wharton university', link: 'http://www.wamdigital.com/ourwork/emails/wharton-email/HTML_Template/SVC_14A.html', class: 'email_newsletter all'},
//     8: {client: 'hilton hhonors', link: 'http://www.wamdigital.com/ourwork/emails/Citi_July_Hilton/', class: 'email_newsletter all'},
//   }
// };
